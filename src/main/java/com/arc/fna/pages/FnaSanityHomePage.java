package com.arc.fna.pages;

import java.awt.AWTException;
import java.awt.Robot;
import java.awt.event.KeyEvent;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Random;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.CacheLookup;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.LoadableComponent;
import org.openqa.selenium.support.ui.Select;
import org.testng.Assert;

import com.arc.fna.utils.PropertyReader;
import com.arc.fna.utils.SkySiteUtils;
import com.arc.fna.utils.randomFileName;
import com.arcautoframe.utils.Log;

public class FnaSanityHomePage extends LoadableComponent<FnaSanityHomePage> {
	WebDriver driver;
	private boolean isPageLoaded;
	
	@Override
	protected void load() {
		isPageLoaded = true;
		//SkySiteUtils.waitForElement(driver, txtBoxUserName, 60);
		
	}

	@Override
	protected void isLoaded() throws Error {
		if (!isPageLoaded) {
			Assert.fail();
		}
	}
	/**
	 * Declaring constructor for initializing web elements using PageFactory class.
	 * @param driver
	 */
	public FnaSanityHomePage(WebDriver driver) {
		   this.driver = driver;
		   PageFactory.initElements(this.driver, this);
		 }
	
	/**
	 * Method written for doing login valid credential.
	 * 
	 * @return
	 * @throws AWTException
	 */

	public boolean loginValidation() throws AWTException
	{
		SkySiteUtils.waitTill(5000);
		String Exptitle = "SKYSITE Facilities & Archive";// Expected Page title
		String Acttitle_AfterLogin = driver.getTitle();// Getting Actual Page title
		SkySiteUtils.waitTill(5000);
		Log.message("Login Validation:");
		/*
		 * int Feedback_Alert =
		 * driver.findElements(By.xpath("//input[@id='Savefeedback']")).size();//Check
		 * feedback alert comes
		 * Log.message("Checking feedback alert is there or not : "+Feedback_Alert);
		 * if(Feedback_Alert>0) {
		 * driver.findElement(By.xpath("//span[@id='btnlater']")).click();//Click on
		 * 'Ask me later' link
		 * Log.message("Clicked on 'Ask me later' link from feedback!!!"); }
		 */

		if (driver.findElement(By.xpath("//*[@id='divAutoFeedback']/div/div/div[1]/h4")).isDisplayed()) 
		{
			driver.findElement(By.xpath(".//*[@id='divAutoFeedback']/div/div/div[2]/div[1]/div[2]/div")).click();
			driver.findElement(By.xpath(".//*[@id='autofeedback']")).sendKeys("Hi");
			driver.findElement(By.xpath(".//*[@id='Savefeedback']")).click();
			SkySiteUtils.waitTill(8000);
		}
		if (Acttitle_AfterLogin.equals(Exptitle))
			return true;
		else
			return false;
	}
	/**
	 * Method for handling the new feature pop over window Scripted Trinanjwan
	 */

	public void handlingNewFeaturepopover() {
		SkySiteUtils.waitTill(5000);
		int Feedback_Alert = driver
				.findElements(By.cssSelector(
						"#whatsNew > div:nth-child(1) > div:nth-child(1) > div:nth-child(3) > button:nth-child(2)"))
				.size();
		Log.message("Checking feedback alert is there or not : " + Feedback_Alert);
		if (Feedback_Alert > 0) {
			driver.findElement(By.cssSelector(
					"#whatsNew > div:nth-child(1) > div:nth-child(1) > div:nth-child(3) > button:nth-child(2)"))
					.click();
			Log.message("Clicked on 'Got it' link!!!");
		}
	}
    
	
	/**
	 * Method for handling demo video pop over window | Scripted Trinanjwan
	 */

	public void handlingDemoVideoepopover() {
		SkySiteUtils.waitTill(5000);
		int dem_video = driver
				.findElements(By.cssSelector(
						"#divWelcomeVid > div:nth-child(1) > div:nth-child(1) > div:nth-child(1) > button:nth-child(1)"))
				.size();
		Log.message("Checking demo video pop over is present or not : " + dem_video);
		if (dem_video > 0) {
			driver.findElement(By.cssSelector(
					"#divWelcomeVid > div:nth-child(1) > div:nth-child(1) > div:nth-child(1) > button:nth-child(1)"))
					.click();
			Log.message("Clicked on 'X' button to close the pop over");
		}
	}
	
	@FindBy(css = "#user-info")
	@CacheLookup
	WebElement MyProfilebtn;
	/**
	 * Method to check the presence of My Profile Button Scripted Trinanjwan
	 */

	public boolean presenceOfMyProfileBtn() {

		if (MyProfilebtn.isDisplayed() == true) {
			Log.message("My Profile Button is displayed");
			return true;
		}
		return false;
	}
	@FindBy(xpath = "//*[@id='Collections']/a")

	WebElement clkcollection;
	

	@FindBy(xpath = "//*[@id='btnNewProject']")

	WebElement btncollection;

	@FindBy(xpath = "//*[@id='txtProjectName']")

	WebElement txtProjectName;

	@FindBy(xpath = "//*[@id='txtProjectNumber']")

	WebElement txtProjectNumber;

	@FindBy(css = "#txtProjectStartDate")

	WebElement txtProjectStartDate;

	@FindBy(css = ".next")

	WebElement nxtdatemonth;

	@FindBy(xpath = "(//td[@class='day'])[8]")

	WebElement sltdate;

	@FindBy(css = "#txtProjectDesc")

	WebElement txtProjectDesc;

	@FindBy(css = "#txtProjectAddress1")

	WebElement txtProjectAddress1;

	@FindBy(css = "#txtProjectCity")

	WebElement txtProjectCity;
	@FindBy(css = "#txtProjectZip")

	WebElement txtProjectZip;

	@FindBy(css = "#chkFav")

	WebElement chkFav;

	@FindBy(css = "#btnSave")

	WebElement btnSave;

	@FindBy(css = ".selectedTreeRow")

	WebElement selectedTreeRow;
	
	
	
	/**
	 * Method written for Random Name Scripted By: Sekhar
	 * 
	 * @return
	 */

	public String Random_Collectionname() 
	{

		String str = "Collect_Sanity";
		Random r = new Random(System.currentTimeMillis());
		String abc = str + String.valueOf((10000 + r.nextInt(20000)));
		return abc;

	}
	
	/**
	 * Method written for create collections
	 * Scripted By: Sekhar
	 * 
	 * @return
	 */
	public boolean Create_Collections(String CollectionName) throws IOException 
	{
		driver.switchTo().defaultContent();
		SkySiteUtils.waitForElement(driver, clkcollection, 30);
		SkySiteUtils.waitTill(5000);
		clkcollection.click();// click on collection Selection
		Log.message("collection has been clicked.");
		SkySiteUtils.waitTill(5000);
		driver.switchTo().frame(driver.findElement(By.id("myFrame"))); // Back the Switch to Frame
		SkySiteUtils.waitTill(2000);
		btncollection.click();
		Log.message("Add collection has been clicked.");
		SkySiteUtils.waitTill(5000);
		String parentHandle = driver.getWindowHandle();
		Log.message(parentHandle);
		for (String winHandle : driver.getWindowHandles())
		{
			driver.switchTo().window(winHandle); // switch focus of WebDriver to the next found window handle (that's
													// your newly opened window)
		}
		SkySiteUtils.waitTill(2000);
		// String CollectionName=FnaHomePage.this.Random_Collectionname();
		Log.message(CollectionName);
		txtProjectName.sendKeys(CollectionName);
		SkySiteUtils.waitTill(2000);
		txtProjectNumber.sendKeys(CollectionName);
		Log.message("Collection number is:" + CollectionName);
		SkySiteUtils.waitTill(2000);
		txtProjectStartDate.click();
		Log.message("Start date has been clicked");
		SkySiteUtils.waitTill(3000);
		nxtdatemonth.click();
		Log.message("next month button has been clicked");
		SkySiteUtils.waitTill(3000);
		sltdate.click();
		Log.message("date button has been clicked");
		SkySiteUtils.waitTill(2000);
		String Descrip = PropertyReader.getProperty("Description");
		txtProjectDesc.sendKeys(Descrip);
		SkySiteUtils.waitTill(2000);
		txtProjectAddress1.sendKeys("salt lake");
		Log.message("Address has been entered");
		SkySiteUtils.waitTill(2000);
		txtProjectCity.sendKeys("Kolkata");
		Log.message("city has been entered");
		SkySiteUtils.waitTill(2000);
		txtProjectZip.sendKeys("700091");
		Log.message("Zip has been entered");
		SkySiteUtils.waitTill(2000);
		Select drpcountry = new Select(driver.findElement(By.name("ddlCompCountry")));
		drpcountry.selectByVisibleText("USA");
		Select drpstate = new Select(driver.findElement(By.name("ddlCompState")));
		drpstate.selectByVisibleText("California");
		Log.message("state has been selected");
		SkySiteUtils.waitTill(3000);
		btnSave.click();
		Log.message("save button has been clicked");
		SkySiteUtils.waitTill(2000);
		driver.switchTo().window(parentHandle);// Switch back to folder page
		SkySiteUtils.waitTill(2000);
		driver.switchTo().frame(driver.findElement(By.id("myFrame"))); // Back the Switch to Frame
		SkySiteUtils.waitTill(2000);
		SkySiteUtils.waitForElement(driver, selectedTreeRow, 60);
		//SkySiteUtils.waitTill(5000);
		String Collection = selectedTreeRow.getText();
		Log.message("Collection name is:" + Collection);
		SkySiteUtils.waitTill(2000);
		if (Collection.contains(CollectionName)) 
		{
			Log.message("Create Collection successfull : " + CollectionName);
			SkySiteUtils.waitTill(2000);
			return true;
		}
		else
		{
			Log.message("Create Collection Unsuccessfull  " + CollectionName);
			SkySiteUtils.waitTill(2000);
			return false;
		}
	}
	
	
	@FindBy(xpath = "//*[@id='setting']")

	WebElement drpSetting;
	
	@FindBy(xpath = "//*[@id='btnResetSearch']")

	WebElement resetcollection;
	/**
	 * Method written for select collection
	 *  Scripted By: Sekhar
	 * 
	 * @return
	 */
	public boolean selectcollection(String Collection_Name) 
	{
		SkySiteUtils.waitTill(5000);
		driver.switchTo().defaultContent();
		SkySiteUtils.waitForElement(driver, drpSetting, 70);
		//SkySiteUtils.waitTill(3000);
		clkcollection.click();// click on collection Selection
		Log.message("collection has been clicked.");
		//SkySiteUtils.waitTill(5000);
		driver.switchTo().frame(driver.findElement(By.id("myFrame"))); // Back the Switch to Frame
		SkySiteUtils.waitForElement(driver, resetcollection, 50);
		resetcollection.click();// click on reset button
		Log.message("reset button has been clicked.");
		SkySiteUtils.waitTill(2000);
		int prjCount_prjList = driver.findElements(By.xpath("//*[@id='listGrid']/div[2]/table/tbody/tr/td[1]/img"))
				.size();
		Log.message("prjCount_prjList :" + prjCount_prjList);
		// Loop start '2' as projects list start from tr[2]
		int prj = 0;
		for (prj = 1; prj <= prjCount_prjList; prj++) 
		{
			// x path as dynamically providing all rows(prj) tr[prj]
			String prjName = driver.findElement(By.xpath("(//*[@id='listGrid']/div[2]/table/tbody/tr/td[3]/a)[" + prj + "]")).getText();
			Log.message(prjName);
			SkySiteUtils.waitTill(2000);
			// Checking project name equal with each row of table
			if (prjName.equals(Collection_Name)) 
			{
				Log.message("Select Collection button has been clicked!!!");
				break;
			}
		}
		SkySiteUtils.waitTill(5000);
		// Click on project name where it equal with specified project name
		driver.findElement(By.xpath("(//*[@id='listGrid']/div[2]/table/tbody/tr/td[3]/a)[" + prj + "]")).click();
		SkySiteUtils.waitTill(4000);
		SkySiteUtils.waitForElement(driver, selectedTreeRow, 60);
		SkySiteUtils.waitTill(5000);
		String Collection = selectedTreeRow.getText();
		Log.message("Collection name is:" + Collection);
		SkySiteUtils.waitTill(2000);
		if (Collection.contains(Collection_Name)) 
		{
			Log.message("Select Collection successfull");
			SkySiteUtils.waitTill(2000);
			return true;
		} 
		else
		{
			Log.message("Select Collection Unsuccessfull");
			SkySiteUtils.waitTill(2000);
			return false;
		}

	}
	
	
	/**
	 * Method written for Random Folder Name Scripted By: Sekhar
	 * 
	 * @return
	 */

	public String Random_Foldername() 
	{

		String str = "Folder";
		Random r = new Random(System.currentTimeMillis());
		String abc = str + String.valueOf((10000 + r.nextInt(20000)));
		return abc;

	}
	@FindBy(css = "#btnAddFolder")

	WebElement btnAddFolder;
	@FindBy(css = "#txtFolderName")

	WebElement txtFolderName;

	@FindBy(css = "#btnNewFolderSave")

	WebElement btnNewFolderSave;
	@FindBy(xpath = "(//span[@class='selectedTreeRow'])[1]")

	WebElement slttreefolder;
	/**
	 * Method written for Adding folder Scripted By: Sekhar
	 * 
	 * @return
	 * @throws AWTException
	 */
	public boolean Adding_Folder(String FolderName) throws AWTException 
	{
		SkySiteUtils.waitTill(3000);
		driver.switchTo().defaultContent();
		// Switch to Frame
		driver.switchTo().frame(driver.findElement(By.id("myFrame")));
		Log.message("Swich to I Frame Success!!");
		SkySiteUtils.waitForElement(driver,btnAddFolder, 40);
		//SkySiteUtils.waitTill(5000);
		btnAddFolder.click(); // Adding on New Folder
		Log.message("Add Folder has been clicked");
		SkySiteUtils.waitTill(3000);
		txtFolderName.sendKeys(FolderName);
		Log.message("Folder name has been entered");
        SkySiteUtils.waitForElement(driver,btnNewFolderSave, 60);
		btnNewFolderSave.click();
		Log.message("save button has been clicked");
		SkySiteUtils.waitTill(5000);
		String Folder_name = slttreefolder.getText();
		Log.message("Act Folder Name is:" + Folder_name);
		if (Folder_name.contains(FolderName)) 
		{
			Log.message("Adding the folder validation successfull ");
			SkySiteUtils.waitTill(2000);
			return true;
		} 
		else 
		{
			Log.message("Adding the folder validation Unsuccessfull ");
			SkySiteUtils.waitTill(2000);
			return false;
		}
	}
	
	@FindBy(css = "#lftpnlMore")
	@CacheLookup
	WebElement Moreoption;
	@FindBy(xpath=".//*[@onclick='javascript:RemoveFolder();']")
	@CacheLookup
	WebElement Removefolderoption;
	@FindBy(xpath = "//*[@id='button-1']")

	WebElement btnyes;
	/**Method to delete added folder
	 * 
	 */
	public void deleteFolder() {
		driver.switchTo().defaultContent();
		Log.message("Switching to default content");
		SkySiteUtils.waitTill(4000);
		driver.switchTo().frame(driver.findElement(By.id("myFrame")));
		Log.message("SWitched to frame");
		SkySiteUtils.waitTill(4000);
		// SkySiteUtils.waitForElement(driver, Moreoption, 50);

		Moreoption.click();
		SkySiteUtils.waitTill(4000);
		Log.message("More option is clicked");
		SkySiteUtils.waitTill(4000);
		Removefolderoption.click();
		Log.message("Remove folder button is clicked");
	
		driver.switchTo().defaultContent();
		SkySiteUtils.waitForElement(driver, btnyes, 60);
		btnyes.click();
		Log.message("yes button has been clicked and folder is deleted");
		SkySiteUtils.waitTill(3000);
		}
	
	 @FindBy(css = "#btnUploadFile")

		WebElement btnUploadFile;	
	 
	  @FindBy(xpath=".//*[@id='btnSelectFiles']")
	    WebElement selectbtn;
	  @FindBy(xpath=".//*[@id='btnResetSearch']")
	  WebElement resetbtn;
		/**
		 * Method To UploadFile  Scripted by:Tarak
		 */
		public boolean UploadFile(String FolderPath,String tempfilepath) throws AWTException, IOException {
			// boolean result1=true;
			SkySiteUtils.waitForElement(driver, btnUploadFile, 60);
			btnUploadFile.click(); // Adding on New Folder
			Log.message("upload button has been clicked");
			SkySiteUtils.waitTill(8000);
			String parentHandle = driver.getWindowHandle();
			Log.message(parentHandle);
			for (String winHandle : driver.getWindowHandles()) {
				driver.switchTo().window(winHandle);
				//SkySiteUtils.waitTill(5000);// switch focus of WebDriver to the next found window handle (that's your newly
											// opened window)
			}
			//SkySiteUtils.waitTill(5000);
			SkySiteUtils.waitForElement(driver, selectbtn, 60);
			selectbtn.click();
			Log.message("select files has been clicked");
			SkySiteUtils.waitTill(5000);
			//Writing File names into a text file for using in AutoIT Script
			BufferedWriter output;			
	  		randomFileName rn=new randomFileName();
	  		String tmpFileName=tempfilepath+rn.nextFileName()+".txt";			
	  		output = new BufferedWriter(new FileWriter(tmpFileName,true));	
	  	    SkySiteUtils.waitTill(4000);
	  		String expFilename=null;
	  		File[] files = new File(FolderPath).listFiles();
	  				
	  		for(File file : files)
	  		{
	  			if(file.isFile()) 
	  			{
	  				expFilename=file.getName();//Getting File Names into a variable
	  				Log.message("Expected File name is:"+expFilename);	
	  				output.append('"' + expFilename + '"');
	  				output.append(" ");
	  				SkySiteUtils.waitTill(5000);	
	  			}	
	  		}	
	  		output.flush();
	  		output.close();	
	  				
	  		String AutoItexe_Path = PropertyReader.getProperty("AutoItexe1");
	  		//Executing .exe autoIt file		 
	  		Runtime.getRuntime().exec(AutoItexe_Path+" "+ FolderPath+" "+tmpFileName );			
	  		Log.message("AutoIT Script Executed!!");				
	  		SkySiteUtils.waitTill(14000);
	  				
	  		try
	  		{
	  			File file = new File(tmpFileName);
	  			if(file.delete())
	  			{
	  				Log.message(file.getName() + " is deleted!");
	  			}	
	  			else
	  			{
	  				Log.message("Delete operation is failed.");
	  			}
	  		}
	  		catch(Exception e)
	  		{			
	  			Log.message("Exception occured!!!"+e);
	  		}			
	  		SkySiteUtils.waitTill(8000);
			//driver.findElement(By.xpath("//button[@id='btnFileUpload']")).click();//Clicking on Upload button  
	  		driver.findElement(By.xpath(".//*[@id='multipartUploadBtn']")).click();
	  		Log.message("Upload button has been clicked");
			//SkySiteUtils.waitTill(30000);
			driver.switchTo().window(parentHandle);// Switch back to folder page
			SkySiteUtils.waitTill(3000);
			driver.switchTo().frame(driver.findElement(By.id("myFrame")));
			SkySiteUtils.waitTill(5000);
			SkySiteUtils.waitForElement(driver, resetbtn, 60);
			if(driver.findElement(By.xpath(".//*[@id='divGrid']/div[2]/table/tbody/tr[2]/td[2]/a[3]")).isDisplayed())
			{
				Log.message("File upload successfully");
				return true;
			}
			else {
				Log.message("File cannot be uploaded");
				return false;
			}
			
		}
		
		/**
		 * Method to select folder and file
		 * 
		 */
		public void selectFolder_File(String Foldername) {
			driver.switchTo().defaultContent();
			Log.message("Switched to default content");
			SkySiteUtils.waitTill(5000);
			driver.switchTo().frame(driver.findElement(By.id("myFrame")));
			Log.message("Switched to frame");
			//SkySiteUtils.waitTill(4000);

			int Count_no = driver.findElements(By.xpath(
					"//*[@id='treeboxbox_tree_wrap_container']/div/table/tbody/tr[2]/td[2]/table/tbody/tr/td[2]/table/tbody/tr/td[4]/span"))
					.size();
			Log.message("the number folder present are " + Count_no);
			SkySiteUtils.waitTill(2000);
			int k = 0;
			for (k = 1; k <= Count_no; k++) {

				String foldernamepresent = driver.findElement(By.xpath(
						"(//*[@id='treeboxbox_tree_wrap_container']/div/table/tbody/tr[2]/td[2]/table/tbody/tr/td[2]/table/tbody/tr/td[4]/span)["
								+ k + "]"))
						.getText();
				if (foldernamepresent.contains(Foldername)) {
					Log.message("Required folder is present at " + k + " position in the tree");
					driver.findElement(By.xpath(
							"(//*[@id='treeboxbox_tree_wrap_container']/div/table/tbody/tr[2]/td[2]/table/tbody/tr/td[2]/table/tbody/tr/td[4]/span)["
									+ k + "]"))
							.click();
					Log.message("Folder is selected");
					SkySiteUtils.waitTill(5000);
					break;
				}
				else {
					Log.message("Folder is not present");
				}
			}
		}
		@FindBy(xpath="//*[@class='icon icon-download icon-orange step11']")
		WebElement downloadicon;
		/**Method to download file
		 * 
		 * 
		 */
		public boolean downloadFile(String downloadfilepath,String filename) {
			Log.message("Performing download of file");
			this.Delete_Files_From_Folder(downloadfilepath);
			SkySiteUtils.waitForElement(driver, downloadicon, 50);
			downloadicon.click();
			Log.message("File download icon is clicked");

			  SkySiteUtils.waitTill(20000);
						  driver.switchTo().defaultContent();
						  if(this.isFileDownloaded(downloadfilepath, filename)==true)
			                     return true;
						  else
							  return false;
						
			
	}
		
		
		/**Method to delete all the files from download path
		 * 
		 */
		public boolean Delete_Files_From_Folder(String Folder_Path) 
		{
			try
			{	
				SkySiteUtils.waitTill(5000);		
				Log.message("Cleaning download folder!!! ");
				File file = new File(Folder_Path);      
			    String[] myFiles;    
			    if(file.isDirectory())
			    {
			    	myFiles = file.list();
			        for(int i=0; i<myFiles.length; i++) 
			        {
			           File myFile = new File(file, myFiles[i]); 
			           myFile.delete();
			           SkySiteUtils.waitTill(5000);
			        }
			        Log.message("Available Folders/Files are deleted from download folder successfully!!!");
			    }	         
			}//end try
			catch(Exception e)
			{
				 Log.message("Available Folders/Files are deleted from download folder successfully!!!");
			}
			return false;
		}
		
		
		 /**Method to check if file is downloaded
	     * Scripted By : Tarak
	     * 
	     */
	     public boolean isFileDownloaded(String downloadfolderpath,String filename) {
	    	 Log.message("Searching the downloaded file");
	    	 String Filename=filename;
	    	 Log.message("File to be searched is " +Filename);
	    	 File file=new File(downloadfolderpath);
	    	 File[] Filearray=file.listFiles();
	    	 SkySiteUtils.waitTill(5000);
	    	 if(file.isDirectory()) {
	    		 for(File Myfile:Filearray) {
	    			 if(Myfile.getName().contains(Filename)) {
	    				return true;
	    			 }
	    		 }
	    	 }
	    	return false;
	   }
	     @FindBy(xpath=".//*[@id='divGrid']/div[2]/table/tbody/tr[2]/td[2]/a[3]")
	     WebElement FiletoOpen;
	     @FindBy(xpath=".//*[@id='textToolMenuBtn']/i")
	     WebElement textoolbutton;
	     /**Verify File and be open in viewer
	      * 
	      */
	     public boolean fileOpenInViewer() {
	    	 Log.message("Open file in viewer");
	    	 SkySiteUtils.waitForElement(driver, FiletoOpen, 60);
	    	 FiletoOpen.click();
	    	 Log.message("File is clicked");
	    	 
	    	 String MainWindow = driver.getWindowHandle();
	 		Log.message(MainWindow);
	 		for (String childwindow : driver.getWindowHandles()) {
	 			driver.switchTo().window(childwindow);

	 		}
	 		Log.message("switched to child window");
            SkySiteUtils.waitTill(3000);
	 		driver.switchTo().defaultContent();
	 		
	    	 SkySiteUtils.waitForElement(driver, textoolbutton, 70);
	    	 if(textoolbutton.isDisplayed())
	    		 return true;
	    	 else
	    		 return false;
	    	 
	    	 
	   }
	     @FindBy(css = "#Contacts>a")
	 	WebElement ContactIcon;
	     /**
	 	 * Method for verifying presence of Contact icon Scripted By : Tarak
	 	 */
	 	public boolean contactIconPresent() {
	 		Log.message("Check Contact Icon is Present");
	 		SkySiteUtils.waitForElement(driver, ContactIcon, 30);
	 		if (ContactIcon.isDisplayed())
	 			return true;

	 		else
	 			return false;
	 	}
	 	/**
		 * Method to click Contact icon Scripted by:Tarak
		 */
		public FnaContactTabPage contactClick() {
			JavascriptExecutor executor = (JavascriptExecutor)driver;
			executor.executeScript("arguments[0].click();", ContactIcon);
		
				Log.message("ContactIcon button is clicked");
			

			
		
			return new FnaContactTabPage(driver).get();
		}
		

		@FindBy(css="#ProjectMenu1_Album>a")
		@CacheLookup
		WebElement albumicon;
		public FNAAlbumPage clickAlbumIcon() {
			driver.switchTo().defaultContent();
			SkySiteUtils.waitForElement(driver, albumicon, 60);
			albumicon.click();
			Log.message("Album icon is clicked");
			//SkySiteUtils.waitTill(5000);
			return new FNAAlbumPage(driver).get();
			
		}
	   
		
		//@FindBy(css = "#btnUploadFile")

		//WebElement btnUploadFile;

		//@FindBy(xpath = ".//*[@id='fine-uploader']/div/div[2]/div/div[2]/input")
		@FindBy(css = "#btnSelectFiles")//modify by sekhar
		WebElement btnselctFile;

		/**
		 * Method written for Upload file Scripted By: Sekhar
		 * 
		 * @return
		 * @throws AWTException
		 * @throws IOException
		 */
		public boolean UploadFileForEnableViewer(String FolderPath) throws AWTException, IOException
		{
			// boolean result1=true;
			SkySiteUtils.waitTill(3000);
			btnUploadFile.click(); // Adding on New Folder
			Log.message("upload button has been clicked");
			SkySiteUtils.waitTill(5000);
			String parentHandle = driver.getWindowHandle();
			Log.message(parentHandle);
			for (String winHandle : driver.getWindowHandles()) 
			{
				driver.switchTo().window(winHandle); // switch focus of WebDriver to the next found window handle (that's
														// your newly opened window)
			}
			SkySiteUtils.waitTill(3000);
			btnselctFile.click();
			Log.message("select files has been clicked");
			SkySiteUtils.waitTill(5000);
			// Writing File names into a text file for using in AutoIT Script
			BufferedWriter output;
			randomFileName rn = new randomFileName();
			//String tmpFileName = "./AutoIT_Temp/" + rn.nextFileName() + ".txt";
			String tmpFileName = "./AutoIT_Temp/" + rn.nextFileName() + ".txt";
			output = new BufferedWriter(new FileWriter(tmpFileName, true));

			String expFilename = null;
			File[] files = new File(FolderPath).listFiles();

			for (File file : files) 
			{
				if (file.isFile())
				{
					expFilename = file.getName();// Getting File Names into a variable
					Log.message("Expected File name is:" + expFilename);
					output.append('"' + expFilename + '"');
					output.append(" ");
					SkySiteUtils.waitTill(5000);
				}
			}
			output.flush();
			output.close();

			String AutoItexe_Path = PropertyReader.getProperty("AutoItexe1");
			// Executing .exe autoIt file
			Runtime.getRuntime().exec(AutoItexe_Path + " " + FolderPath + " " + tmpFileName);
			Log.message("AutoIT Script Executed!!");
			SkySiteUtils.waitTill(30000);

			try 
			{
				File file = new File(tmpFileName);
				if (file.delete()) 
				{
					Log.message(file.getName() + " is deleted!");
				}
				else
				{
					Log.message("Delete operation is failed.");
				}
			}
			catch (Exception e)
			{
				Log.message("Exception occured!!!" + e);
			}
			SkySiteUtils.waitTill(5000);
			driver.findElement(By.xpath(".//*[@id='multipartUploadBtn']")).click();//modify by sekhar
			Log.message("Upload button has been clicked");
			SkySiteUtils.waitTill(20000);
			driver.switchTo().window(parentHandle);// Switch back to folder page
			SkySiteUtils.waitTill(5000);
			driver.switchTo().frame(driver.findElement(By.id("myFrame")));
			SkySiteUtils.waitTill(5000);
			if (driver.findElement(By.xpath("//*[@id='divGrid']/div[2]/table/tbody/tr[2]/td[2]/a[3]")).isDisplayed())
				return true;
			else
				return false;
		}
		
		
		@FindBy(xpath = "//input[@id='chkEnableViewer']")

		WebElement chkEnableViewer;

		@FindBy(xpath = "//input[@id='chkAllProjectdocs']")

		WebElement sltallchkbox;
		

		//@FindBy(css = "#Button1")
        @FindBy(xpath=".//*[@id='rgtpnlMore']")
		WebElement btnmore;

		@FindBy(xpath = "(//i[@class='icon icon-link-files'])[3]")

		WebElement drplinkfiles;

		@FindBy(xpath = "//*[@id='btnAddToCommunication']")

		WebElement sendviacommunica;
		@FindBy(xpath = "//*[@id='txtMessageBody_container']/table/tbody/tr[2]/td")

		WebElement txtMessageBody;
		
		@FindBy(xpath = "//input[@id='btnCancel']")

		WebElement btnCancel;
		/**
		 * Method written for Enable Viewer send link and file revision. Scripted By:
		 * Sekhar
		 * @return
		 * @throws AWTException
		 * @throws IOException
		 */
		public boolean EnableViewer_sendlink_revision(String FilePath) throws AWTException, IOException 
		{
			boolean result1 = false;
			boolean result2 = false;

			SkySiteUtils.waitTill(5000);
			sltallchkbox.click();
			Log.message("select all check box has been clicked");
			SkySiteUtils.waitTill(3000);
		/*	btnmore.click();
			Log.message("more option has been clicked");
			SkySiteUtils.waitTill(7000);
			drplinkfiles.click();
			Log.message("link files has been clicked");
			SkySiteUtils.waitTill(3000);*/
			driver.findElement(By.xpath(".//*[@id='aspnetForm']/div[3]/section[2]/div/div[2]/nav/div[5]/div[1]/button[2]")).click();
			Log.message("Link file option is clicked");
			SkySiteUtils.waitTill(5000);
			chkEnableViewer.click();
			Log.message("enable viewer has been clicked");
			SkySiteUtils.waitTill(3000);
			sendviacommunica.click();
			Log.message("send via communication has been clicked");
			SkySiteUtils.waitTill(3000);
			SkySiteUtils.waitForElement(driver, txtMessageBody, 120);
			driver.switchTo().frame(driver.findElement(By.id("txtMessageBody_ifr")));
			Log.message("Switch to frame");
			SkySiteUtils.waitTill(20000);
			driver.findElement(By.xpath("//*[@id='tinymce']/p/a")).click();
		//	SkySiteUtils.waitTill(5000);	

			WebElement R1 = driver.findElement(By.xpath("//*[@id='tinymce']/p/a"));
			Actions builder = new Actions(driver);
			Log.message("selete the path frame");
			SkySiteUtils.waitTill(10000);
			builder.click(R1).contextClick(R1).sendKeys(R1, Keys.ARROW_DOWN).build().perform();
			Log.message("Image clicked and context click down");
			SkySiteUtils.waitTill(10000);
			Robot rb=new Robot();
			rb.keyPress(KeyEvent.VK_DOWN);
			rb.keyPress(KeyEvent.VK_ENTER);
			rb.keyRelease(KeyEvent.VK_DOWN);
			rb.keyRelease(KeyEvent.VK_ENTER);
			Log.message("file opened in new tab");
			SkySiteUtils.waitTill(9000);
		/*	builder.contextClick(R1).sendKeys(Keys.ARROW_DOWN).perform();
			SkySiteUtils.waitTill(5000);
			Robot robot1 = new Robot();
			SkySiteUtils.waitTill(5000);
			robot1.keyPress(KeyEvent.VK_DOWN);
			robot1.keyRelease(KeyEvent.VK_DOWN);
			robot1.keyPress(KeyEvent.VK_DOWN);
			robot1.keyRelease(KeyEvent.VK_DOWN);
			robot1.keyPress(KeyEvent.VK_DOWN);
			robot1.keyRelease(KeyEvent.VK_DOWN);
			robot1.keyPress(KeyEvent.VK_DOWN);
			robot1.keyRelease(KeyEvent.VK_DOWN);
			robot1.keyPress(KeyEvent.VK_DOWN);
			robot1.keyRelease(KeyEvent.VK_DOWN);
			SkySiteUtils.waitTill(5000);
			robot1.keyPress(KeyEvent.VK_ENTER);
			robot1.keyRelease(KeyEvent.VK_ENTER);
			SkySiteUtils.waitTill(10000);
			robot1.keyPress(KeyEvent.VK_CONTROL);
			robot1.keyPress(KeyEvent.VK_T);
			robot1.keyRelease(KeyEvent.VK_T);
			robot1.keyRelease(KeyEvent.VK_CONTROL);
			SkySiteUtils.waitTill(20000);*/

			/*
			 * driver.findElement(By.cssSelector("body")).sendKeys(Keys.CONTROL +"t");
			 * SkySiteUtils.waitTill(10000);
			 */

		/*	Robot robot = new Robot();
			SkySiteUtils.waitTill(5000);
			robot.keyPress(KeyEvent.VK_CONTROL);
			robot.keyPress(KeyEvent.VK_V);
			robot.keyRelease(KeyEvent.VK_CONTROL);
			robot.keyRelease(KeyEvent.VK_V);
			SkySiteUtils.waitTill(5000);
			robot.keyPress(KeyEvent.VK_ENTER);
			robot.keyRelease(KeyEvent.VK_ENTER);
			SkySiteUtils.waitTill(30000);*/
			String parentHandle = driver.getWindowHandle(); // Getting parent window
			Log.message(parentHandle);
			for (String winHandle : driver.getWindowHandles()) 
			{
				driver.switchTo().window(winHandle); // switch focus of WebDriver to the next found window handle (that's
														// your newly opened window)
			}
			SkySiteUtils.waitTill(10000);
			driver.switchTo().defaultContent();
			SkySiteUtils.waitTill(5000);
			driver.findElement(By.xpath("//*[@id='imageViewer']/div[2]/div[1]/div/div[2]/img[1]")).click();
			Log.message("viewer has been clicked");
			//SkySiteUtils.waitTill(3000);
			String FileName = driver.findElement(By.xpath("//span[@class='revision-view-event']")).getText();
			Log.message("Act File Name is:" + FileName);
			//SkySiteUtils.waitTill(5000);
			//SkySiteUtils.waitForElement(driver, textoolbutton, 60);
			if (driver.findElement(By.xpath("//span[@class='revision-view-event']")).isDisplayed() 
					&& (driver.findElement(By.xpath("//*[@id='imageViewer']/div[2]/div[1]/div/div[2]/img[1]")).isDisplayed())) 
					{
				          return true;
					}
			else
			{
				return false;
	
			}
		}
		/**Method to switch back to folder levle
		 * 
		 */
		public void switchBackToFolderLevel() {
			String windowHandle = driver.getWindowHandle();
			ArrayList tabs = new ArrayList(driver.getWindowHandles());
			System.out.println(tabs.size());

			driver.switchTo().window((String) tabs.get(0));
			driver.switchTo().frame(driver.findElement(By.id("myFrame")));
			WebElement backbtn=driver.findElement(By.xpath(".//*[@id='btnCancel']"));

            SkySiteUtils.waitForElement(driver, backbtn, 60);
            backbtn.click();
            Log.message("Back button is clicked");
            SkySiteUtils.waitTill(4000);
		
		}
		@FindBy(xpath=".//*[@id='btnRemoveProject']")
		WebElement removeproject;
		
	    /**Method to delete contact
          * 
          */
              public void deleteCollection(String Collectionname) {
           	   driver.switchTo().defaultContent();
           	   driver.findElement(By.xpath(".//*[@id='Collections']")).click();
           	   Log.message("Collection clicked");
           	driver.switchTo().frame(driver.findElement(By.id("myFrame")));
           	   SkySiteUtils.waitForElement(driver, removeproject, 60);
           	   SkySiteUtils.waitTill(5000);
           	   int Count_list=driver.findElements(By.xpath(".//*[@id='listGrid']/div[2]/table/tbody/tr/td[1]/img")).size();
                  Log.message("Total list count is:"+Count_list);                   
                  //Loop start '2' as projects list start from tr[2]
                  int i=0;
                  for(i = 1; i <= Count_list; i++) 
                  {
                               
                         String Collectionname2=driver.findElement(By.xpath("(.//*[@id='listGrid']/div[2]/table/tbody/tr/td[3]/a)["+i+"]")).getText();
                     
                         Log.message("Collection name is:"+Collectionname2);
                         //SkySiteUtils.waitTill(5000);                   
                         //Checking contact name equal with each row of table
                         if(Collectionname.contains(Collectionname))
                         {         
                               Log.message("Contact name matched is : " +Collectionname);
                               Log.message("Contact name added has been validated!!!");
                               driver.findElement(By.xpath(".//*[@id='divGrid']/div[2]/table/tbody/tr["+(i+1)+"]/td[1]/img")).click();
                               Log.message("Checkbox is clicked");
                               SkySiteUtils.waitForElement(driver, removeproject, 40);
                               removeproject.click();
                               driver.switchTo().defaultContent();
                               SkySiteUtils.waitTill(5000);
                               driver.findElement(By.xpath(".//*[@id='button-1']")).click();
                               Log.message("Yes button is clicked");
                               SkySiteUtils.waitTill(5000);
                               
                               break;
                         }             
                  }
                
                  }
	     
}
