package com.arc.fna.pages;


import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.CacheLookup;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.LoadableComponent;
import org.testng.Assert;

import com.arc.fna.utils.PropertyReader;
import com.arc.fna.utils.SkySiteUtils;
import com.arcautoframe.utils.Log;

public class ProjectsMyProfPage  extends LoadableComponent<ProjectsMyProfPage> {

	WebDriver driver;
	private boolean isPageLoaded;
	
	/**
	 * Identifying web elements using FindBy annotation.
	 */
	

	
	@FindBy(css="#txtASFirstName")
	@CacheLookup
	WebElement FirstName;
	
	
	@FindBy(css="a.btn-primary:nth-child(2) > span:nth-child(2)")
	@CacheLookup
	WebElement sendfilebtn;
	
	
	@FindBy(css=".icon-module-change")
	@CacheLookup
	WebElement ModuleNavigation;
	
	@FindBy(css=".select-module > li:nth-child(1) > a:nth-child(1)")
	@CacheLookup
	WebElement Homebtn;
	
	@FindBy(css=".select-module > li:nth-child(2) > a:nth-child(1)")
	@CacheLookup
	WebElement FnAbtn;
	
	
	@FindBy(css="#txtASEmail")
	@CacheLookup
	WebElement Emailfield;
	
	@FindBy(css="#txtASCompany")
	@CacheLookup
	WebElement CompanyNamefield;
	
	@FindBy(css="#txtASPhone")
	@CacheLookup
	WebElement Phonenumberfield;
	
	
	@FindBy(css="#txtASCountry")
	@CacheLookup
	WebElement Countryfield;
	
	@FindBy(css="#txtASState")
	@CacheLookup
	WebElement Statefield;
	

	
	@Override
	protected void load() {
		isPageLoaded = true;
		// TODO Auto-generated method stub
		
	}

	@Override
	protected void isLoaded() throws Error {
		if (!isPageLoaded) {
			Assert.fail();
		}
		// TODO Auto-generated method stub
		
	}
	
	public ProjectsMyProfPage(WebDriver driver) {
		   this.driver = driver;
		   PageFactory.initElements(this.driver, this);
		 }

	/** 
	 * Method written for checking whether projects my profile page is landed properly
	 * @return
	 * Created By trinanjwan
	 */
	

	public boolean presenceOfPrjDashboardButton()
	{
		
		//SkySiteUtils.waitTill(10000);
	    SkySiteUtils.waitForElement(driver, sendfilebtn, 70);
		if(sendfilebtn.isDisplayed())
		{	
			Log.message("Button is displayed");
			return true;
		}
			else
			return false;
	}
	

	/** 
	 * Method written for navigating to the common login page
	 * @return
	 * Created By trinanjwan
	 */


	public CommonLoginPage navigatingToCommonLoginPage()
	
	{
		SkySiteUtils.waitTill(3000);

		SkySiteUtils.waitForElement(driver, ModuleNavigation, 70);
		ModuleNavigation.click();
		Log.message("Clicked on module navigation button");
		//SkySiteUtils.waitTill(3000);
		SkySiteUtils.waitForElement(driver, ModuleNavigation, 70);
		Homebtn.click();
		Log.message("Clicked on HOME button");
		return new CommonLoginPage(driver).get();
	
		
	}
	
	
	
	
	
	/** Method to navigate to FnA Homepage from PROJECTS module
     * Scripted By :Trinanjwan
		*
		*/
	
	public FnaHomePage navigatingToFnAHome()
	
	{
	
		SkySiteUtils.waitTill(20000);

		SkySiteUtils.waitForElement(driver, ModuleNavigation, 10);
		ModuleNavigation.click();
		Log.message("Clicked on module navigation button");
		SkySiteUtils.waitTill(5000);
		FnAbtn.click();
		Log.message("Clicked on F&A button to navigate to F&A module");
		return new FnaHomePage(driver).get();
		
		
		
	}

	

	
	
	
	
	/**
	 * Method for handling the new feature pop over window Scripted Trinanjwan
	 */

	public void handlingNewFeaturepopover() {
		
		Log.message("Pop over is not present");
		/**	
		SkySiteUtils.waitTill(6000);
		int Feedback_Alert = driver
				.findElements(By.cssSelector(
						".btnWhatsNewFeatureAccepted"))
				.size();
		Log.message("Checking feedback alert is there or not : " + Feedback_Alert);
		SkySiteUtils.waitTill(2000);
		if (Feedback_Alert > 0) {
			driver.findElement(By.cssSelector(
					".btnWhatsNewFeatureAccepted")).click();
			Log.message("Clicked on 'Got it' link!!!");
		}
	 */
	
	}

	
	
	
	
	/**
	 * Method for handling sample video pop over window for SKYSITE Projects | Scripted Trinanjwan
	 */

	public void handlingsamplevideoPROJECTS() {
		

		SkySiteUtils.waitTill(10000);
		int Feedback_Alert = driver
				.findElements(By.cssSelector("#closeWelcomeVideo"))
				.size();
		Log.message("Checking video pop over is there or not : " + Feedback_Alert);
		SkySiteUtils.waitTill(2000);
		if (Feedback_Alert > 0) {
			driver.findElement(By.cssSelector("#closeWelcomeVideo")).click();
			Log.message("Clicked on 'X' button!!!");
		}

	
	}
	
	
	
	}	
	
	
	