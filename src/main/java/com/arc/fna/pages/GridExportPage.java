package com.arc.fna.pages;

import java.awt.AWTException;
import java.awt.Robot;
import java.awt.event.KeyEvent;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.util.Iterator;
import java.util.List;
import java.util.Random;
import java.util.concurrent.TimeUnit;

import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.DataFormatter;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.ss.usermodel.WorkbookFactory;
import org.apache.poi.xssf.usermodel.XSSFCell;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.openqa.selenium.Alert;
import org.openqa.selenium.By;
import org.openqa.selenium.Capabilities;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.openqa.selenium.support.CacheLookup;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.FluentWait;
import org.openqa.selenium.support.ui.LoadableComponent;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.Wait;
import org.testng.Assert;
import org.testng.reporters.Files;

import com.arc.fna.utils.PropertyReader;
import com.arc.fna.utils.SkySiteUtils;
import com.arc.fna.utils.randomFileName;
import com.arc.fna.utils.randomSharedName;
import com.arcautoframe.utils.Log;

public class GridExportPage extends LoadableComponent<GridExportPage>
{

	WebDriver driver;
	private boolean isPageLoaded;
	
	FnaHomePage fnaHomePage;
	
	/**
	 * Identifying web elements using FindfBy annotation.
	 */
	@FindBy(xpath = "//*[@id='setting']")
	WebElement btnSetting;

	@Override
	protected void load()
	{
		isPageLoaded = true;
		SkySiteUtils.waitForElement(driver, btnSetting, 60);
	}

	@Override
	protected void isLoaded() throws Error {
		if (!isPageLoaded) {
			Assert.fail();
		}
	}

	/**
	 * Declaring constructor for initializing web elements using PageFactory class.
	 * 
	 * @param driver
	 */
	public GridExportPage(WebDriver driver) 
	{
		this.driver = driver;
		PageFactory.initElements(this.driver, this);
	}	
	
	/**
	 * Method written for Random Name
	 *  Scripted By: Sekhar
	 * 
	 * @return
	 */

	public String Random_FileName()
	{
		String str = "File";
		Random r = new Random(System.currentTimeMillis());
		String abc = str + String.valueOf((10000 + r.nextInt(20000)));
		return abc;
	}
	
	@FindBy(css = ".icon.icon-star.ico-lg.icon-teams")
	WebElement btnteams;	
	
	@FindBy(xpath = "(//i[@class='icon icon-export ico-lg'])[2]")
	WebElement btnexport;
	
	@FindBy(xpath = "(//i[@class='icon icon-xls icon-lg'])[2]")
	WebElement btnexportasexcel;
	
	/** 
     * Method written for export as csv in collection teams.
     * Scripted By: Sekhar     
	 * @throws IOException 
	 * @throws IOException 
     * @throws AWTException 
     */
	
    public boolean Export_Excel_CollectionTeam(String DownloadPath,String DownloadPath1) throws IOException, AWTException,InvalidFormatException
    {  
    	boolean result1=false;
    	
    	SkySiteUtils.waitTill(3000);
    	driver.switchTo().defaultContent();
    	btnteams.click();
    	Log.message("Teams button has been clicked");
    	SkySiteUtils.waitTill(3000);     		
    	// Calling delete files from download folder script
   		this.Delete_Files_From_Folder(DownloadPath);
    	SkySiteUtils.waitTill(5000);
    	driver.switchTo().frame(driver.findElement(By.id("myFrame")));    	
    	SkySiteUtils.waitTill(3000);
    	btnexport.click();
    	Log.message("Export button has been clicked");
    	SkySiteUtils.waitTill(3000);
    	btnexportasexcel.click();
    	Log.message("Export as Excel button has been clicked");
    	SkySiteUtils.waitTill(3000);   	
    	
    	// Get Browser name on run-time.
    	Capabilities caps = ((RemoteWebDriver) driver).getCapabilities();
    	String browserName = caps.getBrowserName();
    	Log.message("Browser name on run-time is: " + browserName);
    	
    	if (browserName.contains("firefox")) 
    	{
    		// Handling Download PopUp of firefox browser using robot
    		Robot robot1 = null;
    		robot1 = new Robot();			
    		robot1.keyPress(KeyEvent.VK_ALT);		
    		robot1.keyPress(KeyEvent.VK_S);
    		SkySiteUtils.waitTill(4000);
    		robot1.keyRelease(KeyEvent.VK_S);
    		robot1.keyRelease(KeyEvent.VK_ALT);			
    		SkySiteUtils.waitTill(3000);
    		robot1.keyPress(KeyEvent.VK_ENTER);
    		robot1.keyRelease(KeyEvent.VK_ENTER);
    		SkySiteUtils.waitTill(20000);
    	}
    	SkySiteUtils.waitTill(25000);
    	// After checking whether download file or not
    	String expFilename = null;
    	
    	File[] files = new File(DownloadPath).listFiles();
    	
    	for(File file : files)
		{
			if(file.isFile()) 
			{
				expFilename=file.getName();//Getting File Names into a variable
				Log.message("Expected File name is:"+expFilename);				
			}
		}
		SkySiteUtils.waitTill(10000);
		//Working on Excel file
		String xlsxFileToRead = DownloadPath1 +expFilename;
		Log.message(xlsxFileToRead);
							
		InputStream ExcelFileToRead = new FileInputStream(xlsxFileToRead);
		XSSFWorkbook wb = new XSSFWorkbook(ExcelFileToRead);   

		XSSFSheet sheet=wb.getSheetAt(0);
		XSSFRow row; 
		XSSFCell cell;

		Iterator<Row> rows = sheet.rowIterator();
		
		System.out.println(rows);
		
		while (rows.hasNext())
		{
			row=(XSSFRow) rows.next();
			Iterator cells = row.cellIterator();
			
			while (cells.hasNext())
			{
				cell=(XSSFCell) cells.next();
		
				if (cell.getCellType() == XSSFCell.CELL_TYPE_STRING)
				{
					Log.message(cell.getStringCellValue()+" ");
				}
				else if(cell.getCellType() == XSSFCell.CELL_TYPE_NUMERIC)
				{
					Log.message(cell.getNumericCellValue()+" ");
				}
				else
				{
					//System.out.println("Excel as File UnSuccessfully Validated");
				}
			}	
			System.out.println();	
			try
			{
				ExcelFileToRead.close();
			} 
			catch (IOException e) 
			{
				e.printStackTrace();
			}	
		}	
			
		SkySiteUtils.waitTill(20000);
		result1=true;
		if(result1==true)
		{
			Log.message("Excel as File Successfully Validated");
			return true;
		}
		else
		{
			Log.message("Excel as File UnSuccessfully Validated");
			return false;   
		}        
    }
    
 // Deleting files from a folder
 	public boolean Delete_Files_From_Folder(String Folder_Path) 
 	{
 		try
 		{
 			SkySiteUtils.waitTill(5000);
 			Log.message("Cleaning download folder!!! ");
 			File file = new File(Folder_Path);
 			String[] myFiles;
 			if(file.isDirectory())
 			{
 				myFiles = file.list();
 				for (int i = 0; i < myFiles.length; i++) 
 				{
 					File myFile = new File(file, myFiles[i]);
 					myFile.delete();
 					SkySiteUtils.waitTill(5000);
 				}
 				Log.message("Available Folders/Files are deleted from download folder successfully!!!");
 			}
 		} // end try
 		catch (Exception e)
 		{
 			Log.message("Available Folders/Files are deleted from download folder successfully!!!");
 		}
 		return false;
 	}

        
    @FindBy(xpath = "(//i[@class='icon icon-csv icon-lg'])[2]")
	WebElement btnexportascsv;
    
    /** 
     * Method written for export as csv in collection teams.
     * Scripted By: Sekhar  	
	 * @throws IOException 
     * @throws AWTException 
     */
	

	public boolean Export_CSV_CollectionTeam(String DownloadPath,String DownloadPath1,String fileName) throws IOException, AWTException
    {  
    	boolean result1=false;    	
    	SkySiteUtils.waitTill(5000);      	
    	btnteams.click();
    	Log.message("Teams button has been clicked");
    	SkySiteUtils.waitTill(5000);    
   		// Calling delete files from download folder script
    	this.Delete_Files_From_Folder(DownloadPath);
    	SkySiteUtils.waitTill(5000);
    	driver.switchTo().frame(driver.findElement(By.id("myFrame")));    	
    	SkySiteUtils.waitTill(5000);
    	btnexport.click();
    	Log.message("Export button has been clicked");
    	SkySiteUtils.waitTill(3000);
    	btnexportascsv.click();
    	Log.message("Export as CSV button has been clicked");
    	SkySiteUtils.waitTill(3000);      	
    	// Get Browser name on run-time.
    	Capabilities caps = ((RemoteWebDriver) driver).getCapabilities();
    	String browserName = caps.getBrowserName();
    	Log.message("Browser name on run-time is: " + browserName);
    	
    	if (browserName.contains("firefox")) 
    	{
    		// Handling Download PopUp of firefox browser using robot
    		Robot robot1 = null;
    		robot1 = new Robot();			
    		robot1.keyPress(KeyEvent.VK_ALT);		
    		robot1.keyPress(KeyEvent.VK_S);
    		SkySiteUtils.waitTill(4000);
    		robot1.keyRelease(KeyEvent.VK_S);
    		robot1.keyRelease(KeyEvent.VK_ALT);			
    		SkySiteUtils.waitTill(3000);
    		robot1.keyPress(KeyEvent.VK_ENTER);
    		robot1.keyRelease(KeyEvent.VK_ENTER);
    		SkySiteUtils.waitTill(20000);
    	}
    	SkySiteUtils.waitTill(25000);
    	// After checking whether download file or not
    	String expFilename1 = null;
    	
    	File[] files1 = new File(DownloadPath).listFiles();
    	
    	for(File file1 : files1)
		{
			if(file1.isFile()) 
			{
				expFilename1=file1.getName();//Getting File Names into a variable
				Log.message("Expected File name is:"+expFilename1);				
			}
		}
		SkySiteUtils.waitTill(10000);
		//Working on CSV file
		String csvFileToRead = DownloadPath1 +expFilename1;
		Log.message(csvFileToRead);		
		BufferedReader br = null;
		String line = ""; 
		
		String splitBy = ",";
		int count = 0;		
		String DocumentName = null;
		try
		{
			br = new BufferedReader(new FileReader(csvFileToRead)); 
			while ((line = br.readLine()) != null) 
			{  
				count=count+1;
				Log.message("Downloaded csv file from data in : "+count+ " rows");
				String[] ActValue = line.split(splitBy);
				DocumentName = ActValue[0];
				DocumentName = DocumentName.replace("\"", "");
				Log.message("File Name from csv file is: "+ DocumentName);						
			}
			Log.message("Downloaded csv file have data in rows count is:"+count);
			if(DocumentName.contains(fileName))
			{
				result1=true;
				Log.message("Validation the Export as CSV in Collection Teams Successfull!!!!!");
			}
			else
			{
				result1=false;
				Log.message("Validation the Export as CSV in Collection Teams UnSuccessfull!!!!!");				
			}
			br.close();
		}		
		catch (FileNotFoundException e)
		{  
			e.printStackTrace();
		}
		catch (IOException e)
		{ 
			e.printStackTrace();
		}		
		if(result1==true)
			return true;
		else
			return false;
    }
	
	 @FindBy(css = ".icon.icon-collection.ico-lg")
	 WebElement btniconcollections;
	 
	 @FindBy(xpath = "//i[@class='icon icon-export ico-lg']")
	 WebElement btniconexport;
	 
	 @FindBy(xpath = "(//i[@class='icon icon-csv icon-lg'])[1]")
		WebElement btnexportascsv1;
	
	/** 
     * Method written for export as csv in collection list.
     * Scripted By: Sekhar  	
	 * @throws IOException 
     * @throws AWTException 
     */

	public boolean Export_CSV_CollectionList(String DownloadPath,String DownloadPath1,String fileName) throws IOException, AWTException
    {  
    	boolean result1=false;    	
    	SkySiteUtils.waitTill(5000);    	
    	btniconcollections.click();
    	Log.message("collections button has been clicked");
    	SkySiteUtils.waitTill(5000);    	
   		// Calling delete files from download folder script
    	this.Delete_Files_From_Folder(DownloadPath);
    	SkySiteUtils.waitTill(5000);
    	driver.switchTo().frame(driver.findElement(By.id("myFrame")));    	
    	SkySiteUtils.waitTill(5000);
    	btniconexport.click();
    	Log.message("Export button has been clicked");
    	SkySiteUtils.waitTill(3000);
    	btnexportascsv1.click();
    	Log.message("Export as CSV button has been clicked");
    	SkySiteUtils.waitTill(3000);      	
    	// Get Browser name on run-time.
    	Capabilities caps = ((RemoteWebDriver) driver).getCapabilities();
    	String browserName = caps.getBrowserName();
    	Log.message("Browser name on run-time is: " + browserName);
    	
    	if (browserName.contains("firefox")) 
    	{
    		// Handling Download PopUp of firefox browser using robot
    		Robot robot1 = null;
    		robot1 = new Robot();			
    		robot1.keyPress(KeyEvent.VK_ALT);		
    		robot1.keyPress(KeyEvent.VK_S);
    		SkySiteUtils.waitTill(4000);
    		robot1.keyRelease(KeyEvent.VK_S);
    		robot1.keyRelease(KeyEvent.VK_ALT);			
    		SkySiteUtils.waitTill(3000);
    		robot1.keyPress(KeyEvent.VK_ENTER);
    		robot1.keyRelease(KeyEvent.VK_ENTER);
    		SkySiteUtils.waitTill(20000);
    	}
    	SkySiteUtils.waitTill(25000);
    	// After checking whether download file or not
    	String expFilename1 = null;
    	
    	File[] files1 = new File(DownloadPath).listFiles();
    	
    	for(File file1 : files1)
		{
			if(file1.isFile()) 
			{
				expFilename1=file1.getName();//Getting File Names into a variable
				Log.message("Expected File name is:"+expFilename1);				
			}
		}
		SkySiteUtils.waitTill(10000);
		//Working on CSV file
		String csvFileToRead = DownloadPath1 +expFilename1;
		Log.message(csvFileToRead);		
		BufferedReader br = null;
		String line = ""; 
		
		String splitBy = ",";
		int count = 0;		
		String DocumentName = null;
		try
		{
			br = new BufferedReader(new FileReader(csvFileToRead)); 
			while ((line = br.readLine()) != null) 
			{  
				count=count+1;
				Log.message("Downloaded csv file from data in : "+count+ " rows");
				String[] ActValue = line.split(splitBy);
				DocumentName = ActValue[4];
				DocumentName = DocumentName.replace("\"", "");
				Log.message("File Name from csv file is: "+ DocumentName);						
			}
			Log.message("Downloaded csv file have data in rows count is:"+count);
			Log.message(DocumentName);
			if(DocumentName.contains(fileName))
			{
				result1=true;
				Log.message("Validation the Export as CSV in Collection List Successfull!!!!!");
			}
			else
			{
				result1=false;
				Log.message("Validation the Export as CSV in Collection List UnSuccessfull!!!!!");				
			}
			br.close();
		}		
		catch (FileNotFoundException e)
		{  
			e.printStackTrace();
		}
		catch (IOException e)
		{ 
			e.printStackTrace();
		}		
		if(result1==true)
			return true;
		else
			return false;
    }
	
		
	@FindBy(xpath = "(//i[@class='icon icon-xls icon-lg'])[1]")
	WebElement btnexportasexcel1;
	
	/** 
     * Method written for export as excel in collection list.
     * Scripted By: Sekhar     
	 * @throws IOException 
	 * @throws IOException 
     * @throws AWTException 
     */
	
    public boolean Export_Excel_CollectionList(String DownloadPath,String DownloadPath1) throws IOException, AWTException,InvalidFormatException
    {  
    	boolean result1=false;    	
    	SkySiteUtils.waitTill(3000);
    	driver.switchTo().defaultContent();
    	btniconcollections.click();
    	Log.message("collections button has been clicked");
    	SkySiteUtils.waitTill(5000);    	
   		// Calling delete files from download folder script
    	this.Delete_Files_From_Folder(DownloadPath);
    	SkySiteUtils.waitTill(5000);
    	driver.switchTo().frame(driver.findElement(By.id("myFrame")));    	
    	SkySiteUtils.waitTill(5000);
    	btniconexport.click();
    	Log.message("Export button has been clicked");
    	SkySiteUtils.waitTill(3000);
    	btnexportasexcel1.click();
    	Log.message("Export as Excel button has been clicked");
    	SkySiteUtils.waitTill(3000);      	
    	// Get Browser name on run-time.
    	Capabilities caps = ((RemoteWebDriver) driver).getCapabilities();
    	String browserName = caps.getBrowserName();
    	Log.message("Browser name on run-time is: " + browserName);
    	
    	if (browserName.contains("firefox")) 
    	{
    		// Handling Download PopUp of firefox browser using robot
    		Robot robot1 = null;
    		robot1 = new Robot();			
    		robot1.keyPress(KeyEvent.VK_ALT);		
    		robot1.keyPress(KeyEvent.VK_S);
    		SkySiteUtils.waitTill(4000);
    		robot1.keyRelease(KeyEvent.VK_S);
    		robot1.keyRelease(KeyEvent.VK_ALT);			
    		SkySiteUtils.waitTill(3000);
    		robot1.keyPress(KeyEvent.VK_ENTER);
    		robot1.keyRelease(KeyEvent.VK_ENTER);
    		SkySiteUtils.waitTill(20000);
    	}
    	SkySiteUtils.waitTill(25000);
    	// After checking whether download file or not
    	String expFilename = null;
    	
    	File[] files = new File(DownloadPath).listFiles();
    	
    	for(File file : files)
		{
			if(file.isFile()) 
			{
				expFilename=file.getName();//Getting File Names into a variable
				Log.message("Expected File name is:"+expFilename);				
			}
		}
		SkySiteUtils.waitTill(10000);
		//Working on Excel file
		String xlsxFileToRead = DownloadPath1 +expFilename;
		Log.message(xlsxFileToRead);
							
		InputStream ExcelFileToRead = new FileInputStream(xlsxFileToRead);
		XSSFWorkbook wb = new XSSFWorkbook(ExcelFileToRead);   

		XSSFSheet sheet=wb.getSheetAt(0);
		XSSFRow row; 
		XSSFCell cell;

		Iterator<Row> rows = sheet.rowIterator();
		
		System.out.println(rows);
		
		while (rows.hasNext())
		{
			row=(XSSFRow) rows.next();
			Iterator cells = row.cellIterator();
			
			while (cells.hasNext())
			{
				cell=(XSSFCell) cells.next();
		
				if (cell.getCellType() == XSSFCell.CELL_TYPE_STRING)
				{
					Log.message(cell.getStringCellValue()+" ");
				}
				else if(cell.getCellType() == XSSFCell.CELL_TYPE_NUMERIC)
				{
					Log.message(cell.getNumericCellValue()+" ");
				}
				else
				{
					//System.out.println("Excel as File UnSuccessfully Validated");
				}
			}	
			System.out.println();	
			try
			{
				ExcelFileToRead.close();
			} 
			catch (IOException e) 
			{
				e.printStackTrace();
			}	
		}			
		SkySiteUtils.waitTill(20000);
		result1=true;
		if(result1==true)
		{
			Log.message("Excel as File Successfully Validated");
			return true;
		}
		else
		{
			Log.message("Excel as File UnSuccessfully Validated");
			return false;   
		}
    }
    
	@FindBy(xpath = "(//i[@class='icon icon-export ico-lg'])[2]")
	WebElement btniconexport1;
	
	 @FindBy(xpath = "(//i[@class='icon icon-csv icon-lg'])[3]")
	 WebElement btnexportascsv2;
	
    
    /** 
     * Method written for export as csv in collection File.
     * Scripted By: Sekhar  	
	 * @throws IOException 
     * @throws AWTException 
     */

	public boolean Export_CSV_CollectionFile(String DownloadPath,String DownloadPath1,String fileName) throws IOException, AWTException
    {  
    	boolean result1=false;    	
    	SkySiteUtils.waitTill(5000);  
    	// Calling delete files from download folder script
    	this.Delete_Files_From_Folder(DownloadPath);
    	SkySiteUtils.waitTill(5000);
    	driver.switchTo().frame(driver.findElement(By.id("myFrame")));    	
    	SkySiteUtils.waitTill(5000);
    	btniconexport1.click();
    	Log.message("export button has been clicked");
    	SkySiteUtils.waitTill(5000);     	
    	btnexportascsv2.click();
    	Log.message("Export as CSV button has been clicked");
    	SkySiteUtils.waitTill(3000);      	
    	// Get Browser name on run-time.
    	Capabilities caps = ((RemoteWebDriver) driver).getCapabilities();
    	String browserName = caps.getBrowserName();
    	Log.message("Browser name on run-time is: " + browserName);
    	
    	if (browserName.contains("firefox")) 
    	{
    		// Handling Download PopUp of firefox browser using robot
    		Robot robot1 = null;
    		robot1 = new Robot();			
    		robot1.keyPress(KeyEvent.VK_ALT);		
    		robot1.keyPress(KeyEvent.VK_S);
    		SkySiteUtils.waitTill(4000);
    		robot1.keyRelease(KeyEvent.VK_S);
    		robot1.keyRelease(KeyEvent.VK_ALT);			
    		SkySiteUtils.waitTill(3000);
    		robot1.keyPress(KeyEvent.VK_ENTER);
    		robot1.keyRelease(KeyEvent.VK_ENTER);
    		SkySiteUtils.waitTill(20000);
    	}
    	SkySiteUtils.waitTill(25000);
    	// After checking whether download file or not
    	String expFilename1 = null;
    	
    	File[] files1 = new File(DownloadPath).listFiles();
    	
    	for(File file1 : files1)
		{
			if(file1.isFile()) 
			{
				expFilename1=file1.getName();//Getting File Names into a variable
				Log.message("Expected File name is:"+expFilename1);				
			}
		}
		SkySiteUtils.waitTill(10000);
		//Working on CSV file
		String csvFileToRead = DownloadPath1 +expFilename1;
		Log.message(csvFileToRead);		
		BufferedReader br = null;
		String line = ""; 
		
		String splitBy = ",";
		int count = 0;		
		String DocumentName = null;
		try
		{
			br = new BufferedReader(new FileReader(csvFileToRead)); 
			while ((line = br.readLine()) != null) 
			{  
				count=count+1;
				Log.message("Downloaded csv file from data in : "+count+ " rows");
				String[] ActValue = line.split(splitBy);
				DocumentName = ActValue[0];
				DocumentName = DocumentName.replace("\"", "");
				Log.message("File Name from csv file is: "+ DocumentName);						
			}
			Log.message("Downloaded csv file have data in rows count is:"+count);
			Log.message(DocumentName);
			Log.message(fileName);
			if(DocumentName.trim().contentEquals(fileName.trim()))
			{
				result1=true;
				Log.message("Validation the Export as CSV in Collection file Successfull!!!!!");
			}
			else
			{
				result1=false;
				Log.message("Validation the Export as CSV in Collection file UnSuccessfull!!!!!");				
			}
			br.close();
		}		
		catch (FileNotFoundException e)
		{  
			e.printStackTrace();
		}
		catch (IOException e)
		{ 
			e.printStackTrace();
		}		
		if(result1==true)
			return true;
		else
			return false;
    }
	
		
	@FindBy(xpath = "//i[@class='icon icon-star ico-lg icon-files']")
	WebElement btniconfiles;	
	
	@FindBy(xpath = "(//i[@class='icon icon-xls icon-lg'])[3]")
	WebElement btnexportasexcel2;
	
	/** 
     * Method written for export as excel in collection File.
     * Scripted By: Sekhar     
	 * @throws IOException 
	 * @throws IOException 
     * @throws AWTException 
     */
	
    public boolean Export_Excel_CollectionFile(String DownloadPath,String DownloadPath1) throws IOException, AWTException,InvalidFormatException
    {  
    	boolean result1=false;    	
    	SkySiteUtils.waitTill(3000);
    	driver.switchTo().defaultContent();
    	btniconfiles.click();
    	Log.message("files button has been clicked");
    	SkySiteUtils.waitTill(5000);    	
   		// Calling delete files from download folder script
    	this.Delete_Files_From_Folder(DownloadPath);
    	SkySiteUtils.waitTill(5000);
    	driver.switchTo().frame(driver.findElement(By.id("myFrame")));    	
    	SkySiteUtils.waitTill(5000);
    	btniconexport1.click();
    	Log.message("Export button has been clicked");
    	SkySiteUtils.waitTill(3000);
    	btnexportasexcel2.click();
    	Log.message("Export as Excel button has been clicked");
    	SkySiteUtils.waitTill(3000);      	
    	// Get Browser name on run-time.
    	Capabilities caps = ((RemoteWebDriver) driver).getCapabilities();
    	String browserName = caps.getBrowserName();
    	Log.message("Browser name on run-time is: " + browserName);
    	
    	if (browserName.contains("firefox")) 
    	{
    		// Handling Download PopUp of firefox browser using robot
    		Robot robot1 = null;
    		robot1 = new Robot();			
    		robot1.keyPress(KeyEvent.VK_ALT);		
    		robot1.keyPress(KeyEvent.VK_S);
    		SkySiteUtils.waitTill(4000);
    		robot1.keyRelease(KeyEvent.VK_S);
    		robot1.keyRelease(KeyEvent.VK_ALT);			
    		SkySiteUtils.waitTill(3000);
    		robot1.keyPress(KeyEvent.VK_ENTER);
    		robot1.keyRelease(KeyEvent.VK_ENTER);
    		SkySiteUtils.waitTill(20000);
    	}
    	SkySiteUtils.waitTill(25000);
    	// After checking whether download file or not
    	String expFilename = null;
    	
    	File[] files = new File(DownloadPath).listFiles();
    	
    	for(File file : files)
		{
			if(file.isFile()) 
			{
				expFilename=file.getName();//Getting File Names into a variable
				Log.message("Expected File name is:"+expFilename);				
			}
		}
		SkySiteUtils.waitTill(10000);
		//Working on Excel file
		String xlsxFileToRead = DownloadPath1 +expFilename;
		Log.message(xlsxFileToRead);
							
		InputStream ExcelFileToRead = new FileInputStream(xlsxFileToRead);
		XSSFWorkbook wb = new XSSFWorkbook(ExcelFileToRead);   

		XSSFSheet sheet=wb.getSheetAt(0);
		XSSFRow row; 
		XSSFCell cell;

		Iterator<Row> rows = sheet.rowIterator();
		
		System.out.println(rows);
		
		while (rows.hasNext())
		{
			row=(XSSFRow) rows.next();
			Iterator cells = row.cellIterator();
			
			while (cells.hasNext())
			{
				cell=(XSSFCell) cells.next();
		
				if (cell.getCellType() == XSSFCell.CELL_TYPE_STRING)
				{
					Log.message(cell.getStringCellValue()+" ");
				}
				else if(cell.getCellType() == XSSFCell.CELL_TYPE_NUMERIC)
				{
					Log.message(cell.getNumericCellValue()+" ");
				}
				else
				{
					//System.out.println("Excel as File UnSuccessfully Validated");
				}
			}	
			System.out.println();	
			try
			{
				ExcelFileToRead.close();
			} 
			catch (IOException e) 
			{
				e.printStackTrace();
			}	
		}			
		SkySiteUtils.waitTill(20000);
		result1=true;
		if(result1==true)
		{
			Log.message("Excel as File Successfully Validated");
			return true;
		}
		else
		{
			Log.message("Excel as File UnSuccessfully Validated");
			return false;   
		}
    }
    
    
    @FindBy(css = "#lftpnlMore")
	 WebElement btnlftpnlMore;
	
   
   /** 
    * Method written for export as csv in collection Folder.
    * Scripted By: Sekhar  	
	 * @throws IOException 
    * @throws AWTException 
    */

	public boolean Export_CSV_CollectionFolder(String DownloadPath,String DownloadPath1,String fileName) throws IOException, AWTException
   {  
   	boolean result1=false;    	
   	SkySiteUtils.waitTill(5000);  
   	// Calling delete files from download folder script
   	this.Delete_Files_From_Folder(DownloadPath);
   	SkySiteUtils.waitTill(5000);
   	driver.switchTo().frame(driver.findElement(By.id("myFrame")));    	
   	SkySiteUtils.waitTill(5000);
   	btnlftpnlMore.click();
   	Log.message("more button has been clicked");
   	SkySiteUtils.waitTill(5000); 
   	
  //mouse over operations
   	Actions actions = new Actions(driver); 
   	WebElement menuHoverLink = driver.findElement(By.xpath("//i[@class='icon icon-export']")); //select Export Folder
   	
   	actions.moveToElement(menuHoverLink); 
   	WebElement subLink = driver.findElement(By.xpath("(//span[@class='pull-left'])[1]")); //select Export to CSV
   	actions.moveToElement(subLink); 
   	actions.click(); 
   	actions.perform(); 	
 	SkySiteUtils.waitTill(5000); 
 	
   	// Get Browser name on run-time.
   	Capabilities caps = ((RemoteWebDriver) driver).getCapabilities();
   	String browserName = caps.getBrowserName();
   	Log.message("Browser name on run-time is: " + browserName);
   	
   	if (browserName.contains("firefox")) 
   	{
   		// Handling Download PopUp of firefox browser using robot
   		Robot robot1 = null;
   		robot1 = new Robot();			
   		robot1.keyPress(KeyEvent.VK_ALT);		
   		robot1.keyPress(KeyEvent.VK_S);
   		SkySiteUtils.waitTill(4000);
   		robot1.keyRelease(KeyEvent.VK_S);
   		robot1.keyRelease(KeyEvent.VK_ALT);			
   		SkySiteUtils.waitTill(3000);
   		robot1.keyPress(KeyEvent.VK_ENTER);
   		robot1.keyRelease(KeyEvent.VK_ENTER);
   		SkySiteUtils.waitTill(20000);
   	}
   	SkySiteUtils.waitTill(25000);
   	// After checking whether download file or not
   	String expFilename1 = null;
   	
   	File[] files1 = new File(DownloadPath).listFiles();
   	
   	for(File file1 : files1)
		{
			if(file1.isFile()) 
			{
				expFilename1=file1.getName();//Getting File Names into a variable
				Log.message("Expected File name is:"+expFilename1);				
			}
		}
		SkySiteUtils.waitTill(10000);
		//Working on CSV file
		String csvFileToRead = DownloadPath1 +expFilename1;
		Log.message(csvFileToRead);		
		BufferedReader br = null;
		String line = ""; 
		
		String splitBy = ",";
		int count = 0;		
		String DocumentName = null;
		try
		{
			br = new BufferedReader(new FileReader(csvFileToRead)); 
			while ((line = br.readLine()) != null) 
			{  
				count=count+1;
				Log.message("Downloaded csv file from data in : "+count+ " rows");
				String[] ActValue = line.split(splitBy);
				DocumentName = ActValue[0];
				DocumentName = DocumentName.replace("\"", "");
				Log.message("File Name from csv file is: "+ DocumentName);						
			}
			Log.message("Downloaded csv file have data in rows count is:"+count);
			Log.message(DocumentName);
			if(DocumentName.trim().contains(fileName.trim()))
			{
				result1=true;
				Log.message("Validation the Export as CSV in Collection folder Successfull!!!!!");
			}
			else
			{
				result1=false;
				Log.message("Validation the Export as CSV in Collection folder UnSuccessfull!!!!!");				
			}
			br.close();
		}		
		catch (FileNotFoundException e)
		{  
			e.printStackTrace();
		}
		catch (IOException e)
		{ 
			e.printStackTrace();
		}		
		if(result1==true)
			return true;
		else
			return false;
   }
	
	
	/** 
    * Method written for export as excel in collection Folder.
    * Scripted By: Sekhar     
	 * @throws IOException 
    * @throws AWTException 
    */
	
   public boolean Export_Excel_CollectionFolder(String DownloadPath,String DownloadPath1) throws IOException, AWTException,InvalidFormatException
   {  
   	boolean result1=false;    	
   	SkySiteUtils.waitTill(3000);
   	driver.switchTo().defaultContent();
   	btniconfiles.click();
   	Log.message("files button has been clicked");
   	SkySiteUtils.waitTill(5000);    	
  		// Calling delete files from download folder script
   	this.Delete_Files_From_Folder(DownloadPath);
   	SkySiteUtils.waitTill(5000);
   	driver.switchTo().frame(driver.findElement(By.id("myFrame")));    	
   	SkySiteUtils.waitTill(5000);   	
	btnlftpnlMore.click();
   	Log.message("more button has been clicked");
   	SkySiteUtils.waitTill(5000);    	
  //mouse over operations
   	Actions actions = new Actions(driver); 
   	WebElement menuHoverLink = driver.findElement(By.xpath("//i[@class='icon icon-export']")); //select Export Folder
   	
   	actions.moveToElement(menuHoverLink); 
   	WebElement subLink = driver.findElement(By.xpath("(//span[@class='pull-left'])[2]")); //select Export to Excel
   	actions.moveToElement(subLink); 
   	actions.click(); 
   	actions.perform(); 	
 	SkySiteUtils.waitTill(5000);    	
   	// Get Browser name on run-time.
   	Capabilities caps = ((RemoteWebDriver) driver).getCapabilities();
   	String browserName = caps.getBrowserName();
   	Log.message("Browser name on run-time is: " + browserName);
   	
   	if (browserName.contains("firefox")) 
   	{
   		// Handling Download PopUp of firefox browser using robot
   		Robot robot1 = null;
   		robot1 = new Robot();			
   		robot1.keyPress(KeyEvent.VK_ALT);		
   		robot1.keyPress(KeyEvent.VK_S);
   		SkySiteUtils.waitTill(4000);
   		robot1.keyRelease(KeyEvent.VK_S);
   		robot1.keyRelease(KeyEvent.VK_ALT);			
   		SkySiteUtils.waitTill(3000);
   		robot1.keyPress(KeyEvent.VK_ENTER);
   		robot1.keyRelease(KeyEvent.VK_ENTER);
   		SkySiteUtils.waitTill(20000);
   	}
   	SkySiteUtils.waitTill(25000);
   	// After checking whether download file or not
   	String expFilename = null;
   	
   	File[] files = new File(DownloadPath).listFiles();
   	
   	for(File file : files)
		{
			if(file.isFile()) 
			{
				expFilename=file.getName();//Getting File Names into a variable
				Log.message("Expected File name is:"+expFilename);				
			}
		}
		SkySiteUtils.waitTill(10000);
		//Working on Excel file
		String xlsxFileToRead = DownloadPath1 +expFilename;
		Log.message(xlsxFileToRead);
							
		InputStream ExcelFileToRead = new FileInputStream(xlsxFileToRead);
		XSSFWorkbook wb = new XSSFWorkbook(ExcelFileToRead);   

		XSSFSheet sheet=wb.getSheetAt(0);
		XSSFRow row; 
		XSSFCell cell;

		Iterator<Row> rows = sheet.rowIterator();
		
		System.out.println(rows);
		
		while (rows.hasNext())
		{
			row=(XSSFRow) rows.next();
			Iterator cells = row.cellIterator();
			
			while (cells.hasNext())
			{
				cell=(XSSFCell) cells.next();
		
				if (cell.getCellType() == XSSFCell.CELL_TYPE_STRING)
				{
					Log.message(cell.getStringCellValue()+" ");
				}
				else if(cell.getCellType() == XSSFCell.CELL_TYPE_NUMERIC)
				{
					Log.message(cell.getNumericCellValue()+" ");
				}
				else
				{
					//System.out.println("Excel as File UnSuccessfully Validated");
				}
			}	
			System.out.println();	
			try
			{
				ExcelFileToRead.close();
			} 
			catch (IOException e) 
			{
				e.printStackTrace();
			}	
		}			
		SkySiteUtils.waitTill(20000);
		result1=true;
		if(result1==true)
		{
			Log.message("Excel as File Successfully Validated");
			return true;
		}
		else
		{
			Log.message("Excel as File UnSuccessfully Validated");
			return false;   
		}
   }
   
  @FindBy(css = ".icon.icon-settings.ico-lg")
  WebElement btniconsettings;
  
  @FindBy(css = ".icon.icon-manage-user.icon-lg")
  WebElement btniconmanageuser;
  
  @FindBy(xpath = "//i[@class='icon icon-export ico-lg']")
  WebElement btniconiconexport;
  
  @FindBy(xpath = "//i[@class='icon icon-csv icon-lg']")
  WebElement btniconiconcsv;
	
 
 /** 
  * Method written for export as csv in Manage Users.
  * Scripted By: Sekhar  	
	 * @throws IOException 
  * @throws AWTException 
  */

	public boolean Export_CSV_Manage_Users(String DownloadPath,String DownloadPath1,String fileName) throws IOException, AWTException
 {  
 	boolean result1=false;    	
 	SkySiteUtils.waitTill(5000);  
 	// Calling delete files from download folder script
 	this.Delete_Files_From_Folder(DownloadPath);
 	SkySiteUtils.waitTill(5000);
 	btniconsettings.click();
 	Log.message("Setting button has been clicked");
 	SkySiteUtils.waitTill(2000);
 	btniconmanageuser.click();
 	Log.message("manage users button has been clicked");
 	SkySiteUtils.waitTill(5000);
 	driver.switchTo().frame(driver.findElement(By.id("myFrame")));    	
 	SkySiteUtils.waitTill(2000);
 	btniconiconexport.click();
 	Log.message("Export button has been clicked");
 	SkySiteUtils.waitTill(3000); 
 	btniconiconcsv.click();
 	Log.message("Export as csv button has been clicked");
 	SkySiteUtils.waitTill(5000);
 	// Get Browser name on run-time.
 	Capabilities caps = ((RemoteWebDriver) driver).getCapabilities();
 	String browserName = caps.getBrowserName();
 	Log.message("Browser name on run-time is: " + browserName);
 	
 	if (browserName.contains("firefox")) 
 	{
 		// Handling Download PopUp of firefox browser using robot
 		Robot robot1 = null;
 		robot1 = new Robot();			
 		robot1.keyPress(KeyEvent.VK_ALT);		
 		robot1.keyPress(KeyEvent.VK_S);
 		SkySiteUtils.waitTill(4000);
 		robot1.keyRelease(KeyEvent.VK_S);
 		robot1.keyRelease(KeyEvent.VK_ALT);			
 		SkySiteUtils.waitTill(3000);
 		robot1.keyPress(KeyEvent.VK_ENTER);
 		robot1.keyRelease(KeyEvent.VK_ENTER);
 		SkySiteUtils.waitTill(20000);
 	}
 	SkySiteUtils.waitTill(25000);
 	// After checking whether download file or not
 	String expFilename1 = null;
 	
 	File[] files1 = new File(DownloadPath).listFiles();
 	
 	for(File file1 : files1)
		{
			if(file1.isFile()) 
			{
				expFilename1=file1.getName();//Getting File Names into a variable
				Log.message("Expected File name is:"+expFilename1);				
			}
		}
		SkySiteUtils.waitTill(10000);
		//Working on CSV file
		String csvFileToRead = DownloadPath1 +expFilename1;
		Log.message(csvFileToRead);		
		BufferedReader br = null;
		String line = ""; 
		
		String splitBy = ",";
		int count = 0;		
		String DocumentName = null;
		try
		{
			br = new BufferedReader(new FileReader(csvFileToRead)); 
			while ((line = br.readLine()) != null) 
			{  
				count=count+1;
				Log.message("Downloaded csv file from data in : "+count+ " rows");
				String[] ActValue = line.split(splitBy);
				DocumentName = ActValue[0];
				DocumentName = DocumentName.replace("\"", "");
				Log.message("File Name from csv file is: "+ DocumentName);						
			}
			Log.message("Downloaded csv file have data in rows count is:"+count);
			Log.message(DocumentName);
			if(DocumentName.trim().contains(fileName.trim()))
			{
				result1=true;
				Log.message("Validation the Export as CSV in Manage users Successfull!!!!!");
			}
			else
			{
				result1=false;
				Log.message("Validation the Export as CSV in Manage users UnSuccessfull!!!!!");				
			}
			br.close();
		}		
		catch (FileNotFoundException e)
		{  
			e.printStackTrace();
		}
		catch (IOException e)
		{ 
			e.printStackTrace();
		}		
		if(result1==true)
			return true;
		else
			return false;
 }	
	
	@FindBy(xpath = "//i[@class='icon icon-xls icon-lg']")
	WebElement btniconiconxls;	
	
 /** 
  * Method written for export as excel in Manage Users.
  * Scripted By: Sekhar     
  * @throws IOException 
  * @throws AWTException 
  */
	
 public boolean Export_Excel_Manage_Users(String DownloadPath,String DownloadPath1) throws IOException, AWTException,InvalidFormatException
 {  
 	boolean result1=false;    	
 	SkySiteUtils.waitTill(3000);
 	driver.switchTo().defaultContent();	  	
 	// Calling delete files from download folder script
 	this.Delete_Files_From_Folder(DownloadPath);
 	SkySiteUtils.waitTill(5000); 	
 	btniconsettings.click();
 	Log.message("Setting button has been clicked");
 	SkySiteUtils.waitTill(2000);
 	btniconmanageuser.click();
 	Log.message("manage users button has been clicked");
 	SkySiteUtils.waitTill(5000);
 	driver.switchTo().frame(driver.findElement(By.id("myFrame")));    	
 	SkySiteUtils.waitTill(2000);
 	btniconiconexport.click();
 	Log.message("Export button has been clicked");
 	SkySiteUtils.waitTill(3000); 
 	btniconiconxls.click();
 	Log.message("Export as Excel button has been clicked");
 	SkySiteUtils.waitTill(5000); 	  	
 	// Get Browser name on run-time.
 	Capabilities caps = ((RemoteWebDriver) driver).getCapabilities();
 	String browserName = caps.getBrowserName();
 	Log.message("Browser name on run-time is: " + browserName);
 	
 	if (browserName.contains("firefox")) 
 	{
 		// Handling Download PopUp of firefox browser using robot
 		Robot robot1 = null;
 		robot1 = new Robot();			
 		robot1.keyPress(KeyEvent.VK_ALT);		
 		robot1.keyPress(KeyEvent.VK_S);
 		SkySiteUtils.waitTill(4000);
 		robot1.keyRelease(KeyEvent.VK_S);
 		robot1.keyRelease(KeyEvent.VK_ALT);			
 		SkySiteUtils.waitTill(3000);
 		robot1.keyPress(KeyEvent.VK_ENTER);
 		robot1.keyRelease(KeyEvent.VK_ENTER);
 		SkySiteUtils.waitTill(20000);
 	}
 	SkySiteUtils.waitTill(25000);
 	// After checking whether download file or not
 	String expFilename = null;
 	
 	File[] files = new File(DownloadPath).listFiles();
 	
 	for(File file : files)
		{
			if(file.isFile()) 
			{
				expFilename=file.getName();//Getting File Names into a variable
				Log.message("Expected File name is:"+expFilename);				
			}
		}
		SkySiteUtils.waitTill(10000);
		//Working on Excel file
		String xlsxFileToRead = DownloadPath1 +expFilename;
		Log.message(xlsxFileToRead);
							
		InputStream ExcelFileToRead = new FileInputStream(xlsxFileToRead);
		XSSFWorkbook wb = new XSSFWorkbook(ExcelFileToRead);   

		XSSFSheet sheet=wb.getSheetAt(0);
		XSSFRow row; 
		XSSFCell cell;

		Iterator<Row> rows = sheet.rowIterator();
		
		System.out.println(rows);
		
		while (rows.hasNext())
		{
			row=(XSSFRow) rows.next();
			Iterator cells = row.cellIterator();
			
			while (cells.hasNext())
			{
				cell=(XSSFCell) cells.next();
		
				if (cell.getCellType() == XSSFCell.CELL_TYPE_STRING)
				{
					Log.message(cell.getStringCellValue()+" ");
				}
				else if(cell.getCellType() == XSSFCell.CELL_TYPE_NUMERIC)
				{
					Log.message(cell.getNumericCellValue()+" ");
				}
				else
				{
					//System.out.println("Excel as File UnSuccessfully Validated");
				}
			}	
			System.out.println();	
			try
			{
				ExcelFileToRead.close();
			} 
			catch (IOException e) 
			{
				e.printStackTrace();
			}	
		}			
		SkySiteUtils.waitTill(20000);
		result1=true;
		if(result1==true)
		{
			Log.message("Excel as File Successfully Validated");
			return true;
		}
		else
		{
			Log.message("Excel as File UnSuccessfully Validated");
			return false;   
		}
 	}
 
 
 @FindBy(css = ".icon.icon-star.ico-lg.icon-communication")
 WebElement btniconcommunication;
 
 @FindBy(css = "#expBtn")
 WebElement btnexpBtn;
 
 @FindBy(xpath = "//i[@class='icon icon-csv icon-lg']")
 WebElement btniconiconcsv1;
	

/** 
 * Method written for export as csv in Communication.
 * Scripted By: Sekhar  	
	 * @throws IOException 
 * @throws AWTException 
 */

public boolean Export_CSV_Communication(String DownloadPath,String DownloadPath1,String fileName) throws IOException, AWTException
{  
	boolean result1=false;    	
	SkySiteUtils.waitTill(5000);  
	// Calling delete files from download folder script
	this.Delete_Files_From_Folder(DownloadPath);	
	SkySiteUtils.waitTill(5000);
	btniconcommunication.click();
	Log.message("communications button has been clicked");
	SkySiteUtils.waitTill(2000);	
	driver.switchTo().frame(driver.findElement(By.id("myFrame")));    	
	SkySiteUtils.waitTill(2000);
	btnexpBtn.click();
	Log.message("Export button has been clicked");
	SkySiteUtils.waitTill(3000); 
	btniconiconcsv1.click();
	Log.message("Export as csv button has been clicked");
	SkySiteUtils.waitTill(5000);
	// Get Browser name on run-time.
	Capabilities caps = ((RemoteWebDriver) driver).getCapabilities();
	String browserName = caps.getBrowserName();
	Log.message("Browser name on run-time is: " + browserName);
	
	if (browserName.contains("firefox")) 
	{
		// Handling Download PopUp of firefox browser using robot
		Robot robot1 = null;
		robot1 = new Robot();			
		robot1.keyPress(KeyEvent.VK_ALT);		
		robot1.keyPress(KeyEvent.VK_S);
		SkySiteUtils.waitTill(4000);
		robot1.keyRelease(KeyEvent.VK_S);
		robot1.keyRelease(KeyEvent.VK_ALT);			
		SkySiteUtils.waitTill(3000);
		robot1.keyPress(KeyEvent.VK_ENTER);
		robot1.keyRelease(KeyEvent.VK_ENTER);
		SkySiteUtils.waitTill(20000);
	}
	SkySiteUtils.waitTill(25000);
	// After checking whether download file or not
	String expFilename1 = null;
	
	File[] files1 = new File(DownloadPath).listFiles();
	
	for(File file1 : files1)
		{
			if(file1.isFile()) 
			{
				expFilename1=file1.getName();//Getting File Names into a variable
				Log.message("Expected File name is:"+expFilename1);				
			}
		}
		SkySiteUtils.waitTill(10000);
		//Working on CSV file
		String csvFileToRead = DownloadPath1 +expFilename1;
		Log.message(csvFileToRead);		
		BufferedReader br = null;
		String line = ""; 
		
		String splitBy = ",";
		int count = 0;		
		String DocumentName = null;
		try
		{
			br = new BufferedReader(new FileReader(csvFileToRead)); 
			while ((line = br.readLine()) != null) 
			{  
				count=count+1;
				Log.message("Downloaded csv file from data in : "+count+ " rows");
				String[] ActValue = line.split(splitBy);
				DocumentName = ActValue[3];
				DocumentName = DocumentName.replace("\"", "");
				Log.message("File Name from csv file is: "+ DocumentName);						
			}
			Log.message("Downloaded csv file have data in rows count is:"+count);
			Log.message(DocumentName);
			Log.message(fileName);
			if(DocumentName.trim().contains(fileName.trim()))
			{
				result1=true;
				Log.message("Validation the Export as CSV in Communication Successfull!!!!!");
			}
			else
			{
				result1=false;
				Log.message("Validation the Export as CSV in Communication UnSuccessfull!!!!!");				
			}
			br.close();
		}		
		catch (FileNotFoundException e)
		{  
			e.printStackTrace();
		}
		catch (IOException e)
		{ 
			e.printStackTrace();
		}		
		if(result1==true)
			return true;
		else
			return false;
}	
	
	
	@FindBy(xpath = "//i[@class='icon icon-xls icon-lg']")
	WebElement btniconiconxls1;	
	
/** 
 * Method written for export as excel in Communication.
 * Scripted By: Sekhar     
 * @throws IOException 
 * @throws AWTException 
 */
	
public boolean Export_Excel_Communication(String DownloadPath,String DownloadPath1) throws IOException, AWTException,InvalidFormatException
{  
	boolean result1=false;    	
	SkySiteUtils.waitTill(3000);
	driver.switchTo().defaultContent();	  	
	// Calling delete files from download folder script
	this.Delete_Files_From_Folder(DownloadPath);
	SkySiteUtils.waitTill(5000); 	
	btniconcommunication.click();
	Log.message("Communicatios button has been clicked");
	SkySiteUtils.waitTill(2000);	
	driver.switchTo().frame(driver.findElement(By.id("myFrame")));    	
	SkySiteUtils.waitTill(2000);	
	btnexpBtn.click();
	Log.message("Export button has been clicked");
	SkySiteUtils.waitTill(3000); 
	btniconiconxls1.click();
	Log.message("Export as Export button has been clicked");
	SkySiteUtils.waitTill(5000);	  	
	// Get Browser name on run-time.
	Capabilities caps = ((RemoteWebDriver) driver).getCapabilities();
	String browserName = caps.getBrowserName();
	Log.message("Browser name on run-time is: " + browserName);
	
	if (browserName.contains("firefox")) 
	{
		// Handling Download PopUp of firefox browser using robot
		Robot robot1 = null;
		robot1 = new Robot();			
		robot1.keyPress(KeyEvent.VK_ALT);		
		robot1.keyPress(KeyEvent.VK_S);
		SkySiteUtils.waitTill(4000);
		robot1.keyRelease(KeyEvent.VK_S);
		robot1.keyRelease(KeyEvent.VK_ALT);			
		SkySiteUtils.waitTill(3000);
		robot1.keyPress(KeyEvent.VK_ENTER);
		robot1.keyRelease(KeyEvent.VK_ENTER);
		SkySiteUtils.waitTill(20000);
	}
	SkySiteUtils.waitTill(25000);
			
	// After checking whether download file or not
	String expFilename = null;
	
	File[] files = new File(DownloadPath).listFiles();
	
	for(File file : files)
		{
			if(file.isFile()) 
			{
				expFilename=file.getName();//Getting File Names into a variable
				Log.message("Expected File name is:"+expFilename);				
			}
		}
		SkySiteUtils.waitTill(10000);
		//Working on Excel file
		String xlsxFileToRead = DownloadPath1 +expFilename;
		Log.message(xlsxFileToRead);
							
		InputStream ExcelFileToRead = new FileInputStream(xlsxFileToRead);
		XSSFWorkbook wb = new XSSFWorkbook(ExcelFileToRead);   

		XSSFSheet sheet=wb.getSheetAt(0);
		XSSFRow row; 
		XSSFCell cell;

		Iterator<Row> rows = sheet.rowIterator();
		
		System.out.println(rows);
		
		while (rows.hasNext())
		{
			row=(XSSFRow) rows.next();
			Iterator cells = row.cellIterator();
			
			while (cells.hasNext())
			{
				cell=(XSSFCell) cells.next();
		
				if (cell.getCellType() == XSSFCell.CELL_TYPE_STRING)
				{
					Log.message(cell.getStringCellValue()+" ");
				}
				else if(cell.getCellType() == XSSFCell.CELL_TYPE_NUMERIC)
				{
					Log.message(cell.getNumericCellValue()+" ");
				}
				else
				{
					//System.out.println("Excel as File UnSuccessfully Validated");
				}
			}	
			System.out.println();	
			try
			{
				ExcelFileToRead.close();
			} 
			catch (IOException e) 
			{
				e.printStackTrace();
			}	
		}			
		SkySiteUtils.waitTill(20000);
		result1=true;
		if(result1==true)
		{
			Log.message("Excel as File Successfully Validated");
			return true;
		}
		else
		{
			Log.message("Excel as File UnSuccessfully Validated");
			return false;   
		}		
	}
    
}
