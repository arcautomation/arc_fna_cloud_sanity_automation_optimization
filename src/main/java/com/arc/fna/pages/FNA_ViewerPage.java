package com.arc.fna.pages;

import java.awt.AWTException;
import java.awt.Dimension;

import org.sikuli.script.FindFailed;

import org.sikuli.script.Pattern;

import org.sikuli.script.Screen;
import java.awt.Robot;
import java.awt.Toolkit;
import java.awt.datatransfer.Clipboard;
import java.awt.datatransfer.DataFlavor;
import java.awt.datatransfer.UnsupportedFlavorException;
import java.awt.event.InputEvent;
import java.awt.event.KeyEvent;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Random;
import java.util.Set;

import javax.imageio.ImageIO;

import org.apache.commons.io.FileUtils;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.interactions.Mouse;
import org.openqa.selenium.interactions.SendKeysAction;
import org.openqa.selenium.internal.Locatable;
import org.openqa.selenium.remote.BrowserType;
import org.openqa.selenium.support.CacheLookup;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.LoadableComponent;
import org.openqa.selenium.support.ui.Select;
import org.testng.Assert;

import com.arc.fna.utils.PropertyReader;
import com.arc.fna.utils.SkySiteUtils;
import com.arcautoframe.utils.Log;

public class FNA_ViewerPage extends LoadableComponent<FNA_ViewerPage> {
	WebDriver driver;
	private boolean isPageLoaded;

	@Override
	protected void load() {
		// TODO Auto-generated method stub
		isPageLoaded = true;
	}

	@Override
	protected void isLoaded() throws Error {
		// TODO Auto-generated method stub
		if (!isPageLoaded) {
			Assert.fail();
		}
	}

	/**
	 * Declaring constructor for initializing web elements using PageFactory class.
	 * 
	 * @param driver
	 */
	public FNA_ViewerPage(WebDriver driver) {
		this.driver = driver;
		PageFactory.initElements(driver, this);
	}

	/**
	 * Method generate random markup name
	 * 
	 * @return
	 */
	public String Random_Markupname() {

		String str = "Markup";
		Random r = new Random(System.currentTimeMillis());
		String abc = str + String.valueOf((10000 + r.nextInt(20000)));
		return abc;

	}

	/**
	 * Method generate second random markup name
	 * 
	 * @return
	 */
	public String Random_SecondMarkupname() {

		String str = "SecondMarkup";
		Random r = new Random(System.currentTimeMillis());
		String abc = str + String.valueOf((10000 + r.nextInt(20000)));
		return abc;

	}

	/**
	 * Method generate third random markup name
	 * 
	 * @return
	 */
	public String Random_ThirdMarkupname() {

		String str = "ThirdMarkup";
		Random r = new Random(System.currentTimeMillis());
		String abc = str + String.valueOf((10000 + r.nextInt(20000)));
		return abc;

	}

	/**
	 * Method to select folder and file
	 * 
	 */
	public void selectFolder_File(String Foldername) {
		driver.switchTo().defaultContent();
		Log.message("Switched to default content");
		SkySiteUtils.waitTill(5000);
		driver.switchTo().frame(driver.findElement(By.id("myFrame")));
		Log.message("Switched to frame");
		SkySiteUtils.waitTill(4000);

		int Count_no = driver.findElements(By.xpath(
				"//*[@id='treeboxbox_tree_wrap_container']/div/table/tbody/tr[2]/td[2]/table/tbody/tr/td[2]/table/tbody/tr/td[4]/span"))
				.size();
		Log.message("the number folder present are " + Count_no);
		SkySiteUtils.waitTill(5000);
		int k = 0;
		for (k = 1; k <= Count_no; k++) {

			String foldernamepresent = driver.findElement(By.xpath(
					"(//*[@id='treeboxbox_tree_wrap_container']/div/table/tbody/tr[2]/td[2]/table/tbody/tr/td[2]/table/tbody/tr/td[4]/span)["
							+ k + "]"))
					.getText();
			if (foldernamepresent.contains(Foldername)) {
				Log.message("Required folder is present at " + k + " position in the tree");
				driver.findElement(By.xpath(
						"(//*[@id='treeboxbox_tree_wrap_container']/div/table/tbody/tr[2]/td[2]/table/tbody/tr/td[2]/table/tbody/tr/td[4]/span)["
								+ k + "]"))
						.click();
				Log.message("Folder is selected");
				SkySiteUtils.waitTill(5000);
				break;
			}
		}
		driver.findElement(By.xpath(".//*[@id='divGrid']/div[2]/table/tbody/tr[2]/td[2]/a[3]")).click();
		Log.message("File is selected");
	}

	@FindBy(xpath = ".//*[@id='shapeToolMenuBtn']/i")
	@CacheLookup
	WebElement shapemenubtn;
	@FindBy(xpath = ".//*[@id='rectangleCreate']/a/i")
	@CacheLookup
	WebElement drawrectangle;
	@FindBy(xpath = ".//*[@id='lineCreate']/a/i")
	WebElement lineannotation;

	/**
	 * Method to draw square annotation in viewer Scripted by : Tarak
	 * 
	 * @return
	 * @throws AWTException
	 * @throws InterruptedException
	 * @throws FindFailed
	 * @throws IOException
	 */
	public boolean drawLineAnnotation(String markupname, String filepath1)
			throws AWTException, InterruptedException, FindFailed, IOException {

		boolean flag = false;
		Log.message("Switched to default content");
		SkySiteUtils.waitTill(5000);
		/**
		 * driver.switchTo().frame(driver.findElement(By.id("myFrame")));
		 * Log.message("Switched to frame");
		 * driver.findElement(By.xpath(".//*[@id='treeboxbox_tree_wrap_container']/div[2]/table/tbody/tr[2]/td[2]/table/tbody/tr[3]/td[2]/table/tbody/tr/td[4]/span")).click();
		 * Log.message("Folder selected"); SkySiteUtils.waitTill(8000);
		 * driver.findElement(By.xpath(".//*[@id='divGrid']/div[2]/table/tbody/tr[2]/td[2]/a[3]")).click();
		 * Log.message("File is selected");
		 **/
		SkySiteUtils.waitTill(4000);
		String MainWindow = driver.getWindowHandle();
		Log.message(MainWindow);
		for (String childwindow : driver.getWindowHandles()) {
			driver.switchTo().window(childwindow);

		}
		Log.message("switched to child window");

		driver.switchTo().defaultContent();
		SkySiteUtils.waitTill(4000);
		// method to delete existing markup
		this.markupdeletebegin();
		SkySiteUtils.waitTill(4000);
		drawlinetool.click();
		Log.message("Linetool is clicked");
		SkySiteUtils.waitTill(4000);
		Actions act = new Actions(driver);
		act.moveToElement(lineannotation).clickAndHold(lineannotation).build().perform();
		SkySiteUtils.waitTill(2000);
		act.click().build().perform();
		Log.message("Line annotation  tool button is clicked");
		SkySiteUtils.waitTill(2000);
		File fis = new File(PropertyReader.getProperty("Screenshotpath"));
		String filepath = fis.getAbsolutePath().toString();
		String Screenshot1 = filepath + "\\StartpointRect.png";
		String Screenshot2 = filepath + "\\EndpointRect.png";
		Screen s = new Screen();

		Pattern p1 = new Pattern(Screenshot1);

		Pattern p2 = new Pattern(Screenshot2);

		s.click(p1);

		s.drag(p1);

		s.dropAt(p2);
		Log.message("image is drawn");

		SkySiteUtils.waitTill(4000);

		savebtn.click();
		Log.message("save button is clicked");
		SkySiteUtils.waitTill(4000);
		markupfield.sendKeys(markupname);
		Log.message("Enter markup name is " + markupname);
		SkySiteUtils.waitForElement(driver, savemarkupbtn, 40);
		savemarkupbtn.click();
		Log.message("savemarkupbtn is clicked");
		SkySiteUtils.waitTill(14000);
		driver.findElement(By.xpath(".//*[@id='imageViewer']/div[3]/div[4]/div/ul/li[2]/a")).click();
		Log.message("save button is clicked again");
		SkySiteUtils.waitTill(4000);
		// File fis1=new File(PropertyReader.getProperty("SaveScreenshot"));
		// String filepath1=fis1.getAbsolutePath().toString();
		String savedscreenshot1 = filepath1 + "\\BeforesavingmarkupArrow.png";
		String savedscreenshot2 = filepath1 + "\\AftersavingmarkupArrow.png";
		File srcFiler = ((TakesScreenshot) driver).getScreenshotAs(OutputType.FILE);

		FileUtils.copyFile(srcFiler, new File(savedscreenshot1));

		Log.message("Screenshot taken of saved markup");
		SkySiteUtils.waitTill(4000);
		markupdropdown.click();
		Log.message("markup dropdown clicked");
		SkySiteUtils.waitTill(4000);
		for (WebElement markupitem : markuplist)
			if (markupitem.getText().contains(markupname))
				driver.findElement(By.xpath("//*[@data-original-title='" + markupname + "']")).click();

		Log.message("save markup clicked and image drawn disappears");
		SkySiteUtils.waitTill(4000);
		for (WebElement markupitem : markuplist)
			if (markupitem.getText().contains(markupname))
				driver.findElement(By.xpath("//*[@data-original-title='" + markupname + "']")).click();

		// showallmarkup.click();
		Log.message("show all markup clikced");
		SkySiteUtils.waitTill(4000);
		savebtn.click();
		Log.message("save button is clicked");
		SkySiteUtils.waitTill(4000);
		File srcFiler1 = ((TakesScreenshot) driver).getScreenshotAs(OutputType.FILE);

		FileUtils.copyFile(srcFiler1, new File(savedscreenshot2));
		Log.message("Screenshot taken after redrawing");

		BufferedImage imgA = ImageIO.read(new File(savedscreenshot1));

		// Thread.sleep(2000);

		BufferedImage imgB = ImageIO.read(new File(savedscreenshot2));

		// Thread.sleep(2000);

		Log.message("Checking the height and width of both the images are same?");

		if (imgA.getWidth() != imgB.getWidth() && imgA.getHeight() != imgB.getHeight()) {
			Log.message("image height width are different");
			return false;
		}

		int width = imgA.getWidth();
		int height = imgA.getHeight();
		for (int y = 0; y < height; y++) {
			for (int x = 0; x < width; x++) {

				if (imgA.getRGB(x, y) != imgB.getRGB(x, y)) {

					Log.message("Image are different");
					return false;
				}
			}

		}

		return true;
	}

	@FindBy(xpath = ".//*[@id='imageViewer']/div[3]/div[4]/div/ul/li[2]/a")
	WebElement savebtn;
	@FindBy(xpath = ".//*[@id='txtBasicAnnotationTitle']")
	WebElement markupfield;
	@FindBy(xpath = ".//*[@id='btnBasicAnnotationSave']")
	WebElement savemarkupbtn;
	@FindBy(xpath = ".//*[@id='btnLoadMarkup']/a")
	WebElement markupdropdown;
	@FindBy(xpath = ".//*[@id='btnShowAllMarkups']")
	WebElement showallmarkup;
	@FindBy(xpath = ".//*[@id='btnShowAllHyperlinks']")
	WebElement showhyperlinks;
	@FindBy(id = "ulMarkupList")
	List<WebElement> markuplist;
	@FindBy(xpath = "//*[@class='li-striped dropdown-list hyp-annot']")
	List<WebElement> hperlinklist;
	@FindBy(xpath = ".//*[@id='btnShowAllHyperlinks']")
	WebElement showallmarkuphyperlink;

	/**
	 * Method to draw square annotation
	 * 
	 * @throws FindFailed
	 * @throws InterruptedException
	 * @throws IOException
	 * 
	 */
	public boolean drawSquareAnnotation(String markup, String filepath1)
			throws FindFailed, InterruptedException, IOException {
		boolean flag = false;
		Log.message("Switched to default content");
		SkySiteUtils.waitTill(5000);

		String MainWindow = driver.getWindowHandle();
		Log.message(MainWindow);
		for (String childwindow : driver.getWindowHandles()) {
			driver.switchTo().window(childwindow);

		}
		Log.message("switched to child window");

		driver.switchTo().defaultContent();
		SkySiteUtils.waitTill(4000);
		// method to delete existing markup
		this.markupdeletebegin();
		shapemenubtn.click();
		Log.message("Shape menu button is clicked");
		SkySiteUtils.waitTill(3000);
		Actions act = new Actions(driver);
		act.moveToElement(drawrectangle).clickAndHold(drawrectangle).build().perform();
		SkySiteUtils.waitTill(2000);
		act.click().build().perform();
		Log.message("draw rectangle button is clicked");
		SkySiteUtils.waitTill(2000);
		File fis = new File(PropertyReader.getProperty("Screenshotpath"));
		String filepath = fis.getAbsolutePath().toString();
		String Screenshot1 = filepath + "\\StartpointRect.png";
		String Screenshot2 = filepath + "\\EndpointRect.png";
		Screen s = new Screen();

		Pattern p1 = new Pattern(Screenshot1);

		Pattern p2 = new Pattern(Screenshot2);

		s.click(p1);

		s.drag(p1);

		s.dropAt(p2);
		Log.message("image is drawn");
		SkySiteUtils.waitTill(4000);
		savebtn.click();
		Log.message("save button is clicked");
		SkySiteUtils.waitTill(4000);
		markupfield.sendKeys(markup);
		Log.message("Enter markup name is " + markup);
		SkySiteUtils.waitForElement(driver, savemarkupbtn, 40);
		savemarkupbtn.click();
		Log.message("savemarkupbtn is clicked");
		SkySiteUtils.waitTill(14000);
		driver.findElement(By.xpath(".//*[@id='imageViewer']/div[3]/div[4]/div/ul/li[2]/a")).click();
		Log.message("save button is clicked again");
		SkySiteUtils.waitTill(4000);
		// File fis1=new File(PropertyReader.getProperty("SaveScreenshot"));
		// String filepath1=fis1.getAbsolutePath().toString();
		String savedscreenshot1 = filepath1 + "\\BeforesavingmarkupSquare.png";
		String savedscreenshot2 = filepath1 + "\\AfterSavingMarkerSquare.png";
		File srcFiler = ((TakesScreenshot) driver).getScreenshotAs(OutputType.FILE);

		FileUtils.copyFile(srcFiler, new File(savedscreenshot1));

		Log.message("Screenshot taken of saved markup");
		SkySiteUtils.waitTill(4000);
		markupdropdown.click();
		Log.message("markup dropdown clicked");
		SkySiteUtils.waitTill(4000);
		for (WebElement markupitem : markuplist)
			if (markupitem.getText().contains(markup))
				driver.findElement(By.xpath("//*[@data-original-title='" + markup + "']")).click();

		Log.message("save markup clicked and image drawn disappears");
		SkySiteUtils.waitTill(4000);
		SkySiteUtils.waitTill(4000);
		for (WebElement markupitem : markuplist)
			if (markupitem.getText().contains(markup))
				driver.findElement(By.xpath("//*[@data-original-title='" + markup + "']")).click();

		// showallmarkup.click();
		Log.message("Markup is displayed");
		SkySiteUtils.waitTill(4000);
		savebtn.click();
		Log.message("save button is clicked");
		SkySiteUtils.waitTill(4000);
		File srcFiler1 = ((TakesScreenshot) driver).getScreenshotAs(OutputType.FILE);

		FileUtils.copyFile(srcFiler1, new File(savedscreenshot2));
		Log.message("Screenshot taken after redrawing");

		BufferedImage imgA = ImageIO.read(new File(savedscreenshot1));

		// Thread.sleep(2000);

		BufferedImage imgB = ImageIO.read(new File(savedscreenshot2));

		// Thread.sleep(2000);

		Log.message("Checking the height and width of both the images are same?");

		if (imgA.getWidth() != imgB.getWidth() && imgA.getHeight() != imgB.getHeight()) {
			Log.message("image height width are different");
			return false;
		}

		int width = imgA.getWidth();
		int height = imgA.getHeight();
		for (int y = 0; y < height; y++) {
			for (int x = 0; x < width; x++) {

				if (imgA.getRGB(x, y) != imgB.getRGB(x, y)) {

					Log.message("Image are different");
					return false;
				}
			}

		}

		return true;
	}

	@FindBy(xpath = ".//*[@id='button-1']")
	WebElement yesbtn;

	/**
	 * Method to delete the markup added
	 * 
	 * @param markup
	 */
	public void deletemarkup(String markup) {

		markupdropdown.click();
		Log.message("markup dropdown clicked");
		SkySiteUtils.waitTill(8000);
		for (WebElement markupitem : markuplist)
			if (markupitem.getText().contains(markup))
				driver.findElement(By.xpath("//*[@class='icon icon-trash icon-lg']")).click();
		Log.message("added markup deleted successfully");
		JavascriptExecutor executor = (JavascriptExecutor) driver;
		executor.executeScript("arguments[0].click();", yesbtn);
		// yesbtn.click();
		Log.message("Yes button is clicked");
		SkySiteUtils.waitTill(10000);
	}
     @FindBy(xpath="//*[@class='icon icon-trash icon-lg']")
     WebElement icondelete;
	/**
	 * Method to delte markup at begining
	 * 
	 */
	public void markupdeletebegin() {
		{

			if (markupdropdown.isDisplayed()) {
				int sizeoflist = markuplist.size();
				Log.message("size is " + sizeoflist);
				for (int i = 1; i <= sizeoflist; i++) {
					markupdropdown.click();
					Log.message("markup dropdown clicked");
					SkySiteUtils.waitTill(8000);

					if (driver.findElement(By.xpath("//*[@class='icon icon-trash icon-lg']")).isDisplayed())
						//driver.findElement(By.xpath("//*[@class='icon icon-trash icon-lg']")).click();
					{
						Actions act=new Actions(driver);
						act.moveToElement(icondelete).click(icondelete).build().perform();
					}
					Log.message("added markup deleted successfully");
					JavascriptExecutor executor = (JavascriptExecutor) driver;
					executor.executeScript("arguments[0].click();", yesbtn);
					// yesbtn.click();
					Log.message("Yes button is clicked");
					SkySiteUtils.waitTill(10000);
				}
			} else {

				Log.message("no markup is present");

			}

		}
	}
	@FindBy(id="ulMarkupList")
	WebElement markupcontainer;
     /**Delte markup for multipage scenario
      * 
      */
	public void markupdeltemultipage() {
		{
		
			if (markupdropdown.isDisplayed()) {
				markupdropdown.click();
				Log.message("Dropdown is clicked");
				SkySiteUtils.waitTill(5000);
				int size=markupcontainer.findElements(By.xpath("//*[@class='icon icon-trash icon-lg']")).size();
				Log.message("size is " + size);
				SkySiteUtils.waitTill(4000);
				int noofmarkup=markupcontainer.findElements(By.xpath("//span[contains(text(),'Markup')]")).size();
				Log.message("No of markup is " +noofmarkup);
			int index=markuplist.indexOf(driver.findElement(By.xpath("//span[contains(text(),'Markup')]")));
			   Log.message("index is " +index);
				if (driver.findElement(By.xpath("//span[contains(text(),'Markup')]")).isDisplayed())
						//driver.findElement(By.xpath("//*[@class='icon icon-trash icon-lg']")).click();
					{
						Log.message("I am in loop");
						
						driver.findElement(By.xpath("//span[contains(text(),'Markup')]")).click();
						SkySiteUtils.waitTill(4000);
						if(index==-1) {
						//Actions act=new Actions(driver);
						//act.moveToElement(icondelete).click(icondelete).build().perform();
			
						driver.findElement(By.xpath("//*[@class='icon icon-trash icon-lg']")).click();
						}
					}
					Log.message("added markup deleted successfully");
					JavascriptExecutor executor = (JavascriptExecutor) driver;
					executor.executeScript("arguments[0].click();", yesbtn);
					// yesbtn.click();
					Log.message("Yes button is clicked");
					SkySiteUtils.waitTill(10000);
				}
			 else {

				Log.message("no markup is present");

			}

		}
	}
	/**
	 * Method to delete the markup added
	 * 
	 * @param markup
	 */
	public void deleteSecondmarkup(String markup) {
		WebElement seconddelete1 = driver.findElement(By.xpath("//*[@title='Delete']"));
		Actions act = new Actions(driver);
		markupdropdown.click();
		Log.message("markup dropdown clicked");
		SkySiteUtils.waitTill(8000);
		for (WebElement markupitem : markuplist)
			if (markupitem.getText().contains(markup))
				act.moveToElement(seconddelete1).click().build().perform();
		Log.message("added markup deleted successfully");
		SkySiteUtils.waitTill(5000);
		JavascriptExecutor executor = (JavascriptExecutor) driver;
		executor.executeScript("arguments[0].click();", yesbtn);
		// yesbtn.click();
		Log.message("Yes button is clicked");
		SkySiteUtils.waitTill(10000);

	}

	@FindBy(xpath = ".//*[@id='textToolMenuBtn']/i")
	WebElement tootextbtn;
	@FindBy(xpath = ".//*[@id='textCreate']/a/i")
	WebElement textcreatebtn;
	@FindBy(xpath = ".//*[@id='editSheetViewerText']")
	WebElement textfield;
	@FindBy(xpath = ".//*[@id='editTextAnnotation']/div/div[3]/button[2]")
	WebElement okbtn;

	/**
	 * Method to draw Text Annotation
	 * 
	 * @throws FindFailed
	 * @throws IOException
	 * 
	 */
	public boolean drawTextAnnotation(String markupname, String filepath1) throws FindFailed, IOException {
		boolean flag = false;
		Log.message("Switched to default content");
		SkySiteUtils.waitTill(5000);

		SkySiteUtils.waitTill(4000);
		String MainWindow = driver.getWindowHandle();
		Log.message(MainWindow);
		for (String childwindow : driver.getWindowHandles()) {
			driver.switchTo().window(childwindow);

		}
		Log.message("switched to child window");

		driver.switchTo().defaultContent();
		SkySiteUtils.waitTill(4000);
		// method to delete existing markup
		this.markupdeletebegin();
		tootextbtn.click();
		Log.message("text tool is clicked");
		Actions act = new Actions(driver);
		act.moveToElement(textcreatebtn).clickAndHold(textcreatebtn).build().perform();
		SkySiteUtils.waitTill(2000);
		act.click().build().perform();
		Log.message("draw text tool button is clicked");
		SkySiteUtils.waitTill(2000);
		File fis = new File(PropertyReader.getProperty("Screenshotpath"));
		String filepath = fis.getAbsolutePath().toString();
		String Screenshot1 = filepath + "\\StartpointRect.png";
		String Screenshot2 = filepath + "\\EndpointRect.png";
		Screen s = new Screen();

		Pattern p1 = new Pattern(Screenshot1);

		Pattern p2 = new Pattern(Screenshot2);

		s.click(p1);

		// s.click(p1);
		// s.drag(p1);

		// s.dropAt(p2);
		Log.message("image is drawn");

		SkySiteUtils.waitTill(4000);
		textfield.sendKeys("drawing");
		Log.message("text has been entered");
		Select sc = new Select(driver.findElement(By.xpath(".//*[@id='selectFontSize']")));
		sc.selectByValue("36");
		Log.message("36 font size is selected");
		okbtn.click();
		Log.message("Ok button is clicked");
		savebtn.click();
		Log.message("save button is clicked");
		SkySiteUtils.waitTill(4000);
		markupfield.sendKeys(markupname);
		Log.message("Enter markup name is " + markupname);
		SkySiteUtils.waitForElement(driver, savemarkupbtn, 40);
		savemarkupbtn.click();
		Log.message("savemarkupbtn is clicked");
		SkySiteUtils.waitTill(14000);
		driver.findElement(By.xpath(".//*[@id='imageViewer']/div[3]/div[4]/div/ul/li[2]/a")).click();
		Log.message("save button is clicked again");
		SkySiteUtils.waitTill(4000);
		// File fis1=new File(PropertyReader.getProperty("SaveScreenshot"));
		// String filepath1=fis1.getAbsolutePath().toString();
		String savedscreenshot1 = filepath1 + "\\BeforesavingmarkupText.png";
		String savedscreenshot2 = filepath1 + "\\AfterSavingMarkerText.png";
		File srcFiler = ((TakesScreenshot) driver).getScreenshotAs(OutputType.FILE);

		FileUtils.copyFile(srcFiler, new File(savedscreenshot1));

		Log.message("Screenshot taken of saved markup");
		SkySiteUtils.waitTill(4000);
		markupdropdown.click();
		Log.message("markup dropdown clicked");
		SkySiteUtils.waitTill(4000);
		for (WebElement markupitem : markuplist)
			if (markupitem.getText().contains(markupname))
				driver.findElement(By.xpath("//*[@data-original-title='" + markupname + "']")).click();

		Log.message("save markup clicked and image drawn disappears");
		SkySiteUtils.waitTill(4000);
		SkySiteUtils.waitTill(4000);
		for (WebElement markupitem : markuplist)
			if (markupitem.getText().contains(markupname))
				driver.findElement(By.xpath("//*[@data-original-title='" + markupname + "']")).click();

		// showallmarkup.click();
		Log.message("Markup is displayed");
		SkySiteUtils.waitTill(4000);
		savebtn.click();
		Log.message("save button is clicked");
		SkySiteUtils.waitTill(4000);
		File srcFiler1 = ((TakesScreenshot) driver).getScreenshotAs(OutputType.FILE);

		FileUtils.copyFile(srcFiler1, new File(savedscreenshot2));
		Log.message("Screenshot taken after redrawing");

		BufferedImage imgA = ImageIO.read(new File(savedscreenshot1));

		// Thread.sleep(2000);

		BufferedImage imgB = ImageIO.read(new File(savedscreenshot2));

		// Thread.sleep(2000);

		Log.message("Checking the height and width of both the images are same?");

		if (imgA.getWidth() != imgB.getWidth() && imgA.getHeight() != imgB.getHeight()) {
			Log.message("image height width are different");
			return false;
		}

		int width = imgA.getWidth();
		int height = imgA.getHeight();
		for (int y = 0; y < height; y++) {
			for (int x = 0; x < width; x++) {

				if (imgA.getRGB(x, y) != imgB.getRGB(x, y)) {

					Log.message("Image are different");
					return false;
				}
			}

		}

		return true;
	}

	@FindBy(xpath = ".//*[@id='lineToolMenuBtn']/i")
	WebElement drawlinetool;
	@FindBy(xpath = ".//*[@id='penCreate']/a/i")
	WebElement freehandpen;

	/**
	 * Method to draw free hand annotation
	 * 
	 * @throws FindFailed
	 * @throws IOException
	 * 
	 */
	public boolean drawFreeHandAnnotation(String markupname, String filepath1) throws FindFailed, IOException {
		boolean flag = false;
		Log.message("Switched to default content");
		SkySiteUtils.waitTill(5000);

		SkySiteUtils.waitTill(4000);
		String MainWindow = driver.getWindowHandle();
		Log.message(MainWindow);
		for (String childwindow : driver.getWindowHandles()) {
			driver.switchTo().window(childwindow);

		}
		Log.message("switched to child window");

		driver.switchTo().defaultContent();
		SkySiteUtils.waitTill(4000);
		// method to delete existing markup
		this.markupdeletebegin();
		drawlinetool.click();
		Log.message("Linetool is clicked");
		Actions act = new Actions(driver);
		act.moveToElement(freehandpen).clickAndHold(freehandpen).build().perform();
		SkySiteUtils.waitTill(2000);
		act.click().build().perform();
		Log.message("free hand  tool button is clicked");
		SkySiteUtils.waitTill(2000);
		File fis = new File(PropertyReader.getProperty("Screenshotpath"));
		String filepath = fis.getAbsolutePath().toString();
		String Screenshot1 = filepath + "\\StartpointAnnotationNew.png";
		String Screenshot2 = filepath + "\\EndpointRect.png";
		Screen s = new Screen();

		Pattern p1 = new Pattern(Screenshot1);

		Pattern p2 = new Pattern(Screenshot2);

		s.click(p1);

		s.drag(p1);

		s.dropAt(p2);
		Log.message("image is drawn");

		SkySiteUtils.waitTill(4000);

		savebtn.click();
		Log.message("save button is clicked");
		SkySiteUtils.waitTill(4000);
		markupfield.sendKeys(markupname);
		Log.message("Enter markup name is " + markupname);
		SkySiteUtils.waitForElement(driver, savemarkupbtn, 40);
		savemarkupbtn.click();
		Log.message("savemarkupbtn is clicked");
		SkySiteUtils.waitTill(14000);
		driver.findElement(By.xpath(".//*[@id='imageViewer']/div[3]/div[4]/div/ul/li[2]/a")).click();
		Log.message("save button is clicked again");
		SkySiteUtils.waitTill(4000);
		// File fis1=new File(PropertyReader.getProperty("Screenshotpath"));
		// String filepath1=fis1.getAbsolutePath().toString();
		String savedscreenshot1 = filepath1 + "\\BeforesavingmarkupFreehand.png";
		String savedscreenshot2 = filepath1 + "\\AfterSavingMarkerfreehand.png";
		File srcFiler = ((TakesScreenshot) driver).getScreenshotAs(OutputType.FILE);

		FileUtils.copyFile(srcFiler, new File(savedscreenshot1));

		Log.message("Screenshot taken of saved markup");
		SkySiteUtils.waitTill(4000);
		markupdropdown.click();
		Log.message("markup dropdown clicked");
		SkySiteUtils.waitTill(4000);
		for (WebElement markupitem : markuplist)
			if (markupitem.getText().contains(markupname))
				driver.findElement(By.xpath("//*[@data-original-title='" + markupname + "']")).click();

		Log.message("save markup clicked and image drawn disappears");
		SkySiteUtils.waitTill(4000);
		SkySiteUtils.waitTill(4000);
		for (WebElement markupitem : markuplist)
			if (markupitem.getText().contains(markupname))
				driver.findElement(By.xpath("//*[@data-original-title='" + markupname + "']")).click();

		// showallmarkup.click();
		Log.message("Markup is displayed");
		SkySiteUtils.waitTill(4000);
		savebtn.click();
		Log.message("save button is clicked");
		SkySiteUtils.waitTill(4000);
		File srcFiler1 = ((TakesScreenshot) driver).getScreenshotAs(OutputType.FILE);

		FileUtils.copyFile(srcFiler1, new File(savedscreenshot2));
		Log.message("Screenshot taken after redrawing");

		BufferedImage imgA = ImageIO.read(new File(savedscreenshot1));

		// Thread.sleep(2000);

		BufferedImage imgB = ImageIO.read(new File(savedscreenshot2));

		// Thread.sleep(2000);

		Log.message("Checking the height and width of both the images are same?");

		if (imgA.getWidth() != imgB.getWidth() && imgA.getHeight() != imgB.getHeight()) {
			Log.message("image height width are different");
			return false;
		}

		int width = imgA.getWidth();
		int height = imgA.getHeight();
		for (int y = 0; y < height; y++) {
			for (int x = 0; x < width; x++) {

				if (imgA.getRGB(x, y) != imgB.getRGB(x, y)) {

					Log.message("Image are different");
					return false;
				}
			}

		}

		return true;
	}

	@FindBy(xpath = ".//*[@id='arrowCreate']/a/i")
	WebElement arrowannotation;

	/**
	 * Method to draw arrow annotation
	 * 
	 * @throws IOException
	 * @throws FindFailed
	 * 
	 */
	public boolean drawArrowAnnotation(String markupname, String filepath1) throws IOException, FindFailed {
		boolean flag = false;
		Log.message("Switched to default content");
		SkySiteUtils.waitTill(5000);

		SkySiteUtils.waitTill(4000);
		String MainWindow = driver.getWindowHandle();
		Log.message(MainWindow);
		for (String childwindow : driver.getWindowHandles()) {
			driver.switchTo().window(childwindow);

		}
		Log.message("switched to child window");

		driver.switchTo().defaultContent();
		SkySiteUtils.waitTill(4000);
		// method to delete existing markup
		this.markupdeletebegin();
		drawlinetool.click();
		Log.message("Linetool is clicked");
		Actions act = new Actions(driver);
		act.moveToElement(arrowannotation).clickAndHold(arrowannotation).build().perform();
		SkySiteUtils.waitTill(2000);
		act.click().build().perform();
		Log.message("arrow annotation  tool button is clicked");
		SkySiteUtils.waitTill(2000);
		File fis = new File(PropertyReader.getProperty("Screenshotpath"));
		String filepath = fis.getAbsolutePath().toString();
		String Screenshot1 = filepath + "\\StartpointAnnotationNew.png";
		String Screenshot2 = filepath + "\\EndpointRect.png";
		Screen s = new Screen();

		Pattern p1 = new Pattern(Screenshot1);

		Pattern p2 = new Pattern(Screenshot2);

		s.click(p1);

		s.drag(p1);

		s.dropAt(p2);
		Log.message("image is drawn");

		SkySiteUtils.waitTill(4000);

		savebtn.click();
		Log.message("save button is clicked");
		SkySiteUtils.waitTill(4000);
		markupfield.sendKeys(markupname);
		Log.message("Enter markup name is " + markupname);
		SkySiteUtils.waitForElement(driver, savemarkupbtn, 40);
		savemarkupbtn.click();
		Log.message("savemarkupbtn is clicked");
		SkySiteUtils.waitTill(14000);
		driver.findElement(By.xpath(".//*[@id='imageViewer']/div[3]/div[4]/div/ul/li[2]/a")).click();
		Log.message("save button is clicked again");
		SkySiteUtils.waitTill(4000);
		// File fis1=new File(PropertyReader.getProperty("SaveScreenshot"));
		// String filepath1=fis1.getAbsolutePath().toString();
		String savedscreenshot1 = filepath1 + "\\BeforesavingmarkupArrow.png";
		String savedscreenshot2 = filepath1 + "\\AfterSavingMarkerArrow.png";
		File srcFiler = ((TakesScreenshot) driver).getScreenshotAs(OutputType.FILE);

		FileUtils.copyFile(srcFiler, new File(savedscreenshot1));

		Log.message("Screenshot taken of saved markup");
		SkySiteUtils.waitTill(4000);
		markupdropdown.click();
		Log.message("markup dropdown clicked");
		SkySiteUtils.waitTill(4000);
		for (WebElement markupitem : markuplist)
			if (markupitem.getText().contains(markupname))
				driver.findElement(By.xpath("//*[@data-original-title='" + markupname + "']")).click();

		Log.message("save markup clicked and image drawn disappears");
		SkySiteUtils.waitTill(4000);
		SkySiteUtils.waitTill(4000);
		for (WebElement markupitem : markuplist)
			if (markupitem.getText().contains(markupname))
				driver.findElement(By.xpath("//*[@data-original-title='" + markupname + "']")).click();

		// showallmarkup.click();
		Log.message("Markup is displayed");
		SkySiteUtils.waitTill(4000);
		savebtn.click();
		Log.message("save button is clicked");
		SkySiteUtils.waitTill(4000);
		File srcFiler1 = ((TakesScreenshot) driver).getScreenshotAs(OutputType.FILE);

		FileUtils.copyFile(srcFiler1, new File(savedscreenshot2));
		Log.message("Screenshot taken after redrawing");

		BufferedImage imgA = ImageIO.read(new File(savedscreenshot1));

		// Thread.sleep(2000);

		BufferedImage imgB = ImageIO.read(new File(savedscreenshot2));

		// Thread.sleep(2000);

		Log.message("Checking the height and width of both the images are same?");

		if (imgA.getWidth() != imgB.getWidth() && imgA.getHeight() != imgB.getHeight()) {
			Log.message("image height width are different");
			return false;
		}

		int width = imgA.getWidth();
		int height = imgA.getHeight();
		for (int y = 0; y < height; y++) {
			for (int x = 0; x < width; x++) {

				if (imgA.getRGB(x, y) != imgB.getRGB(x, y)) {

					Log.message("Image are different");
					return false;
				}
			}

		}

		return true;
	}

	@FindBy(xpath = ".//*[@id='ellipseCreate']/a/i")
	WebElement drawcircleannotation;

	/**
	 * Method to draw Circle Annotation
	 * 
	 * @throws FindFailed
	 * @throws IOException
	 * 
	 */

	public boolean drawCircleAnnotation(String markupname, String filepath1) throws FindFailed, IOException {
		boolean flag = false;
		driver.switchTo().defaultContent();
		Log.message("Switched to default content");
		SkySiteUtils.waitTill(5000);

		SkySiteUtils.waitTill(4000);
		String MainWindow = driver.getWindowHandle();
		Log.message(MainWindow);
		for (String childwindow : driver.getWindowHandles()) {
			driver.switchTo().window(childwindow);

		}
		Log.message("switched to child window");

		driver.switchTo().defaultContent();
		SkySiteUtils.waitTill(4000);
		// method to delete existing markup
		this.markupdeletebegin();
		shapemenubtn.click();
		Log.message("Shape menu button is clicked");
		SkySiteUtils.waitTill(3000);
		Actions act = new Actions(driver);
		act.moveToElement(drawcircleannotation).clickAndHold(drawcircleannotation).build().perform();
		SkySiteUtils.waitTill(2000);
		act.click().build().perform();
		Log.message("draw circle button is clicked");
		SkySiteUtils.waitTill(2000);
		File fis = new File(PropertyReader.getProperty("Screenshotpath"));
		String filepath = fis.getAbsolutePath().toString();
		String Screenshot1 = filepath + "\\StartpointAnnotationNew.png";
		String Screenshot2 = filepath + "\\EndpointRect.png";
		Screen s = new Screen();

		Pattern p1 = new Pattern(Screenshot1);

		Pattern p2 = new Pattern(Screenshot2);

		s.click(p1);

		s.drag(p1);

		s.dropAt(p2);
		Log.message("image is drawn");
		SkySiteUtils.waitTill(4000);
		savebtn.click();
		Log.message("save button is clicked");
		SkySiteUtils.waitTill(4000);
		markupfield.sendKeys(markupname);
		Log.message("Enter markup name is " + markupname);
		SkySiteUtils.waitForElement(driver, savemarkupbtn, 40);
		savemarkupbtn.click();
		Log.message("savemarkupbtn is clicked");
		SkySiteUtils.waitTill(14000);
		driver.findElement(By.xpath(".//*[@id='imageViewer']/div[3]/div[4]/div/ul/li[2]/a")).click();
		Log.message("save button is clicked again");
		SkySiteUtils.waitTill(4000);
		// File fis1=new File(PropertyReader.getProperty("SaveScreenshot"));
		// String filepath1=fis1.getAbsolutePath().toString();
		String savedscreenshot1 = filepath1 + "\\BeforesavingmarkupCircle.png";
		String savedscreenshot2 = filepath1 + "\\AfterSavingMarkerSCircle.png";
		File srcFiler = ((TakesScreenshot) driver).getScreenshotAs(OutputType.FILE);

		FileUtils.copyFile(srcFiler, new File(savedscreenshot1));

		Log.message("Screenshot taken of saved markup");
		SkySiteUtils.waitTill(4000);
		markupdropdown.click();
		Log.message("markup dropdown clicked");
		SkySiteUtils.waitTill(4000);
		for (WebElement markupitem : markuplist)
			if (markupitem.getText().contains(markupname))
				driver.findElement(By.xpath("//*[@data-original-title='" + markupname + "']")).click();

		Log.message("save markup clicked and image drawn disappears");
		SkySiteUtils.waitTill(4000);
		SkySiteUtils.waitTill(4000);
		for (WebElement markupitem : markuplist)
			if (markupitem.getText().contains(markupname))
				driver.findElement(By.xpath("//*[@data-original-title='" + markupname + "']")).click();

		// showallmarkup.click();
		Log.message("Markup is displayed");
		SkySiteUtils.waitTill(4000);
		savebtn.click();
		Log.message("save button is clicked");
		SkySiteUtils.waitTill(4000);
		File srcFiler1 = ((TakesScreenshot) driver).getScreenshotAs(OutputType.FILE);

		FileUtils.copyFile(srcFiler1, new File(savedscreenshot2));
		Log.message("Screenshot taken after redrawing");

		BufferedImage imgA = ImageIO.read(new File(savedscreenshot1));

		// Thread.sleep(2000);

		BufferedImage imgB = ImageIO.read(new File(savedscreenshot2));

		// Thread.sleep(2000);

		Log.message("Checking the height and width of both the images are same?");

		if (imgA.getWidth() != imgB.getWidth() && imgA.getHeight() != imgB.getHeight()) {
			Log.message("image height width are different");
			return false;
		}

		int width = imgA.getWidth();
		int height = imgA.getHeight();
		for (int y = 0; y < height; y++) {
			for (int x = 0; x < width; x++) {

				if (imgA.getRGB(x, y) != imgB.getRGB(x, y)) {

					Log.message("Image are different");
					return false;
				}
			}

		}

		return true;
	}

	@FindBy(xpath = ".//*[@id='cloudCreate']/a/i")
	WebElement cloudannotation;

	/**
	 * Method to draw cloud annotation
	 * 
	 * @throws IOException
	 * @throws FindFailed
	 * 
	 */
	public boolean drawCloudAnnotation(String markupname, String filepath1) throws IOException, FindFailed {

		boolean flag = false;
		driver.switchTo().defaultContent();
		Log.message("Switched to default content");
		SkySiteUtils.waitTill(5000);

		SkySiteUtils.waitTill(4000);
		String MainWindow = driver.getWindowHandle();
		Log.message(MainWindow);
		for (String childwindow : driver.getWindowHandles()) {
			driver.switchTo().window(childwindow);

		}
		Log.message("switched to child window");

		driver.switchTo().defaultContent();
		SkySiteUtils.waitTill(4000);
		// method to delete existing markup
		this.markupdeletebegin();
		shapemenubtn.click();
		Log.message("Shape menu button is clicked");
		SkySiteUtils.waitTill(3000);
		Actions act = new Actions(driver);
		act.moveToElement(cloudannotation).clickAndHold(cloudannotation).build().perform();
		SkySiteUtils.waitTill(2000);
		act.click().build().perform();
		Log.message("draw cloud button is clicked");
		SkySiteUtils.waitTill(2000);
		File fis = new File(PropertyReader.getProperty("Screenshotpath"));
		String filepath = fis.getAbsolutePath().toString();
		String Screenshot1 = filepath + "\\StartpointAnnotationNew.png";
		String Screenshot2 = filepath + "\\EndpointRect.png";
		Screen s = new Screen();

		Pattern p1 = new Pattern(Screenshot1);

		Pattern p2 = new Pattern(Screenshot2);

		s.click(p1);

		s.drag(p1);

		s.dropAt(p2);
		Log.message("image is drawn");
		SkySiteUtils.waitTill(4000);
		savebtn.click();
		Log.message("save button is clicked");
		SkySiteUtils.waitTill(4000);
		markupfield.sendKeys(markupname);
		Log.message("Enter markup name is " + markupname);
		SkySiteUtils.waitForElement(driver, savemarkupbtn, 40);
		savemarkupbtn.click();
		Log.message("savemarkupbtn is clicked");
		SkySiteUtils.waitTill(14000);
		driver.findElement(By.xpath(".//*[@id='imageViewer']/div[3]/div[4]/div/ul/li[2]/a")).click();
		Log.message("save button is clicked again");
		SkySiteUtils.waitTill(4000);
		// File fis1=new File(PropertyReader.getProperty("SaveScreenshot"));
		// String filepath1=fis1.getAbsolutePath().toString();
		String savedscreenshot1 = filepath1 + "\\BeforesavingmarkupCloud.png";
		String savedscreenshot2 = filepath1 + "\\AfterSavingMarkerSCloud.png";
		File srcFiler = ((TakesScreenshot) driver).getScreenshotAs(OutputType.FILE);

		FileUtils.copyFile(srcFiler, new File(savedscreenshot1));

		Log.message("Screenshot taken of saved markup");
		SkySiteUtils.waitTill(4000);
		markupdropdown.click();
		Log.message("markup dropdown clicked");
		SkySiteUtils.waitTill(4000);
		for (WebElement markupitem : markuplist)
			if (markupitem.getText().contains(markupname))
				driver.findElement(By.xpath("//*[@data-original-title='" + markupname + "']")).click();

		Log.message("save markup clicked and image drawn disappears");
		SkySiteUtils.waitTill(4000);
		SkySiteUtils.waitTill(4000);
		for (WebElement markupitem : markuplist)
			if (markupitem.getText().contains(markupname))
				driver.findElement(By.xpath("//*[@data-original-title='" + markupname + "']")).click();

		// showallmarkup.click();
		Log.message("Markup is displayed");
		SkySiteUtils.waitTill(4000);
		savebtn.click();
		Log.message("save button is clicked");
		SkySiteUtils.waitTill(4000);
		File srcFiler1 = ((TakesScreenshot) driver).getScreenshotAs(OutputType.FILE);

		FileUtils.copyFile(srcFiler1, new File(savedscreenshot2));
		Log.message("Screenshot taken after redrawing");

		BufferedImage imgA = ImageIO.read(new File(savedscreenshot1));

		// Thread.sleep(2000);

		BufferedImage imgB = ImageIO.read(new File(savedscreenshot2));

		// Thread.sleep(2000);

		Log.message("Checking the height and width of both the images are same?");

		if (imgA.getWidth() != imgB.getWidth() && imgA.getHeight() != imgB.getHeight()) {
			Log.message("image height width are different");
			return false;
		}

		int width = imgA.getWidth();
		int height = imgA.getHeight();
		for (int y = 0; y < height; y++) {
			for (int x = 0; x < width; x++) {

				if (imgA.getRGB(x, y) != imgB.getRGB(x, y)) {

					Log.message("Image are different");
					return false;
				}
			}

		}

		return true;
	}

	@FindBy(css = ".icon.icon-lg.icon-callout2")
	WebElement textCallout;
	@FindBy(xpath = ".//*[@id='Textarea1']")
	WebElement textarea;
	@FindBy(css = ".btn.btn-primary.btn-sm.saveLayer")
	WebElement oksavebutton;

	/**
	 * Method to draw text call out annotation
	 * 
	 * @throws IOException
	 * @throws FindFailed
	 * 
	 */
	public boolean drawTextCallOutAnnotation(String markupname, String filepath1) throws IOException, FindFailed {
		boolean flag = false;
		Log.message("Switched to default content");
		SkySiteUtils.waitTill(5000);

		SkySiteUtils.waitTill(4000);
		String MainWindow = driver.getWindowHandle();
		Log.message(MainWindow);
		for (String childwindow : driver.getWindowHandles()) {
			driver.switchTo().window(childwindow);

		}
		Log.message("switched to child window");

		driver.switchTo().defaultContent();

		SkySiteUtils.waitTill(4000);
		// method to delete existing markup
		this.markupdeletebegin();
		SkySiteUtils.waitForElement(driver, tootextbtn, 50);
		tootextbtn.click();
		Log.message("text tool is clicked");
		Actions act = new Actions(driver);
		act.moveToElement(textCallout).clickAndHold(textCallout).build().perform();
		SkySiteUtils.waitTill(2000);
		act.click().build().perform();
		Log.message("draw text callout tool button is clicked");
		SkySiteUtils.waitTill(2000);
		File fis = new File(PropertyReader.getProperty("Screenshotpath"));
		String filepath = fis.getAbsolutePath().toString();
		String Screenshot1 = filepath + "\\StartpointAnnotationNew.png";
		String Screenshot2 = filepath + "\\EndpointRect.png";
		Screen s = new Screen();

		Pattern p1 = new Pattern(Screenshot1);

		Pattern p2 = new Pattern(Screenshot2);

		s.click(p1);

		// s.click(p1);
		// s.drag(p1);

		// s.dropAt(p2);
		Log.message("image is drawn");

		SkySiteUtils.waitTill(4000);
		textarea.sendKeys("Text Call out drawing");
		Log.message("text has been entered");
		Select sc = new Select(driver.findElement(By.xpath(".//*[@id='selectCalloutFontSize']")));
		sc.selectByValue("20");
		Log.message("20 font size is selected");
		oksavebutton.click();

		Log.message("Ok button is clicked");
		SkySiteUtils.waitForElement(driver, savebtn, 50);
		savebtn.click();
		Log.message("save button is clicked");
		SkySiteUtils.waitTill(4000);
		markupfield.sendKeys(markupname);
		SkySiteUtils.waitForElement(driver, savemarkupbtn, 40);
		savemarkupbtn.click();
		Log.message("savemarkupbtn is clicked");
		SkySiteUtils.waitTill(14000);
		driver.findElement(By.xpath(".//*[@id='imageViewer']/div[3]/div[4]/div/ul/li[2]/a")).click();
		Log.message("save button is clicked again");
		SkySiteUtils.waitTill(4000);
		// File fis1=new File(PropertyReader.getProperty("SaveScreenshot"));
		// String filepath1=fis1.getAbsolutePath().toString();
		String savedscreenshot1 = filepath1 + "\\BeforesavingmarkupTextCallout.png";
		String savedscreenshot2 = filepath1 + "\\AfterSavingMarkerTextCallout.png";
		File srcFiler = ((TakesScreenshot) driver).getScreenshotAs(OutputType.FILE);

		FileUtils.copyFile(srcFiler, new File(savedscreenshot1));

		Log.message("Screenshot taken of saved markup");
		SkySiteUtils.waitTill(4000);
		markupdropdown.click();
		Log.message("markup dropdown clicked");
		SkySiteUtils.waitTill(4000);
		for (WebElement markupitem : markuplist)
			if (markupitem.getText().contains(markupname))
				driver.findElement(By.xpath("//*[@data-original-title='" + markupname + "']")).click();

		Log.message("save markup clicked and image drawn disappears");
		SkySiteUtils.waitTill(4000);
		SkySiteUtils.waitTill(4000);
		for (WebElement markupitem : markuplist)
			if (markupitem.getText().contains(markupname))
				driver.findElement(By.xpath("//*[@data-original-title='" + markupname + "']")).click();

		// showallmarkup.click();
		Log.message("Markup is displayed");
		SkySiteUtils.waitTill(4000);
		savebtn.click();
		Log.message("save button is clicked");
		SkySiteUtils.waitTill(4000);
		File srcFiler1 = ((TakesScreenshot) driver).getScreenshotAs(OutputType.FILE);

		FileUtils.copyFile(srcFiler1, new File(savedscreenshot2));
		Log.message("Screenshot taken after redrawing");

		BufferedImage imgA = ImageIO.read(new File(savedscreenshot1));

		// Thread.sleep(2000);

		BufferedImage imgB = ImageIO.read(new File(savedscreenshot2));

		// Thread.sleep(2000);

		Log.message("Checking the height and width of both the images are same?");

		if (imgA.getWidth() != imgB.getWidth() && imgA.getHeight() != imgB.getHeight()) {
			Log.message("image height width are different");
			return false;
		}

		int width = imgA.getWidth();
		int height = imgA.getHeight();
		for (int y = 0; y < height; y++) {
			for (int x = 0; x < width; x++) {

				if (imgA.getRGB(x, y) != imgB.getRGB(x, y)) {

					Log.message("Image are different");
					return false;
				}
			}

		}

		return true;
	}

	@FindBy(css = ".icon.icon-lg.icon-note")
	WebElement textnoteicon;
	@FindBy(css = "#editNoteText")
	WebElement textnotearea;

	/**
	 * Method to draw text note annotation
	 * 
	 * @throws FindFailed
	 * @throws IOException
	 * @throws InterruptedException
	 * 
	 */
	public boolean drawTextNoteAnnotation(String markupname, String filepath1)
			throws FindFailed, IOException, InterruptedException {
		boolean flag = false;
		Log.message("Switched to default content");
		SkySiteUtils.waitTill(5000);

		SkySiteUtils.waitTill(4000);
		String MainWindow = driver.getWindowHandle();
		Log.message(MainWindow);
		for (String childwindow : driver.getWindowHandles()) {
			driver.switchTo().window(childwindow);

		}
		Log.message("switched to child window");

		driver.switchTo().defaultContent();
		SkySiteUtils.waitTill(4000);
		// method to delete existing markup
		this.markupdeletebegin();
		SkySiteUtils.waitForElement(driver, tootextbtn, 50);
		tootextbtn.click();
		Log.message("text tool is clicked");
		Actions act = new Actions(driver);
		act.moveToElement(textnoteicon).clickAndHold().build().perform();
		SkySiteUtils.waitTill(2000);
		act.click(textnoteicon).build().perform();
		Log.message("draw text note tool button is clicked");
		SkySiteUtils.waitTill(2000);
		File fis = new File(PropertyReader.getProperty("Screenshotpath"));
		String filepath = fis.getAbsolutePath().toString();
		String Screenshot1 = filepath + "\\StartpointRect.png";
		String Screenshot2 = filepath + "\\EndpointRect.png";
		String Screenshot3 = filepath + "\\NoteDefaultImage.png";
		Screen s = new Screen();

		Pattern p1 = new Pattern(Screenshot1);

		Pattern p2 = new Pattern(Screenshot2);

		Pattern p3 = new Pattern(Screenshot3);

		s.click(p1);

		// s.click(p1);
		// s.drag(p1);

		// s.dropAt(p2);
		Log.message("image is drawn");
		String note = "My Note";
		SkySiteUtils.waitTill(4000);
		textnotearea.sendKeys(note);
		Log.message("Note has been entered");
		SkySiteUtils.waitForElement(driver, oksavebutton, 40);
		oksavebutton.click();

		Log.message("Ok button is clicked");
		SkySiteUtils.waitForElement(driver, savebtn, 50);
		savebtn.click();
		Log.message("save button is clicked");
		SkySiteUtils.waitTill(4000);

		markupfield.sendKeys(markupname);
		Log.message("Enter markup name is " + markupname);
		SkySiteUtils.waitForElement(driver, savemarkupbtn, 40);
		savemarkupbtn.click();
		Log.message("savemarkupbtn is clicked");
		SkySiteUtils.waitTill(14000);
		s.click(p3);
		Log.message("Saved note is clikced");
		SkySiteUtils.waitTill(4000);
		String text = textnotearea.getText();
		Log.message("Note present is :" + text);
		savebtn.click();
		Log.message("outside of note is clicked on save button");
		if (note.equals(text)) {
			Log.message("Added note is saved");
			return true;
		} else {
			Log.message("Added note not saved");
			return false;
		}

	}

	@FindBy(xpath = "//*[@data-original-title='Draw rectangle highlighter']")
	WebElement hightlightericon;

	/**
	 * Method to draw highlighter annotation
	 * 
	 * @throws IOException
	 * @throws FindFailed
	 * 
	 */
	public boolean drawHighLighterAnnotation(String markupname, String filepath1) throws IOException, FindFailed {
		boolean flag = false;
		Log.message("Switched to default content");
		SkySiteUtils.waitTill(5000);
		driver.switchTo().frame(driver.findElement(By.id("myFrame")));
		Log.message("Switched to frame");
		driver.findElement(By.xpath(
				".//*[@id='treeboxbox_tree_wrap_container']/div[2]/table/tbody/tr[2]/td[2]/table/tbody/tr[3]/td[2]/table/tbody/tr/td[4]/span"))
				.click();
		Log.message("Folder selected");
		SkySiteUtils.waitTill(4000);
		driver.findElement(By.xpath(".//*[@id='divGrid']/div[2]/table/tbody/tr[2]/td[2]/a[3]")).click();
		Log.message("File is selected");
		SkySiteUtils.waitTill(4000);
		String MainWindow = driver.getWindowHandle();
		Log.message(MainWindow);
		for (String childwindow : driver.getWindowHandles()) {
			driver.switchTo().window(childwindow);

		}
		Log.message("switched to child window");

		driver.switchTo().defaultContent();
		SkySiteUtils.waitTill(4000);
		// method to delete existing markup
		this.markupdeletebegin();
		SkySiteUtils.waitTill(4000);

		hightlightericon.click();
		Log.message("draw highlighter button is clicked");
		SkySiteUtils.waitTill(2000);

		File fis = new File(PropertyReader.getProperty("Screenshotpath"));
		String filepath = fis.getAbsolutePath().toString();
		String Screenshot1 = filepath + "\\StartpointRect.png";
		String Screenshot2 = filepath + "\\EndpointRect.png";
		Screen s = new Screen();

		Pattern p1 = new Pattern(Screenshot1);

		Pattern p2 = new Pattern(Screenshot2);

		s.click(p1);

		s.drag(p1);

		s.dropAt(p2);
		Log.message("image is drawn");
		SkySiteUtils.waitTill(4000);
		savebtn.click();
		Log.message("save button is clicked");
		SkySiteUtils.waitTill(4000);
		markupfield.sendKeys(markupname);
		Log.message("Enter markup name is " + markupname);
		SkySiteUtils.waitForElement(driver, savemarkupbtn, 40);
		savemarkupbtn.click();
		Log.message("savemarkupbtn is clicked");
		SkySiteUtils.waitTill(14000);
		driver.findElement(By.xpath(".//*[@id='imageViewer']/div[3]/div[4]/div/ul/li[2]/a")).click();
		Log.message("save button is clicked again");
		SkySiteUtils.waitTill(4000);
		String savedscreenshot1 = filepath1 + "\\Beforesavingmarkuphighlighter.png";
		String savedscreenshot2 = filepath1 + "\\AfterSavingMarkerHightlighter.png";
		// File fistemp=new File(PropertyReader.getProperty("tempScreenshot"));
		// String filepathtemp=fistemp.getAbsolutePath().toString();
		// String tempscreenshotpath=filepathtemp + "\\temp_Screenshot.png";
		File srcFiler = ((TakesScreenshot) driver).getScreenshotAs(OutputType.FILE);

		FileUtils.copyFile(srcFiler, new File(savedscreenshot1));

		Log.message("Screenshot taken of saved markup");
		SkySiteUtils.waitTill(4000);
		markupdropdown.click();
		Log.message("markup dropdown clicked");
		SkySiteUtils.waitTill(4000);
		for (WebElement markupitem : markuplist)
			if (markupitem.getText().contains(markupname))
				driver.findElement(By.xpath("//*[@data-original-title='" + markupname + "']")).click();

		Log.message("save markup clicked and image drawn disappears");
		SkySiteUtils.waitTill(4000);
		SkySiteUtils.waitTill(4000);
		for (WebElement markupitem : markuplist)
			if (markupitem.getText().contains(markupname))
				driver.findElement(By.xpath("//*[@data-original-title='" + markupname + "']")).click();

		// showallmarkup.click();
		Log.message("Markup is displayed");
		SkySiteUtils.waitTill(4000);
		savebtn.click();
		Log.message("save button is clicked");
		SkySiteUtils.waitTill(4000);
		File srcFiler1 = ((TakesScreenshot) driver).getScreenshotAs(OutputType.FILE);

		FileUtils.copyFile(srcFiler1, new File(savedscreenshot2));
		Log.message("Screenshot taken after redrawing");

		BufferedImage imgA = ImageIO.read(new File(savedscreenshot1));

		// Thread.sleep(2000);

		BufferedImage imgB = ImageIO.read(new File(savedscreenshot2));

		// Thread.sleep(2000);

		Log.message("Checking the height and width of both the images are same?");

		if (imgA.getWidth() != imgB.getWidth() && imgA.getHeight() != imgB.getHeight()) {
			Log.message("image height width are different");
			return false;
		}

		int width = imgA.getWidth();
		int height = imgA.getHeight();
		for (int y = 0; y < height; y++) {
			for (int x = 0; x < width; x++) {

				if (imgA.getRGB(x, y) != imgB.getRGB(x, y)) {

					Log.message("Image are different");
					return false;
				}
			}

		}

		return true;
	}

	/**
	 * Method to delete screenshot
	 * 
	 * @param Screenshotpath
	 * @return
	 */
	public void delteScreenShot(String Screenshotpath) {
		Log.message("Searching screenshot downloaded");
		File file = new File(Screenshotpath);
		File[] Filearray = file.listFiles();
		if (file.isDirectory()) {
			for (File Myfile : Filearray) {
				if (Myfile.getName().contains(".png")) {
					Myfile.delete();
					Log.message("File is deleted successfully");
				}
			}
		}

	}

	@FindBy(xpath = ".//*[@id='btnLoadMarkupHL']/a")
	WebElement hyperlinkdropdown;
	@FindBy(xpath = "//*[@class='btn btn-primary btn-sm done saveLayerL']")
	WebElement okbutton;
	@FindBy(xpath = "//*[@class='btn btn-primary btn-sm saveLayerF']")
	WebElement okbuttonexternalhyperlink;
	@FindBy(xpath = ".//*[@id='linkRectCreate']")
	WebElement recthyperlink;
	@FindBy(xpath = ".//*[@id='linkToolMenuBtn']")
	WebElement hyperlinktool;

	/**
	 * Method to draw and save square hyperlink
	 * 
	 * @throws IOException
	 * @throws FindFailed
	 * 
	 */
	public boolean drawAndSaveSquareHyperLink(String markupname, String filepath1) throws IOException, FindFailed {
		boolean flag = false;
		Log.message("Switched to default content");
		SkySiteUtils.waitTill(5000);
		driver.switchTo().frame(driver.findElement(By.id("myFrame")));
		Log.message("Switched to frame");
		driver.findElement(By.xpath("//*[text()='SameFolderDiffFile']")).click();
		Log.message("Folder selected");
		SkySiteUtils.waitTill(4000);
		driver.findElement(By.xpath(".//*[@id='divGrid']/div[2]/table/tbody/tr[2]/td[2]/a[3]")).click();
		Log.message("File is selected");
		SkySiteUtils.waitTill(4000);
		String MainWindow = driver.getWindowHandle();
		Log.message(MainWindow);
		for (String childwindow : driver.getWindowHandles()) {
			driver.switchTo().window(childwindow);

		}
		Log.message("switched to child window");

		driver.switchTo().defaultContent();
		SkySiteUtils.waitTill(4000);
		// method to delete existing markup
		this.markupdeletebegin();
		SkySiteUtils.waitTill(4000);
		hyperlinktool.click();
		Log.message("Hyperlink tool is clicked");
		SkySiteUtils.waitForElement(driver, recthyperlink, 40);
		Actions act = new Actions(driver);
		act.moveToElement(recthyperlink).clickAndHold().build().perform();
		SkySiteUtils.waitTill(2000);
		act.click(recthyperlink).build().perform();
		Log.message("Rect Hyper Link button is clicked");
		SkySiteUtils.waitTill(2000);
		File fis = new File(PropertyReader.getProperty("Screenshotpath"));
		String filepath = fis.getAbsolutePath().toString();
		String Screenshot1 = filepath + "\\StartPointFolderLevel.png";
		String Screenshot2 = filepath + "\\EndpointFolderLevel.png";
		Screen s = new Screen();

		Pattern p1 = new Pattern(Screenshot1);

		Pattern p2 = new Pattern(Screenshot2);

		// s.click(p1);

		s.drag(p1);

		s.dropAt(p2);
		Log.message("image is drawn");
		SkySiteUtils.waitTill(4000);
		// driver.findElement(By.xpath(".//*[@id='hyperLinkFolderList']/ul/li[2]/div/a/div[3]")).click();
		driver.findElement(By.xpath(".//*[@id='hyperLinkFolderList']/ul/li[13]/div/a/div[3]")).click();
		Log.message("Folder is selected");
		SkySiteUtils.waitTill(4000);
		// driver.findElement(By.xpath(".//*[@id='hyperLinkFolderList']/ul/li[2]/div/div[2]/h4/a")).click();
		driver.findElement(By.xpath(".//*[@id='hyperLinkFolderList']/ul/li[1]/div/div[2]/h4/a")).click();
		Log.message("File is selected");
		SkySiteUtils.waitForElement(driver, okbutton, 50);
		okbutton.click();
		Log.message("Ok button is clicked");

		SkySiteUtils.waitTill(4000);
		driver.findElement(By.xpath("//*[@class='leaflet-control-nav-home']")).click();
		Log.message("Home page is clicked");
		SkySiteUtils.waitForElement(driver, savebtn, 50);
		savebtn.click();
		Log.message("save button is clicked");
		SkySiteUtils.waitTill(4000);
		markupfield.sendKeys(markupname);
		Log.message("Enter markup name is " + markupname);
		SkySiteUtils.waitForElement(driver, savemarkupbtn, 40);
		savemarkupbtn.click();
		Log.message("savemarkupbtn is clicked");
		SkySiteUtils.waitTill(14000);
		driver.findElement(By.xpath(".//*[@id='imageViewer']/div[3]/div[4]/div/ul/li[2]/a")).click();
		Log.message("save button is clicked again");
		SkySiteUtils.waitTill(4000);
		String savedscreenshot1 = filepath1 + "\\BeforesavingSquareHyperLink.png";
		String savedscreenshot2 = filepath1 + "\\AfterSavingSquareHyperLink.png";
		// File fistemp=new File(PropertyReader.getProperty("tempScreenshot"));
		// String filepathtemp=fistemp.getAbsolutePath().toString();
		// String tempscreenshotpath=filepathtemp + "\\temp_Screenshot.png";
		File srcFiler = ((TakesScreenshot) driver).getScreenshotAs(OutputType.FILE);

		FileUtils.copyFile(srcFiler, new File(savedscreenshot1));

		Log.message("Screenshot taken of saved markup");
		SkySiteUtils.waitTill(4000);
		hyperlinkdropdown.click();
		Log.message("hyperlink markup dropdown clicked");
		SkySiteUtils.waitTill(4000);
		// for(WebElement markupitem:hperlinklist)
		// if(markupitem.getText().contains(markupname))
		// driver.findElement(By.xpath("//*[text()='"+markupname+"']")).click();
		driver.findElement(By.xpath(".//*[@id='btnHideAllHyperlinks']")).click();
		Log.message("hide all button clicked and image disappears");

		SkySiteUtils.waitTill(4000);
		showhyperlinks.click();
		Log.message("show all hyperlink clikced");
		SkySiteUtils.waitTill(4000);
		savebtn.click();
		Log.message("save button is clicked");
		SkySiteUtils.waitTill(4000);
		File srcFiler1 = ((TakesScreenshot) driver).getScreenshotAs(OutputType.FILE);

		FileUtils.copyFile(srcFiler1, new File(savedscreenshot2));
		Log.message("Screenshot taken after redrawing");

		BufferedImage imgA = ImageIO.read(new File(savedscreenshot1));

		// Thread.sleep(2000);

		BufferedImage imgB = ImageIO.read(new File(savedscreenshot2));

		// Thread.sleep(2000);

		Log.message("Checking the height and width of both the images are same?");

		if (imgA.getWidth() != imgB.getWidth() && imgA.getHeight() != imgB.getHeight()) {
			Log.message("image height width are different");
			return false;
		}

		int width = imgA.getWidth();
		int height = imgA.getHeight();
		for (int y = 0; y < height; y++) {
			for (int x = 0; x < width; x++) {

				if (imgA.getRGB(x, y) != imgB.getRGB(x, y)) {

					Log.message("Image are different");
					return false;
				}
			}

		}

		return true;
	}

	@FindBy(xpath = ".//*[@id='btnCopyLink']")
	WebElement copylink;

	/**
	 * Method to verify working of square hyperlink for same folder diff file
	 * 
	 * @throws FindFailed
	 * @throws IOException
	 * @throws UnsupportedFlavorException
	 * 
	 */
	public boolean VerifyfunctionalitySquareHyperLinkSameFolderDiffFile(String markupname, String filepath1)
			throws FindFailed, UnsupportedFlavorException, IOException {
		// driver.switchTo().defaultContent();
		SkySiteUtils.waitTill(9000);
		File fistemp = new File(PropertyReader.getProperty("tempScreenshot"));
		String filepathtemp = fistemp.getAbsolutePath().toString();
		String tempscreenshotpath = filepathtemp + "\\tempfolderlevel.png";
		Screen s = new Screen();

		Pattern p1 = new Pattern(tempscreenshotpath);

		SkySiteUtils.waitTill(10000);
		s.click(p1);
		Log.message("saved hyperlink is clicked");
		SkySiteUtils.waitTill(4000);
		// JavascriptExecutor executor = (JavascriptExecutor)driver;
		// executor.executeScript("arguments[0].click();", copylink);
		// Log.message("Copy to clipboard clicked");
		/*
		 * Toolkit toolkit = Toolkit.getDefaultToolkit(); Clipboard clipboard =
		 * toolkit.getSystemClipboard(); String result = (String)
		 * clipboard.getData(DataFlavor.stringFlavor);
		 * System.out.println("String from Clipboard:" + result);
		 */
		WebElement imagelink = driver.findElement(By.id("linkImgDiv"));
		JavascriptExecutor executor = (JavascriptExecutor) driver;
		executor.executeScript("arguments[0].click();", imagelink);
		Log.message("image is clicked");

		String filename = driver.findElement(By.xpath("//*[@data-original-title='1_EName.pdf']")).getText();
		Log.message("File name is " + filename);
		String expectedfilename = "(R-1) 1_EName.pdf";
		if (filename.equals(expectedfilename))
			return true;

		else
			return false;

	}

	/**
	 * Method to click default file
	 * 
	 */
	public void clickDefaultFile() {
		SkySiteUtils.waitTill(4000);
		WebElement documenttab = driver.findElement(By.xpath("//*[@data-original-title='1_Document.pdf']"));
		JavascriptExecutor executor = (JavascriptExecutor) driver;
		executor.executeScript("arguments[0].click();", documenttab);
		Log.message("Original file is clicked");
		SkySiteUtils.waitTill(5000);

	}
	/**
	 * Method to click default file
	 * 
	 */
	public void clickDefaultFileForHyperLink() {
		SkySiteUtils.waitTill(4000);
		WebElement documenttab = driver.findElement(By.xpath("//*[@data-original-title='1 mb.pdf']"));
		JavascriptExecutor executor = (JavascriptExecutor) driver;
		executor.executeScript("arguments[0].click();", documenttab);
		Log.message("Original file is clicked");
		SkySiteUtils.waitTill(5000);

	}

	/**
	 * Method to draw and save square hyperlink for diff folder and diff file
	 * 
	 * @throws IOException
	 * @throws FindFailed
	 * 
	 */
	public boolean drawAndSaveSquareHyperLinkForDiffFolderDifffile(String markupname, String filepath1)
			throws IOException, FindFailed {
		boolean flag = false;
		Log.message("Switched to default content");
		SkySiteUtils.waitTill(5000);
		driver.switchTo().frame(driver.findElement(By.id("myFrame")));
		Log.message("Switched to frame");
		driver.findElement(By.xpath(".//*[text()='FolderViewerTesting_DONotDelete']")).click();
		Log.message("Folder selected");
		SkySiteUtils.waitTill(4000);
		driver.findElement(By.xpath(".//*[@id='divGrid']/div[2]/table/tbody/tr[2]/td[2]/a[3]")).click();
		Log.message("File is selected");
		SkySiteUtils.waitTill(4000);
		String MainWindow = driver.getWindowHandle();
		Log.message(MainWindow);
		for (String childwindow : driver.getWindowHandles()) {
			driver.switchTo().window(childwindow);

		}
		Log.message("switched to child window");

		driver.switchTo().defaultContent();
		SkySiteUtils.waitTill(4000);
		// method to delete existing markup
		this.markupdeletebegin();
		SkySiteUtils.waitTill(4000);
		hyperlinktool.click();
		Log.message("Hyperlink tool is clicked");
		SkySiteUtils.waitForElement(driver, recthyperlink, 40);
		Actions act = new Actions(driver);
		act.moveToElement(recthyperlink).clickAndHold().build().perform();
		SkySiteUtils.waitTill(2000);
		act.click(recthyperlink).build().perform();
		Log.message("Rect Hyper Link button is clicked");
		SkySiteUtils.waitTill(2000);
		File fis = new File(PropertyReader.getProperty("Screenshotpath"));
		String filepath = fis.getAbsolutePath().toString();
		String Screenshot1 = filepath + "\\StartPointFolderLevel.png";
		String Screenshot2 = filepath + "\\EndpointFolderLevel.png";
		Screen s = new Screen();

		Pattern p1 = new Pattern(Screenshot1);

		Pattern p2 = new Pattern(Screenshot2);

		// s.click(p1);

		s.drag(p1);

		s.dropAt(p2);
		Log.message("image is drawn");
		SkySiteUtils.waitTill(4000);
		// driver.findElement(By.xpath(".//*[@id='hyperLinkFolderList']/ul/li[2]/div/a/div[3]")).click();
		driver.findElement(By.xpath(".//*[@id='hyperLinkFolderList']/ul/li[1]/div/a/div[3]")).click();
		Log.message("Folder is selected");
		SkySiteUtils.waitTill(4000);
		// driver.findElement(By.xpath(".//*[@id='hyperLinkFolderList']/ul/li[2]/div/div[2]/h4/a")).click();
		driver.findElement(By.xpath(".//*[@id='hyperLinkFolderList']/ul/li[1]/div/div[2]/h4/a")).click();
		Log.message("File is selected");
		SkySiteUtils.waitTill(3000);
		//driver.findElement(By.xpath(".//*[@id='dvHLPage_2']/div/ul/li[1]/a")).click();
	//	Log.message("Page1 is selected");
		SkySiteUtils.waitForElement(driver, okbutton, 50);
		okbutton.click();
		Log.message("Ok button is clicked");

		SkySiteUtils.waitTill(4000);
		driver.findElement(By.xpath("//*[@class='leaflet-control-nav-home']")).click();
		Log.message("Home page is clicked");
		SkySiteUtils.waitForElement(driver, savebtn, 50);
		savebtn.click();
		Log.message("save button is clicked");
		SkySiteUtils.waitTill(4000);
		markupfield.sendKeys(markupname);
		Log.message("Enter markup name is " + markupname);
		SkySiteUtils.waitForElement(driver, savemarkupbtn, 40);
		savemarkupbtn.click();
		Log.message("savemarkupbtn is clicked");
		SkySiteUtils.waitTill(14000);
		driver.findElement(By.xpath(".//*[@id='imageViewer']/div[3]/div[4]/div/ul/li[2]/a")).click();
		Log.message("save button is clicked again");
		SkySiteUtils.waitTill(4000);

		String savedscreenshot1 = filepath1 + "\\BeforesavingSquareHyperLink.png";
		String savedscreenshot2 = filepath1 + "\\AfterSavingSquareHyperLink.png";
		// File fistemp=new File(PropertyReader.getProperty("tempScreenshot"));
		// String filepathtemp=fistemp.getAbsolutePath().toString();
		// String tempscreenshotpath=filepathtemp + "\\temp_Screenshot.png";
		File srcFiler = ((TakesScreenshot) driver).getScreenshotAs(OutputType.FILE);

		FileUtils.copyFile(srcFiler, new File(savedscreenshot1));

		Log.message("Screenshot taken of saved markup");
		SkySiteUtils.waitTill(4000);
		hyperlinkdropdown.click();
		Log.message("hyperlink markup dropdown clicked");
		SkySiteUtils.waitTill(4000);
		// for(WebElement markupitem:hperlinklist)
		// if(markupitem.getText().contains(markupname))
		// driver.findElement(By.xpath("//*[text()='"+markupname+"']")).click();
		driver.findElement(By.xpath(".//*[@id='btnHideAllHyperlinks']")).click();
		Log.message("hide all button clicked and image disappears");

		SkySiteUtils.waitTill(4000);
		showhyperlinks.click();
		Log.message("show all hyperlink clikced");
		SkySiteUtils.waitTill(4000);
		savebtn.click();
		Log.message("save button is clicked");
		SkySiteUtils.waitTill(4000);
		File srcFiler1 = ((TakesScreenshot) driver).getScreenshotAs(OutputType.FILE);

		FileUtils.copyFile(srcFiler1, new File(savedscreenshot2));
		Log.message("Screenshot taken after redrawing");

		BufferedImage imgA = ImageIO.read(new File(savedscreenshot1));

		// Thread.sleep(2000);

		BufferedImage imgB = ImageIO.read(new File(savedscreenshot2));

		// Thread.sleep(2000);

		Log.message("Checking the height and width of both the images are same?");

		if (imgA.getWidth() != imgB.getWidth() && imgA.getHeight() != imgB.getHeight()) {
			Log.message("image height width are different");
			return false;
		}

		int width = imgA.getWidth();
		int height = imgA.getHeight();
		for (int y = 0; y < height; y++) {
			for (int x = 0; x < width; x++) {

				if (imgA.getRGB(x, y) != imgB.getRGB(x, y)) {

					Log.message("Image are different");
					return false;
				}
			}

		}

		return true;
	}

	/**
	 * Method to verify working of square hyperlink for diff folder diff file
	 * 
	 * @throws FindFailed
	 * @throws IOException
	 * @throws UnsupportedFlavorException
	 * 
	 */

	public boolean VerifyfunctionalitySquareHyperLinkdiffFolderDiffFile(String markupname, String filepath1)
			throws FindFailed, UnsupportedFlavorException, IOException {
		// driver.switchTo().defaultContent();
		SkySiteUtils.waitTill(8000);
		File fistemp = new File(PropertyReader.getProperty("tempScreenshot"));
		String filepathtemp = fistemp.getAbsolutePath().toString();
		String tempscreenshotpath = filepathtemp + "\\tempfolderlevel.png";
		Screen s = new Screen();

		Pattern p1 = new Pattern(tempscreenshotpath);

		SkySiteUtils.waitTill(4000);
		s.click(p1);
		Log.message("saved hyperlink is clicked");
		SkySiteUtils.waitTill(4000);
		// JavascriptExecutor executor = (JavascriptExecutor)driver;
		// executor.executeScript("arguments[0].click();", copylink);
		// Log.message("Copy to clipboard clicked");
		/*
		 * Toolkit toolkit = Toolkit.getDefaultToolkit(); Clipboard clipboard =
		 * toolkit.getSystemClipboard(); String result = (String)
		 * clipboard.getData(DataFlavor.stringFlavor);
		 * System.out.println("String from Clipboard:" + result);
		 */
		WebElement imagelink = driver.findElement(By.id("linkImgDiv"));
		JavascriptExecutor executor = (JavascriptExecutor) driver;
		executor.executeScript("arguments[0].click();", imagelink);
		Log.message("image is clicked");
		SkySiteUtils.waitTill(5000);
		String filename = driver.findElement(By.xpath("//*[@data-original-title='1_EName.pdf']")).getText();
		Log.message("File name is " + filename);
		String expectedfilename = "(R-1) 1_EName.pdf";
		if (filename.equals(expectedfilename))
			return true;

		else
			return false;

	}

	/**
	 * Method to draw and save square hyperlink for diff folder and same file
	 * 
	 * @throws IOException
	 * @throws FindFailed
	 * 
	 */
	public boolean drawAndSaveSquareHyperLinkForDiffFolderSamefileDiffVersion(String markupname, String filepath1)
			throws IOException, FindFailed {
		boolean flag = false;
		Log.message("Switched to default content");
		SkySiteUtils.waitTill(5000);
		driver.switchTo().frame(driver.findElement(By.id("myFrame")));
		Log.message("Switched to frame");
		driver.findElement(By.xpath(".//*[text()='ViewerTesting2']")).click();
		Log.message("Folder selected");
		SkySiteUtils.waitTill(4000);
		
		driver.findElement(By.xpath(".//*[@id='divGrid']/div[2]/table/tbody/tr[2]/td[2]/a[3]")).click();
		Log.message("File is selected");
		SkySiteUtils.waitTill(4000);
		String MainWindow = driver.getWindowHandle();
		Log.message(MainWindow);
		for (String childwindow : driver.getWindowHandles()) {
			driver.switchTo().window(childwindow);

		}
		Log.message("switched to child window");

		driver.switchTo().defaultContent();
		SkySiteUtils.waitTill(4000);
		// method to delete existing markup
		this.markupdeletebegin();
		SkySiteUtils.waitTill(4000);
		hyperlinktool.click();
		Log.message("Hyperlink tool is clicked");
		SkySiteUtils.waitForElement(driver, recthyperlink, 40);
		Actions act = new Actions(driver);
		act.moveToElement(recthyperlink).clickAndHold().build().perform();
		SkySiteUtils.waitTill(2000);
		act.click(recthyperlink).build().perform();
		Log.message("Rect Hyper Link button is clicked");
		SkySiteUtils.waitTill(2000);
		File fis = new File(PropertyReader.getProperty("Screenshotpath"));
		String filepath = fis.getAbsolutePath().toString();
		String Screenshot1 = filepath + "\\StartPointFolderLevel.png";
		String Screenshot2 = filepath + "\\EndpointFolderLevel.png";
		Screen s = new Screen();

		Pattern p1 = new Pattern(Screenshot1);

		Pattern p2 = new Pattern(Screenshot2);

		// s.click(p1);

		s.drag(p1);

		s.dropAt(p2);
		Log.message("image is drawn");
		SkySiteUtils.waitTill(4000);
		// driver.findElement(By.xpath(".//*[@id='hyperLinkFolderList']/ul/li[2]/div/a/div[3]")).click();
		driver.findElement(By.xpath(".//*[@id='hyperLinkFolderList']/ul/li[11]/div/a/div[3]")).click();
		Log.message("Folder is selected");
		SkySiteUtils.waitTill(3000);
		// driver.findElement(By.xpath(".//*[@id='hyperLinkFolderList']/ul/li[2]/div/div[2]/h4/a")).click();
		driver.findElement(By.xpath(".//*[@id='hyperLinkFolderList']/ul/li/div/div[2]/h4/a")).click();
		Log.message("File is selected");
		// driver.findElement(By.xpath(".//*[@id='dvHLPage_1']/div/ul/li[1]/a")).click();
		// Log.message("Page1 is selected");
		SkySiteUtils.waitForElement(driver, okbutton, 50);
		okbutton.click();
		Log.message("Ok button is clicked");

		SkySiteUtils.waitTill(4000);
		driver.findElement(By.xpath("//*[@class='leaflet-control-nav-home']")).click();
		Log.message("Home page is clicked");
		SkySiteUtils.waitForElement(driver, savebtn, 50);
		savebtn.click();
		Log.message("save button is clicked");
		SkySiteUtils.waitTill(4000);
		markupfield.sendKeys(markupname);
		Log.message("Enter markup name is " + markupname);
		SkySiteUtils.waitForElement(driver, savemarkupbtn, 40);
		savemarkupbtn.click();
		Log.message("savemarkupbtn is clicked");
		SkySiteUtils.waitTill(14000);
		driver.findElement(By.xpath(".//*[@id='imageViewer']/div[3]/div[4]/div/ul/li[2]/a")).click();
		Log.message("save button is clicked again");
		SkySiteUtils.waitTill(4000);
		String savedscreenshot1 = filepath1 + "\\BeforesavingSquareHyperLink.png";
		String savedscreenshot2 = filepath1 + "\\AfterSavingSquareHyperLink.png";
		// File fistemp=new File(PropertyReader.getProperty("tempScreenshot"));
		// String filepathtemp=fistemp.getAbsolutePath().toString();
		// String tempscreenshotpath=filepathtemp + "\\temp_Screenshot.png";
		File srcFiler = ((TakesScreenshot) driver).getScreenshotAs(OutputType.FILE);

		FileUtils.copyFile(srcFiler, new File(savedscreenshot1));

		Log.message("Screenshot taken of saved markup");
		SkySiteUtils.waitTill(4000);
		hyperlinkdropdown.click();
		Log.message("hyperlink markup dropdown clicked");
		SkySiteUtils.waitTill(4000);
		// for(WebElement markupitem:hperlinklist)
		// if(markupitem.getText().contains(markupname))
		// driver.findElement(By.xpath("//*[text()='"+markupname+"']")).click();
		driver.findElement(By.xpath(".//*[@id='btnHideAllHyperlinks']")).click();
		Log.message("hide all button clicked and image disappears");

		SkySiteUtils.waitTill(4000);
		showhyperlinks.click();
		Log.message("show all hyperlink clikced");
		SkySiteUtils.waitTill(4000);
		savebtn.click();
		Log.message("save button is clicked");
		SkySiteUtils.waitTill(4000);
		File srcFiler1 = ((TakesScreenshot) driver).getScreenshotAs(OutputType.FILE);

		FileUtils.copyFile(srcFiler1, new File(savedscreenshot2));
		Log.message("Screenshot taken after redrawing");

		BufferedImage imgA = ImageIO.read(new File(savedscreenshot1));

		// Thread.sleep(2000);

		BufferedImage imgB = ImageIO.read(new File(savedscreenshot2));

		// Thread.sleep(2000);

		Log.message("Checking the height and width of both the images are same?");

		if (imgA.getWidth() != imgB.getWidth() && imgA.getHeight() != imgB.getHeight()) {
			Log.message("image height width are different");
			return false;
		}

		int width = imgA.getWidth();
		int height = imgA.getHeight();
		for (int y = 0; y < height; y++) {
			for (int x = 0; x < width; x++) {

				if (imgA.getRGB(x, y) != imgB.getRGB(x, y)) {

					Log.message("Image are different");
					return false;
				}
			}

		}

		return true;
	}

	/**
	 * Method to verify functionality for square hyperlink with diff folder and same
	 * file and different version
	 * 
	 * @param markupname
	 * @param filepath1
	 * @return
	 * @throws FindFailed
	 * @throws UnsupportedFlavorException
	 * @throws IOException
	 */
	public boolean VerifyfunctionalitySquareHyperLinkdiffFolderSameFile(String markupname, String filepath1)
			throws FindFailed, UnsupportedFlavorException, IOException {
		// driver.switchTo().defaultContent();
		SkySiteUtils.waitTill(8000);
		File fistemp = new File(PropertyReader.getProperty("tempScreenshot"));
		String filepathtemp = fistemp.getAbsolutePath().toString();
		String tempscreenshotpath = filepathtemp + "\\tempfolderlevel.png";
		Screen s = new Screen();

		Pattern p1 = new Pattern(tempscreenshotpath);
		SkySiteUtils.waitTill(8000);
		s.click(p1);
		Log.message("saved hyperlink is clicked");
		SkySiteUtils.waitTill(4000);
		WebElement imagelink = driver.findElement(By.id("linkImgDiv"));
		JavascriptExecutor executor = (JavascriptExecutor) driver;
		executor.executeScript("arguments[0].click();", imagelink);
		Log.message("image is clicked");
		SkySiteUtils.waitTill(4000);
		// String
		// filename=driver.findElement(By.xpath("//*[@data-original-title='1_Document.pdf']")).getText();
		String filename = driver.findElement(By.xpath("//*[text()='(R-2) 1_Document.pdf']")).getText();
		Log.message("File name is " + filename);
		String expectedfilename = "(R-2) 1_Document.pdf";
		if (filename.equals(expectedfilename))
			return true;

		else
			return false;

	}

	@FindBy(xpath = ".//*[@id='linkEllipseCreate']/a/i")
	WebElement circlehyperlinktool;

	/**
	 * Method to draw circle hyperlink and xreate hyper link within same folder diff
	 * file.
	 * 
	 * @throws FindFailed
	 * @throws IOException
	 * 
	 * 
	 */
	public boolean drawCircleHyperLink(String markupname, String filepath1) throws FindFailed, IOException {
		boolean flag = false;
		Log.message("Switched to default content");
		SkySiteUtils.waitTill(5000);
		driver.switchTo().frame(driver.findElement(By.id("myFrame")));
		Log.message("Switched to frame");
		driver.findElement(By.xpath(
				".//*[@id='treeboxbox_tree_wrap_container']/div[2]/table/tbody/tr[2]/td[2]/table/tbody/tr[2]/td[2]/table/tbody/tr/td[4]/span"))
				.click();
		Log.message("Folder selected");
		SkySiteUtils.waitTill(4000);
		driver.findElement(By.xpath(".//*[@id='divGrid']/div[2]/table/tbody/tr[2]/td[2]/a[3]")).click();
		Log.message("File is selected");
		SkySiteUtils.waitTill(4000);
		String MainWindow = driver.getWindowHandle();
		Log.message(MainWindow);
		for (String childwindow : driver.getWindowHandles()) {
			driver.switchTo().window(childwindow);

		}
		Log.message("switched to child window");

		driver.switchTo().defaultContent();
		SkySiteUtils.waitTill(4000);
		// method to delete existing markup
		this.markupdeletebegin();
		SkySiteUtils.waitTill(4000);
		hyperlinktool.click();
		Log.message("Hyperlink tool is clicked");
		SkySiteUtils.waitForElement(driver, recthyperlink, 40);
		Actions act = new Actions(driver);
		act.moveToElement(circlehyperlinktool).clickAndHold().build().perform();
		SkySiteUtils.waitTill(2000);
		act.click(circlehyperlinktool).build().perform();
		Log.message("Circle Hyper Link button is clicked");
		SkySiteUtils.waitTill(2000);
		File fis = new File(PropertyReader.getProperty("Screenshotpath"));
		String filepath = fis.getAbsolutePath().toString();
		String Screenshot1 = filepath + "\\StartpointCircleHyperlink.png";
		String Screenshot2 = filepath + "\\EndpointCircleHyperlink.png";
		Screen s = new Screen();

		Pattern p1 = new Pattern(Screenshot1);

		Pattern p2 = new Pattern(Screenshot2);

		// s.click(p1);

		s.drag(p1);

		s.dropAt(p2);
		Log.message("image is drawn");
		SkySiteUtils.waitTill(4000);
		// driver.findElement(By.xpath(".//*[@id='hyperLinkFolderList']/ul/li[2]/div/a/div[3]")).click();
		driver.findElement(By.xpath(".//*[@id='hyperLinkFolderList']/ul/li/div/a/div[3]")).click();
		Log.message("Folder is selected");
		SkySiteUtils.waitTill(4000);
		// driver.findElement(By.xpath(".//*[@id='hyperLinkFolderList']/ul/li[2]/div/div[2]/h4/a")).click();
		driver.findElement(By.xpath(".//*[@id='hyperLinkFolderList']/ul/li[1]/div/div[2]/h4/a")).click();
		Log.message("File is selected");
		SkySiteUtils.waitForElement(driver, okbutton, 50);
		okbutton.click();
		Log.message("Ok button is clicked");

		SkySiteUtils.waitTill(4000);
		driver.findElement(By.xpath("//*[@class='leaflet-control-nav-home']")).click();
		Log.message("Home page is clicked");
		SkySiteUtils.waitForElement(driver, savebtn, 50);
		savebtn.click();
		Log.message("save button is clicked");
		SkySiteUtils.waitTill(4000);
		markupfield.sendKeys(markupname);
		Log.message("Enter markup name is " + markupname);
		SkySiteUtils.waitForElement(driver, savemarkupbtn, 40);
		savemarkupbtn.click();
		Log.message("savemarkupbtn is clicked");
		SkySiteUtils.waitTill(14000);
		driver.findElement(By.xpath(".//*[@id='imageViewer']/div[3]/div[4]/div/ul/li[2]/a")).click();
		Log.message("save button is clicked again");
		SkySiteUtils.waitTill(4000);
		String savedscreenshot1 = filepath1 + "\\BeforesavingSquareHyperLink.png";
		String savedscreenshot2 = filepath1 + "\\AfterSavingSquareHyperLink.png";
		// File fistemp=new File(PropertyReader.getProperty("tempScreenshot"));
		// String filepathtemp=fistemp.getAbsolutePath().toString();
		// String tempscreenshotpath=filepathtemp + "\\temp_Screenshot.png";
		File srcFiler = ((TakesScreenshot) driver).getScreenshotAs(OutputType.FILE);

		FileUtils.copyFile(srcFiler, new File(savedscreenshot1));

		Log.message("Screenshot taken of saved markup");
		SkySiteUtils.waitTill(4000);
		hyperlinkdropdown.click();
		Log.message("hyperlink markup dropdown clicked");
		SkySiteUtils.waitTill(4000);
		// for(WebElement markupitem:hperlinklist)
		// if(markupitem.getText().contains(markupname))
		// driver.findElement(By.xpath("//*[text()='"+markupname+"']")).click();
		driver.findElement(By.xpath(".//*[@id='btnHideAllHyperlinks']")).click();
		Log.message("hide all button clicked and image disappears");

		SkySiteUtils.waitTill(4000);
		showhyperlinks.click();
		Log.message("show all hyperlink clikced");
		SkySiteUtils.waitTill(4000);
		savebtn.click();
		Log.message("save button is clicked");
		SkySiteUtils.waitTill(4000);
		File srcFiler1 = ((TakesScreenshot) driver).getScreenshotAs(OutputType.FILE);

		FileUtils.copyFile(srcFiler1, new File(savedscreenshot2));
		Log.message("Screenshot taken after redrawing");

		BufferedImage imgA = ImageIO.read(new File(savedscreenshot1));

		// Thread.sleep(2000);

		BufferedImage imgB = ImageIO.read(new File(savedscreenshot2));

		// Thread.sleep(2000);

		Log.message("Checking the height and width of both the images are same?");

		if (imgA.getWidth() != imgB.getWidth() && imgA.getHeight() != imgB.getHeight()) {
			Log.message("image height width are different");
			return false;
		}

		int width = imgA.getWidth();
		int height = imgA.getHeight();
		for (int y = 0; y < height; y++) {
			for (int x = 0; x < width; x++) {

				if (imgA.getRGB(x, y) != imgB.getRGB(x, y)) {

					Log.message("Image are different");
					return false;
				}
			}

		}

		return true;
	}

	/**
	 * Method to verify circle hyperlink working properly
	 * 
	 */
	public boolean VerifyfunctionalityCircleHyperLinkSameFolderDiffFile(String markupname, String filepath1)
			throws FindFailed, UnsupportedFlavorException, IOException {
		// driver.switchTo().defaultContent();
		SkySiteUtils.waitTill(8000);
		File fistemp = new File(PropertyReader.getProperty("tempScreenshot"));
		String filepathtemp = fistemp.getAbsolutePath().toString();
		String tempscreenshotpath = filepathtemp + "\\temphyperlinkcircle.png";
		Screen s = new Screen();

		Pattern p1 = new Pattern(tempscreenshotpath);
		SkySiteUtils.waitTill(8000);
		s.click(p1);
		Log.message("saved hyperlink is clicked");
		SkySiteUtils.waitTill(4000);
		WebElement imagelink = driver.findElement(By.id("linkImgDiv"));
		JavascriptExecutor executor = (JavascriptExecutor) driver;
		executor.executeScript("arguments[0].click();", imagelink);
		Log.message("image is clicked");
		SkySiteUtils.waitTill(4000);
		// String
		// filename=driver.findElement(By.xpath("//*[@data-original-title='1_Document.pdf']")).getText();
		String filename = driver.findElement(By.xpath("//*[@data-original-title='1_EName.pdf']")).getText();
		Log.message("File name is " + filename);
		String expectedfilename = "(R-1) 1_EName.pdf";
		if (filename.equals(expectedfilename))
			return true;

		else
			return false;

	}

	/**
	 * Method to click default file for circle hyperlink
	 * 
	 */
	/**
	 * Method to click default file
	 * 
	 */
	public void clickDefaultFileForCircleHyperLink() {
		SkySiteUtils.waitTill(4000);
		WebElement documenttab = driver.findElement(By.xpath("//*[@data-original-title='1 mb.pdf']"));
		JavascriptExecutor executor = (JavascriptExecutor) driver;
		executor.executeScript("arguments[0].click();", documenttab);
		Log.message("Original file is clicked");

	}

	@FindBy(xpath = ".//*[@id='linkCloudCreate']/a/i")
	WebElement cloudtool;

	/**
	 * Method to draw Cloud HyperLink
	 * 
	 * @throws FindFailed
	 * @throws IOException
	 * 
	 */
	public boolean drawCloudHyperLink(String markupname, String filepath1) throws FindFailed, IOException {
		boolean flag = false;
		Log.message("Switched to default content");
		SkySiteUtils.waitTill(5000);
		driver.switchTo().frame(driver.findElement(By.id("myFrame")));
		Log.message("Switched to frame");
		driver.findElement(By.xpath("//*[text()='Viewer_Testing_CircleHyperLink']")).click();
		Log.message("Folder selected");
		SkySiteUtils.waitTill(4000);
		driver.findElement(By.xpath(".//*[@id='divGrid']/div[2]/table/tbody/tr[2]/td[2]/a[3]")).click();
		Log.message("File is selected");
		SkySiteUtils.waitTill(4000);
		String MainWindow = driver.getWindowHandle();
		Log.message(MainWindow);
		for (String childwindow : driver.getWindowHandles()) {
			driver.switchTo().window(childwindow);

		}
		Log.message("switched to child window");

		driver.switchTo().defaultContent();
		SkySiteUtils.waitTill(4000);
		// method to delete existing markup
		this.markupdeletebegin();
		SkySiteUtils.waitTill(6000);
		hyperlinktool.click();
		Log.message("Hyperlink tool is clicked");
		SkySiteUtils.waitForElement(driver, recthyperlink, 40);
		Actions act = new Actions(driver);
		act.moveToElement(cloudtool).clickAndHold().build().perform();
		SkySiteUtils.waitTill(2000);
		act.click(cloudtool).build().perform();
		Log.message("Cloud Hyper Link button is clicked");
		SkySiteUtils.waitTill(2000);
		File fis = new File(PropertyReader.getProperty("Screenshotpath"));
		String filepath = fis.getAbsolutePath().toString();
		String Screenshot1 = filepath + "\\StartpointCircleHyperlink.png";
		String Screenshot2 = filepath + "\\EndpointCircleHyperlink.png";
		Screen s = new Screen();

		Pattern p1 = new Pattern(Screenshot1);

		Pattern p2 = new Pattern(Screenshot2);

		// s.click(p1);

		s.drag(p1);

		s.dropAt(p2);
		Log.message("image is drawn");
		SkySiteUtils.waitTill(4000);
		// driver.findElement(By.xpath(".//*[@id='hyperLinkFolderList']/ul/li[2]/div/a/div[3]")).click();
		driver.findElement(By.xpath(".//*[@id='hyperLinkFolderList']/ul/li/div/a/div[3]")).click();
		Log.message("Folder is selected");
		SkySiteUtils.waitTill(4000);
		// driver.findElement(By.xpath(".//*[@id='hyperLinkFolderList']/ul/li[2]/div/div[2]/h4/a")).click();
		driver.findElement(By.xpath(".//*[@id='hyperLinkFolderList']/ul/li[1]/div/div[2]/h4/a")).click();
		Log.message("File is selected");
		SkySiteUtils.waitForElement(driver, okbutton, 50);
		okbutton.click();
		Log.message("Ok button is clicked");

		SkySiteUtils.waitTill(4000);
		driver.findElement(By.xpath("//*[@class='leaflet-control-nav-home']")).click();
		Log.message("Home page is clicked");
		SkySiteUtils.waitForElement(driver, savebtn, 50);
		savebtn.click();
		Log.message("save button is clicked");
		SkySiteUtils.waitTill(4000);
		markupfield.sendKeys(markupname);
		Log.message("Enter markup name is " + markupname);
		SkySiteUtils.waitForElement(driver, savemarkupbtn, 40);
		savemarkupbtn.click();
		Log.message("savemarkupbtn is clicked");
		SkySiteUtils.waitTill(14000);
		driver.findElement(By.xpath(".//*[@id='imageViewer']/div[3]/div[4]/div/ul/li[2]/a")).click();
		Log.message("save button is clicked again");
		SkySiteUtils.waitTill(4000);
		String savedscreenshot1 = filepath1 + "\\BeforesavingSquareHyperLink.png";
		String savedscreenshot2 = filepath1 + "\\AfterSavingSquareHyperLink.png";
		// File fistemp=new File(PropertyReader.getProperty("tempScreenshot"));
		// String filepathtemp=fistemp.getAbsolutePath().toString();
		// String tempscreenshotpath=filepathtemp + "\\temp_Screenshot.png";
		File srcFiler = ((TakesScreenshot) driver).getScreenshotAs(OutputType.FILE);

		FileUtils.copyFile(srcFiler, new File(savedscreenshot1));

		Log.message("Screenshot taken of saved markup");
		SkySiteUtils.waitTill(4000);
		hyperlinkdropdown.click();
		Log.message("hyperlink markup dropdown clicked");
		SkySiteUtils.waitTill(4000);
		// for(WebElement markupitem:hperlinklist)
		// if(markupitem.getText().contains(markupname))
		// driver.findElement(By.xpath("//*[text()='"+markupname+"']")).click();
		driver.findElement(By.xpath(".//*[@id='btnHideAllHyperlinks']")).click();
		Log.message("hide all button clicked and image disappears");

		SkySiteUtils.waitTill(4000);
		showhyperlinks.click();
		Log.message("show all hyperlink clikced");
		SkySiteUtils.waitTill(4000);
		savebtn.click();
		Log.message("save button is clicked");
		SkySiteUtils.waitTill(4000);
		File srcFiler1 = ((TakesScreenshot) driver).getScreenshotAs(OutputType.FILE);

		FileUtils.copyFile(srcFiler1, new File(savedscreenshot2));
		Log.message("Screenshot taken after redrawing");

		BufferedImage imgA = ImageIO.read(new File(savedscreenshot1));

		// Thread.sleep(2000);

		BufferedImage imgB = ImageIO.read(new File(savedscreenshot2));

		// Thread.sleep(2000);

		Log.message("Checking the height and width of both the images are same?");

		if (imgA.getWidth() != imgB.getWidth() && imgA.getHeight() != imgB.getHeight()) {
			Log.message("image height width are different");
			return false;
		}

		int width = imgA.getWidth();
		int height = imgA.getHeight();
		for (int y = 0; y < height; y++) {
			for (int x = 0; x < width; x++) {

				if (imgA.getRGB(x, y) != imgB.getRGB(x, y)) {

					Log.message("Image are different");
					return false;
				}
			}

		}

		return true;
	}

	/**
	 * Method to verify Cloud hyperlink working properly
	 * 
	 */
	public boolean VerifyfunctionalityCloudHyperLinkSameFolderDiffFile(String markupname, String filepath1)
			throws FindFailed, UnsupportedFlavorException, IOException {
		// driver.switchTo().defaultContent();
		SkySiteUtils.waitTill(8000);
		File fistemp = new File(PropertyReader.getProperty("tempScreenshot"));
		String filepathtemp = fistemp.getAbsolutePath().toString();
		String tempscreenshotpath = filepathtemp + "\\temphyperlinkcloud.png";
		Screen s = new Screen();

		Pattern p1 = new Pattern(tempscreenshotpath);
		SkySiteUtils.waitTill(5000);
		s.click(p1);
		Log.message("saved hyperlink is clicked");
		SkySiteUtils.waitTill(4000);
		WebElement imagelink = driver.findElement(By.id("linkImgDiv"));
		JavascriptExecutor executor = (JavascriptExecutor) driver;
		executor.executeScript("arguments[0].click();", imagelink);
		Log.message("image is clicked");
		SkySiteUtils.waitTill(4000);
		// String
		// filename=driver.findElement(By.xpath("//*[@data-original-title='1_Document.pdf']")).getText();
		String filename = driver.findElement(By.xpath("//*[@data-original-title='1_EName.pdf']")).getText();
		Log.message("File name is " + filename);
		String expectedfilename = "(R-1) 1_EName.pdf";
		if (filename.equals(expectedfilename))
			return true;

		else
			return false;

	}

	/**
	 * MEthod to draw square hyperlink for parent child folder for different file
	 * 
	 * @param markupname
	 * @param filepath1
	 * @return
	 * @throws IOException
	 * @throws FindFailed
	 */
	public boolean drawAndSaveSquareHyperLinkParentChildFolder(String markupname, String filepath1)
			throws IOException, FindFailed {
		boolean flag = false;
		Log.message("Switched to default content");
		SkySiteUtils.waitTill(5000);
		driver.switchTo().frame(driver.findElement(By.id("myFrame")));
		Log.message("Switched to frame");
		driver.findElement(By.xpath("//*[text()='ParentFolder']")).click();
		Log.message("Folder selected");
		SkySiteUtils.waitTill(4000);
		driver.findElement(By.xpath(".//*[@id='divGrid']/div[2]/table/tbody/tr[2]/td[2]/a[3]")).click();
		Log.message("File is selected");
		SkySiteUtils.waitTill(4000);
		String MainWindow = driver.getWindowHandle();
		Log.message(MainWindow);
		for (String childwindow : driver.getWindowHandles()) {
			driver.switchTo().window(childwindow);

		}
		Log.message("switched to child window");

		driver.switchTo().defaultContent();
		SkySiteUtils.waitTill(4000);
		// method to delete existing markup
		this.markupdeletebegin();
		SkySiteUtils.waitTill(4000);
		hyperlinktool.click();
		Log.message("Hyperlink tool is clicked");
		SkySiteUtils.waitForElement(driver, recthyperlink, 40);
		Actions act = new Actions(driver);
		act.moveToElement(recthyperlink).clickAndHold().build().perform();
		SkySiteUtils.waitTill(2000);
		act.click(recthyperlink).build().perform();
		Log.message("Rect Hyper Link button is clicked");
		SkySiteUtils.waitTill(2000);
		File fis = new File(PropertyReader.getProperty("Screenshotpath"));
		String filepath = fis.getAbsolutePath().toString();
		String Screenshot1 = filepath + "\\StartPointFolderLevel.png";
		String Screenshot2 = filepath + "\\EndpointFolderLevel.png";
		Screen s = new Screen();

		Pattern p1 = new Pattern(Screenshot1);

		Pattern p2 = new Pattern(Screenshot2);

		// s.click(p1);

		s.drag(p1);

		s.dropAt(p2);
		Log.message("image is drawn");
		SkySiteUtils.waitTill(4000);
		// driver.findElement(By.xpath(".//*[@id='hyperLinkFolderList']/ul/li[2]/div/a/div[3]")).click();
		driver.findElement(By.xpath(".//*[@id='hyperLinkFolderList']/ul/li[3]/div/a/div[3]")).click();
		Log.message(" Parent Folder is selected");
		SkySiteUtils.waitTill(4000);
		driver.findElement(By.xpath(".//*[@id='hyperLinkFolderList']/ul[1]/li/div/a/div[3]")).click();
		Log.message("Child Folder is selected");
		// driver.findElement(By.xpath(".//*[@id='hyperLinkFolderList']/ul/li[2]/div/div[2]/h4/a")).click();
		driver.findElement(By.xpath(".//*[@id='hyperLinkFolderList']/ul/li/div/div[2]/h4/a")).click();
		Log.message("File is selected");
		SkySiteUtils.waitForElement(driver, okbutton, 50);
		okbutton.click();
		Log.message("Ok button is clicked");

		SkySiteUtils.waitTill(4000);
		driver.findElement(By.xpath("//*[@class='leaflet-control-nav-home']")).click();
		Log.message("Home page is clicked");
		SkySiteUtils.waitForElement(driver, savebtn, 50);
		savebtn.click();
		Log.message("save button is clicked");
		SkySiteUtils.waitTill(4000);
		markupfield.sendKeys(markupname);
		Log.message("Enter markup name is " + markupname);
		SkySiteUtils.waitForElement(driver, savemarkupbtn, 40);
		savemarkupbtn.click();
		Log.message("savemarkupbtn is clicked");
		SkySiteUtils.waitTill(14000);
		driver.findElement(By.xpath(".//*[@id='imageViewer']/div[3]/div[4]/div/ul/li[2]/a")).click();
		Log.message("save button is clicked again");
		SkySiteUtils.waitTill(4000);

		String savedscreenshot1 = filepath1 + "\\BeforesavingSquareHyperLink.png";
		String savedscreenshot2 = filepath1 + "\\AfterSavingSquareHyperLink.png";
		// File fistemp=new File(PropertyReader.getProperty("tempScreenshot"));
		// String filepathtemp=fistemp.getAbsolutePath().toString();
		// String tempscreenshotpath=filepathtemp + "\\temp_Screenshot.png";
		File srcFiler = ((TakesScreenshot) driver).getScreenshotAs(OutputType.FILE);

		FileUtils.copyFile(srcFiler, new File(savedscreenshot1));

		Log.message("Screenshot taken of saved markup");
		SkySiteUtils.waitTill(4000);
		hyperlinkdropdown.click();
		Log.message("hyperlink markup dropdown clicked");
		SkySiteUtils.waitTill(4000);
		// for(WebElement markupitem:hperlinklist)
		// if(markupitem.getText().contains(markupname))
		// driver.findElement(By.xpath("//*[text()='"+markupname+"']")).click();
		driver.findElement(By.xpath(".//*[@id='btnHideAllHyperlinks']")).click();
		Log.message("hide all button clicked and image disappears");

		SkySiteUtils.waitTill(4000);
		showhyperlinks.click();
		Log.message("show all hyperlink clikced");
		SkySiteUtils.waitTill(4000);
		savebtn.click();
		Log.message("save button is clicked");
		SkySiteUtils.waitTill(4000);
		File srcFiler1 = ((TakesScreenshot) driver).getScreenshotAs(OutputType.FILE);

		FileUtils.copyFile(srcFiler1, new File(savedscreenshot2));
		Log.message("Screenshot taken after redrawing");

		BufferedImage imgA = ImageIO.read(new File(savedscreenshot1));

		// Thread.sleep(2000);

		BufferedImage imgB = ImageIO.read(new File(savedscreenshot2));

		// Thread.sleep(2000);

		Log.message("Checking the height and width of both the images are same?");

		if (imgA.getWidth() != imgB.getWidth() && imgA.getHeight() != imgB.getHeight()) {
			Log.message("image height width are different");
			return false;
		}

		int width = imgA.getWidth();
		int height = imgA.getHeight();
		for (int y = 0; y < height; y++) {
			for (int x = 0; x < width; x++) {

				if (imgA.getRGB(x, y) != imgB.getRGB(x, y)) {

					Log.message("Image are different");
					return false;
				}
			}

		}

		return true;
	}

	/**
	 * MEthod to verify functionality for square hyperlink when linking parent child
	 * folder with diff file
	 * 
	 * @param markupname
	 * @param filepath1
	 * @return
	 * @throws FindFailed
	 * @throws UnsupportedFlavorException
	 * @throws IOException
	 */
	public boolean VerifyfunctionalitySquareHyperLinkParentCHildFolderDiffFile(String markupname, String filepath1)
			throws FindFailed, UnsupportedFlavorException, IOException {
		// driver.switchTo().defaultContent();
		SkySiteUtils.waitTill(8000);
		File fistemp = new File(PropertyReader.getProperty("tempScreenshot"));
		String filepathtemp = fistemp.getAbsolutePath().toString();
		String tempscreenshotpath = filepathtemp + "\\tempfolderlevel.png";
		Screen s = new Screen();

		Pattern p1 = new Pattern(tempscreenshotpath);

		SkySiteUtils.waitTill(10000);

		s.click(p1);
		Log.message("saved hyperlink is clicked");
		SkySiteUtils.waitTill(4000);
		WebElement imagelink = driver.findElement(By.id("linkImgDiv"));
		JavascriptExecutor executor = (JavascriptExecutor) driver;
		executor.executeScript("arguments[0].click();", imagelink);
		Log.message("image is clicked");

		String filename = driver.findElement(By.xpath("//*[@data-original-title='1_EName.pdf']")).getText();
		Log.message("File name is " + filename);
		String expectedfilename = "(R-1) 1_EName.pdf";
		if (filename.equals(expectedfilename))
			return true;

		else
			return false;

	}

	/**
	 * MEthod to draw square hyperlink for child parent folder for different file
	 * 
	 * @param markupname
	 * @param filepath1
	 * @return
	 * @throws IOException
	 * @throws FindFailed
	 */
	public boolean drawAndSaveSquareHyperLinkChildParentFolder(String markupname, String filepath1)
			throws IOException, FindFailed {
		boolean flag = false;
		Log.message("Switched to default content");
		SkySiteUtils.waitTill(5000);
		driver.switchTo().frame(driver.findElement(By.id("myFrame")));
		Log.message("Switched to frame");
		driver.findElement(By.xpath(
				".//*[@id='treeboxbox_tree_wrap_container']/div[2]/table/tbody/tr[2]/td[2]/table/tbody/tr[7]/td[2]/table/tbody/tr/td[1]/div"))
				.click();
		Log.message("Expand parent folder");
		SkySiteUtils.waitTill(4000);
		driver.findElement(By.xpath("//*[text()='ChildFolder']")).click();
		Log.message("Child folder is selected");
		SkySiteUtils.waitTill(4000);
		driver.findElement(By.xpath(".//*[@id='divGrid']/div[2]/table/tbody/tr[2]/td[2]/a[3]")).click();
		Log.message("File is selected");
		SkySiteUtils.waitTill(4000);
		String MainWindow = driver.getWindowHandle();
		Log.message(MainWindow);
		for (String childwindow : driver.getWindowHandles()) {
			driver.switchTo().window(childwindow);

		}
		Log.message("switched to child window");

		driver.switchTo().defaultContent();
		SkySiteUtils.waitTill(4000);
		// method to delete existing markup
		this.markupdeletebegin();
		SkySiteUtils.waitTill(4000);
		hyperlinktool.click();
		Log.message("Hyperlink tool is clicked");
		SkySiteUtils.waitForElement(driver, recthyperlink, 40);
		Actions act = new Actions(driver);
		act.moveToElement(recthyperlink).clickAndHold().build().perform();
		SkySiteUtils.waitTill(2000);
		act.click(recthyperlink).build().perform();
		Log.message("Rect Hyper Link button is clicked");
		SkySiteUtils.waitTill(2000);
		File fis = new File(PropertyReader.getProperty("Screenshotpath"));
		String filepath = fis.getAbsolutePath().toString();
		String Screenshot1 = filepath + "\\StartPoint_HYperLink_!_ENAMEFILE.png";
		String Screenshot2 = filepath + "\\EndPoint_HyperLink_EnameFile.png";
		Screen s = new Screen();

		Pattern p1 = new Pattern(Screenshot1);

		Pattern p2 = new Pattern(Screenshot2);

		// s.click(p1);

		s.drag(p1);

		s.dropAt(p2);
		Log.message("image is drawn");
		SkySiteUtils.waitTill(4000);
		// driver.findElement(By.xpath(".//*[@id='hyperLinkFolderList']/ul/li[2]/div/a/div[3]")).click();
		driver.findElement(By.xpath(".//*[@id='hyperLinkFolderList']/ul/li[6]/div/a/div[3]")).click();
		Log.message(" Parent Folder is selected");
		SkySiteUtils.waitTill(4000);
		// driver.findElement(By.xpath(".//*[@id='hyperLinkFolderList']/ul[1]/li/div/a/div[3]")).click();
		// Log.message("Child Folder is selected");
		// driver.findElement(By.xpath(".//*[@id='hyperLinkFolderList']/ul/li[2]/div/div[2]/h4/a")).click();
		driver.findElement(By.xpath(".//*[@id='hyperLinkFolderList']/ul[2]/li/div/div[2]/h4/a")).click();
		Log.message("File of parent folder is selected");
		SkySiteUtils.waitForElement(driver, okbutton, 50);
		okbutton.click();
		Log.message("Ok button is clicked");

		SkySiteUtils.waitTill(4000);
		driver.findElement(By.xpath("//*[@class='leaflet-control-nav-home']")).click();
		Log.message("Home page is clicked");
		SkySiteUtils.waitForElement(driver, savebtn, 50);
		savebtn.click();
		Log.message("save button is clicked");
		SkySiteUtils.waitTill(4000);
		markupfield.sendKeys(markupname);
		Log.message("Enter markup name is " + markupname);
		SkySiteUtils.waitForElement(driver, savemarkupbtn, 40);
		savemarkupbtn.click();
		Log.message("savemarkupbtn is clicked");
		SkySiteUtils.waitTill(14000);
		driver.findElement(By.xpath(".//*[@id='imageViewer']/div[3]/div[4]/div/ul/li[2]/a")).click();
		Log.message("save button is clicked again");
		SkySiteUtils.waitTill(4000);
		String savedscreenshot1 = filepath1 + "\\BeforesavingSquareHyperLink.png";
		String savedscreenshot2 = filepath1 + "\\AfterSavingSquareHyperLink.png";
		// File fistemp=new File(PropertyReader.getProperty("tempScreenshot"));
		// String filepathtemp=fistemp.getAbsolutePath().toString();
		// String tempscreenshotpath=filepathtemp + "\\temp_Screenshot.png";
		File srcFiler = ((TakesScreenshot) driver).getScreenshotAs(OutputType.FILE);

		FileUtils.copyFile(srcFiler, new File(savedscreenshot1));

		Log.message("Screenshot taken of saved markup");
		SkySiteUtils.waitTill(4000);
		hyperlinkdropdown.click();
		Log.message("hyperlink markup dropdown clicked");
		SkySiteUtils.waitTill(4000);
		// for(WebElement markupitem:hperlinklist)
		// if(markupitem.getText().contains(markupname))
		// driver.findElement(By.xpath("//*[text()='"+markupname+"']")).click();
		driver.findElement(By.xpath(".//*[@id='btnHideAllHyperlinks']")).click();
		Log.message("hide all button clicked and image disappears");

		SkySiteUtils.waitTill(4000);
		showhyperlinks.click();
		Log.message("show all hyperlink clikced");
		SkySiteUtils.waitTill(4000);
		savebtn.click();
		Log.message("save button is clicked");
		SkySiteUtils.waitTill(4000);
		File srcFiler1 = ((TakesScreenshot) driver).getScreenshotAs(OutputType.FILE);

		FileUtils.copyFile(srcFiler1, new File(savedscreenshot2));
		Log.message("Screenshot taken after redrawing");

		BufferedImage imgA = ImageIO.read(new File(savedscreenshot1));

		// Thread.sleep(2000);

		BufferedImage imgB = ImageIO.read(new File(savedscreenshot2));

		// Thread.sleep(2000);

		Log.message("Checking the height and width of both the images are same?");

		if (imgA.getWidth() != imgB.getWidth() && imgA.getHeight() != imgB.getHeight()) {
			Log.message("image height width are different");
			return false;
		}

		int width = imgA.getWidth();
		int height = imgA.getHeight();
		for (int y = 0; y < height; y++) {
			for (int x = 0; x < width; x++) {

				if (imgA.getRGB(x, y) != imgB.getRGB(x, y)) {

					Log.message("Image are different");
					return false;
				}
			}

		}

		return true;
	}

	/**
	 * MEthod to verify working of square hyperlink for diff folder(Child--->Parent)
	 * & diff file
	 * 
	 */
	public boolean VerifyfunctionalitySquareHyperLinkdiffFolderChildParentDiffFile(String markupname, String filepath1)
			throws FindFailed, UnsupportedFlavorException, IOException {
		// driver.switchTo().defaultContent();
		SkySiteUtils.waitTill(8000);
		File fistemp = new File(PropertyReader.getProperty("tempScreenshot"));
		String filepathtemp = fistemp.getAbsolutePath().toString();
		String tempscreenshotpath = filepathtemp + "\\tempscreenshotforENAMEFILE.png";
		Screen s = new Screen();

		Pattern p1 = new Pattern(tempscreenshotpath);
		SkySiteUtils.waitTill(8000);
		s.click(p1);
		Log.message("saved hyperlink is clicked");
		SkySiteUtils.waitTill(4000);
		WebElement imagelink = driver.findElement(By.id("linkImgDiv"));
		JavascriptExecutor executor = (JavascriptExecutor) driver;
		executor.executeScript("arguments[0].click();", imagelink);
		Log.message("image is clicked");
		SkySiteUtils.waitTill(4000);
		// String
		// filename=driver.findElement(By.xpath("//*[@data-original-title='1_Document.pdf']")).getText();
		String filename = driver.findElement(By.xpath("//*[text()='(R-1) 1_Document.pdf']")).getText();
		Log.message("File name is " + filename);
		String expectedfilename = "(R-1) 1_Document.pdf";
		if (filename.equals(expectedfilename))
			return true;

		else
			return false;

	}

	/**
	 * Method to click default file
	 * 
	 */
	public void clickDefaultFileForEname() {
		SkySiteUtils.waitTill(4000);
		WebElement documenttab = driver.findElement(By.xpath("//*[text()='(R-1) 1_EName.pdf']"));
		JavascriptExecutor executor = (JavascriptExecutor) driver;
		executor.executeScript("arguments[0].click();", documenttab);
		Log.message("Original file is clicked");
		SkySiteUtils.waitTill(5000);

	}

	/**
	 * Method to draw and save hyperlink for multipage file
	 * 
	 * @throws FindFailed
	 * @throws IOException
	 * 
	 */
	public boolean drawAndSaveHyperLinkMultiPage(String markupname, String filepath1) throws FindFailed, IOException {
		boolean flag = false;
		Log.message("Switched to default content");
		SkySiteUtils.waitTill(5000);
		driver.switchTo().frame(driver.findElement(By.id("myFrame")));
		Log.message("Switched to frame");
		driver.findElement(By.xpath("//*[text()='SearchForKeyword2']")).click();
		Log.message("Multi page folder");
		SkySiteUtils.waitTill(4000);
		driver.findElement(By.xpath(".//*[@id='divGrid']/div[2]/table/tbody/tr[3]/td[2]/a[3]")).click();
		Log.message("File is selected");
		SkySiteUtils.waitTill(4000);
		String MainWindow = driver.getWindowHandle();
		Log.message(MainWindow);
		for (String childwindow : driver.getWindowHandles()) {
			driver.switchTo().window(childwindow);

		}
		Log.message("switched to child window");

		driver.switchTo().defaultContent();
		SkySiteUtils.waitTill(4000);
		// method to delete existing markup
		this.markupdeletebegin();
		SkySiteUtils.waitTill(4000);
		hyperlinktool.click();
		Log.message("Hyperlink tool is clicked");
		SkySiteUtils.waitForElement(driver, recthyperlink, 40);
		Actions act = new Actions(driver);
		act.moveToElement(recthyperlink).clickAndHold().build().perform();
		SkySiteUtils.waitTill(2000);
		act.click(recthyperlink).build().perform();
		Log.message("Rect Hyper Link button is clicked");
		SkySiteUtils.waitTill(2000);
		File fis = new File(PropertyReader.getProperty("Screenshotpath"));
		String filepath = fis.getAbsolutePath().toString();
		String Screenshot1 = filepath + "\\StartPoint_MultiPageFile.png";
		String Screenshot2 = filepath + "\\EndPoint_MultiPageFile.png";
		Screen s = new Screen();

		Pattern p1 = new Pattern(Screenshot1);

		Pattern p2 = new Pattern(Screenshot2);

		// s.click(p1);

		s.drag(p1);

		s.dropAt(p2);
		Log.message("image is drawn");
		SkySiteUtils.waitTill(4000);
		// driver.findElement(By.xpath(".//*[@id='hyperLinkFolderList']/ul/li[2]/div/a/div[3]")).click();
		driver.findElement(By.xpath(".//*[@id='hyperLinkFolderList']/ul/li[9]/div/a/div[3]")).click();
		Log.message(" Mutli Page File Folder is selected again for linking");
		SkySiteUtils.waitTill(4000);
		driver.findElement(By.xpath(".//*[@id='hyperLinkFolderList']/ul/li[2]/div/div[2]/h4/a")).click();
		Log.message("File  is selected");
		driver.findElement(By.xpath(".//*[@id='dvHLPage_2']/div/ul/li[11]/a")).click();
		Log.message("11th page is selected");
		// driver.findElement(By.xpath(".//*[@id='dvHLPage_1']/div/ul/li[1]/a")).click();
		// Log.message("1st page folder is clicked");
		SkySiteUtils.waitForElement(driver, okbutton, 50);
		okbutton.click();
		Log.message("Ok button is clicked");

		SkySiteUtils.waitTill(4000);
		driver.findElement(By.xpath("//*[@class='leaflet-control-nav-home']")).click();
		Log.message("Home page is clicked");
		SkySiteUtils.waitForElement(driver, savebtn, 50);
		savebtn.click();
		Log.message("save button is clicked");
		SkySiteUtils.waitTill(4000);
		markupfield.sendKeys(markupname);
		Log.message("Enter markup name is " + markupname);
		SkySiteUtils.waitForElement(driver, savemarkupbtn, 40);
		savemarkupbtn.click();
		Log.message("savemarkupbtn is clicked");
		SkySiteUtils.waitTill(14000);

		String savedscreenshot1 = filepath1 + "\\BeforesavingSquareHyperLink.png";
		String savedscreenshot2 = filepath1 + "\\AfterSavingSquareHyperLink.png";
		// File fistemp=new File(PropertyReader.getProperty("tempScreenshot"));
		// String filepathtemp=fistemp.getAbsolutePath().toString();
		// String tempscreenshotpath=filepathtemp + "\\temp_Screenshot.png";
		File srcFiler = ((TakesScreenshot) driver).getScreenshotAs(OutputType.FILE);

		FileUtils.copyFile(srcFiler, new File(savedscreenshot1));

		Log.message("Screenshot taken of saved markup");
		SkySiteUtils.waitTill(4000);
		hyperlinkdropdown.click();
		Log.message("hyperlink markup dropdown clicked");
		SkySiteUtils.waitTill(4000);
		// for(WebElement markupitem:hperlinklist)
		// if(markupitem.getText().contains(markupname))
		// driver.findElement(By.xpath("//*[text()='"+markupname+"']")).click();
		driver.findElement(By.xpath(".//*[@id='btnHideAllHyperlinks']")).click();
		Log.message("hide all button clicked and image disappears");

		SkySiteUtils.waitTill(4000);
		showhyperlinks.click();
		Log.message("show all hyperlink clikced");
		SkySiteUtils.waitTill(4000);
		savebtn.click();
		Log.message("save button is clicked");
		SkySiteUtils.waitTill(4000);
		File srcFiler1 = ((TakesScreenshot) driver).getScreenshotAs(OutputType.FILE);

		FileUtils.copyFile(srcFiler1, new File(savedscreenshot2));
		Log.message("Screenshot taken after redrawing");

		BufferedImage imgA = ImageIO.read(new File(savedscreenshot1));

		// Thread.sleep(2000);

		BufferedImage imgB = ImageIO.read(new File(savedscreenshot2));

		// Thread.sleep(2000);

		Log.message("Checking the height and width of both the images are same?");

		if (imgA.getWidth() == imgB.getWidth() && imgA.getHeight() == imgB.getHeight()) {

			for (int x = 0; x < imgA.getWidth(); x++) {

				for (int y = 0; y < imgB.getHeight(); y++) {

					if (imgA.getRGB(x, y) == imgB.getRGB(x, y)) {

						flag = true;
						break;
					}
				}

			}
		}
		if (flag == true) {
			Log.message("images are equal");
			return true;
		} else {
			Log.message("images are not equal");
			return false;
		}

	}

	/**
	 * MEthod to verify functionality of hyperlink for multipage file
	 * @throws InterruptedException 
	 * 
	 */
	public boolean VerifyfunctionalitySquareHyperLinkMultiPage(String markupname, String filepath1)
			throws FindFailed, UnsupportedFlavorException, IOException, InterruptedException {
		driver.switchTo().defaultContent();
		File fistemp = new File(PropertyReader.getProperty("tempScreenshot"));
		String filepathtemp = fistemp.getAbsolutePath().toString();
		String tempscreenshotpath = filepathtemp + "\\temphyperlinkmultipage.PNG";
		Screen s = new Screen();

		Pattern p1 = new Pattern(tempscreenshotpath);
		s.click(p1);
		Log.message("saved hyperlink is clicked");
		SkySiteUtils.waitTill(4000);
		WebElement imagelink = driver.findElement(By.id("linkImgDiv"));
		JavascriptExecutor executor = (JavascriptExecutor) driver;
		executor.executeScript("arguments[0].click();", imagelink);
		Log.message("image is clicked");
		SkySiteUtils.waitTill(4000);
		
		
		// String
		// filename=driver.findElement(By.xpath("//*[@data-original-title='1_Document.pdf']")).getText();
		
		
		SkySiteUtils.waitTill(5000);
		File fistemp12 = new File(PropertyReader.getProperty("tempScreenshot"));
		String filepathtemp12 = fistemp12.getAbsolutePath().toString();
		String savedscreenshot1 = filepathtemp12 + "\\New_OutputImageForMultiFile.png";
		String savedscreenshot2 = filepath1 + "\\result1.png";
		SkySiteUtils.waitTill(4000);
		// String tempscreenshotpath=filepathtemp + "\\temp_Screenshot.png";
		File srcFiler = ((TakesScreenshot) driver).getScreenshotAs(OutputType.FILE);

		FileUtils.copyFile(srcFiler, new File(savedscreenshot2));

		Log.message("Screenshot taken of saved markup");
		SkySiteUtils.waitTill(4000);

		BufferedImage imgA = ImageIO.read(new File(savedscreenshot1));
		SkySiteUtils.waitTill(3000);
		BufferedImage imgB = ImageIO.read(new File(savedscreenshot2));

		Thread.sleep(2000);

		Log.message("Checking the height and width of both the images are same?");

		if (imgA.getWidth() != imgB.getWidth() && imgA.getHeight() != imgB.getHeight()) {
			Log.message("image height width are different");
			return false;
		}

		int width = imgA.getWidth();
		int height = imgA.getHeight();
		for (int x = 0; x < width; x++) {
			for (int y = 0; y < height; y++) {

				if (imgA.getRGB(x, y) != imgB.getRGB(x, y)) {

					Log.message("Image are different");
					return false;
				}
			}

		}

		return true;
	}

		

	

	@FindBy(xpath = "//*[@href='#webLink']")
	WebElement externallinltab;
	@FindBy(xpath = ".//*[@id='hyperlinkExternalLinkText']")
	WebElement hyperlinktextarea;

	/**
	 * MEthod to verify square hyperlink getting saved properly for external
	 * hyperlink
	 * 
	 */
	public boolean drawAndSaveHyperLinKforExternalhyperlink(String markupname, String filepath1)
			throws FindFailed, IOException {
		boolean flag = false;
		Log.message("Switched to default content");
		SkySiteUtils.waitTill(5000);
		driver.switchTo().frame(driver.findElement(By.id("myFrame")));
		Log.message("Switched to frame");
		driver.findElement(By.xpath(
				"//*[text()='ExternalHyperLink']"))
				.click();
		Log.message("Folder is selected");
		SkySiteUtils.waitTill(4000);
		driver.findElement(By.xpath(".//*[@id='divGrid']/div[2]/table/tbody/tr[2]/td[2]/a[3]")).click();
		Log.message("File is selected");
		SkySiteUtils.waitTill(4000);
		String MainWindow = driver.getWindowHandle();
		Log.message(MainWindow);
		for (String childwindow : driver.getWindowHandles()) {
			driver.switchTo().window(childwindow);

		}
		Log.message("switched to child window");

		driver.switchTo().defaultContent();

		SkySiteUtils.waitTill(4000);
		// method to delete existing markup
		this.markupdeletebegin();
		SkySiteUtils.waitTill(4000);
		hyperlinktool.click();
		Log.message("Hyperlink tool is clicked");
		SkySiteUtils.waitForElement(driver, recthyperlink, 40);
		Actions act = new Actions(driver);
		act.moveToElement(recthyperlink).clickAndHold().build().perform();
		SkySiteUtils.waitTill(2000);
		act.click(recthyperlink).build().perform();
		Log.message("Rect Hyper Link button is clicked");
		SkySiteUtils.waitTill(2000);
		File fis = new File(PropertyReader.getProperty("Screenshotpath"));
		String filepath = fis.getAbsolutePath().toString();
		String Screenshot1 = filepath + "\\StartPointFolderLevel.png";
		String Screenshot2 = filepath + "\\EndpointFolderlevel.png";
		Screen s = new Screen();

		Pattern p1 = new Pattern(Screenshot1);

		Pattern p2 = new Pattern(Screenshot2);

		// s.click(p1);

		s.drag(p1);

		s.dropAt(p2);
		Log.message("image is drawn");
		SkySiteUtils.waitTill(4000);
		// driver.findElement(By.xpath(".//*[@id='hyperLinkFolderList']/ul/li[2]/div/a/div[3]")).click();
		externallinltab.click();
		Log.message("External hyperlink tab is clicked");
		hyperlinktextarea.sendKeys("www.google.com");

		JavascriptExecutor executor = (JavascriptExecutor) driver;
		executor.executeScript("arguments[0].click();", okbuttonexternalhyperlink);
		// SkySiteUtils.waitForElement(driver, okbutton, 50);
		// okbuttonexternalhyperlink.click();
		Log.message("Ok button is clicked");

		SkySiteUtils.waitTill(4000);
		driver.findElement(By.xpath("//*[@class='leaflet-control-nav-home']")).click();
		Log.message("Home page is clicked");
		SkySiteUtils.waitForElement(driver, savebtn, 50);
		savebtn.click();
		Log.message("save button is clicked");
		SkySiteUtils.waitTill(4000);
		markupfield.sendKeys(markupname);
		Log.message("Enter markup name is " + markupname);
		SkySiteUtils.waitForElement(driver, savemarkupbtn, 40);
		savemarkupbtn.click();
		Log.message("savemarkupbtn is clicked");
		SkySiteUtils.waitTill(14000);
		driver.findElement(By.xpath(".//*[@id='imageViewer']/div[3]/div[4]/div/ul/li[2]/a")).click();
		Log.message("save button is clicked again");
		SkySiteUtils.waitTill(4000);

		String savedscreenshot1 = filepath1 + "\\BeforesavingSquareHyperLink.png";
		String savedscreenshot2 = filepath1 + "\\AfterSavingSquareHyperLink.png";
		// File fistemp=new File(PropertyReader.getProperty("tempScreenshot"));
		// String filepathtemp=fistemp.getAbsolutePath().toString();
		// String tempscreenshotpath=filepathtemp + "\\temp_Screenshot.png";
		File srcFiler = ((TakesScreenshot) driver).getScreenshotAs(OutputType.FILE);

		FileUtils.copyFile(srcFiler, new File(savedscreenshot1));

		Log.message("Screenshot taken of saved markup");
		SkySiteUtils.waitTill(4000);
		hyperlinkdropdown.click();
		Log.message("hyperlink markup dropdown clicked");
		SkySiteUtils.waitTill(4000);
		// for(WebElement markupitem:hperlinklist)
		// if(markupitem.getText().contains(markupname))
		// driver.findElement(By.xpath("//*[text()='"+markupname+"']")).click();
		driver.findElement(By.xpath(".//*[@id='btnHideAllHyperlinks']")).click();
		Log.message("hide all button clicked and image disappears");

		SkySiteUtils.waitTill(4000);
		showhyperlinks.click();
		Log.message("show all hyperlink clikced");
		SkySiteUtils.waitTill(4000);
		savebtn.click();
		Log.message("save button is clicked");
		SkySiteUtils.waitTill(4000);
		File srcFiler1 = ((TakesScreenshot) driver).getScreenshotAs(OutputType.FILE);

		FileUtils.copyFile(srcFiler1, new File(savedscreenshot2));
		Log.message("Screenshot taken after redrawing");

		BufferedImage imgA = ImageIO.read(new File(savedscreenshot1));

		// Thread.sleep(2000);

		BufferedImage imgB = ImageIO.read(new File(savedscreenshot2));

		// Thread.sleep(2000);

		Log.message("Checking the height and width of both the images are same?");

		if (imgA.getWidth() != imgB.getWidth() && imgA.getHeight() != imgB.getHeight()) {
			Log.message("image height width are different");
			return false;
		}

		int width = imgA.getWidth();
		int height = imgA.getHeight();
		for (int y = 0; y < height; y++) {
			for (int x = 0; x < width; x++) {

				if (imgA.getRGB(x, y) != imgB.getRGB(x, y)) {

					Log.message("Image are different");
					return false;
				}
			}

		}

		return true;
	}

	/**
	 * MEthod to verify functionality of hyperlink for External hyperlink
	 * 
	 */
	public boolean VerifyfunctionalitySquareHyperExternalHyperlink(String markupname, String filepath1)
			throws FindFailed, UnsupportedFlavorException, IOException {
		// driver.switchTo().defaultContent();
		SkySiteUtils.waitTill(5000);
		File fistemp = new File(PropertyReader.getProperty("tempScreenshot"));
		String filepathtemp = fistemp.getAbsolutePath().toString();
		String tempscreenshotpath = filepathtemp + "\\tempfolderlevel.PNG";
		Screen s = new Screen();

		Pattern p1 = new Pattern(tempscreenshotpath);
		SkySiteUtils.waitTill(5000);
		s.click(p1);
		Log.message("saved hyperlink is clicked");
		SkySiteUtils.waitTill(4000);
		// WebElement
		// imagelink=driver.findElement(By.xpath("//*[text()='http://www.google.com']"));linkFrameDivWrap
		// JavascriptExecutor executor = (JavascriptExecutor)driver;
		// executor.executeScript("arguments[0].click();", imagelink);
		driver.findElement(By.id("linkFrameDivWrap")).click();
		Log.message("image is clicked");
		SkySiteUtils.waitTill(4000);
		// String
		// filename=driver.findElement(By.xpath("//*[@data-original-title='1_Document.pdf']")).getText();
		String MainWindow = driver.getWindowHandle();
		Log.message(MainWindow);
		for (String childwindow : driver.getWindowHandles()) {
			driver.switchTo().window(childwindow);

		}
		Log.message("switched to child window");

		driver.switchTo().defaultContent();
		SkySiteUtils.waitTill(5000);
		String currenturl = driver.getCurrentUrl();
		Log.message("URl is " + currenturl);
		driver.switchTo().window(MainWindow);
		if (currenturl.contains("www.google.com")) {
			Log.message("Linked url is opened in new tab");
			return true;
		} else {
			Log.message("Linked url is not opened in new tab");
			return false;
		}

	}

	/**
	 * MEthod to draw and verify hyperlink with folder level
	 * 
	 * @throws FindFailed
	 * @throws IOException
	 * 
	 */
	public boolean drawAndSaveHyperlinkWithFolderLevel(String markupname, String filepath1)
			throws FindFailed, IOException {
		boolean flag = false;
		Log.message("Switched to default content");
		SkySiteUtils.waitTill(5000);
		driver.switchTo().frame(driver.findElement(By.id("myFrame")));
		Log.message("Switched to frame");
		driver.findElement(By.xpath("//*[text()='Viewertesting']")).click();
		Log.message("Folder selected");
		Log.message("Folder selected");
		SkySiteUtils.waitTill(4000);
		driver.findElement(By.xpath(".//*[@id='divGrid']/div[2]/table/tbody/tr[2]/td[2]/a[3]")).click();
		Log.message("File is selected");
		SkySiteUtils.waitTill(4000);
		String MainWindow = driver.getWindowHandle();
		Log.message(MainWindow);
		for (String childwindow : driver.getWindowHandles()) {
			driver.switchTo().window(childwindow);

		}
		Log.message("switched to child window");

		driver.switchTo().defaultContent();
		SkySiteUtils.waitTill(4000);
		// method to delete existing markup
		this.markupdeletebegin();
		SkySiteUtils.waitTill(4000);
		hyperlinktool.click();
		Log.message("Hyperlink tool is clicked");
		SkySiteUtils.waitForElement(driver, recthyperlink, 40);
		Actions act = new Actions(driver);
		act.moveToElement(recthyperlink).clickAndHold().build().perform();
		SkySiteUtils.waitTill(2000);
		act.click(recthyperlink).build().perform();
		Log.message("Rect Hyper Link button is clicked");
		SkySiteUtils.waitTill(2000);
		File fis = new File(PropertyReader.getProperty("Screenshotpath"));
		String filepath = fis.getAbsolutePath().toString();
		String Screenshot1 = filepath + "\\StartPointFolderLevel.PNG";
		String Screenshot2 = filepath + "\\EndpointFolderLevel.png";
		Screen s = new Screen();

		Pattern p1 = new Pattern(Screenshot1);

		Pattern p2 = new Pattern(Screenshot2);

		// s.click(p1);

		s.drag(p1);

		s.dropAt(p2);
		Log.message("image is drawn");
		SkySiteUtils.waitTill(4000);
		// driver.findElement(By.xpath(".//*[@id='hyperLinkFolderList']/ul/li[2]/div/a/div[3]")).click();
		driver.findElement(By.xpath(".//*[@id='hyperLinkFolderList']/ul/li[1]/div/a/div[3]")).click();
		Log.message(" Folder is selected");
		SkySiteUtils.waitTill(4000);

		// driver.findElement(By.xpath(".//*[@id='hyperLinkFolderList']/ul/li[2]/div/div[2]/h4/a")).click();

		SkySiteUtils.waitForElement(driver, okbuttonexternalhyperlink, 50);
		JavascriptExecutor executor = (JavascriptExecutor) driver;
		executor.executeScript("arguments[0].click();", okbuttonexternalhyperlink);
		Log.message("Ok button is clicked");

		SkySiteUtils.waitTill(4000);
		driver.findElement(By.xpath("//*[@class='leaflet-control-nav-home']")).click();
		Log.message("Home page is clicked");
		SkySiteUtils.waitForElement(driver, savebtn, 50);
		savebtn.click();
		Log.message("save button is clicked");
		SkySiteUtils.waitTill(4000);
		markupfield.sendKeys(markupname);
		Log.message("Enter markup name is " + markupname);
		SkySiteUtils.waitForElement(driver, savemarkupbtn, 40);
		savemarkupbtn.click();
		Log.message("savemarkupbtn is clicked");
		SkySiteUtils.waitTill(14000);
		driver.findElement(By.xpath(".//*[@id='imageViewer']/div[3]/div[4]/div/ul/li[2]/a")).click();
		Log.message("save button is clicked again");
		SkySiteUtils.waitTill(4000);

		String savedscreenshot1 = filepath1 + "\\BeforesavingSquareHyperLink.png";
		String savedscreenshot2 = filepath1 + "\\AfterSavingSquareHyperLink.png";
		// File fistemp=new File(PropertyReader.getProperty("tempScreenshot"));
		// String filepathtemp=fistemp.getAbsolutePath().toString();
		// String tempscreenshotpath=filepathtemp + "\\temp_Screenshot.png";
		File srcFiler = ((TakesScreenshot) driver).getScreenshotAs(OutputType.FILE);

		FileUtils.copyFile(srcFiler, new File(savedscreenshot1));

		Log.message("Screenshot taken of saved markup");
		SkySiteUtils.waitTill(4000);
		hyperlinkdropdown.click();
		Log.message("hyperlink markup dropdown clicked");
		SkySiteUtils.waitTill(4000);
		// for(WebElement markupitem:hperlinklist)
		// if(markupitem.getText().contains(markupname))
		// driver.findElement(By.xpath("//*[text()='"+markupname+"']")).click();
		driver.findElement(By.xpath(".//*[@id='btnHideAllHyperlinks']")).click();
		Log.message("hide all button clicked and image disappears");

		SkySiteUtils.waitTill(4000);
		showhyperlinks.click();
		Log.message("show all hyperlink clikced");
		SkySiteUtils.waitTill(4000);
		savebtn.click();
		Log.message("save button is clicked");
		SkySiteUtils.waitTill(4000);
		File srcFiler1 = ((TakesScreenshot) driver).getScreenshotAs(OutputType.FILE);

		FileUtils.copyFile(srcFiler1, new File(savedscreenshot2));
		Log.message("Screenshot taken after redrawing");

		BufferedImage imgA = ImageIO.read(new File(savedscreenshot1));

		// Thread.sleep(2000);

		BufferedImage imgB = ImageIO.read(new File(savedscreenshot2));

		// Thread.sleep(2000);

		Log.message("Checking the height and width of both the images are same?");

		if (imgA.getWidth() != imgB.getWidth() && imgA.getHeight() != imgB.getHeight()) {
			Log.message("image height width are different");
			return false;
		}

		int width = imgA.getWidth();
		int height = imgA.getHeight();
		for (int y = 0; y < height; y++) {
			for (int x = 0; x < width; x++) {

				if (imgA.getRGB(x, y) != imgB.getRGB(x, y)) {

					Log.message("Image are different");
					return false;
				}
			}

		}

		return true;
	}

	/**
	 * verify hpyerlink functionality for folder level
	 * 
	 * @param markupname
	 * @param filepath1
	 * @return
	 * @throws FindFailed
	 * @throws UnsupportedFlavorException
	 * @throws IOException
	 * @throws AWTException
	 */
	public boolean VerifyfunctionalitySquareHyperWithFolderLevel(String markupname, String filepath1)
			throws FindFailed, UnsupportedFlavorException, IOException, AWTException {
		boolean flag = false;
		// driver.switchTo().defaultContent();
		SkySiteUtils.waitTill(8000);
		File fistemp = new File(PropertyReader.getProperty("tempScreenshot"));
		String filepathtemp = fistemp.getAbsolutePath().toString();
		String tempscreenshotpath = filepathtemp + "\\tempfolderlevel.PNG";
		Screen s = new Screen();

		Pattern p1 = new Pattern(tempscreenshotpath);
		SkySiteUtils.waitTill(8000);

		s.click(p1);
		Log.message("saved hyperlink is clicked");
		SkySiteUtils.waitTill(4000);
		WebElement imagelink = driver.findElement(By.id("linkImgDiv"));
		JavascriptExecutor executor = (JavascriptExecutor) driver;
		executor.executeScript("arguments[0].click();", imagelink);
		Log.message("image is clicked");
		SkySiteUtils.waitTill(4000);
		String windowHandle = driver.getWindowHandle();
		ArrayList tabs = new ArrayList(driver.getWindowHandles());
		System.out.println(tabs.size());

		driver.switchTo().window((String) tabs.get(0));
		SkySiteUtils.waitTill(5000);
		driver.switchTo().defaultContent();
		SkySiteUtils.waitTill(5000);
		driver.switchTo().frame(driver.findElement(By.id("myFrame")));
		SkySiteUtils.waitTill(9000);

		WebElement file = driver.findElement(By.xpath(".//*[@id='divGrid']/div[2]/table/tbody/tr[2]/td[2]/a[3]"));
		String filename = file.getText();
		Log.message("File name is " + filename);

		if (driver.findElement(By.xpath(".//*[@id='divGrid']/div[2]/table/tbody/tr[2]/td[2]/a[3]")).isDisplayed()
				&& filename.equals("1_EName.pdf")) {
			Log.message("Folder hyperlink works");
			return true;

		}

		else {
			Log.message("Folder hyperlink does not work");
			return false;
		}

	}
	/**
	 * verify hpyerlink functionality for sub  folder level
	 * 
	 * @param markupname
	 * @param filepath1
	 * @return
	 * @throws FindFailed
	 * @throws UnsupportedFlavorException
	 * @throws IOException
	 * @throws AWTException
	 */
	public boolean VerifyfunctionalitySquareHyperWithSubFolderLevel(String markupname, String filepath1)
			throws FindFailed, UnsupportedFlavorException, IOException, AWTException {
		boolean flag = false;
		// driver.switchTo().defaultContent();
		SkySiteUtils.waitTill(8000);
		File fistemp = new File(PropertyReader.getProperty("tempScreenshot"));
		String filepathtemp = fistemp.getAbsolutePath().toString();
		String tempscreenshotpath = filepathtemp + "\\tempfolderlevel.PNG";
		Screen s = new Screen();

		Pattern p1 = new Pattern(tempscreenshotpath);
		SkySiteUtils.waitTill(8000);

		s.click(p1);
		Log.message("saved hyperlink is clicked");
		SkySiteUtils.waitTill(4000);
		WebElement imagelink = driver.findElement(By.id("linkImgDiv"));
		JavascriptExecutor executor = (JavascriptExecutor) driver;
		executor.executeScript("arguments[0].click();", imagelink);
		Log.message("image is clicked");
		SkySiteUtils.waitTill(4000);
		String windowHandle = driver.getWindowHandle();
		ArrayList tabs = new ArrayList(driver.getWindowHandles());
		System.out.println(tabs.size());

		driver.switchTo().window((String) tabs.get(0));
		SkySiteUtils.waitTill(5000);
		driver.switchTo().defaultContent();
		SkySiteUtils.waitTill(5000);
		driver.switchTo().frame(driver.findElement(By.id("myFrame")));
		SkySiteUtils.waitTill(9000);

		WebElement file = driver.findElement(By.xpath(".//*[@id='divGrid']/div[2]/table/tbody/tr[2]/td[2]/a[3]"));
		String filename = file.getText();
		Log.message("File name is " + filename);

		if (driver.findElement(By.xpath(".//*[@id='divGrid']/div[2]/table/tbody/tr[2]/td[2]/a[3]")).isDisplayed()
				&& filename.equals("Contacts.csv")) {
			Log.message(" Sub Folder hyperlink works");
			return true;

		}

		else {
			Log.message(" Sub Folder hyperlink does not work");
			return false;
		}

	}

	/**
	 * MEthod to switch to viewer tab
	 * 
	 * @throws AWTException
	 * 
	 */
	public void switchToViewerTab() throws AWTException {
		String MainWindow = driver.getWindowHandle();
		Log.message(MainWindow);
		for (String childwindow : driver.getWindowHandles()) {
			driver.switchTo().window(childwindow);

		}
		Log.message("switched to child window");

		driver.switchTo().defaultContent();

	}

	/**
	 * MEthod to draw and verify hyperlink with folder level
	 * 
	 * @throws FindFailed
	 * @throws IOException
	 * 
	 */
	public boolean drawAndSaveHyperlinkWithSubFolderLevel(String markupname, String filepath1)
			throws FindFailed, IOException {
		boolean flag = false;
		Log.message("Switched to default content");
		SkySiteUtils.waitTill(5000);
		driver.switchTo().frame(driver.findElement(By.id("myFrame")));
		Log.message("Switched to frame");
		driver.findElement(By.xpath("//*[text()='SubFolderHyperlink']")).click();
		Log.message("Folder selected");
		SkySiteUtils.waitTill(4000);
		driver.findElement(By.xpath(".//*[@id='divGrid']/div[2]/table/tbody/tr[2]/td[2]/a[3]")).click();
		Log.message("File is selected");
		SkySiteUtils.waitTill(4000);
		String MainWindow = driver.getWindowHandle();
		Log.message(MainWindow);
		for (String childwindow : driver.getWindowHandles()) {
			driver.switchTo().window(childwindow);

		}
		Log.message("switched to child window");

		driver.switchTo().defaultContent();
		SkySiteUtils.waitTill(4000);
		// method to delete existing markup
		this.markupdeletebegin();
		SkySiteUtils.waitTill(4000);
		hyperlinktool.click();
		Log.message("Hyperlink tool is clicked");
		SkySiteUtils.waitForElement(driver, recthyperlink, 40);
		Actions act = new Actions(driver);
		act.moveToElement(recthyperlink).clickAndHold().build().perform();
		SkySiteUtils.waitTill(2000);
		act.click(recthyperlink).build().perform();
		Log.message("Rect Hyper Link button is clicked");
		SkySiteUtils.waitTill(2000);
		File fis = new File(PropertyReader.getProperty("Screenshotpath"));
		String filepath = fis.getAbsolutePath().toString();
		String Screenshot1 = filepath + "\\StartPointFolderLevel.png";
		String Screenshot2 = filepath + "\\EndpointFolderLevel.png";
		Screen s = new Screen();

		Pattern p1 = new Pattern(Screenshot1);

		Pattern p2 = new Pattern(Screenshot2);

		// s.click(p1);

		s.drag(p1);

		s.dropAt(p2);
		Log.message("image is drawn");
		SkySiteUtils.waitTill(4000);
		// driver.findElement(By.xpath(".//*[@id='hyperLinkFolderList']/ul/li[2]/div/a/div[3]")).click();
		driver.findElement(By.xpath(".//*[@id='hyperLinkFolderList']/ul/li/div/a/div[3]")).click();
		Log.message("Parent Folder is selected");
		SkySiteUtils.waitTill(4000);
		driver.findElement(By.xpath(".//*[@id='hyperLinkFolderList']/ul[1]/li/div/a/div[3]")).click();
		Log.message("Sub Folder is selected");
		// driver.findElement(By.xpath(".//*[@id='hyperLinkFolderList']/ul/li[2]/div/div[2]/h4/a")).click();

		SkySiteUtils.waitForElement(driver, okbuttonexternalhyperlink, 50);
		JavascriptExecutor executor = (JavascriptExecutor) driver;
		executor.executeScript("arguments[0].click();", okbuttonexternalhyperlink);
		Log.message("Ok button is clicked");

		SkySiteUtils.waitTill(4000);
		driver.findElement(By.xpath("//*[@class='leaflet-control-nav-home']")).click();
		Log.message("Home page is clicked");
		SkySiteUtils.waitForElement(driver, savebtn, 50);
		savebtn.click();
		Log.message("save button is clicked");
		SkySiteUtils.waitTill(4000);
		markupfield.sendKeys(markupname);
		Log.message("Enter markup name is " + markupname);
		SkySiteUtils.waitForElement(driver, savemarkupbtn, 40);
		savemarkupbtn.click();
		Log.message("savemarkupbtn is clicked");
		SkySiteUtils.waitTill(14000);
		driver.findElement(By.xpath(".//*[@id='imageViewer']/div[3]/div[4]/div/ul/li[2]/a")).click();
		Log.message("save button is clicked again");
		SkySiteUtils.waitTill(4000);

		String savedscreenshot1 = filepath1 + "\\BeforesavingSquareHyperLink.png";
		String savedscreenshot2 = filepath1 + "\\AfterSavingSquareHyperLink.png";
		// File fistemp=new File(PropertyReader.getProperty("tempScreenshot"));
		// String filepathtemp=fistemp.getAbsolutePath().toString();
		// String tempscreenshotpath=filepathtemp + "\\temp_Screenshot.png";
		File srcFiler = ((TakesScreenshot) driver).getScreenshotAs(OutputType.FILE);

		FileUtils.copyFile(srcFiler, new File(savedscreenshot1));

		Log.message("Screenshot taken of saved markup");
		SkySiteUtils.waitTill(4000);
		hyperlinkdropdown.click();
		Log.message("hyperlink markup dropdown clicked");
		SkySiteUtils.waitTill(4000);
		// for(WebElement markupitem:hperlinklist)
		// if(markupitem.getText().contains(markupname))
		// driver.findElement(By.xpath("//*[text()='"+markupname+"']")).click();
		driver.findElement(By.xpath(".//*[@id='btnHideAllHyperlinks']")).click();
		Log.message("hide all button clicked and image disappears");

		SkySiteUtils.waitTill(4000);
		showhyperlinks.click();
		Log.message("show all hyperlink clikced");
		SkySiteUtils.waitTill(4000);
		savebtn.click();
		Log.message("save button is clicked");
		SkySiteUtils.waitTill(4000);
		File srcFiler1 = ((TakesScreenshot) driver).getScreenshotAs(OutputType.FILE);

		FileUtils.copyFile(srcFiler1, new File(savedscreenshot2));
		Log.message("Screenshot taken after redrawing");

		BufferedImage imgA = ImageIO.read(new File(savedscreenshot1));

		// Thread.sleep(2000);

		BufferedImage imgB = ImageIO.read(new File(savedscreenshot2));

		// Thread.sleep(2000);

		Log.message("Checking the height and width of both the images are same?");

		if (imgA.getWidth() != imgB.getWidth() && imgA.getHeight() != imgB.getHeight()) {
			Log.message("image height width are different");
			return false;
		}

		int width = imgA.getWidth();
		int height = imgA.getHeight();
		for (int y = 0; y < height; y++) {
			for (int x = 0; x < width; x++) {

				if (imgA.getRGB(x, y) != imgB.getRGB(x, y)) {

					Log.message("Image are different");
					return false;
				}
			}

		}

		return true;
	}

	@FindBy(xpath = ".//*[@id='rotateMenuBtn']/i")
	WebElement rotatemenu;
	@FindBy(xpath = ".//*[@id='btnRotateAntiClockWise']")
	WebElement Rotateanticlockwisemenu;
	@FindBy(xpath = ".//*[@id='btnSaveRotation']")
	WebElement saveafterrotate;

	/**
	 * Method to verify 90 rotation getting saved properly
	 * 
	 * @throws IOException
	 * @throws InterruptedException 
	 */
	public boolean saveRotationofFile(String markupname, String filepath1) throws IOException, InterruptedException {
		boolean flag = false;
		Log.message("Switched to default content");
		SkySiteUtils.waitTill(5000);
		driver.switchTo().frame(driver.findElement(By.id("myFrame")));
		Log.message("Switched to frame");
		driver.findElement(By.xpath("//*[text()='Rotate90']")).click();
		Log.message("Folder selected");
		SkySiteUtils.waitTill(4000);
		driver.findElement(By.xpath(".//*[@id='divGrid']/div[2]/table/tbody/tr[2]/td[2]/a[3]")).click();
		Log.message("File is selected");
		SkySiteUtils.waitTill(4000);
		String MainWindow = driver.getWindowHandle();
		Log.message(MainWindow);
		for (String childwindow : driver.getWindowHandles()) {
			driver.switchTo().window(childwindow);

		}
		Log.message("switched to child window");

		driver.switchTo().defaultContent();
		SkySiteUtils.waitTill(4000);
		// method to delete existing markup
		this.markupdeletebegin();
		SkySiteUtils.waitTill(4000);
		WebElement viewer = driver.findElement(By.xpath(".//*[@id='imageViewer']/div[3]"));
		// File fistemp=new File(PropertyReader.getProperty("tempScreenshot"));
		// String filepathtemp=fistemp.getAbsolutePath().toString();
		// String tempscreenshotpath=filepathtemp + "\\temp_Screenshot.png";

		
		SkySiteUtils.waitTill(4000);
		rotatemenu.click();
		Log.message("Rotate tool is clicked");

		SkySiteUtils.waitTill(2000);
		WebElement aniclockicon = driver.findElement(By.id("btnRotateClockWise"));
		SkySiteUtils.waitForElement(driver, Rotateanticlockwisemenu, 40);

		Actions act = new Actions(driver);
		act.moveToElement(aniclockicon).click().build().perform();
		// JavascriptExecutor executor = (JavascriptExecutor)driver;
		// executor.executeScript("arguments[0].click();",Rotateanticlockwisemenu);
		SkySiteUtils.waitTill(2000);

		SkySiteUtils.waitTill(2000);
		saveafterrotate.click();
		Log.message("Save button is clicked");
		SkySiteUtils.waitTill(20000);

		

		File fistemp12 = new File(PropertyReader.getProperty("tempScreenshot"));
		String filepathtemp12 = fistemp12.getAbsolutePath().toString();
		String savedscreenshot1 = filepathtemp12 + "\\outputafterrotate.png";
		String savedscreenshot2 = filepath1 + "\\resultpic.png";
		SkySiteUtils.waitTill(4000);
		// String tempscreenshotpath=filepathtemp + "\\temp_Screenshot.png";
		File srcFiler = ((TakesScreenshot) driver).getScreenshotAs(OutputType.FILE);

		FileUtils.copyFile(srcFiler, new File(savedscreenshot2));

		Log.message("Screenshot taken of saved markup");
		SkySiteUtils.waitTill(4000);

		BufferedImage imgA = ImageIO.read(new File(savedscreenshot1));
		SkySiteUtils.waitTill(3000);
		BufferedImage imgB = ImageIO.read(new File(savedscreenshot2));

		Thread.sleep(2000);

		Log.message("Checking the height and width of both the images are same?");

		if (imgA.getWidth() != imgB.getWidth() && imgA.getHeight() != imgB.getHeight()) {
			Log.message("image height width are different");
			return false;
		}

		int width = imgA.getWidth();
		int height = imgA.getHeight();
		for (int x = 0; x < width; x++) {
			for (int y = 0; y < height; y++) {

				if (imgA.getRGB(x, y) != imgB.getRGB(x, y)) {

					Log.message("Image are different");
					return false;
				}
			}

		}

		return true;
	}
	/**Method to change the rotated file to default
	 * 
	 */
	public void changeFileToDefault() {
		SkySiteUtils.waitTill(4000);
		WebElement viewer = driver.findElement(By.xpath(".//*[@id='imageViewer']/div[3]"));
		// File fistemp=new File(PropertyReader.getProperty("tempScreenshot"));
		// String filepathtemp=fistemp.getAbsolutePath().toString();
		// String tempscreenshotpath=filepathtemp + "\\temp_Screenshot.png";

		
		SkySiteUtils.waitTill(4000);
		rotatemenu.click();
		Log.message("Rotate tool is clicked");

		SkySiteUtils.waitTill(2000);
		WebElement aniclockicon = driver.findElement(By.xpath(".//*[@id='btnRotateAntiClockWise']"));
		SkySiteUtils.waitForElement(driver, Rotateanticlockwisemenu, 40);

		Actions act = new Actions(driver);
		act.moveToElement(aniclockicon).click().build().perform();
		// JavascriptExecutor executor = (JavascriptExecutor)driver;
		// executor.executeScript("arguments[0].click();",Rotateanticlockwisemenu);
		SkySiteUtils.waitTill(2000);

		SkySiteUtils.waitTill(2000);
		saveafterrotate.click();
		Log.message("Save button is clicked");
		SkySiteUtils.waitTill(20000);
		Log.message("File is rotated and changed to default state successfully");

	}

	@FindBy(xpath = ".//*[@id='measureToolMenuBtn']/i")
	WebElement calibrator;
	@FindBy(xpath = ".//*[@id='calibrationCreate']/a/i")
	WebElement Calibratorruler;
	@FindBy(xpath = ".//*[@id='measurementSquareCreate']/a/i")
	WebElement squarecreation;

	/**
	 * Method to draw and save calibrator
	 * 
	 * @throws FindFailed
	 * @throws IOException
	 * @throws InterruptedException
	 * 
	 * 
	 */
	public boolean draWaANDSaveCalibrator(String markupname, String filepath1,String filepathforscreenshot)
			throws FindFailed, IOException, InterruptedException {
		boolean flag = false;
		Log.message("Switched to default content");
		SkySiteUtils.waitTill(5000);
		driver.switchTo().frame(driver.findElement(By.id("myFrame")));
		Log.message("Switched to frame");
		driver.findElement(By.xpath("//*[text()='ZCalibratorSaving']")).click();
		Log.message("Folder selected");
		SkySiteUtils.waitTill(4000);
		driver.findElement(By.xpath(".//*[@id='divGrid']/div[2]/table/tbody/tr[2]/td[2]/a[3]")).click();
		Log.message("File is selected");
		SkySiteUtils.waitTill(4000);
		SkySiteUtils.waitTill(4000);
		String MainWindow = driver.getWindowHandle();
		Log.message(MainWindow);
		for (String childwindow : driver.getWindowHandles()) {
			driver.switchTo().window(childwindow);

		}
		Log.message("switched to child window");

		driver.switchTo().defaultContent();
		SkySiteUtils.waitTill(4000);
		// method to delete existing markup
		this.markupdeletebegin();
		SkySiteUtils.waitTill(4000);
		calibrator.click();
		Log.message("Calibrator tool is clicked");
		SkySiteUtils.waitForElement(driver, Calibratorruler, 40);
		Actions act = new Actions(driver);
		act.moveToElement(Calibratorruler).clickAndHold().build().perform();
		SkySiteUtils.waitTill(2000);
		act.click(Calibratorruler).build().perform();
		Log.message("Calibrator button is clicked");
		SkySiteUtils.waitTill(2000);
		File fis = new File(PropertyReader.getProperty("Screenshotpath"));
		String filepath = fis.getAbsolutePath().toString();
		String Screenshot1 = filepath + "\\StartpointCalibrator.PNG";
		String Screenshot2 = filepath + "\\EndpointRect.png";
		Screen s = new Screen();

		Pattern p1 = new Pattern(Screenshot1);

		Pattern p2 = new Pattern(Screenshot2);

		s.drag(p1);

		s.dropAt(p2);
		Log.message("image is drawn");
		SkySiteUtils.waitTill(4000);

		driver.findElement(By.xpath(".//*[@id='txtMeasureUnitLength']")).sendKeys("6");

		Log.message("Length in feet is entered");
		driver.findElement(By.xpath(".//*[@id='txtSubMeasureUnitLength']")).sendKeys("4");
		Log.message("Length in inch is entered");
		driver.findElement(By.xpath(".//*[@id='editCalibrateRulerAnnotation']/div/div[4]/button[2]")).click();
		Log.message("ok is clicked");
		// driver.findElement(By.xpath(".//*[@id='hyperLinkFolderList']/ul/li[2]/div/a/div[3]")).click();
		driver.findElement(By.xpath(".//*[@id='measureToolMenuBtn']/i")).click();
		Log.message("measure tool button is clicked again");
		Actions act1 = new Actions(driver);
		act1.moveToElement(squarecreation).clickAndHold().build().perform();
		SkySiteUtils.waitTill(2000);
		act.click(squarecreation).build().perform();
		Log.message("Square Calibrator selected");
		String Screenshot4 = filepath + "\\CalibratorEndRect.png";
		String Screenshot5 = filepath + "\\CalibratorstartRect.png";
		Screen s1 = new Screen();

		Pattern p3 = new Pattern(Screenshot4);

		Pattern p4 = new Pattern(Screenshot5);

		s.click(p3);

		s.drag(p3);

		s.dropAt(p4);

		SkySiteUtils.waitTill(4000);
		driver.findElement(By.xpath("//*[@class='leaflet-control-nav-home']")).click();
		Log.message("Home page is clicked");
		SkySiteUtils.waitForElement(driver, savebtn, 50);
		savebtn.click();
		Log.message("save button is clicked");
		SkySiteUtils.waitTill(4000);
		markupfield.sendKeys(markupname);
		Log.message("Enter markup name is " + markupname);
		SkySiteUtils.waitForElement(driver, savemarkupbtn, 40);
		savemarkupbtn.click();
		Log.message("savemarkupbtn is clicked");
		SkySiteUtils.waitTill(14000);
		driver.findElement(By.xpath(".//*[@id='imageViewer']/div[3]/div[4]/div/ul/li[2]/a")).click();
		Log.message("save button is clicked again");
		SkySiteUtils.waitTill(7000);
		
		File fistemp = new File(PropertyReader.getProperty("tempforcalibrator"));
		String filepathtemp = fistemp.getAbsolutePath().toString();
		String savedscreenshot1 = filepathtemp + "\\TempSnapForCalibrator.png";
		String savedscreenshot2 = filepathforscreenshot + "\\Outputpic.png";
		SkySiteUtils.waitTill(7000);
		// String tempscreenshotpath=filepathtemp + "\\temp_Screenshot.png";
		File srcFiler = ((TakesScreenshot) driver).getScreenshotAs(OutputType.FILE);

		FileUtils.copyFile(srcFiler, new File(savedscreenshot2));

		Log.message("Screenshot taken of saved markup");
		SkySiteUtils.waitTill(4000);

		BufferedImage imgA = ImageIO.read(new File(savedscreenshot1));
		SkySiteUtils.waitTill(3000);
		BufferedImage imgB = ImageIO.read(new File(savedscreenshot2));

		Thread.sleep(2000);

		Log.message("Checking the height and width of both the images are same?");
		Thread.sleep(6000);
		
         //modifed
		if (imgA.getWidth() != imgB.getWidth() && imgA.getHeight() != imgB.getHeight()) {
			Log.message("image height width are different");
			return false;
		}

		int width = imgA.getWidth();
		int height = imgA.getHeight();
		for (int y = 0; y < height; y++) {
			for (int x = 0; x < width; x++) {

				if (imgA.getRGB(x, y) != imgB.getRGB(x, y)) {

					Log.message("Image are different");
					return false;
				}
			}

		}

		return true;
	/*	if (imgA.getWidth() != imgB.getWidth() && imgA.getHeight() != imgB.getHeight()) {
			Log.message("image height width are different");
			return false;
		}

		int width = imgA.getWidth();
		int height = imgA.getHeight();
		for (int x = 0; x < width; x++) {
			for (int y = 0; y < height; y++) {
				if (imgA.getRGB(x, y) != imgB.getRGB(x, y)) {

					Log.message("Image are different");
					return false;
				}
			}

		}

		return true;*/
	}

	/**
	 * Method to delete all the files from download path
	 * 
	 */
	public boolean Delete_Files_From_Folder(String Folder_Path) {
		try {
			SkySiteUtils.waitTill(5000);
			Log.message("Cleaning download folder!!! ");
			File file = new File(Folder_Path);
			String[] myFiles;
			if (file.isDirectory()) {
				myFiles = file.list();
				for (int i = 0; i < myFiles.length; i++) {
					File myFile = new File(file, myFiles[i]);
					myFile.delete();
					SkySiteUtils.waitTill(5000);
				}
				Log.message("Available Folders/Files are deleted from download folder successfully!!!");
			}
		} // end try
		catch (Exception e) {
			Log.message("Available Folders/Files are deleted from download folder successfully!!!");
		}
		return false;
	}

	/**
	 * Method to check if file is downloaded Scripted By : Tarak
	 * 
	 */
	public boolean isFileDownloaded(String downloadfolderpath, String Downloadfilename) {
		Log.message("Searching the downloaded file");
		String Filename = Downloadfilename;
		Log.message("Downloaded file name is " + Filename);
		File file = new File(downloadfolderpath);
		File[] Filearray = file.listFiles();
		SkySiteUtils.waitTill(9000);
		if (file.isDirectory()) {
			for (File Myfile : Filearray) {
				if (Myfile.getName().contains(Filename)) {
					Log.message("File is found");
					return true;
				}
			}
		}
		return false;
	}

	/*
	 * Method to compare two pdf
	 * 
	 */
	public boolean comparetwopdfFile(String downloadpath) throws IOException {
		File fistemp = new File(PropertyReader.getProperty("tempScreenshot"));
		String filepathtemp = fistemp.getAbsolutePath().toString();
		File test_file_1 = new File(filepathtemp + "\\1_Document.pdf");
		File test_file_2 = new File(downloadpath + "\\1_Document.pdf");

		boolean compareResult = FileUtils.contentEquals(test_file_1, test_file_2);
		Log.message("Are the files same? " + compareResult);
		if (compareResult == true)
			return true;
		else
			return false;
	}

	@FindBy(xpath = ".//*[@id='workspaceBar']/div[1]/div/ul[2]/li[7]/a")
	WebElement downloadmenu;
	@FindBy(xpath = ".//*[@id='aDownload']/a/span")
	WebElement downloadbutton;
	@FindBy(xpath = ".//*[@id='markupdownload']")
	WebElement showdrawingoption;
    @FindBy(xpath=".//*[@id='allmarkupdownload']")
    WebElement allmarkupoption;
	/**
	 * Method to download drawn calibrator
	 * 
	 * @throws IOException
	 * 
	 */
	public boolean downLoadCalibrator(String downloadpath, String filename) throws IOException {
		this.Delete_Files_From_Folder(downloadpath);
		SkySiteUtils.waitForElement(driver, downloadmenu, 50);
		downloadmenu.click();
		Log.message("Download menu is clicked");
		SkySiteUtils.waitForElement(driver, downloadbutton, 40);
		downloadbutton.click();
		Log.message("download button is clicked");
		SkySiteUtils.waitForElement(driver, showdrawingoption, 40);
		allmarkupoption.click();
		//showdrawingoption.click();
		Log.message("Show drawing option is clicked");
		SkySiteUtils.waitTill(20000);
		if (this.isFileDownloaded(downloadpath, filename) == true && this.comparetwopdfFile(downloadpath))
			return true;
		else
			return false;

	}

	/**
	 * Method to draw and save calibrator and ruler seperatly
	 * 
	 * @throws InterruptedException
	 * @throws FindFailed
	 * @throws IOException
	 * 
	 */
	public boolean saveCalibratorAndRulerSeperatly(String markupname, String markupname2, String filepath1)
			throws InterruptedException, FindFailed, IOException {

		boolean flag = false;
		Log.message("Switched to default content");
		SkySiteUtils.waitTill(5000);
		driver.switchTo().frame(driver.findElement(By.id("myFrame")));
		Log.message("Switched to frame");
		driver.findElement(By.xpath("//*[text()='CalibratorSeperateMarkup']")).click();
		Log.message("Folder selected");
		SkySiteUtils.waitTill(4000);
		driver.findElement(By.xpath(".//*[@id='divGrid']/div[2]/table/tbody/tr[2]/td[2]/a[3]")).click();
		Log.message("File is selected");
		SkySiteUtils.waitTill(4000);
		SkySiteUtils.waitTill(4000);
		String MainWindow = driver.getWindowHandle();
		Log.message(MainWindow);
		for (String childwindow : driver.getWindowHandles()) {
			driver.switchTo().window(childwindow);

		}
		Log.message("switched to child window");

		driver.switchTo().defaultContent();
		SkySiteUtils.waitTill(4000);
		// method to delete existing markup
		this.markupdeletebegin();
		SkySiteUtils.waitTill(4000);
		calibrator.click();
		Log.message("Calibrator tool is clicked");
		SkySiteUtils.waitForElement(driver, Calibratorruler, 40);
		Actions act = new Actions(driver);
		act.moveToElement(Calibratorruler).clickAndHold().build().perform();
		SkySiteUtils.waitTill(2000);
		act.click(Calibratorruler).build().perform();
		Log.message("Calibrator  ruler button is clicked");
		SkySiteUtils.waitTill(2000);

		File fis = new File(PropertyReader.getProperty("Screenshotpath"));
		String filepath = fis.getAbsolutePath().toString();
		String Screenshot1 = filepath + "\\StartpointRect.png";
		String Screenshot2 = filepath + "\\EndpointRect.png";
		Screen s = new Screen();

		Pattern p1 = new Pattern(Screenshot1);

		Pattern p2 = new Pattern(Screenshot2);

		s.drag(p1);

		s.dropAt(p2);
		Log.message("image is drawn");
		SkySiteUtils.waitTill(4000);

		driver.findElement(By.xpath(".//*[@id='txtMeasureUnitLength']")).sendKeys("6");

		Log.message("Length in feet is entered");
		driver.findElement(By.xpath(".//*[@id='txtSubMeasureUnitLength']")).sendKeys("4");
		Log.message("Length in inch is entered");
		driver.findElement(By.xpath(".//*[@id='editCalibrateRulerAnnotation']/div/div[4]/button[2]")).click();
		Log.message("ok is clicked");
		driver.findElement(By.xpath(".//*[@id='imageViewer']/div[3]/div[4]/div/ul/li[2]/a/i")).click();
		Log.message("Save button is clicked");
		SkySiteUtils.waitForElement(driver, markupfield, 50);
		markupfield.sendKeys(markupname);
		Log.message("Enter markup name is " + markupname);
		SkySiteUtils.waitForElement(driver, savemarkupbtn, 40);
		savemarkupbtn.click();
		Log.message("savemarkupbtn is clicked");
		SkySiteUtils.waitTill(14000);
		// driver.findElement(By.xpath(".//*[@id='hyperLinkFolderList']/ul/li[2]/div/a/div[3]")).click();
		driver.findElement(By.xpath(".//*[@id='measureToolMenuBtn']/i")).click();
		Log.message("measure tool button is clicked again");
		Actions act1 = new Actions(driver);
		act1.moveToElement(squarecreation).clickAndHold().build().perform();
		SkySiteUtils.waitTill(2000);
		act.click(squarecreation).build().perform();
		Log.message("Square Calibrator selected");
		String Screenshot4 = filepath + "\\CalibratorEndRect.png";
		String Screenshot5 = filepath + "\\CalibratorstartRect.png";
		Screen s1 = new Screen();

		Pattern p3 = new Pattern(Screenshot4);

		Pattern p4 = new Pattern(Screenshot5);

		s.click(p3);

		s.drag(p3);

		s.dropAt(p4);

		SkySiteUtils.waitTill(4000);
		driver.findElement(By.xpath("//*[@class='leaflet-control-nav-home']")).click();
		Log.message("Home page is clicked");
		SkySiteUtils.waitForElement(driver, savebtn, 50);
		savebtn.click();
		Log.message("save button is clicked");
		SkySiteUtils.waitTill(4000);
		markupfield.sendKeys(markupname2);
		Log.message("Enter markup name is " + markupname2);
		SkySiteUtils.waitForElement(driver, savemarkupbtn, 40);
		savemarkupbtn.click();
		Log.message("savemarkupbtn is clicked");
		SkySiteUtils.waitTill(14000);
		driver.findElement(By.xpath(".//*[@id='imageViewer']/div[3]/div[4]/div/ul/li[2]/a")).click();
		Log.message("save button is clicked again");
		SkySiteUtils.waitTill(4000);
		File fistemp = new File(PropertyReader.getProperty("tempScreenshot"));
		String filepathtemp = fistemp.getAbsolutePath().toString();
		String savedscreenshot1 = filepathtemp + "\\AfterSavingSquareHyperLink12.png";
		String savedscreenshot2 = filepath1 + "\\testoutputpic1.png";

		// String tempscreenshotpath=filepathtemp + "\\temp_Screenshot.png";
		File srcFiler = ((TakesScreenshot) driver).getScreenshotAs(OutputType.FILE);

		FileUtils.copyFile(srcFiler, new File(savedscreenshot2));

		Log.message("Screenshot taken of saved markup");
		SkySiteUtils.waitTill(4000);

		BufferedImage imgA = ImageIO.read(new File(savedscreenshot1));

		BufferedImage imgB = ImageIO.read(new File(savedscreenshot2));

		Thread.sleep(2000);

		Log.message("Checking the height and width of both the images are same?");

		if (imgA.getWidth() != imgB.getWidth() && imgA.getHeight() != imgB.getHeight()) {
			Log.message("image height width are different");
			return false;
		}

		int width = imgA.getWidth();
		int height = imgA.getHeight();
		for (int x = 0; x < width; x++) {
			for (int y = 0; y < height; y++) {
			
				if (imgA.getRGB(x, y) != imgB.getRGB(x, y)) {

					Log.message("Image are different");
					return false;
				}
			}

		}

		return true;
	}

	/**
	 * Method to draw multiple ruler on a single document.
	 * 
	 * @throws IOException
	 * @throws FindFailed
	 * @throws InterruptedException
	 * @throws AWTException 
	 * 
	 */
	public boolean drawMultipleRulerOnsinglePage(String markupname, String markupname2, String filepath1)
			throws IOException, FindFailed, InterruptedException, AWTException {

		boolean flag = false;
		Log.message("Switched to default content");
		SkySiteUtils.waitTill(5000);
		driver.switchTo().frame(driver.findElement(By.id("myFrame")));
		Log.message("Switched to frame");
		driver.findElement(By.xpath("//*[text()='WFolderRotation']")).click();
		Log.message("Folder selected");
		SkySiteUtils.waitTill(4000);
		driver.findElement(By.xpath(".//*[@id='divGrid']/div[2]/table/tbody/tr[2]/td[2]/a[3]")).click();
		Log.message("File is selected");
		SkySiteUtils.waitTill(4000);
		SkySiteUtils.waitTill(4000);
		String MainWindow = driver.getWindowHandle();
		Log.message(MainWindow);
		for (String childwindow : driver.getWindowHandles()) {
			driver.switchTo().window(childwindow);

		}
		Log.message("switched to child window");

		driver.switchTo().defaultContent();
		SkySiteUtils.waitTill(4000);
		// method to delete existing markup
		this.markupdeletebegin();
		SkySiteUtils.waitTill(4000);
		calibrator.click();
		Log.message("Calibrator tool is clicked");
		SkySiteUtils.waitForElement(driver, Calibratorruler, 40);
		Actions act = new Actions(driver);
		act.moveToElement(Calibratorruler).clickAndHold().build().perform();
		SkySiteUtils.waitTill(2000);
		act.click(Calibratorruler).build().perform();
		Log.message("Calibrator  ruler button is clicked");
		SkySiteUtils.waitTill(2000);

		File fis = new File(PropertyReader.getProperty("Screenshotpath"));
		String filepath = fis.getAbsolutePath().toString();
		String Screenshot1 = filepath + "\\StartpointRect.png";
		String Screenshot2 = filepath + "\\EndpointRect.png";
		Screen s = new Screen();

		Pattern p1 = new Pattern(Screenshot1);

		Pattern p2 = new Pattern(Screenshot2);

		s.drag(p1);

		s.dropAt(p2);
		Log.message("image is drawn");
		SkySiteUtils.waitTill(4000);

		driver.findElement(By.xpath(".//*[@id='txtMeasureUnitLength']")).sendKeys("681");

		Log.message("Length in feet is entered");
		driver.findElement(By.xpath(".//*[@id='txtSubMeasureUnitLength']")).sendKeys("9");
		Log.message("Length in inch is entered");
		driver.findElement(By.xpath(".//*[@id='editCalibrateRulerAnnotation']/div/div[4]/button[2]")).click();
		Log.message("ok is clicked");

		SkySiteUtils.waitTill(14000);
		// driver.findElement(By.xpath(".//*[@id='hyperLinkFolderList']/ul/li[2]/div/a/div[3]")).click();
		driver.findElement(By.xpath(".//*[@id='measureToolMenuBtn']/i")).click();
		Log.message("measure tool button is clicked again");
		Actions act1 = new Actions(driver);
		act1.moveToElement(squarecreation).clickAndHold().build().perform();
		SkySiteUtils.waitTill(2000);
		act.click(squarecreation).build().perform();
		Log.message("Square Calibrator selected");
		String Screenshot4 = filepath + "\\CalibratorEndRect.png";
		String Screenshot5 = filepath + "\\CalibratorstartRect.png";
		Screen s1 = new Screen();

		Pattern p3 = new Pattern(Screenshot4);

		Pattern p4 = new Pattern(Screenshot5);

		s.click(p3);

		s.drag(p3);

		s.dropAt(p4);

		SkySiteUtils.waitTill(4000);
		driver.findElement(By.xpath("//*[@class='leaflet-control-nav-home']")).click();
		Log.message("Home page is clicked");
		SkySiteUtils.waitForElement(driver, savebtn, 50);
		savebtn.click();
		Log.message("save button is clicked");
		SkySiteUtils.waitTill(4000);
		markupfield.sendKeys(markupname2);
		Log.message("Enter markup name is " + markupname2);
		SkySiteUtils.waitForElement(driver, savemarkupbtn, 40);
		savemarkupbtn.click();
		Log.message("savemarkupbtn is clicked");
		SkySiteUtils.waitTill(14000);
		// driver.findElement(By.xpath(".//*[@id='imageViewer']/div[3]/div[4]/div/ul/li[2]/a")).click();
		// Log.message("save button is clicked again");

		markupdropdown.click();
		Log.message("markup dropdown clicked");
		SkySiteUtils.waitTill(4000);
		for (WebElement markupitem : markuplist)
			if (markupitem.getText().contains(markupname))
				driver.findElement(By.xpath("//*[@data-original-title='" + markupname2 + "']")).click();

		Log.message("save markup clicked and image drawn disappears");
		driver.findElement(By.xpath(".//*[@id='imageViewer']/div[3]/div[4]/div/ul/li[2]/a")).click();
		Log.message("save button is clicked again");

		calibrator.click();
		Log.message("Calibrator tool is clicked");
		SkySiteUtils.waitForElement(driver, Calibratorruler, 40);
		Actions act12 = new Actions(driver);
		act12.moveToElement(Calibratorruler).clickAndHold().build().perform();
		SkySiteUtils.waitTill(2000);
		act12.click(Calibratorruler).build().perform();
		Log.message("Calibrator  ruler button is clicked");
		SkySiteUtils.waitTill(2000);
		File fis2 = new File(PropertyReader.getProperty("Screenshotpath"));
		String filepath2 = fis2.getAbsolutePath().toString();
		String Screenshotruler = filepath2 + "\\StartpointRuler.png";
		String Screenshot2ruler = filepath2 + "\\EndPointRuler.png";
		Screen s2 = new Screen();

		Pattern p5 = new Pattern(Screenshotruler);

		Pattern p6 = new Pattern(Screenshot2ruler);

		s2.drag(p5);

		s2.dropAt(p6);
		Log.message("image is drawn");
		SkySiteUtils.waitTill(4000);
		driver.findElement(By.xpath(".//*[@id='editCalibrateAnnotationRuler']/div/div[1]/div/label[2]")).click();
		Log.message("metric option is selected");
		SkySiteUtils.waitTill(5000);
		driver.findElement(By.xpath(".//*[@id='txtMeasureUnitLength']")).sendKeys("22");

		Log.message("Length in metre is entered");
		driver.findElement(By.xpath(".//*[@id='txtSubMeasureUnitLength']")).sendKeys("45");
		Log.message("Length in Centimetre is entered");
		driver.findElement(By.xpath(".//*[@id='editCalibrateRulerAnnotation']/div/div[4]/button[2]")).click();
		Log.message("ok is clicked");

		driver.findElement(By.xpath("//*[@class='leaflet-control-nav-home']")).click();
		Log.message("Home page is clicked");
		SkySiteUtils.waitForElement(driver, savebtn, 50);
		savebtn.click();
		Log.message("save button is clicked");
		SkySiteUtils.waitTill(4000);
		markupfield.sendKeys(markupname);
		Log.message("Enter markup name is " + markupname);

		SkySiteUtils.waitForElement(driver, savemarkupbtn, 40);
		savemarkupbtn.click();
		Log.message("savemarkupbtn is clicked");
		SkySiteUtils.waitTill(14000);
		SkySiteUtils.waitTill(14000);
		driver.findElement(By.xpath("btnHideAllGlobal")).click();
		Log.message("Hide all button is clicked");
		SkySiteUtils.waitTill(14000);
		Robot rb = new Robot();
		rb.keyPress(KeyEvent.VK_CONTROL);
		rb.keyPress(KeyEvent.VK_W);
		rb.keyRelease(KeyEvent.VK_W);
		rb.keyRelease(KeyEvent.VK_CONTROL);
		SkySiteUtils.waitTill(5000);
		ArrayList tabs = new ArrayList(driver.getWindowHandles());
		System.out.println(tabs.size());

		driver.switchTo().window((String) tabs.get(0));
		SkySiteUtils.waitTill(5000);
		driver.switchTo().defaultContent();
		Log.message("Switched to default content");
		SkySiteUtils.waitTill(5000);
		driver.switchTo().frame(driver.findElement(By.id("myFrame")));
		Log.message("Switched to frame");
		driver.findElement(By.xpath("//*[text()='WFolderRotation']")).click();
		Log.message("Folder selected");
		SkySiteUtils.waitTill(4000);
		driver.findElement(By.xpath(".//*[@id='divGrid']/div[2]/table/tbody/tr[2]/td[2]/a[3]")).click();
		Log.message("File is selected");
		SkySiteUtils.waitTill(5000);
		String MainWindow2 = driver.getWindowHandle();
		Log.message(MainWindow);
		for (String childwindow : driver.getWindowHandles()) {
			driver.switchTo().window(childwindow);

		}
		Log.message("switched to child window");

		driver.switchTo().defaultContent();
		driver.findElement(By.xpath(".//*[@id='btnViewAllGlobal']")).click();
		Log.message("View all button is clicked");
		SkySiteUtils.waitTill(5000);

		// driver.findElement(By.xpath(".//*[@id='btnViewAllGlobal']")).click();
		// Log.message("View all button is clicked");
		SkySiteUtils.waitTill(5000);
		//driver.findElement(By.xpath(".//*[@id='btnViewAllGlobal']")).click();
		//Log.message("View all button is clicked");
		SkySiteUtils.waitTill(5000);
		File fistemp = new File(PropertyReader.getProperty("tempScreenshot"));
		String filepathtemp = fistemp.getAbsolutePath().toString();
		String savedscreenshot1 = filepathtemp + "\\MutliRuler.png";
		String savedscreenshot2 = filepath1 + "\\Afterdrawnsnap.png";
		SkySiteUtils.waitTill(4000);
		// String tempscreenshotpath=filepathtemp + "\\temp_Screenshot.png";
		File srcFiler = ((TakesScreenshot) driver).getScreenshotAs(OutputType.FILE);

		FileUtils.copyFile(srcFiler, new File(savedscreenshot2));

		Log.message("Screenshot taken of saved markup");
		SkySiteUtils.waitTill(4000);

		BufferedImage imgA = ImageIO.read(new File(savedscreenshot1));
		SkySiteUtils.waitTill(3000);
		BufferedImage imgB = ImageIO.read(new File(savedscreenshot2));

		Thread.sleep(2000);

		Log.message("Checking the height and width of both the images are same?");

		if (imgA.getWidth() != imgB.getWidth() && imgA.getHeight() != imgB.getHeight()) {
			Log.message("image height width are different");
			return false;
		}

		int width = imgA.getWidth();
		int height = imgA.getHeight();
		for (int x = 0; x < width; x++) {
			for (int y = 0; y < height; y++) {

				if (imgA.getRGB(x, y) != imgB.getRGB(x, y)) {

					Log.message("Image are different");
					return false;
				}
			}

		}

		return true;
	}

	/**
	 * MEthod to verify measurement of drawn calibrator ruler can be changed
	 * 
	 * @throws FindFailed
	 * @throws IOException
	 * @throws InterruptedException
	 * 
	 */
	public boolean changeMeasurementRuler(String markupname, String filepath1)
			throws FindFailed, IOException, InterruptedException {
		boolean flag = false;
		Log.message("Switched to default content");
		SkySiteUtils.waitTill(5000);
		driver.switchTo().frame(driver.findElement(By.id("myFrame")));
		Log.message("Switched to frame");
		driver.findElement(By.xpath("//*[text()='FolderViewerTesting_DONotDelete']")).click();
		Log.message("Folder selected");
		SkySiteUtils.waitTill(4000);
		driver.findElement(By.xpath(".//*[@id='divGrid']/div[2]/table/tbody/tr[2]/td[2]/a[3]")).click();
		Log.message("File is selected");
		SkySiteUtils.waitTill(4000);
		SkySiteUtils.waitTill(4000);
		String MainWindow = driver.getWindowHandle();
		Log.message(MainWindow);
		for (String childwindow : driver.getWindowHandles()) {
			driver.switchTo().window(childwindow);

		}
		Log.message("switched to child window");

		driver.switchTo().defaultContent();
		SkySiteUtils.waitTill(4000);
		// method to delete existing markup
		this.markupdeletebegin();
		SkySiteUtils.waitTill(4000);
		calibrator.click();
		Log.message("Calibrator tool is clicked");
		SkySiteUtils.waitForElement(driver, Calibratorruler, 40);
		Actions act = new Actions(driver);
		act.moveToElement(Calibratorruler).clickAndHold().build().perform();
		SkySiteUtils.waitTill(2000);
		act.click(Calibratorruler).build().perform();
		Log.message("Calibrator button is clicked");
		SkySiteUtils.waitTill(2000);
		File fis = new File(PropertyReader.getProperty("Screenshotpath"));
		String filepath = fis.getAbsolutePath().toString();
		String Screenshot1 = filepath + "\\StartPointFolderLevel.png";
		String Screenshot2 = filepath + "\\EndpointFolderLevel.png";
		Screen s = new Screen();

		Pattern p1 = new Pattern(Screenshot1);

		Pattern p2 = new Pattern(Screenshot2);

		// s.click(p1);

		s.drag(p1);

		s.dropAt(p2);
		Log.message("image is drawn");
		SkySiteUtils.waitTill(4000);

		driver.findElement(By.xpath(".//*[@id='txtMeasureUnitLength']")).sendKeys("6");

		Log.message("Length in feet is entered");
		driver.findElement(By.xpath(".//*[@id='txtSubMeasureUnitLength']")).sendKeys("4");
		Log.message("Length in inch is entered");
		driver.findElement(By.xpath(".//*[@id='editCalibrateRulerAnnotation']/div/div[4]/button[2]")).click();
		Log.message("ok is clicked");
		SkySiteUtils.waitTill(15000);
		File fistemp = new File(PropertyReader.getProperty("tempScreenshot"));
		String filepathtemp = fistemp.getAbsolutePath().toString();
		String savedscreenshot1 = filepathtemp + "\\tmpRulerdefaultMeasurement.PNG";
		Screen sc1 = new Screen();
		Pattern p = new Pattern(savedscreenshot1);
		SkySiteUtils.waitTill(6000);
		sc1.click(p);
		Log.message("drawn rulerv is clicked");
		SkySiteUtils.waitTill(5000);
		//driver.findElement(By.xpath(".//*[@id='editCalibrateAnnotationRuler']/div/div[1]/div/label[2]")).click();
		driver.findElement(By.xpath("//*[@for='radioMeasureUnitMetric']")).click();
		Log.message("metric option is selected");
		SkySiteUtils.waitTill(5000);
		WebElement field1 = driver.findElement(By.xpath(".//*[@id='txtMeasureUnitLength']"));
		field1.clear();
		field1.sendKeys("9");
		Log.message("Length in metre is entered");
		WebElement field2 = driver.findElement(By.xpath(".//*[@id='txtSubMeasureUnitLength']"));
		field2.clear();
		field2.sendKeys("4");
		Log.message("Length in Centimetre is entered");
		driver.findElement(By.xpath(".//*[@id='editCalibrateRulerAnnotation']/div/div[4]/button[2]")).click();
		Log.message("ok is clicked");
        SkySiteUtils.waitTill(4000);
		driver.findElement(By.xpath("//*[@class='leaflet-control-nav-home']")).click();
		Log.message("Home page is clicked");
	     SkySiteUtils.waitTill(4000);
		SkySiteUtils.waitForElement(driver, savebtn, 50);
		savebtn.click();
		Log.message("save button is clicked");
		SkySiteUtils.waitTill(4000);
		markupfield.sendKeys(markupname);
		Log.message("Enter markup name is " + markupname);

		SkySiteUtils.waitForElement(driver, savemarkupbtn, 40);
		savemarkupbtn.click();
		Log.message("savemarkupbtn is clicked");
		SkySiteUtils.waitTill(14000);
		File fistemp2 = new File(PropertyReader.getProperty("tempScreenshot"));
		String filepathtemp2 = fistemp2.getAbsolutePath().toString();
		String savedscreenshot12 = filepathtemp2 + "\\TestEditpic.png";
		String savedscreenshot2 = filepath1 + "\\Outputscreenshot.png";
		SkySiteUtils.waitTill(4000);
		// String tempscreenshotpath=filepathtemp + "\\temp_Screenshot.png";
		File srcFiler = ((TakesScreenshot) driver).getScreenshotAs(OutputType.FILE);

		FileUtils.copyFile(srcFiler, new File(savedscreenshot2));

		Log.message("Screenshot taken of saved markup");
		SkySiteUtils.waitTill(4000);

		BufferedImage imgA = ImageIO.read(new File(savedscreenshot12));
		SkySiteUtils.waitTill(3000);
		BufferedImage imgB = ImageIO.read(new File(savedscreenshot2));

		Thread.sleep(2000);

		Log.message("Checking the height and width of both the images are same?");

		if (imgA.getWidth() != imgB.getWidth() && imgA.getHeight() != imgB.getHeight()) {
			Log.message("image height width are different");
			return false;
		}
        ///modification
		int width = imgA.getWidth();
		int height = imgA.getHeight();
		for (int y = 0; y < height; y++) {
			for (int x = 0; x < width; x++) {

				if (imgA.getRGB(x, y) != imgB.getRGB(x, y)) {

					Log.message("Image are different");
					return false;
				}
			}

		}

		return true;

		/*int width = imgA.getWidth();
		int height = imgA.getHeight();
		for (int x = 0; x < width; x++) {
		for (int y = 0; y < height; y++) {
		

				if (imgA.getRGB(x, y) != imgB.getRGB(x, y)) {

					Log.message("Image are different");
					return false;
				}
			}

		}

		return true;*/
	}

	//@FindBy(xpath = "//*[text()='Delete']")
	@FindBy(xpath="//*[@class='btn btn-default btn-sm pull-left deleteLayer']")
	WebElement deleteoption;

	/**
	 * Method to delete drawn ruler
	 * 
	 * @throws FindFailed
	 * @throws IOException
	 * @throws InterruptedException
	 * 
	 */
	public boolean deleteDrawnRuler(String filepath1) throws FindFailed, IOException, InterruptedException {
		boolean flag = false;
		Log.message("Switched to default content");
		SkySiteUtils.waitTill(5000);
		driver.switchTo().frame(driver.findElement(By.id("myFrame")));
		Log.message("Switched to frame");
		driver.findElement(By.xpath("//*[text()='FolderViewerTesting_DONotDelete']")).click();
		Log.message("Folder selected");
		SkySiteUtils.waitTill(4000);
		driver.findElement(By.xpath(".//*[@id='divGrid']/div[2]/table/tbody/tr[2]/td[2]/a[3]")).click();
		Log.message("File is selected");
		SkySiteUtils.waitTill(4000);
		SkySiteUtils.waitTill(4000);
		String MainWindow = driver.getWindowHandle();
		Log.message(MainWindow);
		for (String childwindow : driver.getWindowHandles()) {
			driver.switchTo().window(childwindow);

		}
		Log.message("switched to child window");

		driver.switchTo().defaultContent();
		SkySiteUtils.waitTill(4000);
		// method to delete existing markup
		this.markupdeletebegin();
		SkySiteUtils.waitTill(4000);
		calibrator.click();
		Log.message("Calibrator tool is clicked");
		SkySiteUtils.waitForElement(driver, Calibratorruler, 40);
		Actions act = new Actions(driver);
		act.moveToElement(Calibratorruler).clickAndHold().build().perform();
		SkySiteUtils.waitTill(2000);
		act.click(Calibratorruler).build().perform();
		Log.message("Calibrator button is clicked");
		SkySiteUtils.waitTill(2000);
		File fis = new File(PropertyReader.getProperty("Screenshotpath"));
		String filepath = fis.getAbsolutePath().toString();
		String Screenshot1 = filepath + "\\StartPointFolderLevel.png";
		String Screenshot2 = filepath + "\\EndpointFolderLevel.png";
		Screen s = new Screen();

		Pattern p1 = new Pattern(Screenshot1);

		Pattern p2 = new Pattern(Screenshot2);

		// s.click(p1);

		s.drag(p1);

		s.dropAt(p2);
		Log.message("image is drawn");
		
		SkySiteUtils.waitTill(4000);

		driver.findElement(By.xpath(".//*[@id='txtMeasureUnitLength']")).sendKeys("6");

		Log.message("Length in feet is entered");
		driver.findElement(By.xpath(".//*[@id='txtSubMeasureUnitLength']")).sendKeys("4");
		Log.message("Length in inch is entered");
		driver.findElement(By.xpath(".//*[@id='editCalibrateRulerAnnotation']/div/div[4]/button[2]")).click();
		Log.message("ok is clicked");
		SkySiteUtils.waitTill(5000);
		driver.findElement(By.xpath(".//*[@id='imageViewer']/div[3]/div[1]/div/ul/li[3]/a/i")).click();
		Log.message("Home button is clicked");
		SkySiteUtils.waitTill(15000);
		File fistemp = new File(PropertyReader.getProperty("tempScreenshot"));
		String filepathtemp = fistemp.getAbsolutePath().toString();
		String savedscreenshot1 = filepathtemp + "\\tmpRulerdefaultMeasurement.PNG";
		Screen sc1 = new Screen();
		Pattern p = new Pattern(savedscreenshot1);
		SkySiteUtils.waitTill(9000);
		sc1.click(p);
		Log.message("drawn rulerv is clicked");

		SkySiteUtils.waitTill(9000);
		deleteoption.click();
		Log.message("Delete option is clicked");
		SkySiteUtils.waitTill(6000);
	driver.findElement(By.xpath(".//*[@id='imageViewer']/div[3]/div[1]/div/ul/li[3]/a/i")).click();
	Log.message("Home button is clicked");
		// driver.findElement(By.xpath(".//*[@id='imageViewer']/div[3]/div[1]/div/ul/li[3]/a/i")).click();
		// Log.message("Home button is clicked");
		// SkySiteUtils.waitTill(9000);
		File fistemp2 = new File(PropertyReader.getProperty("tempScreenshot"));
		String filepathtemp2 = fistemp2.getAbsolutePath().toString();
		String savedscreenshot12 = filepathtemp2 + "\\outputpic1.png";
		String savedscreenshot2 = filepath1 + "\\Deleterulercnap.png";
		SkySiteUtils.waitTill(4000);
		// String tempscreenshotpath=filepathtemp + "\\temp_Screenshot.png";
		File srcFiler = ((TakesScreenshot) driver).getScreenshotAs(OutputType.FILE);

		FileUtils.copyFile(srcFiler, new File(savedscreenshot2));

		Log.message("Screenshot taken of saved markup");
		SkySiteUtils.waitTill(4000);

		BufferedImage imgA = ImageIO.read(new File(savedscreenshot12));
		SkySiteUtils.waitTill(3000);
		BufferedImage imgB = ImageIO.read(new File(savedscreenshot2));

		Thread.sleep(2000);

		Log.message("Checking the height and width of both the images are same?");

		if (imgA.getWidth() != imgB.getWidth() && imgA.getHeight() != imgB.getHeight()) {
			Log.message("image height width are different");
			return false;
		}

		int width = imgA.getWidth();
		int height = imgA.getHeight();
		for (int y = 0; y < height; y++) {
			for (int x = 0; x < width; x++) {

				if (imgA.getRGB(x, y) != imgB.getRGB(x, y)) {

					Log.message("Image are different");
					return false;
				}
			}

		}

		return true;

	}

	@FindBy(xpath = ".//*[@id='measurementCircleCreate']/a/i")
	WebElement circletool;

	/**
	 * Method Verify whether unit of all the measurment calibrator is getting
	 * changed or not with respect to Active calibrator ruler
	 * 
	 * @throws IOException
	 * @throws FindFailed
	 * @throws InterruptedException
	 * @throws AWTException
	 * 
	 */
	public boolean MultiCalibratorTakingActiveRulerMeasurementSize(String markupname, String markupname2,
			String filepath1) throws IOException, FindFailed, InterruptedException, AWTException {

		boolean flag = false;
		Log.message("Switched to default content");
		SkySiteUtils.waitTill(5000);
		driver.switchTo().frame(driver.findElement(By.id("myFrame")));
		Log.message("Switched to frame");
		driver.findElement(By.xpath("//*[text()='ViewerTestingMultiCalibrator']")).click();
		Log.message("Folder selected");
		SkySiteUtils.waitTill(4000);
		driver.findElement(By.xpath(".//*[@id='divGrid']/div[2]/table/tbody/tr[2]/td[2]/a[3]")).click();
		Log.message("File is selected");
		SkySiteUtils.waitTill(4000);
		SkySiteUtils.waitTill(4000);
		String MainWindow = driver.getWindowHandle();
		Log.message(MainWindow);
		for (String childwindow : driver.getWindowHandles()) {
			driver.switchTo().window(childwindow);

		}
		Log.message("switched to child window");

		driver.switchTo().defaultContent();
		SkySiteUtils.waitTill(4000);
		// method to delete existing markup
		this.markupdeletebegin();
		SkySiteUtils.waitTill(4000);
		calibrator.click();
		Log.message("Calibrator tool is clicked");
		SkySiteUtils.waitForElement(driver, Calibratorruler, 40);
		Actions act = new Actions(driver);
		act.moveToElement(Calibratorruler).clickAndHold().build().perform();
		SkySiteUtils.waitTill(2000);
		act.click(Calibratorruler).build().perform();
		Log.message("Calibrator  ruler button is clicked");
		SkySiteUtils.waitTill(2000);

		File fis = new File(PropertyReader.getProperty("Screenshotpath"));
		String filepath = fis.getAbsolutePath().toString();
		String Screenshot1 = filepath + "\\StartpointRect.png";
		String Screenshot2 = filepath + "\\EndpointRect.png";
		Screen s = new Screen();

		Pattern p1 = new Pattern(Screenshot1);

		Pattern p2 = new Pattern(Screenshot2);

		s.drag(p1);

		s.dropAt(p2);
		Log.message("image is drawn");
		SkySiteUtils.waitTill(4000);

		driver.findElement(By.xpath(".//*[@id='txtMeasureUnitLength']")).sendKeys("6");

		Log.message("Length in feet is entered");
		driver.findElement(By.xpath(".//*[@id='txtSubMeasureUnitLength']")).sendKeys("4");
		Log.message("Length in inch is entered");
		driver.findElement(By.xpath(".//*[@id='editCalibrateRulerAnnotation']/div/div[4]/button[2]")).click();
		Log.message("ok is clicked");

		SkySiteUtils.waitTill(14000);
		// driver.findElement(By.xpath(".//*[@id='hyperLinkFolderList']/ul/li[2]/div/a/div[3]")).click();
		driver.findElement(By.xpath(".//*[@id='measureToolMenuBtn']/i")).click();
		Log.message("measure tool button is clicked again");
		Actions act1 = new Actions(driver);
		act1.moveToElement(squarecreation).clickAndHold().build().perform();
		SkySiteUtils.waitTill(2000);
		act.click(squarecreation).build().perform();
		Log.message("Square Calibrator selected");
		String Screenshot4 = filepath + "\\CalibratorEndRect.png";
		String Screenshot5 = filepath + "\\CalibratorstartRect.png";
		Screen s1 = new Screen();

		Pattern p3 = new Pattern(Screenshot4);

		Pattern p4 = new Pattern(Screenshot5);

		s.click(p3);

		s.drag(p3);

		s.dropAt(p4);

		SkySiteUtils.waitTill(4000);
		driver.findElement(By.xpath(".//*[@id='measureToolMenuBtn']/i")).click();
		Log.message("measure tool button is clicked again");
		Actions act12 = new Actions(driver);
		act12.moveToElement(circletool).clickAndHold().build().perform();
		SkySiteUtils.waitTill(2000);
		act.click(circletool).build().perform();
		Log.message("Circle Calibrator selected");
		String Screenshotcircle = filepath + "\\tempcirclestart.PNG";
		String Screenshotcircleend = filepath + "\\CircleDrawEnd.PNG";
		Screen sob = new Screen();

		Pattern pcircle = new Pattern(Screenshotcircle);

		Pattern pcircleend = new Pattern(Screenshotcircleend);

		sob.click(pcircle);

		sob.drag(pcircle);

		sob.dropAt(pcircleend);
		SkySiteUtils.waitTill(4000);
		driver.findElement(By.xpath("//*[@class='leaflet-control-nav-home']")).click();
		Log.message("Home page is clicked");
		SkySiteUtils.waitForElement(driver, savebtn, 50);
		savebtn.click();
		Log.message("save button is clicked");
		SkySiteUtils.waitTill(4000);
		markupfield.sendKeys(markupname2);
		Log.message("Enter markup name is " + markupname2);
		SkySiteUtils.waitForElement(driver, savemarkupbtn, 40);
		savemarkupbtn.click();
		Log.message("savemarkupbtn is clicked");
		SkySiteUtils.waitTill(14000);
		// driver.findElement(By.xpath(".//*[@id='imageViewer']/div[3]/div[4]/div/ul/li[2]/a")).click();
		// Log.message("save button is clicked again");

		markupdropdown.click();
		Log.message("markup dropdown clicked");
		SkySiteUtils.waitTill(4000);
		for (WebElement markupitem : markuplist)
			if (markupitem.getText().contains(markupname))
				driver.findElement(By.xpath("//*[@data-original-title='" + markupname2 + "']")).click();

		Log.message("save markup clicked and image drawn disappears");
		driver.findElement(By.xpath(".//*[@id='imageViewer']/div[3]/div[4]/div/ul/li[2]/a")).click();
		Log.message("save button is clicked again");

		calibrator.click();
		Log.message("Calibrator tool is clicked");
		SkySiteUtils.waitForElement(driver, Calibratorruler, 40);
		Actions act123 = new Actions(driver);
		act123.moveToElement(Calibratorruler).clickAndHold().build().perform();
		SkySiteUtils.waitTill(2000);
		act123.click(Calibratorruler).build().perform();
		Log.message("Calibrator  ruler button is clicked");
		SkySiteUtils.waitTill(2000);
		File fis2 = new File(PropertyReader.getProperty("Screenshotpath"));
		String filepath2 = fis2.getAbsolutePath().toString();
		String Screenshotruler = filepath2 + "\\StartpointRuler.png";
		String Screenshot2ruler = filepath2 + "\\EndPointRuler.png";
		Screen s2 = new Screen();

		Pattern p5 = new Pattern(Screenshotruler);

		Pattern p6 = new Pattern(Screenshot2ruler);

		s2.drag(p5);

		s2.dropAt(p6);
		Log.message("image is drawn");
		SkySiteUtils.waitTill(4000);
		driver.findElement(By.xpath(".//*[@id='editCalibrateAnnotationRuler']/div/div[1]/div/label[2]")).click();
		Log.message("metric option is selected");
		SkySiteUtils.waitTill(5000);
		driver.findElement(By.xpath(".//*[@id='txtMeasureUnitLength']")).sendKeys("9");

		Log.message("Length in metre is entered");
		driver.findElement(By.xpath(".//*[@id='txtSubMeasureUnitLength']")).sendKeys("4");
		Log.message("Length in Centimetre is entered");
		driver.findElement(By.xpath(".//*[@id='editCalibrateRulerAnnotation']/div/div[4]/button[2]")).click();
		Log.message("ok is clicked");

		driver.findElement(By.xpath("//*[@class='leaflet-control-nav-home']")).click();
		Log.message("Home page is clicked");
		SkySiteUtils.waitForElement(driver, savebtn, 50);
		savebtn.click();
		Log.message("save button is clicked");
		SkySiteUtils.waitTill(4000);
		markupfield.sendKeys(markupname);
		Log.message("Enter markup name is " + markupname);

		SkySiteUtils.waitForElement(driver, savemarkupbtn, 40);
		savemarkupbtn.click();
		Log.message("savemarkupbtn is clicked");
		SkySiteUtils.waitTill(14000);
		driver.findElement(By.xpath(".//*[@id='btnViewAllGlobal']")).click();
		Log.message("View all button is clicked");
		SkySiteUtils.waitTill(5000);
		Robot rb = new Robot();
		rb.keyPress(KeyEvent.VK_CONTROL);
		rb.keyPress(KeyEvent.VK_W);
		rb.keyRelease(KeyEvent.VK_W);
		rb.keyRelease(KeyEvent.VK_CONTROL);
		SkySiteUtils.waitTill(5000);
		ArrayList tabs = new ArrayList(driver.getWindowHandles());
		System.out.println(tabs.size());

		driver.switchTo().window((String) tabs.get(0));
		SkySiteUtils.waitTill(5000);
		driver.switchTo().defaultContent();
		Log.message("Switched to default content");
		SkySiteUtils.waitTill(5000);
		driver.switchTo().frame(driver.findElement(By.id("myFrame")));
		Log.message("Switched to frame");
		driver.findElement(By.xpath("//*[text()='ViewerTestingMultiCalibrator']")).click();
		Log.message("Folder selected");
		SkySiteUtils.waitTill(4000);
		driver.findElement(By.xpath(".//*[@id='divGrid']/div[2]/table/tbody/tr[2]/td[2]/a[3]")).click();
		Log.message("File is selected");
		SkySiteUtils.waitTill(5000);
		String MainWindow2 = driver.getWindowHandle();
		Log.message(MainWindow);
		for (String childwindow : driver.getWindowHandles()) {
			driver.switchTo().window(childwindow);

		}
		Log.message("switched to child window");

		driver.switchTo().defaultContent();
		driver.findElement(By.xpath(".//*[@id='btnViewAllGlobal']")).click();
		Log.message("View all button is clicked");
		File fistemp = new File(PropertyReader.getProperty("tempScreenshot"));
		String filepathtemp = fistemp.getAbsolutePath().toString();
		String savedscreenshot1 = filepathtemp + "\\Afterdrawnsnap.png";
		String savedscreenshot2 = filepath1 + "\\multicalibrator.png";
		SkySiteUtils.waitTill(4000);
		// String tempscreenshotpath=filepathtemp + "\\temp_Screenshot.png";
		File srcFiler = ((TakesScreenshot) driver).getScreenshotAs(OutputType.FILE);

		FileUtils.copyFile(srcFiler, new File(savedscreenshot2));

		Log.message("Screenshot taken of saved markup");
		SkySiteUtils.waitTill(4000);

		BufferedImage imgA = ImageIO.read(new File(savedscreenshot1));
		SkySiteUtils.waitTill(3000);
		BufferedImage imgB = ImageIO.read(new File(savedscreenshot2));

		Thread.sleep(2000);

		Log.message("Checking the height and width of both the images are same?");

		if (imgA.getWidth() != imgB.getWidth() && imgA.getHeight() != imgB.getHeight()) {
			Log.message("image height width are different");
			return false;
		}

		int width = imgA.getWidth();
		int height = imgA.getHeight();
		
		for (int x = 0; x < width; x++)
		for (int y = 0; y < height; y++) {
			 {

				if (imgA.getRGB(x, y) != imgB.getRGB(x, y)) {

					Log.message("Image are different");
					return false;
				}
			}

		}

		return true;
	}

	/**
	 * Method to draw square annotation on zooming
	 * 
	 * @param markup
	 * @param filepath1
	 * @return
	 * @throws FindFailed
	 * @throws InterruptedException
	 * @throws IOException
	 */
	public boolean drawSquareAnnotationOnZooming(String markup, String filepath1)
			throws FindFailed, InterruptedException, IOException {
		boolean flag = false;
		Log.message("Switched to default content");
		SkySiteUtils.waitTill(5000);

		String MainWindow = driver.getWindowHandle();
		Log.message(MainWindow);
		for (String childwindow : driver.getWindowHandles()) {
			driver.switchTo().window(childwindow);

		}
		Log.message("switched to child window");

		driver.switchTo().defaultContent();
		SkySiteUtils.waitTill(4000);
		// method to delete existing markup
		this.markupdeletebegin();
		for (int i = 1; i <= 2; i++) {
			driver.findElement(By.xpath(".//*[@id='imageViewer']/div[3]/div[1]/div/ul/li[1]/a/i")).click();
			Log.message("zoom button is pressed " + i + "time");
			SkySiteUtils.waitTill(3000);
		}
		SkySiteUtils.waitTill(4000);
		shapemenubtn.click();
		Log.message("Shape menu button is clicked");
		SkySiteUtils.waitTill(3000);
		Actions act = new Actions(driver);
		act.moveToElement(drawrectangle).clickAndHold(drawrectangle).build().perform();
		SkySiteUtils.waitTill(2000);
		act.click().build().perform();
		Log.message("draw rectangle button is clicked");
		SkySiteUtils.waitTill(2000);
		File fis = new File(PropertyReader.getProperty("Screenshotpath"));
		String filepath = fis.getAbsolutePath().toString();
		String Screenshot1 = filepath + "\\StartPointRectZoom.png";
		String Screenshot2 = filepath + "\\EndPointRectZoom.png";
		Screen s = new Screen();

		Pattern p1 = new Pattern(Screenshot1);

		Pattern p2 = new Pattern(Screenshot2);

		s.click(p1);

		s.drag(p1);

		s.dropAt(p2);
		Log.message("image is drawn");
		SkySiteUtils.waitTill(4000);
		savebtn.click();
		Log.message("save button is clicked");
		SkySiteUtils.waitTill(4000);
		markupfield.sendKeys(markup);
		Log.message("Enter markup name is " + markup);
		SkySiteUtils.waitForElement(driver, savemarkupbtn, 40);
		savemarkupbtn.click();
		Log.message("savemarkupbtn is clicked");
		SkySiteUtils.waitTill(14000);
		driver.findElement(By.xpath(".//*[@id='imageViewer']/div[3]/div[4]/div/ul/li[2]/a")).click();
		Log.message("save button is clicked again");
		SkySiteUtils.waitTill(4000);
		// File fis1=new File(PropertyReader.getProperty("SaveScreenshot"));
		// String filepath1=fis1.getAbsolutePath().toString();
		String savedscreenshot1 = filepath1 + "\\BeforesavingmarkupSquarezoom.png";
		String savedscreenshot2 = filepath1 + "\\AfterSavingMarkerSquarezoom.png";
		File srcFiler = ((TakesScreenshot) driver).getScreenshotAs(OutputType.FILE);

		FileUtils.copyFile(srcFiler, new File(savedscreenshot1));

		Log.message("Screenshot taken of saved markup");
		SkySiteUtils.waitTill(4000);
		markupdropdown.click();
		Log.message("markup dropdown clicked");
		SkySiteUtils.waitTill(4000);
		for (WebElement markupitem : markuplist)
			if (markupitem.getText().contains(markup))
				driver.findElement(By.xpath("//*[@data-original-title='" + markup + "']")).click();

		Log.message("save markup clicked and image drawn disappears");
		SkySiteUtils.waitTill(4000);
		SkySiteUtils.waitTill(4000);
		for (WebElement markupitem : markuplist)
			if (markupitem.getText().contains(markup))
				driver.findElement(By.xpath("//*[@data-original-title='" + markup + "']")).click();

		// showallmarkup.click();
		Log.message("Markup is displayed");
		SkySiteUtils.waitTill(4000);
		savebtn.click();
		Log.message("save button is clicked");
		SkySiteUtils.waitTill(4000);
		File srcFiler1 = ((TakesScreenshot) driver).getScreenshotAs(OutputType.FILE);

		FileUtils.copyFile(srcFiler1, new File(savedscreenshot2));
		Log.message("Screenshot taken after redrawing");

		BufferedImage imgA = ImageIO.read(new File(savedscreenshot1));

		// Thread.sleep(2000);

		BufferedImage imgB = ImageIO.read(new File(savedscreenshot2));

		// Thread.sleep(2000);

		Log.message("Checking the height and width of both the images are same?");

		if (imgA.getWidth() != imgB.getWidth() && imgA.getHeight() != imgB.getHeight()) {
			Log.message("image height width are different");
			return false;
		}

		int width = imgA.getWidth();
		int height = imgA.getHeight();
		for (int y = 0; y < height; y++) {
			for (int x = 0; x < width; x++) {

				if (imgA.getRGB(x, y) != imgB.getRGB(x, y)) {

					Log.message("Image are different");
					return false;
				}
			}

		}

		return true;
	}

	/**
	 * Method to draw arrow annotation on zooming
	 * 
	 */

	public boolean drawArrowAnnotationOnZooming(String markupname, String filepath1) throws IOException, FindFailed {
		boolean flag = false;
		Log.message("Switched to default content");
		SkySiteUtils.waitTill(5000);

		SkySiteUtils.waitTill(4000);
		String MainWindow = driver.getWindowHandle();
		Log.message(MainWindow);
		for (String childwindow : driver.getWindowHandles()) {
			driver.switchTo().window(childwindow);

		}
		Log.message("switched to child window");

		driver.switchTo().defaultContent();
		SkySiteUtils.waitTill(4000);
		// method to delete existing markup
		this.markupdeletebegin();

		for (int i = 1; i <= 2; i++) {
			driver.findElement(By.xpath(".//*[@id='imageViewer']/div[3]/div[1]/div/ul/li[1]/a/i")).click();
			Log.message("zoom button is pressed " + i + "time");
			SkySiteUtils.waitTill(3000);
		}
		SkySiteUtils.waitTill(3000);
		drawlinetool.click();
		Log.message("Linetool is clicked");
		Actions act = new Actions(driver);
		act.moveToElement(arrowannotation).clickAndHold(arrowannotation).build().perform();
		SkySiteUtils.waitTill(2000);
		act.click().build().perform();
		Log.message("arrow annotation  tool button is clicked");
		SkySiteUtils.waitTill(2000);
		File fis = new File(PropertyReader.getProperty("Screenshotpath"));
		String filepath = fis.getAbsolutePath().toString();
		String Screenshot1 = filepath + "\\StartPointRectZoom.png";
		String Screenshot2 = filepath + "\\EndPointRectZoom.png";
		Screen s = new Screen();

		Pattern p1 = new Pattern(Screenshot1);

		Pattern p2 = new Pattern(Screenshot2);

		s.click(p1);

		s.drag(p1);

		s.dropAt(p2);
		Log.message("image is drawn");

		SkySiteUtils.waitTill(4000);

		savebtn.click();
		Log.message("save button is clicked");
		SkySiteUtils.waitTill(4000);
		markupfield.sendKeys(markupname);
		Log.message("Enter markup name is " + markupname);
		SkySiteUtils.waitForElement(driver, savemarkupbtn, 40);
		savemarkupbtn.click();
		Log.message("savemarkupbtn is clicked");
		SkySiteUtils.waitTill(14000);
		driver.findElement(By.xpath(".//*[@id='imageViewer']/div[3]/div[4]/div/ul/li[2]/a")).click();
		Log.message("save button is clicked again");
		SkySiteUtils.waitTill(4000);
		// File fis1=new File(PropertyReader.getProperty("SaveScreenshot"));
		// String filepath1=fis1.getAbsolutePath().toString();
		String savedscreenshot1 = filepath1 + "\\BeforesavingmarkupArrow.png";
		String savedscreenshot2 = filepath1 + "\\AfterSavingMarkerArrow.png";
		File srcFiler = ((TakesScreenshot) driver).getScreenshotAs(OutputType.FILE);

		FileUtils.copyFile(srcFiler, new File(savedscreenshot1));

		Log.message("Screenshot taken of saved markup");
		SkySiteUtils.waitTill(4000);
		markupdropdown.click();
		Log.message("markup dropdown clicked");
		SkySiteUtils.waitTill(4000);
		for (WebElement markupitem : markuplist)
			if (markupitem.getText().contains(markupname))
				driver.findElement(By.xpath("//*[@data-original-title='" + markupname + "']")).click();

		Log.message("save markup clicked and image drawn disappears");
		SkySiteUtils.waitTill(4000);
		SkySiteUtils.waitTill(4000);
		for (WebElement markupitem : markuplist)
			if (markupitem.getText().contains(markupname))
				driver.findElement(By.xpath("//*[@data-original-title='" + markupname + "']")).click();

		// showallmarkup.click();
		Log.message("Markup is displayed");
		SkySiteUtils.waitTill(4000);
		savebtn.click();
		Log.message("save button is clicked");
		SkySiteUtils.waitTill(4000);
		File srcFiler1 = ((TakesScreenshot) driver).getScreenshotAs(OutputType.FILE);

		FileUtils.copyFile(srcFiler1, new File(savedscreenshot2));
		Log.message("Screenshot taken after redrawing");

		BufferedImage imgA = ImageIO.read(new File(savedscreenshot1));

		// Thread.sleep(2000);

		BufferedImage imgB = ImageIO.read(new File(savedscreenshot2));

		// Thread.sleep(2000);

		Log.message("Checking the height and width of both the images are same?");

		if (imgA.getWidth() != imgB.getWidth() && imgA.getHeight() != imgB.getHeight()) {
			Log.message("image height width are different");
			return false;
		}

		int width = imgA.getWidth();
		int height = imgA.getHeight();
		for (int y = 0; y < height; y++) {
			for (int x = 0; x < width; x++) {

				if (imgA.getRGB(x, y) != imgB.getRGB(x, y)) {

					Log.message("Image are different");
					return false;
				}
			}

		}

		return true;
	}

	/**
	 * Method to draw text call out annotation on zooming
	 * 
	 */
	/**
	 * Method to draw text call out annotation
	 * 
	 * @throws IOException
	 * @throws FindFailed
	 * 
	 */
	public boolean drawTextCallOutAnnotationOnZooming(String markupname, String filepath1)
			throws IOException, FindFailed {
		boolean flag = false;
		Log.message("Switched to default content");
		SkySiteUtils.waitTill(5000);

		SkySiteUtils.waitTill(4000);
		String MainWindow = driver.getWindowHandle();
		Log.message(MainWindow);
		for (String childwindow : driver.getWindowHandles()) {
			driver.switchTo().window(childwindow);

		}
		Log.message("switched to child window");

		driver.switchTo().defaultContent();
		for (int i = 1; i <= 2; i++) {
			driver.findElement(By.xpath(".//*[@id='imageViewer']/div[3]/div[1]/div/ul/li[1]/a/i")).click();
			Log.message("zoom button is pressed " + i + "time");
			SkySiteUtils.waitTill(3000);
		}
		SkySiteUtils.waitTill(4000);
		// method to delete existing markup
		this.markupdeletebegin();
		SkySiteUtils.waitForElement(driver, tootextbtn, 50);
		tootextbtn.click();
		Log.message("text tool is clicked");
		Actions act = new Actions(driver);
		act.moveToElement(textCallout).clickAndHold(textCallout).build().perform();
		SkySiteUtils.waitTill(2000);
		act.click().build().perform();
		Log.message("draw text callout tool button is clicked");
		SkySiteUtils.waitTill(2000);
		File fis = new File(PropertyReader.getProperty("Screenshotpath"));
		String filepath = fis.getAbsolutePath().toString();
		String Screenshot1 = filepath + "\\StartPointRectZoom.png";
		String Screenshot2 = filepath + "\\EndPointRectZoom.png";
		Screen s = new Screen();

		Pattern p1 = new Pattern(Screenshot1);

		Pattern p2 = new Pattern(Screenshot2);

		s.click(p1);

		// s.click(p1);
		// s.drag(p1);

		// s.dropAt(p2);
		Log.message("image is drawn");

		SkySiteUtils.waitTill(4000);
		textarea.sendKeys("Text Call out drawing");
		Log.message("text has been entered");
		Select sc = new Select(driver.findElement(By.xpath(".//*[@id='selectCalloutFontSize']")));
		sc.selectByValue("8");
		Log.message("8 font size is selected");
		oksavebutton.click();

		Log.message("Ok button is clicked");
		SkySiteUtils.waitForElement(driver, savebtn, 50);
		savebtn.click();
		Log.message("save button is clicked");
		SkySiteUtils.waitTill(4000);
		markupfield.sendKeys(markupname);
		SkySiteUtils.waitForElement(driver, savemarkupbtn, 40);
		savemarkupbtn.click();
		Log.message("savemarkupbtn is clicked");
		SkySiteUtils.waitTill(14000);
		driver.findElement(By.xpath(".//*[@id='imageViewer']/div[3]/div[4]/div/ul/li[2]/a")).click();
		Log.message("save button is clicked again");
		SkySiteUtils.waitTill(4000);
		// File fis1=new File(PropertyReader.getProperty("SaveScreenshot"));
		// String filepath1=fis1.getAbsolutePath().toString();
		String savedscreenshot1 = filepath1 + "\\BeforesavingmarkupTextCallout.png";
		String savedscreenshot2 = filepath1 + "\\AfterSavingMarkerTextCallout.png";
		File srcFiler = ((TakesScreenshot) driver).getScreenshotAs(OutputType.FILE);

		FileUtils.copyFile(srcFiler, new File(savedscreenshot1));

		Log.message("Screenshot taken of saved markup");
		SkySiteUtils.waitTill(4000);
		markupdropdown.click();
		Log.message("markup dropdown clicked");
		SkySiteUtils.waitTill(4000);
		for (WebElement markupitem : markuplist)
			if (markupitem.getText().contains(markupname))
				driver.findElement(By.xpath("//*[@data-original-title='" + markupname + "']")).click();

		Log.message("save markup clicked and image drawn disappears");
		SkySiteUtils.waitTill(4000);
		SkySiteUtils.waitTill(4000);
		for (WebElement markupitem : markuplist)
			if (markupitem.getText().contains(markupname))
				driver.findElement(By.xpath("//*[@data-original-title='" + markupname + "']")).click();

		// showallmarkup.click();
		Log.message("Markup is displayed");
		SkySiteUtils.waitTill(4000);
		savebtn.click();
		Log.message("save button is clicked");
		SkySiteUtils.waitTill(4000);
		File srcFiler1 = ((TakesScreenshot) driver).getScreenshotAs(OutputType.FILE);

		FileUtils.copyFile(srcFiler1, new File(savedscreenshot2));
		Log.message("Screenshot taken after redrawing");

		BufferedImage imgA = ImageIO.read(new File(savedscreenshot1));

		// Thread.sleep(2000);

		BufferedImage imgB = ImageIO.read(new File(savedscreenshot2));

		// Thread.sleep(2000);

		Log.message("Checking the height and width of both the images are same?");

		if (imgA.getWidth() != imgB.getWidth() && imgA.getHeight() != imgB.getHeight()) {
			Log.message("image height width are different");
			return false;
		}

		int width = imgA.getWidth();
		int height = imgA.getHeight();
		for (int y = 0; y < height; y++) {
			for (int x = 0; x < width; x++) {

				if (imgA.getRGB(x, y) != imgB.getRGB(x, y)) {

					Log.message("Image are different");
					return false;
				}
			}

		}

		return true;
	}

	/**
	 * Method to draw free hand annotation
	 * 
	 * @throws FindFailed
	 * @throws IOException
	 * 
	 */
	public boolean drawFreeHandAnnotationOnZooming(String markupname, String filepath1) throws FindFailed, IOException {
		boolean flag = false;
		Log.message("Switched to default content");
		SkySiteUtils.waitTill(5000);

		SkySiteUtils.waitTill(4000);
		String MainWindow = driver.getWindowHandle();
		Log.message(MainWindow);
		for (String childwindow : driver.getWindowHandles()) {
			driver.switchTo().window(childwindow);

		}
		Log.message("switched to child window");

		driver.switchTo().defaultContent();
		this.markupdeletebegin();
		SkySiteUtils.waitTill(4000);
		for (int i = 1; i <= 2; i++) {
			driver.findElement(By.xpath(".//*[@id='imageViewer']/div[3]/div[1]/div/ul/li[1]/a/i")).click();
			Log.message("zoom button is pressed " + i + "time");
			SkySiteUtils.waitTill(3000);
		}
		SkySiteUtils.waitTill(4000);
		// method to delete existing markup
		this.markupdeletebegin();
		drawlinetool.click();
		Log.message("Linetool is clicked");
		Actions act = new Actions(driver);
		act.moveToElement(freehandpen).clickAndHold(freehandpen).build().perform();
		SkySiteUtils.waitTill(2000);
		act.click().build().perform();
		Log.message("free hand  tool button is clicked");
		SkySiteUtils.waitTill(2000);
		File fis = new File(PropertyReader.getProperty("Screenshotpath"));
		String filepath = fis.getAbsolutePath().toString();
		String Screenshot1 = filepath + "\\StartPointRectZoom.png";
		String Screenshot2 = filepath + "\\EndPointRectZoom.png";
		Screen s = new Screen();

		Pattern p1 = new Pattern(Screenshot1);

		Pattern p2 = new Pattern(Screenshot2);

		s.click(p1);

		s.drag(p1);

		s.dropAt(p2);
		Log.message("image is drawn");

		SkySiteUtils.waitTill(4000);

		savebtn.click();
		Log.message("save button is clicked");
		SkySiteUtils.waitTill(4000);
		markupfield.sendKeys(markupname);
		Log.message("Enter markup name is " + markupname);
		SkySiteUtils.waitForElement(driver, savemarkupbtn, 40);
		savemarkupbtn.click();
		Log.message("savemarkupbtn is clicked");
		SkySiteUtils.waitTill(14000);
		driver.findElement(By.xpath(".//*[@id='imageViewer']/div[3]/div[4]/div/ul/li[2]/a")).click();
		Log.message("save button is clicked again");
		SkySiteUtils.waitTill(4000);
		// File fis1=new File(PropertyReader.getProperty("Screenshotpath"));
		// String filepath1=fis1.getAbsolutePath().toString();
		String savedscreenshot1 = filepath1 + "\\BeforesavingmarkupFreehand.png";
		String savedscreenshot2 = filepath1 + "\\AfterSavingMarkerfreehand.png";
		File srcFiler = ((TakesScreenshot) driver).getScreenshotAs(OutputType.FILE);

		FileUtils.copyFile(srcFiler, new File(savedscreenshot1));

		Log.message("Screenshot taken of saved markup");
		SkySiteUtils.waitTill(4000);
		markupdropdown.click();
		Log.message("markup dropdown clicked");
		SkySiteUtils.waitTill(4000);
		for (WebElement markupitem : markuplist)
			if (markupitem.getText().contains(markupname))
				driver.findElement(By.xpath("//*[@data-original-title='" + markupname + "']")).click();

		Log.message("save markup clicked and image drawn disappears");
		SkySiteUtils.waitTill(4000);
		SkySiteUtils.waitTill(4000);
		for (WebElement markupitem : markuplist)
			if (markupitem.getText().contains(markupname))
				driver.findElement(By.xpath("//*[@data-original-title='" + markupname + "']")).click();

		// showallmarkup.click();
		Log.message("Markup is displayed");
		SkySiteUtils.waitTill(4000);
		savebtn.click();
		Log.message("save button is clicked");
		SkySiteUtils.waitTill(4000);
		File srcFiler1 = ((TakesScreenshot) driver).getScreenshotAs(OutputType.FILE);

		FileUtils.copyFile(srcFiler1, new File(savedscreenshot2));
		Log.message("Screenshot taken after redrawing");

		BufferedImage imgA = ImageIO.read(new File(savedscreenshot1));

		// Thread.sleep(2000);

		BufferedImage imgB = ImageIO.read(new File(savedscreenshot2));

		// Thread.sleep(2000);

		Log.message("Checking the height and width of both the images are same?");

		if (imgA.getWidth() != imgB.getWidth() && imgA.getHeight() != imgB.getHeight()) {
			Log.message("image height width are different");
			return false;
		}

		int width = imgA.getWidth();
		int height = imgA.getHeight();
		for (int y = 0; y < height; y++) {
			for (int x = 0; x < width; x++) {

				if (imgA.getRGB(x, y) != imgB.getRGB(x, y)) {

					Log.message("Image are different");
					return false;
				}
			}

		}

		return true;
	}

	/**
	 * Method to draw and save square hyperlink on zooming
	 * 
	 * @throws IOException
	 * @throws FindFailed
	 * 
	 */
	public boolean drawAndSaveSquareHyperLinkonzooming(String markupname, String filepath1)
			throws IOException, FindFailed {
		boolean flag = false;
		Log.message("Switched to default content");
		SkySiteUtils.waitTill(5000);

		String MainWindow = driver.getWindowHandle();
		Log.message(MainWindow);
		for (String childwindow : driver.getWindowHandles()) {
			driver.switchTo().window(childwindow);

		}
		Log.message("switched to child window");

		driver.switchTo().defaultContent();
		SkySiteUtils.waitTill(4000);
		this.markupdeletebegin();
		SkySiteUtils.waitTill(4000);
		for (int i = 1; i <= 2; i++) {
			driver.findElement(By.xpath(".//*[@id='imageViewer']/div[3]/div[1]/div/ul/li[1]/a/i")).click();
			Log.message("zoom button is pressed " + i + "time");
			SkySiteUtils.waitTill(3000);
		}
		SkySiteUtils.waitTill(4000);
		// method to delete existing markup

		hyperlinktool.click();
		Log.message("Hyperlink tool is clicked");
		SkySiteUtils.waitForElement(driver, recthyperlink, 40);
		Actions act = new Actions(driver);
		act.moveToElement(recthyperlink).clickAndHold().build().perform();
		SkySiteUtils.waitTill(2000);
		act.click(recthyperlink).build().perform();
		Log.message("Rect Hyper Link button is clicked");
		SkySiteUtils.waitTill(2000);
		File fis = new File(PropertyReader.getProperty("Screenshotpath"));
		String filepath = fis.getAbsolutePath().toString();
		String Screenshot1 = filepath + "\\SquareHyperLinkZoomStartPoint.png";
		String Screenshot2 = filepath + "\\SquareHyperLinkZoomEndPoint.png";
		Screen s = new Screen();

		Pattern p1 = new Pattern(Screenshot1);

		Pattern p2 = new Pattern(Screenshot2);

		// s.click(p1);

		s.drag(p1);

		s.dropAt(p2);
		Log.message("image is drawn");
		SkySiteUtils.waitTill(4000);
		// driver.findElement(By.xpath(".//*[@id='hyperLinkFolderList']/ul/li[2]/div/a/div[3]")).click();
		driver.findElement(By.xpath(".//*[@id='hyperLinkFolderList']/ul/li[10]/div/a/div[3]")).click();
		Log.message("Folder is selected");
		SkySiteUtils.waitTill(4000);
		// driver.findElement(By.xpath(".//*[@id='hyperLinkFolderList']/ul/li[2]/div/div[2]/h4/a")).click();
		driver.findElement(By.xpath(".//*[@id='hyperLinkFolderList']/ul/li[1]/div/div[2]/h4/a")).click();
		Log.message("File is selected");
		driver.findElement(By.xpath(".//*[@id='dvHLPage_1']/div/ul/li[1]/a")).click();
		Log.message("Page is selected");
		SkySiteUtils.waitForElement(driver, okbutton, 50);
		okbutton.click();
		Log.message("Ok button is clicked");

		SkySiteUtils.waitTill(4000);
		// driver.findElement(By.xpath("//*[@class='leaflet-control-nav-home']")).click();
		// Log.message("Home page is clicked");
		SkySiteUtils.waitForElement(driver, savebtn, 50);
		savebtn.click();
		Log.message("save button is clicked");
		SkySiteUtils.waitTill(4000);
		markupfield.sendKeys(markupname);
		Log.message("Enter markup name is " + markupname);
		SkySiteUtils.waitForElement(driver, savemarkupbtn, 40);
		savemarkupbtn.click();
		Log.message("savemarkupbtn is clicked");
		SkySiteUtils.waitTill(14000);
		driver.findElement(By.xpath(".//*[@id='imageViewer']/div[3]/div[4]/div/ul/li[2]/a")).click();
		Log.message("save button is clicked again");
		SkySiteUtils.waitTill(4000);
		String savedscreenshot1 = filepath1 + "\\BeforesavingSquareHyperLink.png";
		String savedscreenshot2 = filepath1 + "\\AfterSavingSquareHyperLink.png";
		// File fistemp=new File(PropertyReader.getProperty("tempScreenshot"));
		// String filepathtemp=fistemp.getAbsolutePath().toString();
		// String tempscreenshotpath=filepathtemp + "\\temp_Screenshot.png";
		File srcFiler = ((TakesScreenshot) driver).getScreenshotAs(OutputType.FILE);

		FileUtils.copyFile(srcFiler, new File(savedscreenshot1));

		Log.message("Screenshot taken of saved markup");
		SkySiteUtils.waitTill(4000);
		hyperlinkdropdown.click();
		Log.message("hyperlink markup dropdown clicked");
		SkySiteUtils.waitTill(4000);
		// for(WebElement markupitem:hperlinklist)
		// if(markupitem.getText().contains(markupname))
		// driver.findElement(By.xpath("//*[text()='"+markupname+"']")).click();
		driver.findElement(By.xpath(".//*[@id='btnHideAllHyperlinks']")).click();
		Log.message("hide all button clicked and image disappears");

		SkySiteUtils.waitTill(4000);
		showhyperlinks.click();
		Log.message("show all hyperlink clikced");
		SkySiteUtils.waitTill(4000);
		savebtn.click();
		Log.message("save button is clicked");
		SkySiteUtils.waitTill(4000);
		File srcFiler1 = ((TakesScreenshot) driver).getScreenshotAs(OutputType.FILE);

		FileUtils.copyFile(srcFiler1, new File(savedscreenshot2));
		Log.message("Screenshot taken after redrawing");

		BufferedImage imgA = ImageIO.read(new File(savedscreenshot1));

		// Thread.sleep(2000);

		BufferedImage imgB = ImageIO.read(new File(savedscreenshot2));

		// Thread.sleep(2000);

		Log.message("Checking the height and width of both the images are same?");

		if (imgA.getWidth() != imgB.getWidth() && imgA.getHeight() != imgB.getHeight()) {
			Log.message("image height width are different");
			return false;
		}

		int width = imgA.getWidth();
		int height = imgA.getHeight();
		for (int y = 0; y < height; y++) {
			for (int x = 0; x < width; x++) {

				if (imgA.getRGB(x, y) != imgB.getRGB(x, y)) {

					Log.message("Image are different");
					return false;
				}
			}

		}

		return true;
	}

	/**
	 * Method to verify working of square hyperlink for same folder diff file on
	 * zooming
	 * 
	 * @throws FindFailed
	 * @throws IOException
	 * @throws UnsupportedFlavorException
	 * 
	 */
	public boolean VerifyfunctionalitySquareHyperLinkSameFolderDiffFileOnZooming(String markupname, String filepath1)
			throws FindFailed, UnsupportedFlavorException, IOException {
		// driver.switchTo().defaultContent();
		SkySiteUtils.waitTill(6000);
		File fistemp = new File(PropertyReader.getProperty("tempScreenshot"));
		String filepathtemp = fistemp.getAbsolutePath().toString();
		String tempscreenshotpath = filepathtemp + "\\Tempzoominghyperlink2.png";
		Screen s = new Screen();

		Pattern p1 = new Pattern(tempscreenshotpath);
	
	
		s.click(p1);
		Log.message("saved hyperlink is clicked");
		SkySiteUtils.waitTill(4000);
		// JavascriptExecutor executor = (JavascriptExecutor)driver;
		// executor.executeScript("arguments[0].click();", copylink);
		// Log.message("Copy to clipboard clicked");
		/*
		 * Toolkit toolkit = Toolkit.getDefaultToolkit(); Clipboard clipboard =
		 * toolkit.getSystemClipboard(); String result = (String)
		 * clipboard.getData(DataFlavor.stringFlavor);
		 * System.out.println("String from Clipboard:" + result);
		 */
		WebElement imagelink = driver.findElement(By.id("linkImgDiv"));
		JavascriptExecutor executor = (JavascriptExecutor) driver;
		executor.executeScript("arguments[0].click();", imagelink);
		Log.message("image is clicked");

		String filename = driver.findElement(By.xpath("//*[@data-original-title='sum123.pdf']")).getText();
		Log.message("File name is " + filename);
		String expectedfilename = "(R-1) sum123.pdf";
		if (filename.equals(expectedfilename))
			return true;

		else
			return false;

	}

	/**
	 * Method to click default file
	 * 
	 */
	public void clickDefaultFileAfterZoom() {
		SkySiteUtils.waitTill(4000);
		WebElement documenttab = driver.findElement(By.xpath("//*[@data-original-title='re1(13).pdf']"));
		JavascriptExecutor executor = (JavascriptExecutor) driver;
		executor.executeScript("arguments[0].click();", documenttab);
		Log.message("Original file is clicked");
		SkySiteUtils.waitTill(5000);

	}

	/**
	 * Method to draw only multiple calibration without any reference calbrator
	 * ruler on a single document
	 * 
	 * @throws InterruptedException
	 * @throws FindFailed
	 * @throws IOException
	 * @throws AWTException
	 * 
	 */
	public boolean drawMultipleCalibrationRuler(String markupname, String markupname2, String thirdmarkup,
			String filepath1) throws InterruptedException, FindFailed, IOException, AWTException {

		boolean flag = false;
		Log.message("Switched to default content");
		SkySiteUtils.waitTill(5000);
		driver.switchTo().frame(driver.findElement(By.id("myFrame")));
		Log.message("Switched to frame");
		driver.findElement(By.xpath("//*[text()='WFolderRotation']")).click();
		Log.message("Folder selected");
		SkySiteUtils.waitTill(4000);
		driver.findElement(By.xpath(".//*[@id='divGrid']/div[2]/table/tbody/tr[2]/td[2]/a[3]")).click();
		Log.message("File is selected");
		SkySiteUtils.waitTill(4000);
		SkySiteUtils.waitTill(4000);
		String MainWindow = driver.getWindowHandle();
		Log.message(MainWindow);
		for (String childwindow : driver.getWindowHandles()) {
			driver.switchTo().window(childwindow);

		}
		Log.message("switched to child window");

		driver.switchTo().defaultContent();
		SkySiteUtils.waitTill(4000);
		// method to delete existing markup
		this.markupdeletebegin();
		SkySiteUtils.waitTill(4000);
		calibrator.click();
		Log.message("Calibrator tool is clicked");
		SkySiteUtils.waitForElement(driver, Calibratorruler, 40);
		Actions act = new Actions(driver);
		act.moveToElement(Calibratorruler).clickAndHold().build().perform();
		SkySiteUtils.waitTill(2000);
		act.click(Calibratorruler).build().perform();
		Log.message("Calibrator  ruler button is clicked");
		SkySiteUtils.waitTill(2000);

		File fis = new File(PropertyReader.getProperty("Screenshotpath"));
		String filepath = fis.getAbsolutePath().toString();
		String Screenshot1 = filepath + "\\StartpointRect.png";
		String Screenshot2 = filepath + "\\EndpointRect.png";
		Screen s = new Screen();

		Pattern p1 = new Pattern(Screenshot1);

		Pattern p2 = new Pattern(Screenshot2);

		s.drag(p1);

		s.dropAt(p2);
		Log.message("image is drawn");
		SkySiteUtils.waitTill(4000);

		driver.findElement(By.xpath(".//*[@id='txtMeasureUnitLength']")).sendKeys("6");

		Log.message("Length in feet is entered");
		driver.findElement(By.xpath(".//*[@id='txtSubMeasureUnitLength']")).sendKeys("4");
		Log.message("Length in inch is entered");
		driver.findElement(By.xpath(".//*[@id='editCalibrateRulerAnnotation']/div/div[4]/button[2]")).click();
		Log.message("ok is clicked");

		SkySiteUtils.waitTill(14000);
		// driver.findElement(By.xpath(".//*[@id='hyperLinkFolderList']/ul/li[2]/div/a/div[3]")).click();
		/*
		 * driver.findElement(By.xpath(".//*[@id='measureToolMenuBtn']/i")).click();
		 * Log.message("measure tool button is clicked again"); Actions act1=new
		 * Actions(driver);
		 * act1.moveToElement(squarecreation).clickAndHold().build().perform();
		 * SkySiteUtils.waitTill(2000); act.click(squarecreation).build().perform();
		 * Log.message("Square Calibrator selected"); String
		 * Screenshot4=filepath+"\\CalibratorEndRect.png"; String
		 * Screenshot5=filepath+"\\CalibratorstartRect.png"; Screen s1=new Screen();
		 * 
		 * Pattern p3 = new Pattern(Screenshot4);
		 * 
		 * Pattern p4 = new Pattern(Screenshot5);
		 * 
		 * s.click(p3);
		 * 
		 * s.drag(p3);
		 * 
		 * s.dropAt(p4);
		 */

		SkySiteUtils.waitTill(4000);
		driver.findElement(By.xpath("//*[@class='leaflet-control-nav-home']")).click();
		Log.message("Home page is clicked");
		SkySiteUtils.waitForElement(driver, savebtn, 50);
		savebtn.click();
		Log.message("save button is clicked");
		SkySiteUtils.waitTill(4000);
		markupfield.sendKeys(markupname2);
		Log.message("Enter markup name is " + markupname2);
		SkySiteUtils.waitForElement(driver, savemarkupbtn, 40);
		savemarkupbtn.click();
		Log.message("savemarkupbtn is clicked");
		SkySiteUtils.waitTill(14000);
		// driver.findElement(By.xpath(".//*[@id='imageViewer']/div[3]/div[4]/div/ul/li[2]/a")).click();
		// Log.message("save button is clicked again");

		markupdropdown.click();
		Log.message("markup dropdown clicked");
		SkySiteUtils.waitTill(4000);
		for (WebElement markupitem : markuplist)
			if (markupitem.getText().contains(markupname2))
				driver.findElement(By.xpath("//*[@data-original-title='" + markupname2 + "']")).click();

		Log.message("save markup clicked and image drawn disappears");
		driver.findElement(By.xpath(".//*[@id='imageViewer']/div[3]/div[4]/div/ul/li[2]/a")).click();
		Log.message("save button is clicked again");

		calibrator.click();
		Log.message("Calibrator tool is clicked");
		SkySiteUtils.waitForElement(driver, Calibratorruler, 40);
		Actions act12 = new Actions(driver);
		act12.moveToElement(Calibratorruler).clickAndHold().build().perform();
		SkySiteUtils.waitTill(2000);
		act12.click(Calibratorruler).build().perform();
		Log.message("Calibrator  ruler button is clicked");
		SkySiteUtils.waitTill(2000);
		File fis2 = new File(PropertyReader.getProperty("Screenshotpath"));
		String filepath2 = fis2.getAbsolutePath().toString();
		String Screenshotruler = filepath2 + "\\StartpointRuler.png";
		String Screenshot2ruler = filepath2 + "\\EndPointRuler.png";
		Screen s2 = new Screen();

		Pattern p5 = new Pattern(Screenshotruler);

		Pattern p6 = new Pattern(Screenshot2ruler);

		s2.drag(p5);

		s2.dropAt(p6);
		Log.message("image is drawn");
		SkySiteUtils.waitTill(4000);
		driver.findElement(By.xpath(".//*[@id='editCalibrateAnnotationRuler']/div/div[1]/div/label[2]")).click();
		Log.message("metric option is selected");
		SkySiteUtils.waitTill(5000);
		driver.findElement(By.xpath(".//*[@id='txtMeasureUnitLength']")).sendKeys("9");

		Log.message("Length in metre is entered");
		driver.findElement(By.xpath(".//*[@id='txtSubMeasureUnitLength']")).sendKeys("4");
		Log.message("Length in Centimetre is entered");
		driver.findElement(By.xpath(".//*[@id='editCalibrateRulerAnnotation']/div/div[4]/button[2]")).click();
		Log.message("ok is clicked");

		driver.findElement(By.xpath("//*[@class='leaflet-control-nav-home']")).click();
		Log.message("Home page is clicked");
		SkySiteUtils.waitForElement(driver, savebtn, 50);
		savebtn.click();
		Log.message("save button is clicked");
		SkySiteUtils.waitTill(4000);
		markupfield.sendKeys(markupname);
		Log.message("Enter markup name is " + markupname);

		SkySiteUtils.waitForElement(driver, savemarkupbtn, 40);
		savemarkupbtn.click();
		Log.message("savemarkupbtn is clicked");
		SkySiteUtils.waitTill(14000);

		SkySiteUtils.waitTill(14000);
		// driver.findElement(By.xpath(".//*[@id='imageViewer']/div[3]/div[4]/div/ul/li[2]/a")).click();
		// Log.message("save button is clicked again");

		markupdropdown.click();
		Log.message("markup dropdown clicked");
		SkySiteUtils.waitTill(4000);
		for (WebElement markupitem : markuplist)
			if (markupitem.getText().contains(markupname))
				driver.findElement(By.xpath("//*[@data-original-title='" + markupname + "']")).click();

		Log.message("save markup clicked and image drawn disappears");
		driver.findElement(By.xpath(".//*[@id='imageViewer']/div[3]/div[4]/div/ul/li[2]/a")).click();
		Log.message("save button is clicked again");

		calibrator.click();
		Log.message("Calibrator tool is clicked");
		SkySiteUtils.waitForElement(driver, Calibratorruler, 40);
		Actions act123 = new Actions(driver);
		act123.moveToElement(Calibratorruler).clickAndHold().build().perform();
		SkySiteUtils.waitTill(2000);
		act123.click(Calibratorruler).build().perform();
		Log.message("Calibrator  ruler button is clicked");
		SkySiteUtils.waitTill(2000);
		File fis3 = new File(PropertyReader.getProperty("Screenshotpath"));
		String filepath22 = fis3.getAbsolutePath().toString();
		String Screenshot3startruler = filepath22 + "\\StartPointMultiRuler.png";
		String Screenshot3Endruler = filepath22 + "\\EndPointMultiRuler.png";
		Screen s3 = new Screen();

		Pattern p31 = new Pattern(Screenshot3startruler);

		Pattern p32 = new Pattern(Screenshot3Endruler);

		s3.drag(p31);

		s2.dropAt(p32);
		Log.message("image is drawn");
		SkySiteUtils.waitTill(4000);
		driver.findElement(By.xpath(".//*[@id='editCalibrateAnnotationRuler']/div/div[1]/div/label[2]")).click();
		Log.message("metric option is selected");
		SkySiteUtils.waitTill(5000);
		driver.findElement(By.xpath(".//*[@id='txtMeasureUnitLength']")).sendKeys("15");

		Log.message("Length in metre is entered");
		driver.findElement(By.xpath(".//*[@id='txtSubMeasureUnitLength']")).sendKeys("9");
		Log.message("Length in Centimetre is entered");
		driver.findElement(By.xpath(".//*[@id='editCalibrateRulerAnnotation']/div/div[4]/button[2]")).click();
		Log.message("ok is clicked");

		driver.findElement(By.xpath("//*[@class='leaflet-control-nav-home']")).click();
		Log.message("Home page is clicked");
		SkySiteUtils.waitForElement(driver, savebtn, 50);
		savebtn.click();
		Log.message("save button is clicked");
		SkySiteUtils.waitTill(4000);
		markupfield.sendKeys(thirdmarkup);
		Log.message("Enter markup name is " + thirdmarkup);

		SkySiteUtils.waitForElement(driver, savemarkupbtn, 40);
		savemarkupbtn.click();
		Log.message("savemarkupbtn is clicked");
		SkySiteUtils.waitTill(14000);
		Robot rb = new Robot();
		rb.keyPress(KeyEvent.VK_CONTROL);
		rb.keyPress(KeyEvent.VK_W);
		rb.keyRelease(KeyEvent.VK_W);
		rb.keyRelease(KeyEvent.VK_CONTROL);
		SkySiteUtils.waitTill(5000);
		ArrayList tabs = new ArrayList(driver.getWindowHandles());
		System.out.println(tabs.size());

		driver.switchTo().window((String) tabs.get(0));
		SkySiteUtils.waitTill(5000);
		driver.switchTo().defaultContent();
		Log.message("Switched to default content");
		SkySiteUtils.waitTill(5000);
		driver.switchTo().frame(driver.findElement(By.id("myFrame")));
		Log.message("Switched to frame");
		driver.findElement(By.xpath("//*[text()='WFolderRotation']")).click();
		Log.message("Folder selected");
		SkySiteUtils.waitTill(4000);
		driver.findElement(By.xpath(".//*[@id='divGrid']/div[2]/table/tbody/tr[2]/td[2]/a[3]")).click();
		Log.message("File is selected");
		SkySiteUtils.waitTill(5000);
		String MainWindow2 = driver.getWindowHandle();
		Log.message(MainWindow);
		for (String childwindow : driver.getWindowHandles()) {
			driver.switchTo().window(childwindow);

		}
		Log.message("switched to child window");

		driver.switchTo().defaultContent();
		driver.findElement(By.xpath(".//*[@id='btnViewAllGlobal']")).click();
		Log.message("View all button is clicked");
		// driver.findElement(By.xpath(".//*[@id='btnViewAllGlobal']")).click();
		// Log.message("View all button is clicked");
		SkySiteUtils.waitTill(5000);
		File fistemp = new File(PropertyReader.getProperty("tempScreenshot"));
		String filepathtemp = fistemp.getAbsolutePath().toString();
		String savedscreenshot1 = filepathtemp + "\\OutPutpicformultiruler.png";
		String savedscreenshot2 = filepath1 + "\\Multiruleroutput.png";
		SkySiteUtils.waitTill(4000);
		// String tempscreenshotpath=filepathtemp + "\\temp_Screenshot.png";
		File srcFiler = ((TakesScreenshot) driver).getScreenshotAs(OutputType.FILE);

		FileUtils.copyFile(srcFiler, new File(savedscreenshot2));

		Log.message("Screenshot taken of saved markup");
		SkySiteUtils.waitTill(4000);

		BufferedImage imgA = ImageIO.read(new File(savedscreenshot1));
		SkySiteUtils.waitTill(3000);
		BufferedImage imgB = ImageIO.read(new File(savedscreenshot2));

		Thread.sleep(2000);

		Log.message("Checking the height and width of both the images are same?");

		if (imgA.getWidth() != imgB.getWidth() && imgA.getHeight() != imgB.getHeight()) {
			Log.message("image height width are different");
			return false;
		}

		int width = imgA.getWidth();
		int height = imgA.getHeight();
		for (int x = 0; x < width; x++) {
			for (int y = 0; y < height; y++) {

				if (imgA.getRGB(x, y) != imgB.getRGB(x, y)) {

					Log.message("Image are different");
					return false;
				}
			}

		}

		return true;
	}

	@FindBy(xpath = ".//*[@id='measurementCircleCreate']/a/i")
	WebElement circleicon;
	@FindBy(xpath = ".//*[@id='measurementFreehandLengthCreate']/a/i")
	WebElement frehandicon;

	/**
	 * Method to draw combination of ruler and reference calibrator in one markup
	 * 
	 * @throws FindFailed
	 * @throws IOException 
	 * @throws InterruptedException 
	 * 
	 * 
	 */
	public boolean drawCombinationRulerAndcalibrator(String markupname, String filepath1) throws FindFailed, IOException, InterruptedException {
		boolean flag = false;
		Log.message("Switched to default content");
		SkySiteUtils.waitTill(5000);
		driver.switchTo().frame(driver.findElement(By.id("myFrame")));
		Log.message("Switched to frame");
		driver.findElement(By.xpath("//*[text()='MultiCombinationFile']")).click();
		Log.message("Folder selected");
		SkySiteUtils.waitTill(4000);
		driver.findElement(By.xpath(".//*[@id='divGrid']/div[2]/table/tbody/tr[2]/td[2]/a[3]")).click();
		Log.message("File is selected");
		SkySiteUtils.waitTill(4000);
		SkySiteUtils.waitTill(4000);
		String MainWindow = driver.getWindowHandle();
		Log.message(MainWindow);
		for (String childwindow : driver.getWindowHandles()) {
			driver.switchTo().window(childwindow);

		}
		Log.message("switched to child window");

		driver.switchTo().defaultContent();
		SkySiteUtils.waitTill(4000);
		// method to delete existing markup
		this.markupdeletebegin();
		SkySiteUtils.waitTill(4000);
		calibrator.click();
		Log.message("Calibrator tool is clicked");
		SkySiteUtils.waitForElement(driver, Calibratorruler, 40);
		Actions act = new Actions(driver);
		act.moveToElement(Calibratorruler).clickAndHold().build().perform();
		SkySiteUtils.waitTill(2000);
		act.click(Calibratorruler).build().perform();
		Log.message("Calibrator  ruler button is clicked");
		SkySiteUtils.waitTill(2000);

		File fis = new File(PropertyReader.getProperty("Screenshotpath"));
		String filepath = fis.getAbsolutePath().toString();
		String Screenshot1 = filepath + "\\StartRuler43.png";
		String Screenshot2 = filepath + "\\EndRuler43.png";
		Screen s = new Screen();

		Pattern p1 = new Pattern(Screenshot1);

		Pattern p2 = new Pattern(Screenshot2);

		s.drag(p1);

		s.dropAt(p2);
		Log.message("image is drawn");
		SkySiteUtils.waitTill(4000);

		driver.findElement(By.xpath(".//*[@id='txtMeasureUnitLength']")).sendKeys("6");

		Log.message("Length in feet is entered");
		driver.findElement(By.xpath(".//*[@id='txtSubMeasureUnitLength']")).sendKeys("4");
		Log.message("Length in inch is entered");
		driver.findElement(By.xpath(".//*[@id='editCalibrateRulerAnnotation']/div/div[4]/button[2]")).click();
		Log.message("ok is clicked");

		SkySiteUtils.waitTill(14000);
		
		driver.findElement(By.xpath(".//*[@id='imageViewer']/div[3]/div[1]/div/ul/li[3]/a/i")).click();
		Log.message("home button is clicked");
		// driver.findElement(By.xpath(".//*[@id='hyperLinkFolderList']/ul/li[2]/div/a/div[3]")).click();
		driver.findElement(By.xpath(".//*[@id='measureToolMenuBtn']/i")).click();
		Log.message("measure tool button is clicked again");
		Actions act1 = new Actions(driver);
		act1.moveToElement(squarecreation).clickAndHold().build().perform();
		SkySiteUtils.waitTill(2000);
		act.click(squarecreation).build().perform();
		Log.message("Square Calibrator selected");
		String Screenshot4 = filepath + "\\StartRect43.png";
		String Screenshot5 = filepath + "\\EndRect43.png";
		Screen s1 = new Screen();

		Pattern p3 = new Pattern(Screenshot4);

		Pattern p4 = new Pattern(Screenshot5);

		s.click(p3);

		s.drag(p3);

		s.dropAt(p4);

		SkySiteUtils.waitTill(4000);
		driver.findElement(By.xpath("//*[@class='leaflet-control-nav-home']")).click();
		Log.message("Home page is clicked");
		SkySiteUtils.waitTill(4000);
		driver.findElement(By.xpath(".//*[@id='measureToolMenuBtn']/i")).click();
		Log.message("measure tool button is clicked again");
		Actions act2 = new Actions(driver);
		act2.moveToElement(circleicon).clickAndHold().build().perform();
		SkySiteUtils.waitTill(2000);
		act.click(circleicon).build().perform();
		Log.message("circle Calibrator selected");

		String Screenshot6 = filepath + "\\StartCircle43.png";
		String Screenshot7 = filepath + "\\EndCircle43.png";
		Screen s2 = new Screen();

		Pattern p5 = new Pattern(Screenshot6);

		Pattern p6 = new Pattern(Screenshot7);

		s2.click(p5);

		s2.drag(p5);

		s2.dropAt(p6);
   
		SkySiteUtils.waitTill(4000);
		driver.findElement(By.xpath("//*[@class='leaflet-control-nav-home']")).click();
		Log.message("Home page is clicked");
		
       WebElement linetool=driver.findElement(By.xpath(".//*[@id='measurementLineCreate']/a/i"));
		SkySiteUtils.waitTill(4000);
		Actions act33=new Actions(driver);
		act33.sendKeys(Keys.ESCAPE);
		for(int i=0;i<2;i++)
		{
		driver.findElement(By.xpath(".//*[@id='measureToolMenuBtn']/i")).click();
		Log.message("measure tool button is clicked again");
		Actions act3 = new Actions(driver);
		act3.moveToElement(circleicon).clickAndHold().build().perform();
		SkySiteUtils.waitTill(6000);
		act.click(circleicon).build().perform();
		Log.message("free hand tool  Calibrator selected");
		SkySiteUtils.waitTill(6000);
		}
		String Screenshot8 = filepath + "\\Startfreeruler43.png";
		String Screenshot9 = filepath + "\\EndFreeRuler43.png";
		Screen s3 = new Screen();

		Pattern p7 = new Pattern(Screenshot8);

		Pattern p8 = new Pattern(Screenshot9);

	     s3.click(p7);

		s3.drag(p7);

		s3.dropAt(p8);
		driver.findElement(By.xpath("//*[@class='leaflet-control-nav-home']")).click();
		Log.message("Home page is clicked");
		SkySiteUtils.waitTill(4000);
		SkySiteUtils.waitTill(4000);
		SkySiteUtils.waitForElement(driver, savebtn, 50);
		savebtn.click();
		Log.message("save button is clicked");
		SkySiteUtils.waitTill(4000);
		markupfield.sendKeys(markupname);
		Log.message("Enter markup name is " + markupname);
		SkySiteUtils.waitForElement(driver, savemarkupbtn, 40);
		savemarkupbtn.click();
		Log.message("savemarkupbtn is clicked");
		SkySiteUtils.waitTill(14000);
		// driver.findElement(By.xpath(".//*[@id='imageViewer']/div[3]/div[4]/div/ul/li[2]/a")).click();
		// Log.message("save button is clicked again");

		/*markupdropdown.click();
		Log.message("markup dropdown clicked");
		SkySiteUtils.waitTill(4000);
		for (WebElement markupitem : markuplist)
			if (markupitem.getText().contains(markupname))
				driver.findElement(By.xpath("//*[@data-original-title='" + markupname + "']")).click();

		Log.message("save markup clicked and image drawn disappears");
		driver.findElement(By.xpath(".//*[@id='imageViewer']/div[3]/div[4]/div/ul/li[2]/a")).click();
		Log.message("save button is clicked again");*/

		SkySiteUtils.waitTill(5000);
		File fistemp = new File(PropertyReader.getProperty("tempScreenshot"));
		String filepathtemp = fistemp.getAbsolutePath().toString();
		String savedscreenshot1 = filepathtemp + "\\tarakoutput.png";
		String savedscreenshot2 = filepath1 + "\\Alldrawing.png";
		SkySiteUtils.waitTill(4000);
		// String tempscreenshotpath=filepathtemp + "\\temp_Screenshot.png";
		File srcFiler = ((TakesScreenshot) driver).getScreenshotAs(OutputType.FILE);

		FileUtils.copyFile(srcFiler, new File(savedscreenshot2));

		Log.message("Screenshot taken of saved markup");
		SkySiteUtils.waitTill(4000);

		BufferedImage imgA = ImageIO.read(new File(savedscreenshot1));
		SkySiteUtils.waitTill(3000);
		BufferedImage imgB = ImageIO.read(new File(savedscreenshot2));

		Thread.sleep(2000);

		Log.message("Checking the height and width of both the images are same?");

		if (imgA.getWidth() != imgB.getWidth() && imgA.getHeight() != imgB.getHeight()) {
			Log.message("image height width are different");
			return false;
		}

		int width = imgA.getWidth();
		int height = imgA.getHeight();
		for (int x = 0; x < width; x++) {
			for (int y = 0; y < height; y++) {

				if (imgA.getRGB(x, y) != imgB.getRGB(x, y)) {

					Log.message("Image are different");
					return false;
				}
			}

		}

		return true;
	}
   /**Method to draw square hyperlink to link with non viewable file
    * 
    */
	/**
	 * Method to draw and save square hyperlink
	 * 
	 * @throws IOException
	 * @throws FindFailed
	 * 
	 */
	public boolean drawAndSaveSquareHyperLinktolinknonviewablefile(String markupname, String filepath1) throws IOException, FindFailed {
		boolean flag = false;
		Log.message("Switched to default content");
		SkySiteUtils.waitTill(5000);
		driver.switchTo().frame(driver.findElement(By.id("myFrame")));
		Log.message("Switched to frame");
		driver.findElement(By.xpath("//*[text()='Nonviewablefilelink']")).click();
		Log.message("Folder selected");
		SkySiteUtils.waitTill(4000);
		driver.findElement(By.xpath(".//*[@id='divGrid']/div[2]/table/tbody/tr[2]/td[2]/a[3]")).click();
		Log.message("File is selected");
		SkySiteUtils.waitTill(4000);
		String MainWindow = driver.getWindowHandle();
		Log.message(MainWindow);
		for (String childwindow : driver.getWindowHandles()) {
			driver.switchTo().window(childwindow);

		}
		Log.message("switched to child window");

		driver.switchTo().defaultContent();
		SkySiteUtils.waitTill(4000);
		// method to delete existing markup
		this.markupdeletebegin();
		SkySiteUtils.waitTill(4000);
		hyperlinktool.click();
		Log.message("Hyperlink tool is clicked");
		SkySiteUtils.waitForElement(driver, recthyperlink, 40);
		Actions act = new Actions(driver);
		act.moveToElement(recthyperlink).clickAndHold().build().perform();
		SkySiteUtils.waitTill(2000);
		act.click(recthyperlink).build().perform();
		Log.message("Rect Hyper Link button is clicked");
		SkySiteUtils.waitTill(2000);
		File fis = new File(PropertyReader.getProperty("Screenshotpath"));
		String filepath = fis.getAbsolutePath().toString();
		String Screenshot1 = filepath + "\\StartPointFolderLevel.png";
		String Screenshot2 = filepath + "\\EndpointFolderLevel.png";
		Screen s = new Screen();

		Pattern p1 = new Pattern(Screenshot1);

		Pattern p2 = new Pattern(Screenshot2);

		// s.click(p1);

		s.drag(p1);

		s.dropAt(p2);
		Log.message("image is drawn");
		SkySiteUtils.waitTill(4000);
		// driver.findElement(By.xpath(".//*[@id='hyperLinkFolderList']/ul/li[2]/div/a/div[3]")).click();
		driver.findElement(By.xpath(".//*[@id='hyperLinkFolderList']/ul/li[16]/div/a/div[3]")).click();
		Log.message("Folder is selected");
		SkySiteUtils.waitTill(4000);
		// driver.findElement(By.xpath(".//*[@id='hyperLinkFolderList']/ul/li[2]/div/div[2]/h4/a")).click();
		driver.findElement(By.xpath(".//*[@id='hyperLinkFolderList']/ul/li[1]/div/div[2]/h4/a")).click();
		Log.message("File is selected");
		SkySiteUtils.waitForElement(driver, okbutton, 50);
		okbutton.click();
		Log.message("Ok button is clicked");

		SkySiteUtils.waitTill(4000);
		driver.findElement(By.xpath("//*[@class='leaflet-control-nav-home']")).click();
		Log.message("Home page is clicked");
		SkySiteUtils.waitForElement(driver, savebtn, 50);
		savebtn.click();
		Log.message("save button is clicked");
		SkySiteUtils.waitTill(4000);
		markupfield.sendKeys(markupname);
		Log.message("Enter markup name is " + markupname);
		SkySiteUtils.waitForElement(driver, savemarkupbtn, 40);
		savemarkupbtn.click();
		Log.message("savemarkupbtn is clicked");
		SkySiteUtils.waitTill(14000);
		driver.findElement(By.xpath(".//*[@id='imageViewer']/div[3]/div[4]/div/ul/li[2]/a")).click();
		Log.message("save button is clicked again");
		SkySiteUtils.waitTill(4000);
		String savedscreenshot1 = filepath1 + "\\BeforesavingSquareHyperLink.png";
		String savedscreenshot2 = filepath1 + "\\AfterSavingSquareHyperLink.png";
		// File fistemp=new File(PropertyReader.getProperty("tempScreenshot"));
		// String filepathtemp=fistemp.getAbsolutePath().toString();
		// String tempscreenshotpath=filepathtemp + "\\temp_Screenshot.png";
		File srcFiler = ((TakesScreenshot) driver).getScreenshotAs(OutputType.FILE);

		FileUtils.copyFile(srcFiler, new File(savedscreenshot1));

		Log.message("Screenshot taken of saved markup");
		SkySiteUtils.waitTill(4000);
		hyperlinkdropdown.click();
		Log.message("hyperlink markup dropdown clicked");
		SkySiteUtils.waitTill(4000);
		// for(WebElement markupitem:hperlinklist)
		// if(markupitem.getText().contains(markupname))
		// driver.findElement(By.xpath("//*[text()='"+markupname+"']")).click();
		driver.findElement(By.xpath(".//*[@id='btnHideAllHyperlinks']")).click();
		Log.message("hide all button clicked and image disappears");

		SkySiteUtils.waitTill(4000);
		showhyperlinks.click();
		Log.message("show all hyperlink clikced");
		SkySiteUtils.waitTill(4000);
		savebtn.click();
		Log.message("save button is clicked");
		SkySiteUtils.waitTill(4000);
		File srcFiler1 = ((TakesScreenshot) driver).getScreenshotAs(OutputType.FILE);

		FileUtils.copyFile(srcFiler1, new File(savedscreenshot2));
		Log.message("Screenshot taken after redrawing");

		BufferedImage imgA = ImageIO.read(new File(savedscreenshot1));

		// Thread.sleep(2000);

		BufferedImage imgB = ImageIO.read(new File(savedscreenshot2));

		// Thread.sleep(2000);

		Log.message("Checking the height and width of both the images are same?");

		if (imgA.getWidth() != imgB.getWidth() && imgA.getHeight() != imgB.getHeight()) {
			Log.message("image height width are different");
			return false;
		}

		int width = imgA.getWidth();
		int height = imgA.getHeight();
		for (int y = 0; y < height; y++) {
			for (int x = 0; x < width; x++) {

				if (imgA.getRGB(x, y) != imgB.getRGB(x, y)) {

					Log.message("Image are different");
					return false;
				}
			}

		}

		return true;
	}
	/**Method to verify linked non viewable file is downloaded successfully
	 * 
	 */
	public boolean VerifyLinkedNonViewableFileDownloaded(String markupname, String filepath1,String downloadpath,String filename)
			throws FindFailed, UnsupportedFlavorException, IOException {
		// driver.switchTo().defaultContent();
		this.Delete_Files_From_Folder(downloadpath);
		SkySiteUtils.waitTill(9000);
		File fistemp = new File(PropertyReader.getProperty("tempScreenshot"));
		String filepathtemp = fistemp.getAbsolutePath().toString();
		String tempscreenshotpath = filepathtemp + "\\tempfolderlevel.png";
		Screen s = new Screen();

		Pattern p1 = new Pattern(tempscreenshotpath);

		SkySiteUtils.waitTill(10000);
		s.click(p1);
		Log.message("saved hyperlink is clicked");
		SkySiteUtils.waitTill(9000);
		// JavascriptExecutor executor = (JavascriptExecutor)driver;
		// executor.executeScript("arguments[0].click();", copylink);
		// Log.message("Copy to clipboard clicked");
		/*
		 * Toolkit toolkit = Toolkit.getDefaultToolkit(); Clipboard clipboard =
		 * toolkit.getSystemClipboard(); String result = (String)
		 * clipboard.getData(DataFlavor.stringFlavor);
		 * System.out.println("String from Clipboard:" + result);
		 */
		WebElement imagelink = driver.findElement(By.id("linkImgDiv"));
		JavascriptExecutor executor = (JavascriptExecutor) driver;
		executor.executeScript("arguments[0].click();", imagelink);
		Log.message("image is clicked");
		SkySiteUtils.waitTill(9000);
		if (this.isLinkedFileDownloaded(downloadpath, filename)==true)
			return true;
		else
			return false;
	
	}
	/**
	 * Method to check if linked file is downloaded in the folder Scripted By : Tarak
	 * 
	 */
	public boolean isLinkedFileDownloaded(String downloadfolderpath, String Downloadfilename) {
		Log.message("Searching the downloaded file");
		String Filename = Downloadfilename;
		Log.message("Downloaded file name is " + Filename);
		File file = new File(downloadfolderpath);
		File[] Filearray = file.listFiles();
		SkySiteUtils.waitTill(9000);
		if (file.isDirectory()) {
			for (File Myfile : Filearray) {
				if (Myfile.getName().equals(Filename)) {
					Log.message("File is found");
					return true;
				}
			}
		}
		return false;
	}

	
	/**Method to rotate 180 degree counter clockwise
	 * @throws IOException 
	 * @throws InterruptedException 
	 * 
	 */
	public boolean rotateCounterClockWise(String markupname,String filepath1) throws IOException, InterruptedException {
		boolean flag = false;
		Log.message("Switched to default content");
		SkySiteUtils.waitTill(5000);
		driver.switchTo().frame(driver.findElement(By.id("myFrame")));
		Log.message("Switched to frame");
		driver.findElement(By.xpath("//*[text()='Rotate180']")).click();
		Log.message("Folder selected");
		SkySiteUtils.waitTill(4000);
		driver.findElement(By.xpath(".//*[@id='divGrid']/div[2]/table/tbody/tr[2]/td[2]/a[3]")).click();
		Log.message("File is selected");
		SkySiteUtils.waitTill(4000);
		String MainWindow = driver.getWindowHandle();
		Log.message(MainWindow);
		for (String childwindow : driver.getWindowHandles()) {
			driver.switchTo().window(childwindow);

		}
		Log.message("switched to child window");

		driver.switchTo().defaultContent();
		SkySiteUtils.waitTill(4000);
		// method to delete existing markup
		this.markupdeletebegin();
		SkySiteUtils.waitTill(4000);
		WebElement viewer = driver.findElement(By.xpath(".//*[@id='imageViewer']/div[3]"));
		// File fistemp=new File(PropertyReader.getProperty("tempScreenshot"));
		// String filepathtemp=fistemp.getAbsolutePath().toString();
		// String tempscreenshotpath=filepathtemp + "\\temp_Screenshot.png";

		
		SkySiteUtils.waitTill(4000);
		rotatemenu.click();
		Log.message("Rotate tool is clicked");
         WebElement counterclockwisetool=driver.findElement(By.xpath(".//*[@id='btnRotateAntiClockWise']"));
         
		SkySiteUtils.waitTill(2000);
		//WebElement aniclockicon = driver.findElement(By.id("btnRotateClockWise"));
		SkySiteUtils.waitForElement(driver, counterclockwisetool, 40);
        for(int i=0;i<2;i++) {
		Actions act = new Actions(driver);
		act.moveToElement(counterclockwisetool).click().build().perform();
		SkySiteUtils.waitTill(5000);
        }
		// JavascriptExecutor executor = (JavascriptExecutor)driver;
		// executor.executeScript("arguments[0].click();",Rotateanticlockwisemenu);
		SkySiteUtils.waitTill(2000);

		SkySiteUtils.waitTill(2000);
		saveafterrotate.click();
		Log.message("Save button is clicked");
		SkySiteUtils.waitTill(20000);

		

		File fistemp12 = new File(PropertyReader.getProperty("tempScreenshot"));
		String filepathtemp12 = fistemp12.getAbsolutePath().toString();
		String savedscreenshot1 = filepathtemp12 + "\\Resultscreenshotafterrotation.png";
		String savedscreenshot2 = filepath1 + "\\output180.png";
		SkySiteUtils.waitTill(4000);
		// String tempscreenshotpath=filepathtemp + "\\temp_Screenshot.png";
		File srcFiler = ((TakesScreenshot) driver).getScreenshotAs(OutputType.FILE);

		FileUtils.copyFile(srcFiler, new File(savedscreenshot2));

		Log.message("Screenshot taken of saved markup");
		SkySiteUtils.waitTill(4000);

		BufferedImage imgA = ImageIO.read(new File(savedscreenshot1));
		SkySiteUtils.waitTill(3000);
		BufferedImage imgB = ImageIO.read(new File(savedscreenshot2));

		Thread.sleep(2000);

		Log.message("Checking the height and width of both the images are same?");

		if (imgA.getWidth() != imgB.getWidth() && imgA.getHeight() != imgB.getHeight()) {
			Log.message("image height width are different");
			return false;
		}

		int width = imgA.getWidth();
		int height = imgA.getHeight();
		for (int x = 0; x < width; x++) {
			for (int y = 0; y < height; y++) {

				if (imgA.getRGB(x, y) != imgB.getRGB(x, y)) {

					Log.message("Image are different");
					return false;
				}
			}

		}

		return true;
  }
	/**Method to change the 180 rotated file to default
	 * 
	 */
	public void changecounterrotatedfile() {

			SkySiteUtils.waitTill(4000);
			WebElement viewer = driver.findElement(By.xpath(".//*[@id='imageViewer']/div[3]"));
			// File fistemp=new File(PropertyReader.getProperty("tempScreenshot"));
			// String filepathtemp=fistemp.getAbsolutePath().toString();
			// String tempscreenshotpath=filepathtemp + "\\temp_Screenshot.png";

			
			SkySiteUtils.waitTill(4000);
			rotatemenu.click();
			Log.message("Rotate tool is clicked");

			SkySiteUtils.waitTill(2000);
			WebElement aniclockicon = driver.findElement(By.xpath(".//*[@id='btnRotateClockWise']"));
			SkySiteUtils.waitForElement(driver, aniclockicon, 40);
            for(int i=0;i<2;i++) {
			Actions act = new Actions(driver);
			act.moveToElement(aniclockicon).click().build().perform();
            }
			// JavascriptExecutor executor = (JavascriptExecutor)driver;
			// executor.executeScript("arguments[0].click();",Rotateanticlockwisemenu);
			SkySiteUtils.waitTill(2000);

			SkySiteUtils.waitTill(2000);
			saveafterrotate.click();
			Log.message("Save button is clicked");
			SkySiteUtils.waitTill(20000);
			Log.message("File is rotated and changed to default state successfully");

		}
      @FindBy(xpath=".//*[@id='btnSubmitPassword']")
      WebElement submitbutton;
	
	/**Method to open password protected file in viewer
	 * @throws InterruptedException 
	 * @throws FindFailed 
	 * @throws IOException 
	 * 
	 */
	public boolean openPasswordProtectedFile(String markup,String filepath1) throws InterruptedException, FindFailed, IOException {

		boolean flag = false;
		Log.message("Switched to default content");
		SkySiteUtils.waitTill(5000);
		driver.switchTo().frame(driver.findElement(By.id("myFrame")));
		Log.message("Switched to frame");
		driver.findElement(By.xpath("//*[text()='Passwordprotected']")).click();
		Log.message("Folder selected");
		SkySiteUtils.waitTill(4000);
		driver.findElement(By.xpath(".//*[@id='divGrid']/div[2]/table/tbody/tr[2]/td[2]/a[3]")).click();
		Log.message("File is selected");
		SkySiteUtils.waitTill(4000);
		SkySiteUtils.waitTill(4000);
		String MainWindow = driver.getWindowHandle();
		Log.message(MainWindow);
		for (String childwindow : driver.getWindowHandles()) {
			driver.switchTo().window(childwindow);

		}
		Log.message("switched to child window");

		driver.switchTo().defaultContent();
		SkySiteUtils.waitTill(4000);
		// method to delete existing markup
		//this.markupdeletebegin();
		SkySiteUtils.waitTill(4000);
	    driver.findElement(By.xpath(".//*[@id='txtFilePassword']")).sendKeys("Arcind@123");
	    Log.message("Password is entered");
	    submitbutton.click();
	    Log.message("Submit button is clicked");
	    SkySiteUtils.waitTill(7000);
		File fistemp = new File(PropertyReader.getProperty("tempScreenshot"));
		String filepathtemp = fistemp.getAbsolutePath().toString();
		String savedscreenshot1 = filepathtemp + "\\Passwordprotectfile.png";
		String savedscreenshot2 = filepath1 + "\\output57.png";

		// String tempscreenshotpath=filepathtemp + "\\temp_Screenshot.png";
		File srcFiler = ((TakesScreenshot) driver).getScreenshotAs(OutputType.FILE);

		FileUtils.copyFile(srcFiler, new File(savedscreenshot2));

		Log.message("Screenshot taken of saved markup");
		SkySiteUtils.waitTill(4000);

		BufferedImage imgA = ImageIO.read(new File(savedscreenshot1));
		SkySiteUtils.waitTill(3000);
		BufferedImage imgB = ImageIO.read(new File(savedscreenshot2));

		Thread.sleep(2000);

		Log.message("Checking the height and width of both the images are same?");
		Thread.sleep(6000);
		if (imgA.getWidth() != imgB.getWidth() && imgA.getHeight() != imgB.getHeight()) {
			Log.message("image height width are different");
			return false;
		}

		int width = imgA.getWidth();
		int height = imgA.getHeight();
		for (int x = 0; x < width; x++) {
			for (int y = 0; y < height; y++) {
				if (imgA.getRGB(x, y) != imgB.getRGB(x, y)) {

					Log.message("Image are different");
					return false;
				}
			}

		}

		return true;
	}
	/**
	 * Method to draw and save hyperlink for multipage file
	 * 
	 * @throws FindFailed
	 * @throws IOException
	 * 
	 */
	public boolean drawAndSaveHyperLinkMultiPagewithmarkup(String markupname, String filepath1) throws FindFailed, IOException {
		boolean flag = false;
		Log.message("Switched to default content");
		SkySiteUtils.waitTill(5000);
		driver.switchTo().frame(driver.findElement(By.id("myFrame")));
		Log.message("Switched to frame");
		driver.findElement(By.xpath("//*[text()='Multipagewithmarkup']")).click();
		Log.message("Multi page folder");
		SkySiteUtils.waitTill(4000);
		driver.findElement(By.xpath(".//*[@id='divGrid']/div[2]/table/tbody/tr[2]/td[2]/a[3]")).click();
		Log.message("File is selected");
		SkySiteUtils.waitTill(4000);
		String MainWindow = driver.getWindowHandle();
		Log.message(MainWindow);
		for (String childwindow : driver.getWindowHandles()) {
			driver.switchTo().window(childwindow);

		}
		Log.message("switched to child window");

		driver.switchTo().defaultContent();
		SkySiteUtils.waitTill(4000);
		// method to delete existing markup
		this.markupdeltemultipage();
		SkySiteUtils.waitTill(4000);
		hyperlinktool.click();
		Log.message("Hyperlink tool is clicked");
		SkySiteUtils.waitForElement(driver, recthyperlink, 40);
		Actions act = new Actions(driver);
		act.moveToElement(recthyperlink).clickAndHold().build().perform();
		SkySiteUtils.waitTill(2000);
		act.click(recthyperlink).build().perform();
		Log.message("Rect Hyper Link button is clicked");
		SkySiteUtils.waitTill(2000);
		File fis = new File(PropertyReader.getProperty("Screenshotpath"));
		String filepath = fis.getAbsolutePath().toString();
		String Screenshot1 = filepath + "\\StartPoint_MultiPageFile.png";
		String Screenshot2 = filepath + "\\EndPoint_MultiPageFile.png";
		Screen s = new Screen();

		Pattern p1 = new Pattern(Screenshot1);

		Pattern p2 = new Pattern(Screenshot2);

		// s.click(p1);

		s.drag(p1);

		s.dropAt(p2);
		Log.message("image is drawn");
		SkySiteUtils.waitTill(4000);
		// driver.findElement(By.xpath(".//*[@id='hyperLinkFolderList']/ul/li[2]/div/a/div[3]")).click();
		driver.findElement(By.xpath(".//*[@id='hyperLinkFolderList']/ul/li[19]/div/a/div[3]")).click();
		Log.message(" Mutli Page File Folder is selected again for linking");
		SkySiteUtils.waitTill(4000);
		driver.findElement(By.xpath(".//*[@id='hyperLinkFolderList']/ul/li/div/div[2]/h4/a")).click();
		Log.message("File  is selected");
		driver.findElement(By.xpath(".//*[@id='dvHLPage_1']/div/ul/li[14]/a")).click();
		Log.message("9th page is selected");
		// driver.findElement(By.xpath(".//*[@id='dvHLPage_1']/div/ul/li[1]/a")).click();
		// Log.message("1st page folder is clicked");
		SkySiteUtils.waitForElement(driver, okbutton, 50);
		okbutton.click();
		Log.message("Ok button is clicked");

		SkySiteUtils.waitTill(4000);
		driver.findElement(By.xpath("//*[@class='leaflet-control-nav-home']")).click();
		Log.message("Home page is clicked");
		SkySiteUtils.waitForElement(driver, savebtn, 50);
		savebtn.click();
		Log.message("save button is clicked");
		SkySiteUtils.waitTill(4000);
		markupfield.sendKeys(markupname);
		Log.message("Enter markup name is " + markupname);
		SkySiteUtils.waitForElement(driver, savemarkupbtn, 40);
		savemarkupbtn.click();
		Log.message("savemarkupbtn is clicked");
		SkySiteUtils.waitTill(14000);

		String savedscreenshot1 = filepath1 + "\\BeforesavingSquareHyperLink.png";
		String savedscreenshot2 = filepath1 + "\\AfterSavingSquareHyperLink.png";
		// File fistemp=new File(PropertyReader.getProperty("tempScreenshot"));
		// String filepathtemp=fistemp.getAbsolutePath().toString();
		// String tempscreenshotpath=filepathtemp + "\\temp_Screenshot.png";
		File srcFiler = ((TakesScreenshot) driver).getScreenshotAs(OutputType.FILE);

		FileUtils.copyFile(srcFiler, new File(savedscreenshot1));

		Log.message("Screenshot taken of saved markup");
		SkySiteUtils.waitTill(4000);
		hyperlinkdropdown.click();
		Log.message("hyperlink markup dropdown clicked");
		SkySiteUtils.waitTill(4000);
		// for(WebElement markupitem:hperlinklist)
		// if(markupitem.getText().contains(markupname))
		// driver.findElement(By.xpath("//*[text()='"+markupname+"']")).click();
		driver.findElement(By.xpath(".//*[@id='btnHideAllHyperlinks']")).click();
		Log.message("hide all button clicked and image disappears");

		SkySiteUtils.waitTill(4000);
		showhyperlinks.click();
		Log.message("show all hyperlink clikced");
		SkySiteUtils.waitTill(4000);
		savebtn.click();
		Log.message("save button is clicked");
		SkySiteUtils.waitTill(4000);
		File srcFiler1 = ((TakesScreenshot) driver).getScreenshotAs(OutputType.FILE);

		FileUtils.copyFile(srcFiler1, new File(savedscreenshot2));
		Log.message("Screenshot taken after redrawing");

		BufferedImage imgA = ImageIO.read(new File(savedscreenshot1));

		// Thread.sleep(2000);

		BufferedImage imgB = ImageIO.read(new File(savedscreenshot2));

		// Thread.sleep(2000);

		Log.message("Checking the height and width of both the images are same?");

		if (imgA.getWidth() == imgB.getWidth() && imgA.getHeight() == imgB.getHeight()) {

			for (int x = 0; x < imgA.getWidth(); x++) {

				for (int y = 0; y < imgB.getHeight(); y++) {

					if (imgA.getRGB(x, y) == imgB.getRGB(x, y)) {

						flag = true;
						break;
					}
				}

			}
		}
		if (flag == true) {
			Log.message("images are equal");
			return true;
		} else {
			Log.message("images are not equal");
			return false;
		}

	}
 /**Method to link multipage with markup drawn
 * @throws IOException 
 * @throws FindFailed 
 * @throws InterruptedException 
  * 
  */ 
public boolean multipageLinkWithMarkupDrawing(String markupname,String filepath1) throws IOException, FindFailed, InterruptedException {
	
	driver.switchTo().defaultContent();
	File fistemp = new File(PropertyReader.getProperty("tempScreenshot"));
	String filepathtemp = fistemp.getAbsolutePath().toString();
	String tempscreenshotpath = filepathtemp + "\\temphyperlinkmultipage.PNG";
	Screen s = new Screen();

	Pattern p1 = new Pattern(tempscreenshotpath);
	s.click(p1);
	Log.message("saved hyperlink is clicked");
	SkySiteUtils.waitTill(4000);
	WebElement imagelink = driver.findElement(By.id("linkImgDiv"));
	JavascriptExecutor executor = (JavascriptExecutor) driver;
	executor.executeScript("arguments[0].click();", imagelink);
	Log.message("image is clicked");
	SkySiteUtils.waitTill(4000);
	
	WebElement viewallbtn=driver.findElement(By.xpath(".//*[@id='btnViewAllGlobal']"));
	// String
	// filename=driver.findElement(By.xpath("//*[@data-original-title='1_Document.pdf']")).getText();
	viewallbtn.click();
	Log.message("view all button is clicked");
	SkySiteUtils.waitTill(4000);
	
	SkySiteUtils.waitTill(5000);
	
	File fistemp12 = new File(PropertyReader.getProperty("tempScreenshot"));
	String filepathtemp12 = fistemp12.getAbsolutePath().toString();
	String savedscreenshot1 = filepathtemp12 + "\\Multipagefile.png";
	String savedscreenshot2 = filepath1 + "\\OutputMultipagefile.png";
	SkySiteUtils.waitTill(4000);
	// String tempscreenshotpath=filepathtemp + "\\temp_Screenshot.png";
	File srcFiler = ((TakesScreenshot) driver).getScreenshotAs(OutputType.FILE);

	FileUtils.copyFile(srcFiler, new File(savedscreenshot2));

	Log.message("Screenshot taken of saved markup");
	SkySiteUtils.waitTill(4000);

	BufferedImage imgA = ImageIO.read(new File(savedscreenshot1));
	SkySiteUtils.waitTill(3000);
	BufferedImage imgB = ImageIO.read(new File(savedscreenshot2));

	Thread.sleep(2000);

	Log.message("Checking the height and width of both the images are same?");

	if (imgA.getWidth() != imgB.getWidth() && imgA.getHeight() != imgB.getHeight()) {
		Log.message("image height width are different");
		return false;
	}

	int width = imgA.getWidth();
	int height = imgA.getHeight();
	for (int x = 0; x < width; x++) {
		for (int y = 0; y < height; y++) {

			if (imgA.getRGB(x, y) != imgB.getRGB(x, y)) {

				Log.message("Image are different");
				return false;
			}
		}

	}

	return true;
}
/**
 * Method to draw and save square hyperlink for diff folder and diff file with markup
 * 
 * @throws IOException
 * @throws FindFailed
 * 
 */
public boolean drawAndSaveSquareHyperLinkForDiffFolderDifffileWithMarkup(String markupname, String filepath1)
		throws IOException, FindFailed {
	boolean flag = false;
	Log.message("Switched to default content");
	SkySiteUtils.waitTill(5000);
	driver.switchTo().frame(driver.findElement(By.id("myFrame")));
	Log.message("Switched to frame");
	driver.findElement(By.xpath(".//*[text()='FolderMarkuplink']")).click();
	Log.message("Folder selected");
	SkySiteUtils.waitTill(4000);
	driver.findElement(By.xpath(".//*[@id='divGrid']/div[2]/table/tbody/tr[2]/td[2]/a[3]")).click();
	Log.message("File is selected");
	SkySiteUtils.waitTill(4000);
	String MainWindow = driver.getWindowHandle();
	Log.message(MainWindow);
	for (String childwindow : driver.getWindowHandles()) {
		driver.switchTo().window(childwindow);

	}
	Log.message("switched to child window");

	driver.switchTo().defaultContent();
	SkySiteUtils.waitTill(4000);
	// method to delete existing markup
	this.markupdeletebegin();
	SkySiteUtils.waitTill(4000);
	hyperlinktool.click();
	Log.message("Hyperlink tool is clicked");
	SkySiteUtils.waitForElement(driver, recthyperlink, 40);
	Actions act = new Actions(driver);
	act.moveToElement(recthyperlink).clickAndHold().build().perform();
	SkySiteUtils.waitTill(2000);
	act.click(recthyperlink).build().perform();
	Log.message("Rect Hyper Link button is clicked");
	SkySiteUtils.waitTill(2000);
	File fis = new File(PropertyReader.getProperty("Screenshotpath"));
	String filepath = fis.getAbsolutePath().toString();
	String Screenshot1 = filepath + "\\StartPointFolderLevel.png";
	String Screenshot2 = filepath + "\\EndpointFolderLevel.png";
	Screen s = new Screen();

	Pattern p1 = new Pattern(Screenshot1);

	Pattern p2 = new Pattern(Screenshot2);

	// s.click(p1);

	s.drag(p1);

	s.dropAt(p2);
	Log.message("image is drawn");
	SkySiteUtils.waitTill(4000);
	// driver.findElement(By.xpath(".//*[@id='hyperLinkFolderList']/ul/li[2]/div/a/div[3]")).click();
	driver.findElement(By.xpath(".//*[@id='hyperLinkFolderList']/ul/li[1]/div/a/div[3]")).click();
	Log.message("Folder is selected");
	SkySiteUtils.waitTill(4000);
	// driver.findElement(By.xpath(".//*[@id='hyperLinkFolderList']/ul/li[2]/div/div[2]/h4/a")).click();
	driver.findElement(By.xpath(".//*[@id='hyperLinkFolderList']/ul/li/div/div[2]/h4/a")).click();
	Log.message("File is selected");
	SkySiteUtils.waitTill(3000);
	//driver.findElement(By.xpath(".//*[@id='dvHLPage_2']/div/ul/li[1]/a")).click();
//	Log.message("Page1 is selected");
	SkySiteUtils.waitForElement(driver, okbutton, 50);
	okbutton.click();
	Log.message("Ok button is clicked");

	SkySiteUtils.waitTill(4000);
	driver.findElement(By.xpath("//*[@class='leaflet-control-nav-home']")).click();
	Log.message("Home page is clicked");
	SkySiteUtils.waitForElement(driver, savebtn, 50);
	savebtn.click();
	Log.message("save button is clicked");
	SkySiteUtils.waitTill(4000);
	markupfield.sendKeys(markupname);
	Log.message("Enter markup name is " + markupname);
	SkySiteUtils.waitForElement(driver, savemarkupbtn, 40);
	savemarkupbtn.click();
	Log.message("savemarkupbtn is clicked");
	SkySiteUtils.waitTill(14000);
	driver.findElement(By.xpath(".//*[@id='imageViewer']/div[3]/div[4]/div/ul/li[2]/a")).click();
	Log.message("save button is clicked again");
	SkySiteUtils.waitTill(4000);

	String savedscreenshot1 = filepath1 + "\\BeforesavingSquareHyperLink.png";
	String savedscreenshot2 = filepath1 + "\\AfterSavingSquareHyperLink.png";
	// File fistemp=new File(PropertyReader.getProperty("tempScreenshot"));
	// String filepathtemp=fistemp.getAbsolutePath().toString();
	// String tempscreenshotpath=filepathtemp + "\\temp_Screenshot.png";
	File srcFiler = ((TakesScreenshot) driver).getScreenshotAs(OutputType.FILE);

	FileUtils.copyFile(srcFiler, new File(savedscreenshot1));

	Log.message("Screenshot taken of saved markup");
	SkySiteUtils.waitTill(4000);
	hyperlinkdropdown.click();
	Log.message("hyperlink markup dropdown clicked");
	SkySiteUtils.waitTill(4000);
	// for(WebElement markupitem:hperlinklist)
	// if(markupitem.getText().contains(markupname))
	// driver.findElement(By.xpath("//*[text()='"+markupname+"']")).click();
	driver.findElement(By.xpath(".//*[@id='btnHideAllHyperlinks']")).click();
	Log.message("hide all button clicked and image disappears");

	SkySiteUtils.waitTill(4000);
	showhyperlinks.click();
	Log.message("show all hyperlink clikced");
	SkySiteUtils.waitTill(4000);
	savebtn.click();
	Log.message("save button is clicked");
	SkySiteUtils.waitTill(4000);
	File srcFiler1 = ((TakesScreenshot) driver).getScreenshotAs(OutputType.FILE);

	FileUtils.copyFile(srcFiler1, new File(savedscreenshot2));
	Log.message("Screenshot taken after redrawing");

	BufferedImage imgA = ImageIO.read(new File(savedscreenshot1));

	// Thread.sleep(2000);

	BufferedImage imgB = ImageIO.read(new File(savedscreenshot2));

	// Thread.sleep(2000);

	Log.message("Checking the height and width of both the images are same?");

	if (imgA.getWidth() != imgB.getWidth() && imgA.getHeight() != imgB.getHeight()) {
		Log.message("image height width are different");
		return false;
	}

	int width = imgA.getWidth();
	int height = imgA.getHeight();
	for (int y = 0; y < height; y++) {
		for (int x = 0; x < width; x++) {

			if (imgA.getRGB(x, y) != imgB.getRGB(x, y)) {

				Log.message("Image are different");
				return false;
			}
		}

	}

	return true;
}
/**
 * Method to verify working of square hyperlink for diff folder diff file withv markup
 * 
 * @throws FindFailed
 * @throws IOException
 * @throws UnsupportedFlavorException
 * @throws InterruptedException 
 * 
 */

public boolean VerifyfunctionalitySquareHyperLinkdiffFolderDiffFileWithMarkup(String markupname, String filepath1)
		throws FindFailed, UnsupportedFlavorException, IOException, InterruptedException {
	// driver.switchTo().defaultContent();
	SkySiteUtils.waitTill(8000);
	File fistemp = new File(PropertyReader.getProperty("tempScreenshot"));
	String filepathtemp = fistemp.getAbsolutePath().toString();
	String tempscreenshotpath = filepathtemp + "\\tempfolderlevel.png";
	Screen s = new Screen();

	Pattern p1 = new Pattern(tempscreenshotpath);

	SkySiteUtils.waitTill(4000);
	s.click(p1);
	Log.message("saved hyperlink is clicked");
	SkySiteUtils.waitTill(4000);
	// JavascriptExecutor executor = (JavascriptExecutor)driver;
	// executor.executeScript("arguments[0].click();", copylink);
	// Log.message("Copy to clipboard clicked");
	/*
	 * Toolkit toolkit = Toolkit.getDefaultToolkit(); Clipboard clipboard =
	 * toolkit.getSystemClipboard(); String result = (String)
	 * clipboard.getData(DataFlavor.stringFlavor);
	 * System.out.println("String from Clipboard:" + result);
	 */
	WebElement imagelink = driver.findElement(By.id("linkImgDiv"));
	JavascriptExecutor executor = (JavascriptExecutor) driver;
	executor.executeScript("arguments[0].click();", imagelink);
	Log.message("image is clicked");
	SkySiteUtils.waitTill(5000);
	WebElement viewallbtn=driver.findElement(By.xpath(".//*[@id='btnViewAllGlobal']"));
	SkySiteUtils.waitForElement(driver, viewallbtn, 60);
	viewallbtn.click();
	Log.message("view all button is clicked");
	SkySiteUtils.waitTill(4000);
	File fistemp2= new File(PropertyReader.getProperty("tempScreenshot"));
	String filepathtemp2 = fistemp2.getAbsolutePath().toString();
	String savedscreenshot1 = filepathtemp2 + "\\resultlinkfilewithmarkup.png";
	String savedscreenshot2 = filepath1 + "\\outputpicwithmarkup.png";
	SkySiteUtils.waitTill(4000);
	// String tempscreenshotpath=filepathtemp + "\\temp_Screenshot.png";
	File srcFiler = ((TakesScreenshot) driver).getScreenshotAs(OutputType.FILE);

	FileUtils.copyFile(srcFiler, new File(savedscreenshot2));

	Log.message("Screenshot taken of saved markup");
	SkySiteUtils.waitTill(4000);
	 
BufferedImage imgA = ImageIO.read(new File(savedscreenshot1));
SkySiteUtils.waitTill(3000);
BufferedImage imgB = ImageIO.read(new File(savedscreenshot2));

			Thread.sleep(2000);

			Log.message("Checking the height and width of both the images are same?");
			Thread.sleep(6000);
			if (imgA.getWidth() != imgB.getWidth() && imgA.getHeight() != imgB.getHeight()) {
				Log.message("image height width are different");
				return false;
			}

			int width = imgA.getWidth();
			int height = imgA.getHeight();
			for (int x = 0; x < width; x++) {
				for (int y = 0; y < height; y++) {
					if (imgA.getRGB(x, y) != imgB.getRGB(x, y)) {

						Log.message("Image are different");
						return false;
					}
				}

			}

			return true;
		}
      @FindBy(xpath=".//*[@id='imageViewer']/div[3]/div[1]/div/ul/li[4]/a/i")
      WebElement fullscreen;
/**
 * Method to draw and save square hyperlink for same folder diff file with full screen
 * 
 * @throws IOException
 * @throws FindFailed
 * 
 */
public boolean drawAndSaveSquareHyperLinkOmnFullScreen(String markupname, String filepath1) throws IOException, FindFailed {
	boolean flag = false;
	Log.message("Switched to default content");
	SkySiteUtils.waitTill(5000);
	driver.switchTo().frame(driver.findElement(By.id("myFrame")));
	Log.message("Switched to frame");
	driver.findElement(By.xpath("//*[text()='Viewer_Testing_CircleHyperLink']")).click();
	Log.message("Folder selected");
	SkySiteUtils.waitTill(4000);
	driver.findElement(By.xpath(".//*[@id='divGrid']/div[2]/table/tbody/tr[2]/td[2]/a[3]")).click();
	Log.message("File is selected");
	SkySiteUtils.waitTill(4000);
	String MainWindow = driver.getWindowHandle();
	Log.message(MainWindow);
	for (String childwindow : driver.getWindowHandles()) {
		driver.switchTo().window(childwindow);

	}
	Log.message("switched to child window");

	driver.switchTo().defaultContent();
	SkySiteUtils.waitTill(4000);
	// method to delete existing markup
	this.markupdeletebegin();
	SkySiteUtils.waitTill(4000);
	fullscreen.click();
	Log.message("Full screen is enabled");
	
	SkySiteUtils.waitTill(4000);
	hyperlinktool.click();
	Log.message("Hyperlink tool is clicked");
	SkySiteUtils.waitForElement(driver, recthyperlink, 40);
	Actions act = new Actions(driver);
	act.moveToElement(recthyperlink).clickAndHold().build().perform();
	SkySiteUtils.waitTill(2000);
	act.click(recthyperlink).build().perform();
	Log.message("Rect Hyper Link button is clicked");
	SkySiteUtils.waitTill(2000);
	File fis = new File(PropertyReader.getProperty("Screenshotpath"));
	String filepath = fis.getAbsolutePath().toString();
	String Screenshot1 = filepath + "\\StartPointFolderLevel.png";
	String Screenshot2 = filepath + "\\EndpointFolderLevel.png";
	Screen s = new Screen();

	Pattern p1 = new Pattern(Screenshot1);

	Pattern p2 = new Pattern(Screenshot2);

	// s.click(p1);

	s.drag(p1);

	s.dropAt(p2);
	Log.message("image is drawn");
	SkySiteUtils.waitTill(4000);
	// driver.findElement(By.xpath(".//*[@id='hyperLinkFolderList']/ul/li[2]/div/a/div[3]")).click();
	driver.findElement(By.xpath(".//*[@id='hyperLinkFolderList']/ul/li[3]/div/a/div[3]")).click();
	Log.message("Folder is selected");
	SkySiteUtils.waitTill(4000);
	// driver.findElement(By.xpath(".//*[@id='hyperLinkFolderList']/ul/li[2]/div/div[2]/h4/a")).click();
	driver.findElement(By.xpath(".//*[@id='hyperLinkFolderList']/ul/li[1]/div/div[2]/h4/a")).click();
	Log.message("File is selected");
	SkySiteUtils.waitForElement(driver, okbutton, 50);
	okbutton.click();
	Log.message("Ok button is clicked");

	SkySiteUtils.waitTill(4000);
	driver.findElement(By.xpath("//*[@class='leaflet-control-nav-home']")).click();
	Log.message("Home page is clicked");
	SkySiteUtils.waitForElement(driver, savebtn, 50);
	savebtn.click();
	Log.message("save button is clicked");
	SkySiteUtils.waitTill(4000);
	markupfield.sendKeys(markupname);
	Log.message("Enter markup name is " + markupname);
	SkySiteUtils.waitForElement(driver, savemarkupbtn, 40);
	savemarkupbtn.click();
	Log.message("savemarkupbtn is clicked");
	SkySiteUtils.waitTill(14000);
	driver.findElement(By.xpath(".//*[@id='imageViewer']/div[3]/div[4]/div/ul/li[2]/a")).click();
	Log.message("save button is clicked again");
	SkySiteUtils.waitTill(4000);
	String savedscreenshot1 = filepath1 + "\\BeforesavingSquareHyperLink.png";
	String savedscreenshot2 = filepath1 + "\\AfterSavingSquareHyperLink.png";
	// File fistemp=new File(PropertyReader.getProperty("tempScreenshot"));
	// String filepathtemp=fistemp.getAbsolutePath().toString();
	// String tempscreenshotpath=filepathtemp + "\\temp_Screenshot.png";
	File srcFiler = ((TakesScreenshot) driver).getScreenshotAs(OutputType.FILE);

	FileUtils.copyFile(srcFiler, new File(savedscreenshot1));

	Log.message("Screenshot taken of saved markup");
	SkySiteUtils.waitTill(4000);
	hyperlinkdropdown.click();
	Log.message("hyperlink markup dropdown clicked");
	SkySiteUtils.waitTill(4000);
	// for(WebElement markupitem:hperlinklist)
	// if(markupitem.getText().contains(markupname))
	// driver.findElement(By.xpath("//*[text()='"+markupname+"']")).click();
	driver.findElement(By.xpath(".//*[@id='btnHideAllHyperlinks']")).click();
	Log.message("hide all button clicked and image disappears");

	SkySiteUtils.waitTill(4000);
	showhyperlinks.click();
	Log.message("show all hyperlink clikced");
	SkySiteUtils.waitTill(4000);
	savebtn.click();
	Log.message("save button is clicked");
	SkySiteUtils.waitTill(4000);
	File srcFiler1 = ((TakesScreenshot) driver).getScreenshotAs(OutputType.FILE);

	FileUtils.copyFile(srcFiler1, new File(savedscreenshot2));
	Log.message("Screenshot taken after redrawing");

	BufferedImage imgA = ImageIO.read(new File(savedscreenshot1));

	// Thread.sleep(2000);

	BufferedImage imgB = ImageIO.read(new File(savedscreenshot2));

	// Thread.sleep(2000);

	Log.message("Checking the height and width of both the images are same?");

	if (imgA.getWidth() != imgB.getWidth() && imgA.getHeight() != imgB.getHeight()) {
		Log.message("image height width are different");
		return false;
	}

	int width = imgA.getWidth();
	int height = imgA.getHeight();
	for (int y = 0; y < height; y++) {
		for (int x = 0; x < width; x++) {

			if (imgA.getRGB(x, y) != imgB.getRGB(x, y)) {

				Log.message("Image are different");
				return false;
			}
		}

	}

	return true;
}
/**
 * Method to verify working of square hyperlink for same folder diff file on full screen
 * 
 * @throws FindFailed
 * @throws IOException
 * @throws UnsupportedFlavorException
 * @throws AWTException 
 * 
 */
public boolean VerifyfunctionalitySquareHyperLinkSameFolderDiffFileOnFullScreen(String markupname, String filepath1)
		throws FindFailed, UnsupportedFlavorException, IOException, AWTException {
	// driver.switchTo().defaultContent();
	SkySiteUtils.waitTill(9000);
	File fistemp = new File(PropertyReader.getProperty("tempScreenshot"));
	String filepathtemp = fistemp.getAbsolutePath().toString();
	String tempscreenshotpath = filepathtemp + "\\tempfolderlevel.png";
	Screen s = new Screen();

	Pattern p1 = new Pattern(tempscreenshotpath);

	SkySiteUtils.waitTill(10000);
	s.click(p1);
	Log.message("saved hyperlink is clicked");
	SkySiteUtils.waitTill(4000);
	// JavascriptExecutor executor = (JavascriptExecutor)driver;
	// executor.executeScript("arguments[0].click();", copylink);
	// Log.message("Copy to clipboard clicked");
	/*
	 * Toolkit toolkit = Toolkit.getDefaultToolkit(); Clipboard clipboard =
	 * toolkit.getSystemClipboard(); String result = (String)
	 * clipboard.getData(DataFlavor.stringFlavor);
	 * System.out.println("String from Clipboard:" + result);
	 */
	WebElement imagelink = driver.findElement(By.id("linkImgDiv"));
	JavascriptExecutor executor = (JavascriptExecutor) driver;
	executor.executeScript("arguments[0].click();", imagelink);
	Log.message("image is clicked");
    SkySiteUtils.waitTill(4000);
    Robot rb=new Robot();
    rb.keyPress(KeyEvent.VK_ESCAPE);
    rb.keyRelease(KeyEvent.VK_ESCAPE);
    Log.message("Escape is entered");
    SkySiteUtils.waitTill(4000);
	String filename = driver.findElement(By.xpath("//*[@data-original-title='1_EName.pdf']")).getText();
	Log.message("File name is " + filename);
	String expectedfilename = "(R-1) 1_EName.pdf";
	if (filename.equals(expectedfilename))
		return true;

	else
		return false;

}
/**
 * Method to edit hyperlink 
 * 
 * @throws IOException
 * @throws FindFailed
 * 
 */
public boolean drawAndSaveSquareHyperlinkafterChanging(String markupname, String filepath1)
		throws IOException, FindFailed {
	boolean flag = false;
	Log.message("Switched to default content");
	SkySiteUtils.waitTill(5000);
	driver.switchTo().frame(driver.findElement(By.id("myFrame")));
	Log.message("Switched to frame");
	driver.findElement(By.xpath(".//*[text()='Viewertesting']")).click();
	Log.message("Folder selected");
	SkySiteUtils.waitTill(4000);
	driver.findElement(By.xpath(".//*[@id='divGrid']/div[2]/table/tbody/tr[2]/td[2]/a[3]")).click();
	Log.message("File is selected");
	SkySiteUtils.waitTill(4000);
	String MainWindow = driver.getWindowHandle();
	Log.message(MainWindow);
	for (String childwindow : driver.getWindowHandles()) {
		driver.switchTo().window(childwindow);

	}
	Log.message("switched to child window");

	driver.switchTo().defaultContent();
	SkySiteUtils.waitTill(4000);
	// method to delete existing markup
	this.markupdeletebegin();
	SkySiteUtils.waitTill(4000);
	hyperlinktool.click();
	Log.message("Hyperlink tool is clicked");
	SkySiteUtils.waitForElement(driver, recthyperlink, 40);
	Actions act = new Actions(driver);
	act.moveToElement(recthyperlink).clickAndHold().build().perform();
	SkySiteUtils.waitTill(2000);
	act.click(recthyperlink).build().perform();
	Log.message("Rect Hyper Link button is clicked");
	SkySiteUtils.waitTill(2000);
	File fis = new File(PropertyReader.getProperty("Screenshotpath"));
	String filepath = fis.getAbsolutePath().toString();
	String Screenshot1 = filepath + "\\StartPointFolderLevel.png";
	String Screenshot2 = filepath + "\\EndpointFolderLevel.png";
	Screen s = new Screen();

	Pattern p1 = new Pattern(Screenshot1);

	Pattern p2 = new Pattern(Screenshot2);

	// s.click(p1);

	s.drag(p1);

	s.dropAt(p2);
	Log.message("image is drawn");
	SkySiteUtils.waitTill(4000);
	// driver.findElement(By.xpath(".//*[@id='hyperLinkFolderList']/ul/li[2]/div/a/div[3]")).click();
	driver.findElement(By.xpath(".//*[@id='hyperLinkFolderList']/ul/li[3]/div/a/div[3]")).click();
	Log.message("Folder is selected");
	SkySiteUtils.waitTill(4000);
	// driver.findElement(By.xpath(".//*[@id='hyperLinkFolderList']/ul/li[2]/div/div[2]/h4/a")).click();
	driver.findElement(By.xpath(".//*[@id='hyperLinkFolderList']/ul/li[2]/div/div[2]/h4/a")).click();
	Log.message("File is selected");
	SkySiteUtils.waitTill(3000);
	driver.findElement(By.xpath(".//*[@id='btnhyperLinkEdit']")).click();
	Log.message("Change link option is clicked");
	SkySiteUtils.waitTill(4000);
	driver.findElement(By.xpath(".//*[@id='hyperLinkFolderList']/ul/li[1]/div/div[2]/h4/a")).click();
	Log.message("Original file to be link is selected");
	//driver.findElement(By.xpath(".//*[@id='dvHLPage_2']/div/ul/li[1]/a")).click();
//	Log.message("Page1 is selected");
	SkySiteUtils.waitForElement(driver, okbutton, 50);
	okbutton.click();
	Log.message("Ok button is clicked");

	SkySiteUtils.waitTill(4000);
	driver.findElement(By.xpath("//*[@class='leaflet-control-nav-home']")).click();
	Log.message("Home page is clicked");
	SkySiteUtils.waitForElement(driver, savebtn, 50);
	savebtn.click();
	Log.message("save button is clicked");
	SkySiteUtils.waitTill(4000);
	markupfield.sendKeys(markupname);
	Log.message("Enter markup name is " + markupname);
	SkySiteUtils.waitForElement(driver, savemarkupbtn, 40);
	savemarkupbtn.click();
	Log.message("savemarkupbtn is clicked");
	SkySiteUtils.waitTill(14000);
	driver.findElement(By.xpath(".//*[@id='imageViewer']/div[3]/div[4]/div/ul/li[2]/a")).click();
	Log.message("save button is clicked again");
	SkySiteUtils.waitTill(4000);

	String savedscreenshot1 = filepath1 + "\\BeforesavingSquareHyperLink.png";
	String savedscreenshot2 = filepath1 + "\\AfterSavingSquareHyperLink.png";
	// File fistemp=new File(PropertyReader.getProperty("tempScreenshot"));
	// String filepathtemp=fistemp.getAbsolutePath().toString();
	// String tempscreenshotpath=filepathtemp + "\\temp_Screenshot.png";
	File srcFiler = ((TakesScreenshot) driver).getScreenshotAs(OutputType.FILE);

	FileUtils.copyFile(srcFiler, new File(savedscreenshot1));

	Log.message("Screenshot taken of saved markup");
	SkySiteUtils.waitTill(4000);
	hyperlinkdropdown.click();
	Log.message("hyperlink markup dropdown clicked");
	SkySiteUtils.waitTill(4000);
	// for(WebElement markupitem:hperlinklist)
	// if(markupitem.getText().contains(markupname))
	// driver.findElement(By.xpath("//*[text()='"+markupname+"']")).click();
	driver.findElement(By.xpath(".//*[@id='btnHideAllHyperlinks']")).click();
	Log.message("hide all button clicked and image disappears");

	SkySiteUtils.waitTill(4000);
	showhyperlinks.click();
	Log.message("show all hyperlink clikced");
	SkySiteUtils.waitTill(4000);
	savebtn.click();
	Log.message("save button is clicked");
	SkySiteUtils.waitTill(4000);
	File srcFiler1 = ((TakesScreenshot) driver).getScreenshotAs(OutputType.FILE);

	FileUtils.copyFile(srcFiler1, new File(savedscreenshot2));
	Log.message("Screenshot taken after redrawing");

	BufferedImage imgA = ImageIO.read(new File(savedscreenshot1));

	// Thread.sleep(2000);

	BufferedImage imgB = ImageIO.read(new File(savedscreenshot2));

	// Thread.sleep(2000);

	Log.message("Checking the height and width of both the images are same?");

	if (imgA.getWidth() != imgB.getWidth() && imgA.getHeight() != imgB.getHeight()) {
		Log.message("image height width are different");
		return false;
	}

	int width = imgA.getWidth();
	int height = imgA.getHeight();
	for (int y = 0; y < height; y++) {
		for (int x = 0; x < width; x++) {

			if (imgA.getRGB(x, y) != imgB.getRGB(x, y)) {

				Log.message("Image are different");
				return false;
			}
		}

	}

	return true;
}

/**
 * Method to delete hyperlink 
 * 
 * @throws IOException
 * @throws FindFailed
 * 
 */
public boolean drawHyperLinkAndDelete(String markupname, String filepath1)
		throws IOException, FindFailed {
	boolean flag = false;
	Log.message("Switched to default content");
	SkySiteUtils.waitTill(5000);
	driver.switchTo().frame(driver.findElement(By.id("myFrame")));
	Log.message("Switched to frame");
	driver.findElement(By.xpath(".//*[text()='Viewertesting']")).click();
	Log.message("Folder selected");
	SkySiteUtils.waitTill(4000);
	driver.findElement(By.xpath(".//*[@id='divGrid']/div[2]/table/tbody/tr[2]/td[2]/a[3]")).click();
	Log.message("File is selected");
	SkySiteUtils.waitTill(4000);
	String MainWindow = driver.getWindowHandle();
	Log.message(MainWindow);
	for (String childwindow : driver.getWindowHandles()) {
		driver.switchTo().window(childwindow);

	}
	Log.message("switched to child window");

	driver.switchTo().defaultContent();
	SkySiteUtils.waitTill(4000);
	// method to delete existing markup
	this.markupdeletebegin();
	SkySiteUtils.waitTill(4000);
	driver.findElement(By.xpath(".//*[@id='imageViewer']/div[3]/div[1]/div/ul/li[3]/a/i")).click();
	Log.message("Home page is clicked");
	SkySiteUtils.waitTill(4000);
	String savedscreenshot1 = filepath1 + "\\BeforesavingSquareHyperLink.png";
	File srcFiler = ((TakesScreenshot) driver).getScreenshotAs(OutputType.FILE);

	FileUtils.copyFile(srcFiler, new File(savedscreenshot1));
	Log.message("screenshot taken of default file");
	SkySiteUtils.waitTill(4000);
	hyperlinktool.click();
	Log.message("Hyperlink tool is clicked");
	SkySiteUtils.waitForElement(driver, recthyperlink, 40);
	Actions act = new Actions(driver);
	act.moveToElement(recthyperlink).clickAndHold().build().perform();
	SkySiteUtils.waitTill(2000);
	act.click(recthyperlink).build().perform();
	Log.message("Rect Hyper Link button is clicked");
	SkySiteUtils.waitTill(2000);
	File fis = new File(PropertyReader.getProperty("Screenshotpath"));
	String filepath = fis.getAbsolutePath().toString();
	String Screenshot1 = filepath + "\\StartPointFolderLevel.png";
	String Screenshot2 = filepath + "\\EndpointFolderLevel.png";
	Screen s = new Screen();

	Pattern p1 = new Pattern(Screenshot1);

	Pattern p2 = new Pattern(Screenshot2);

	// s.click(p1);

	s.drag(p1);

	s.dropAt(p2);
	Log.message("image is drawn");
	SkySiteUtils.waitTill(4000);
	// driver.findElement(By.xpath(".//*[@id='hyperLinkFolderList']/ul/li[2]/div/a/div[3]")).click();
	
	//driver.findElement(By.xpath(".//*[@id='dvHLPage_2']/div/ul/li[1]/a")).click();
//	Log.message("Page1 is selected");
	
	driver.findElement(By.xpath(".//*[@id='imageViewer']/div[2]/div[2]/div[4]/div/div[1]/div/div[2]/div[3]/button[1]")).click();
	Log.message("Delete button is clicked");
	SkySiteUtils.waitTill(4000);
	driver.findElement(By.xpath(".//*[@id='imageViewer']/div[3]/div[1]/div/ul/li[3]/a/i")).click();
	Log.message("Home page is clicked");
	String savedscreenshot2 = filepath1 + "\\AfterSavingSquareHyperLink.png";
	// File fistemp=new File(PropertyReader.getProperty("tempScreenshot"));
	// String filepathtemp=fistemp.getAbsolutePath().toString();
	// String tempscreenshotpath=filepathtemp + "\\temp_Screenshot.png";
	

	Log.message("Screenshot taken after deleting hyperlink");
	SkySiteUtils.waitTill(4000);
	
	File srcFiler1 = ((TakesScreenshot) driver).getScreenshotAs(OutputType.FILE);

	FileUtils.copyFile(srcFiler1, new File(savedscreenshot2));
	Log.message("Screenshot taken after redrawing");

	BufferedImage imgA = ImageIO.read(new File(savedscreenshot1));

	

	BufferedImage imgB = ImageIO.read(new File(savedscreenshot2));

	

	Log.message("Checking the height and width of both the images are same?");

	if (imgA.getWidth() != imgB.getWidth() && imgA.getHeight() != imgB.getHeight()) {
		Log.message("image height width are different");
		return false;
	}

	int width = imgA.getWidth();
	int height = imgA.getHeight();
	for (int y = 0; y < height; y++) {
		for (int x = 0; x < width; x++) {

			if (imgA.getRGB(x, y) != imgB.getRGB(x, y)) {

				Log.message("Image are different");
				return false;
			}
		}

	}

	return true;
}


/**
 * Method to draw and save square hyperlink for diff folder and diff file(DWG File)
 * 
 * @throws IOException
 * @throws FindFailed
 * 
 */
public boolean drawAndSaveSquareHyperLinkForDiffFolderDifffileDWGFile(String markupname, String filepath1)
		throws IOException, FindFailed {
	boolean flag = false;
	Log.message("Switched to default content");
	SkySiteUtils.waitTill(5000);
	driver.switchTo().frame(driver.findElement(By.id("myFrame")));
	Log.message("Switched to frame");
	driver.findElement(By.xpath(".//*[text()='ViewerTesting2']")).click();
	Log.message("Folder selected");
	SkySiteUtils.waitTill(4000);
	driver.findElement(By.xpath(".//*[@id='divGrid']/div[2]/table/tbody/tr[2]/td[2]/a[3]")).click();
	Log.message("File is selected");
	SkySiteUtils.waitTill(4000);
	String MainWindow = driver.getWindowHandle();
	Log.message(MainWindow);
	for (String childwindow : driver.getWindowHandles()) {
		driver.switchTo().window(childwindow);

	}
	Log.message("switched to child window");

	driver.switchTo().defaultContent();
	SkySiteUtils.waitTill(4000);
	// method to delete existing markup
	this.markupdeletebegin();
	SkySiteUtils.waitTill(4000);
	hyperlinktool.click();
	Log.message("Hyperlink tool is clicked");
	SkySiteUtils.waitForElement(driver, recthyperlink, 40);
	Actions act = new Actions(driver);
	act.moveToElement(recthyperlink).clickAndHold().build().perform();
	SkySiteUtils.waitTill(2000);
	act.click(recthyperlink).build().perform();
	Log.message("Rect Hyper Link button is clicked");
	SkySiteUtils.waitTill(2000);
	File fis = new File(PropertyReader.getProperty("Screenshotpath"));
	String filepath = fis.getAbsolutePath().toString();
	String Screenshot1 = filepath + "\\StartPointFolderLevel.png";
	String Screenshot2 = filepath + "\\EndpointFolderLevel.png";
	Screen s = new Screen();

	Pattern p1 = new Pattern(Screenshot1);

	Pattern p2 = new Pattern(Screenshot2);

	// s.click(p1);

	s.drag(p1);

	s.dropAt(p2);
	Log.message("image is drawn");
	SkySiteUtils.waitTill(4000);
	// driver.findElement(By.xpath(".//*[@id='hyperLinkFolderList']/ul/li[2]/div/a/div[3]")).click();
	driver.findElement(By.xpath(".//*[@id='hyperLinkFolderList']/ul/li[19]/div/a/div[3]")).click();
	Log.message("Folder is selected");
	SkySiteUtils.waitTill(4000);
	// driver.findElement(By.xpath(".//*[@id='hyperLinkFolderList']/ul/li[2]/div/div[2]/h4/a")).click();
	driver.findElement(By.xpath(".//*[@id='hyperLinkFolderList']/ul/li/div/div[2]/h4/a")).click();
	Log.message("File is selected");
	SkySiteUtils.waitTill(3000);
	driver.findElement(By.xpath(".//*[@id='dvHLPage_1']/div/ul/li[1]/a")).click();
	Log.message("Page 1 is selected");
	//driver.findElement(By.xpath(".//*[@id='dvHLPage_2']/div/ul/li[1]/a")).click();
//	Log.message("Page1 is selected");
	SkySiteUtils.waitForElement(driver, okbutton, 50);
	okbutton.click();
	Log.message("Ok button is clicked");

	SkySiteUtils.waitTill(4000);
	driver.findElement(By.xpath("//*[@class='leaflet-control-nav-home']")).click();
	Log.message("Home page is clicked");
	SkySiteUtils.waitForElement(driver, savebtn, 50);
	savebtn.click();
	Log.message("save button is clicked");
	SkySiteUtils.waitTill(4000);
	markupfield.sendKeys(markupname);
	Log.message("Enter markup name is " + markupname);
	SkySiteUtils.waitForElement(driver, savemarkupbtn, 40);
	savemarkupbtn.click();
	Log.message("savemarkupbtn is clicked");
	SkySiteUtils.waitTill(14000);
	driver.findElement(By.xpath(".//*[@id='imageViewer']/div[3]/div[4]/div/ul/li[2]/a")).click();
	Log.message("save button is clicked again");
	SkySiteUtils.waitTill(4000);

	String savedscreenshot1 = filepath1 + "\\BeforesavingSquareHyperLink.png";
	String savedscreenshot2 = filepath1 + "\\AfterSavingSquareHyperLink.png";
	// File fistemp=new File(PropertyReader.getProperty("tempScreenshot"));
	// String filepathtemp=fistemp.getAbsolutePath().toString();
	// String tempscreenshotpath=filepathtemp + "\\temp_Screenshot.png";
	File srcFiler = ((TakesScreenshot) driver).getScreenshotAs(OutputType.FILE);

	FileUtils.copyFile(srcFiler, new File(savedscreenshot1));

	Log.message("Screenshot taken of saved markup");
	SkySiteUtils.waitTill(4000);
	hyperlinkdropdown.click();
	Log.message("hyperlink markup dropdown clicked");
	SkySiteUtils.waitTill(4000);
	// for(WebElement markupitem:hperlinklist)
	// if(markupitem.getText().contains(markupname))
	// driver.findElement(By.xpath("//*[text()='"+markupname+"']")).click();
	driver.findElement(By.xpath(".//*[@id='btnHideAllHyperlinks']")).click();
	Log.message("hide all button clicked and image disappears");

	SkySiteUtils.waitTill(4000);
	showhyperlinks.click();
	Log.message("show all hyperlink clikced");
	SkySiteUtils.waitTill(4000);
	savebtn.click();
	Log.message("save button is clicked");
	SkySiteUtils.waitTill(4000);
	File srcFiler1 = ((TakesScreenshot) driver).getScreenshotAs(OutputType.FILE);

	FileUtils.copyFile(srcFiler1, new File(savedscreenshot2));
	Log.message("Screenshot taken after redrawing");

	BufferedImage imgA = ImageIO.read(new File(savedscreenshot1));

	// Thread.sleep(2000);

	BufferedImage imgB = ImageIO.read(new File(savedscreenshot2));

	// Thread.sleep(2000);

	Log.message("Checking the height and width of both the images are same?");

	if (imgA.getWidth() != imgB.getWidth() && imgA.getHeight() != imgB.getHeight()) {
		Log.message("image height width are different");
		return false;
	}

	int width = imgA.getWidth();
	int height = imgA.getHeight();
	for (int y = 0; y < height; y++) {
		for (int x = 0; x < width; x++) {

			if (imgA.getRGB(x, y) != imgB.getRGB(x, y)) {

				Log.message("Image are different");
				return false;
			}
		}

	}

	return true;
}


/**
 * Method to verify working of square hyperlink for diff folder diff file DWG File
 * 
 * @throws FindFailed
 * @throws IOException
 * @throws UnsupportedFlavorException
 * 
 */

public boolean VerifyfunctionalitySquareHyperLinkdiffFolderDiffFileDWGFile(String markupname, String filepath1)
		throws FindFailed, UnsupportedFlavorException, IOException {
	// driver.switchTo().defaultContent();
	SkySiteUtils.waitTill(8000);
	File fistemp = new File(PropertyReader.getProperty("tempScreenshot"));
	String filepathtemp = fistemp.getAbsolutePath().toString();
	String tempscreenshotpath = filepathtemp + "\\tempfolderlevel.png";
	Screen s = new Screen();

	Pattern p1 = new Pattern(tempscreenshotpath);

	SkySiteUtils.waitTill(4000);
	s.click(p1);
	Log.message("saved hyperlink is clicked");
	SkySiteUtils.waitTill(4000);
	// JavascriptExecutor executor = (JavascriptExecutor)driver;
	// executor.executeScript("arguments[0].click();", copylink);
	// Log.message("Copy to clipboard clicked");
	/*
	 * Toolkit toolkit = Toolkit.getDefaultToolkit(); Clipboard clipboard =
	 * toolkit.getSystemClipboard(); String result = (String)
	 * clipboard.getData(DataFlavor.stringFlavor);
	 * System.out.println("String from Clipboard:" + result);
	 */
	WebElement imagelink = driver.findElement(By.id("linkImgDiv"));
	JavascriptExecutor executor = (JavascriptExecutor) driver;
	executor.executeScript("arguments[0].click();", imagelink);
	Log.message("image is clicked");
	SkySiteUtils.waitTill(5000);
	String filename = driver.findElement(By.xpath("//*[@data-original-title='Test.dwg']")).getText();
	Log.message("File name is " + filename);
	String expectedfilename = "(R-1) Test.dwg";
	if (filename.equals(expectedfilename))
		return true;

	else
		return false;

}
}
	

	




