package com.arc.fna.pages;

import java.awt.AWTException;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.LoadableComponent;
import org.testng.Assert;

import com.arc.fna.utils.SkySiteUtils;
import com.arcautoframe.utils.Log;

public class FnaLoginPage extends LoadableComponent<FnaLoginPage> {
	WebDriver driver;
	private boolean isPageLoaded;
	
	/**
	 * Identifying web elements using FindfBy annotation.
	 */
	
	@FindBy(css="#UserID")
	WebElement txtBoxUserName;
	
	@FindBy(css="#Password")
	WebElement txtBoxPassword;
	
	@FindBy(css="#btnLogin")
	WebElement btnLogin;
	
	/*@FindBy(xpath="//button[@id='btnNewProject']")
    WebElement buttonAddCollection;*/

	
	@Override
	protected void load() {
		isPageLoaded = true;
		SkySiteUtils.waitForElement(driver, txtBoxUserName, 60);
		
	}

	@Override
	protected void isLoaded() throws Error {
		if (!isPageLoaded) {
			Assert.fail();
		}
	}
	
	/**
	 * Declaring constructor for initializing web elements using PageFactory class.
	 * @param driver
	 */
	public FnaLoginPage(WebDriver driver) {
		   this.driver = driver;
		   PageFactory.initElements(this.driver, this);
		 }
	
	/** 
	 * Method written for doing login with valid credential.
	 * @throws AWTException 
	 */
	public FnaSanityHomePage login(String uName,String pWord) throws AWTException
	{
		 //SkySiteUtils.waitTill(5000);
		//driver.findElement(By.xpath(".//*[@id='maintenance']/div/div/div[2]/button")).click();
		/***edited by tarak **/
		 if(driver.findElement(By.xpath(".//*[@id='maintenance']/div/div/div[1]/img")).isDisplayed())
		 {
			 driver.findElement(By.xpath(".//*[@id='maintenance']/div/div/div[2]/button")).click();
		 }
		SkySiteUtils.waitForElement(driver, txtBoxUserName, 60);
		//String uName = PropertyReader.getProperty("Username");
		txtBoxUserName.clear();
		txtBoxUserName.sendKeys(uName);
		Log.message("Username: " + uName + " " + "has been engtered in Username text box." );
		//String pWord = PropertyReader.getProperty("Password");
		txtBoxPassword.clear();
		txtBoxPassword.sendKeys(pWord);
		Log.message("Password: " + pWord + " " + "has been engtered in Password text box." );
		SkySiteUtils.waitForElement(driver, btnLogin, 60);
		SkySiteUtils.waitTill(2000);
		btnLogin.click();
		Log.message("LogIn button clicked.");
		return new FnaSanityHomePage(driver).get();		
	}
}
