package com.arc.fna.pages;

import java.awt.AWTException;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Random;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.CacheLookup;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.LoadableComponent;
import org.openqa.selenium.support.ui.Select;
import org.testng.Assert;

import com.arc.fna.utils.PropertyReader;
import com.arc.fna.utils.SkySiteUtils;
import com.arc.fna.utils.randomFileName;
import com.arcautoframe.utils.Log;




public class FnaContactTabPage extends LoadableComponent<FnaContactTabPage> {
    WebDriver driver;
    private  boolean isPageLoaded;
  
    /** Find Elements using FindBy annotation
     * 
     */
    @FindBy(css="#btnCreateGroup")
    WebElement CreateGrpbtn;
    @FindBy(xpath=".//*[@id='btnNewContact']")
    WebElement NewContactBtn;
    @FindBy(css="#ctl00_DefaultContent_rdoHostedUserType")
    WebElement Employeebtn;
    @FindBy(css="#ctl00_DefaultContent_txtFirstName")
    WebElement Firstname;
    @FindBy(css="#ctl00_DefaultContent_txtLastName")
    WebElement Lastname;
    @FindBy(css="#ctl00_DefaultContent_txtCity")
    WebElement city;
    @FindBy(css="#ctl00_DefaultContent_txtZip")
    WebElement Postalcode;
    @FindBy(css="#ctl00_DefaultContent_txtTitle")
    WebElement Title;
    @FindBy(css="#ctl00_DefaultContent_ucCountryState_ddlCountry")
    WebElement Country;
    @FindBy(css="#ctl00_DefaultContent_txtCompanyName")
    WebElement Company;
    @FindBy(css="#ctl00_DefaultContent_ucCountryState_ddlState")
    WebElement statedropdown;
    @FindBy(css="#ctl00_DefaultContent_txtAddress1")
    WebElement Address1;
    @FindBy(css="#ctl00_DefaultContent_txtEmail")
    WebElement Email;
    @FindBy(css="#ctl00_DefaultContent_txtAddress2")
    WebElement Address2;
    @FindBy(css="#ctl00_DefaultContent_txtPhoneWork")
    WebElement PhoneNo;
    @FindBy(css="#ctl00_DefaultContent_txtWebURL")
    WebElement Companywebsite;
    @FindBy(css="#ctl00_DefaultContent_ddlProjectRole")
    WebElement ProjectRole;
    @FindBy(css="#ctl00_DefaultContent_DDLCompBussiness")
    WebElement Buisness;
    @FindBy(css="#ctl00_DefaultContent_DDLCompOccupation")
    WebElement Occupation;
    @FindBy(css="#ctl00_DefaultContent_Button3")
	WebElement Savebutton;
    @FindBy(css="#btnExpImpAddBook")
    WebElement Exporticon;
    @FindBy(xpath=".//*[@id='tdbtnExpImpAddBook']/ul/li[2]/a")
    WebElement ExportExceloption;
    @FindBy(css="#txtSerachValue")
    WebElement Searchboxnew;
    
    
    
    @FindBy(css="#btnSearch")
    WebElement Searchbtn;
  
    @FindBy(css=".ev_material > td:nth-child(2) > a:nth-child(2)")
    WebElement Grpnamelist;
    
    @FindBy(css="#chkAllPT")
    @CacheLookup
    WebElement SelctAlldel;
    
    @FindBy(css="#btnRemoveContact")
    @CacheLookup
    WebElement Deletebtn;
    
    
    @FindBy(css="#ctl00_DefaultContent_rdoContactType")
    @CacheLookup
    WebElement Cntctselbtn;
    
    
    @FindBy(css="#divFinalAlertForDeletion > div:nth-child(1) > div:nth-child(1) > div:nth-child(3) > button:nth-child(1)")
    @CacheLookup
    WebElement delconfirmbtn;
    
    @FindBy(css="#opt1")
    @CacheLookup
    WebElement delradiobtn;
    
    
    @FindBy(css="#divFinalAlertForDeletion > div:nth-child(1) > div:nth-child(1) > div:nth-child(3) > button:nth-child(2)")
    @CacheLookup
    WebElement cancelbtn;

    @Override
	protected void load() {
		// TODO Auto-generated method stub
		isPageLoaded=true;

		driver.switchTo().frame("myFrame");
		SkySiteUtils.waitForElement(driver,CreateGrpbtn,30);
		driver.switchTo().defaultContent();
	}

	@Override
	protected void isLoaded() throws Error {
		// TODO Auto-generated method stub
		if (!isPageLoaded)
		{
			Assert.fail();
		}
	}
	/**
	 * Declaring constructor for initializing web elements using PageFactory class.
	 * @param driver
	 */
	public FnaContactTabPage(WebDriver driver){
		this.driver=driver;
		PageFactory.initElements(driver, this);
	}
    /** Method for Address Page landing verification
     *  Scripted By :Tarak
     */
	public boolean AddressPage_IsLanded() {
		driver.switchTo().frame("myFrame");
		Log.message("Swtiched into Frame");
		if(NewContactBtn.isDisplayed()==true) {
			Log.message("AddNewContact Button is displayed");
			return true;
		}
			return false;
		
	}
	/** Method for Random Contactname
	 * Scripted By: Tarak
	 * @return
	 */
	 public String Random_Contactname()
	    {
	           
	           String str="Contact";
	           Random r = new Random( System.currentTimeMillis() );
	           String abc = str+String.valueOf((10000 + r.nextInt(20000)));
	           return abc;
	           
	    }
	 /** Method for Random Email
	  * Scripted By : Tarak
	  * @return
	  */
	 public String Random_Email()
	    {
	           
	           String str1="derasri";
	           String str2="@gmail.com";
	           Random r = new Random( System.currentTimeMillis() );
	           String abc = str1+String.valueOf((10000 + r.nextInt(20000)))+str2;
	           return abc;
	           
	    }
	 /** Method for Random Company name
	  * Scripted By : Tarak
	  * @return
	  */
	 public String Random_CompanyName()
	    {
	           
	           String str1="Companyname";

	           Random r = new Random( System.currentTimeMillis() );
	           String abc = str1+String.valueOf((10000 + r.nextInt(20000)));
	           return abc;
	           
	    }
	 /** Method for Random update Email
	  * Scripted By : Tarak
	  * @return
	  */
	 public String Random_updateEmail()
	    {
	           
	           String str1="Test1";
	           String str2="@gmail.com";
	           Random r = new Random( System.currentTimeMillis() );
	           String abc = str1+String.valueOf((10000 + r.nextInt(20000)))+str2;
	           return abc;
	           
	    }
	 public String Random_LastName() {
		 String str1="Lastname";
		 Random r = new Random( System.currentTimeMillis());
		 String abc=str1+String.valueOf((10000 + r.nextInt(20000)));
		 return abc;
		}
	 /** Method to deletefile for folder
	  * 
	  * @param Folder_Path
	  * @return
	  */
	//Deleting files from a folder
		public boolean Delete_Files_From_Folder(String Folder_Path) 
		{
			try
			{	
				SkySiteUtils.waitTill(5000);		
				Log.message("Cleaning download folder!!! ");
				File file = new File(Folder_Path);      
			    String[] myFiles;    
			    if(file.isDirectory())
			    {
			    	myFiles = file.list();
			        for(int i=0; i<myFiles.length; i++) 
			        {
			           File myFile = new File(file, myFiles[i]); 
			           myFile.delete();
			           SkySiteUtils.waitTill(5000);
			        }
			        Log.message("Available Folders/Files are deleted from download folder successfully!!!");
			    }	         
			}//end try
			catch(Exception e)
			{
				 Log.message("Available Folders/Files are deleted from download folder successfully!!!");
			}
			return false;
		}
	@FindBy(xpath="//input[@value='rdoHostedUserType']")
	WebElement employeeuserbtn;
	
	
	   /** Method for Export Address book
	    * Scripted By: Tarak
	 * @throws IOException 
	    * 
	    */
	
	    public boolean exportAddressBook(String downloadpath) throws IOException {
           driver.switchTo().defaultContent();
           SkySiteUtils.waitTill(5000);
           this.Delete_Files_From_Folder(downloadpath);
           Log.message("downloadpath is : " +downloadpath);
         //  SkySiteUtils.waitTill(5000);
           driver.switchTo().frame(driver.findElement(By.id("myFrame")));
           Log.message("swtiched to frame!!");
       SkySiteUtils.waitForElement(driver, Exporticon, 60);
           Exporticon.click();
           Log.message("Export icon is clicked");
           SkySiteUtils.waitForElement(driver, ExportExceloption, 80);
           int Count_list=driver.findElements(By.xpath("//*[@id='divGrid']/div[2]/table/tbody/tr/td[1]/img")).size();
	 	   Log.message("Total number of contact are:"+Count_list);	
	 	   SkySiteUtils.waitTill(4000);
           ExportExceloption.click();
           Log.message("Export Excel option clicked");
           SkySiteUtils.waitTill(4000);
           driver.switchTo().defaultContent();
           Log.message("Switch to default content");
           SkySiteUtils.waitTill(5000);
           if ((this.isFileDownloaded(downloadpath)==true)&&(this.readEntry(downloadpath,Count_list)==true))
        	   return true;
           else 
        	   return false;
         }
	    /**Method to check if file is downloaded
	     * Scripted By : Tarak
	     * 
	     */
	     public boolean isFileDownloaded(String downloadfolderpath) {
	    	 Log.message("Searching contact file downloaded");
	    	 File file=new File(downloadfolderpath);
	    	 File[] Filearray=file.listFiles();
	    	 if(file.isDirectory()) {
	    		 for(File Myfile:Filearray) {
	    			 if(Myfile.getName().contains(PropertyReader.getProperty("Exportfilename"))) {
	    				return true;
	    			 }
	    		 }
	    	 }
	    	return false;
	   }
	     /**Method to read entry of downloaded excel
	     * @throws IOException 
	      * Scripted By : Tarak
	      */
	     public boolean readEntry(String downloadpath,int count) throws IOException {
	    	 int rowcount;
	    	  
	    		 SkySiteUtils.waitTill(4000);
	    		 FileInputStream fis=new FileInputStream(downloadpath + "\\Contacts.xlsx");
	    		  XSSFWorkbook workbook = new XSSFWorkbook(fis);
	              XSSFSheet sheet = workbook.getSheetAt(0);
	             rowcount=sheet.getLastRowNum()-sheet.getFirstRowNum();
	             Log.message("the row count in the excel is " +rowcount);
	             SkySiteUtils.waitTill(5000);
	             if(rowcount==count) {
	 	 		Log.message("Number of entry in the excel matched with the number of contacts");
	 	 	      return true;
	 	 		}
	 	 	   else {
	 	 		   return false;
	 	 				   
	 	 	   }
	    	   	
	     }
	    	 
	    	 
	    	 
	
	    
	            /** Method to create contact with type as contact
	              * By Trinanjwan
	              */
	       
	        
	               public boolean CreateNewContact_cntcsel(String Newcontactname,String Emailid,String Lastnamerandom)
	            {
	                   driver.switchTo().defaultContent();//switch to default window after adress page landed
	                  // SkySiteUtils.waitTill(5000);
	                   driver.switchTo().frame("myFrame");//Again switch to frame to click the addnewcontact button
	                  // SkySiteUtils.waitTill(6000);
	                   SkySiteUtils.waitForElement(driver, NewContactBtn, 50);
	                   NewContactBtn.click();
	                   Log.message("AddNewContact button is clicked");
	                   //SkySiteUtils.waitTill(15000);
	                   String MainWindow=driver.getWindowHandle();
	                   Log.message(MainWindow);
	                   for(String childwindow:driver.getWindowHandles())
	                   {
	                          driver.switchTo().window(childwindow);//swtich to child window
	                   
	                   }
	                   SkySiteUtils.waitForElement(driver, Employeebtn, 70);
	                   Cntctselbtn.click();
	                   Log.message("Contact user is selected");
	                  SkySiteUtils.waitForElement(driver, Firstname, 50);
	                   Firstname.click();
	                   Firstname.sendKeys(Newcontactname);
	                   Log.message("Contactname entered is : " +Newcontactname);
	                   //SkySiteUtils.waitTill(5000);
	                   SkySiteUtils.waitForElement(driver, Lastname, 50);
	                   Lastname.click();
	                   Log.message("Last name box clicked");
	                   Lastname.sendKeys(Lastnamerandom);
	                   Log.message("Lastname entered is " +Lastnamerandom);
	                   //SkySiteUtils.waitTill(5000);
	                   String CityName=PropertyReader.getProperty("City");
	                   SkySiteUtils.waitForElement(driver, city, 50);
	                   city.sendKeys(CityName);
	                   Log.message("City entered is " +CityName);
	                   //SkySiteUtils.waitTill(5000);
	                   String Pcode=PropertyReader.getProperty("Postcode");
	                   Postalcode.sendKeys(Pcode);
	                   Log.message("postal code entered is " +Pcode);
	                   //SkySiteUtils.waitTill(5000);
	                   String TName=PropertyReader.getProperty("Titlename");
	                   Title.sendKeys(TName);
	                   Log.message("Title entered is " +TName);
	                   SkySiteUtils.waitTill(2000);
	                   String Cname=PropertyReader.getProperty("Countryname");
	                   Select sc=new Select(Country);
	                   sc.selectByVisibleText(Cname);
	                   Log.message("Country selected is :" +Cname);
	                   SkySiteUtils.waitTill(3000);
	                   String Companynm=PropertyReader.getProperty("Company");
	                   Company.sendKeys(Companynm);
	                   Log.message("Companyname entered is " +Companynm);
	                   //SkySiteUtils.waitTill(5000);
	                   String statenm=PropertyReader.getProperty("Statenm");
	                   Select selectstate=new Select(statedropdown);
	                selectstate.selectByVisibleText(statenm);
	                Log.message("State selected is " +statenm);
	                 //  SkySiteUtils.waitTill(5000);
	                String addressfirst=PropertyReader.getProperty("Address1");
	                Address1.sendKeys(addressfirst);
	                Log.message("Address1 entered is" +addressfirst);
	                  // SkySiteUtils.waitTill(5000);
	             Email.sendKeys(Emailid);
	             Log.message("Email id entered is" +Emailid);
	            SkySiteUtils.waitTill(2000);
	             String addresssecond=PropertyReader.getProperty("Address2");
	             Address2.sendKeys(addresssecond);
	             Log.message("Address2 entered is " +addresssecond);
	           // SkySiteUtils.waitTill(5000);
	             String Phnno=PropertyReader.getProperty("Phoneno");
	             PhoneNo.sendKeys(Phnno);
	             Log.message("Phone no entered is : " +Phnno);
	           // SkySiteUtils.waitTill(5000);
	             String Compwebsite=PropertyReader.getProperty("CompanyWebSite");
	             Companywebsite.sendKeys(Compwebsite);
	             Log.message("Company website entered : " +Compwebsite);
	          //  SkySiteUtils.waitTill(5000);
	            JavascriptExecutor js = (JavascriptExecutor) driver;
	             js.executeScript("window.scrollBy(0,1000)");
	             Log.message("Scrolled down to window");
	          //   SkySiteUtils.waitTill(5000);
	             String Projectroll=PropertyReader.getProperty("Projectrole");
	             Select scrole=new Select(ProjectRole);
	             scrole.selectByVisibleText(Projectroll);
	           // SkySiteUtils.waitTill(5000);
	             Log.message("ProjectRoll selected is " +Projectroll);
	             String Buisnessname=PropertyReader.getProperty("Buisnessdetails");
	             Select selectBuisness=new Select(Buisness);
	             selectBuisness.selectByVisibleText(Buisnessname);
	             Log.message("Buisness selected is " +Buisnessname);
	          //   SkySiteUtils.waitTill(5000);
	             Select selectoccupation=new Select(Occupation);
	             selectoccupation.selectByIndex(2);
	             Log.message("Occupation is selected");
	             
	             
//	           String Occupationname=PropertyReader.getProperty("Occupationdetails");
//	           Select selectoccupation=new Select(Occupation);
//	           selectoccupation.selectByVisibleText(Occupationname);
//	           Log.message("Occupation selected is " +Occupationname);
	           //  SkySiteUtils.waitTill(5000);
	             Savebutton.click();
	             Log.message("Save button is clicked");
	             //SkySiteUtils.waitTill(3000);
	             driver.switchTo().window(MainWindow);//switch to main window
	             Log.message("Switched to main window");
	            // SkySiteUtils.waitTill(6000);
	             driver.switchTo().frame(driver.findElement(By.id("myFrame")));
	             Log.message("Switched to frame");
	             SkySiteUtils.waitTill(3000);
	             WebElement Removecontacticon=driver.findElement(By.xpath(".//*[@id='btnRemoveContact']"));
	             SkySiteUtils.waitForElement(driver, Removecontacticon, 60);
	             int Count_list=driver.findElements(By.xpath("//*[@id='divGrid']/div[2]/table/tbody/tr/td[1]/img")).size();
	                   Log.message("Total list count is:"+Count_list);                   
	                   //Loop start '2' as projects list start from tr[2]
	                   int i=0;
	                   for(i = 1; i <= Count_list; i++) 
	                   {
	                                
	                          String contactName=driver.findElement(By.xpath("(//*[@id='divGrid']/div[2]/table/tbody/tr/td[2])["+i+"]")).getText();
	                      
	                          Log.message("Contact name is:"+contactName);
	                          SkySiteUtils.waitTill(5000);                   
	                          //Checking contact name equal with each row of table
	                          if(contactName.contains(Newcontactname))
	                          {         
	                                Log.message("Contact name matched is : " +contactName);
	                                Log.message("Contact name added has been validated!!!");                            
	                                break;
	                          }             
	                   }
	                   String contactName1=driver.findElement(By.xpath("(//*[@id='divGrid']/div[2]/table/tbody/tr/td[2])["+i+"]")).getText();
	                   Log.message("****New Contact added is**** " +contactName1);
	                   if(contactName1.contains(Newcontactname))     
	                          return true;
	                   else
	                          return false;
	                   }
	               @FindBy(xpath=".//*[@id='btnRemoveContact']")
	               WebElement RemoveContact;
	          /**Method to delete contact
	           * 
	           */
	               public void deleteContact(String Newcontactname) {
	            	   
	            	   int Count_list=driver.findElements(By.xpath("//*[@id='divGrid']/div[2]/table/tbody/tr/td[1]/img")).size();
	                   Log.message("Total list count is:"+Count_list);                   
	                   //Loop start '2' as projects list start from tr[2]
	                   int i=0;
	                   for(i = 1; i <= Count_list; i++) 
	                   {
	                                
	                          String contactName=driver.findElement(By.xpath("(//*[@id='divGrid']/div[2]/table/tbody/tr/td[2])["+i+"]")).getText();
	                      
	                          Log.message("Contact name is:"+contactName);
	                          //SkySiteUtils.waitTill(5000);                   
	                          //Checking contact name equal with each row of table
	                          if(contactName.contains(Newcontactname))
	                          {         
	                                Log.message("Contact name matched is : " +contactName);
	                                Log.message("Contact name added has been validated!!!");
	                                driver.findElement(By.xpath(".//*[@id='divGrid']/div[2]/table/tbody/tr["+(i+1)+"]/td[1]/img")).click();
	                                Log.message("Checkbox is clicked");
	                                SkySiteUtils.waitForElement(driver, RemoveContact, 40);
	                                RemoveContact.click();
	                                WebElement Confirmbtn=driver.findElement(By.xpath(".//*[@id='divFinalAlertForDeletion']/div/div/div[3]/button[1]"));
	                                //SkySiteUtils.waitTill(3000);
	                                SkySiteUtils.waitForElement(driver, Confirmbtn, 50);
	                                driver.findElement(By.xpath(".//*[@id='opt1']")).click();
	                                Log.message("Delete contact option 1 is clicked");
	                             
	                                Confirmbtn.click();
	                                Log.message("Confirm button is clicked and contact is deleted");
	                                SkySiteUtils.waitForElement(driver, RemoveContact, 60);
	                                SkySiteUtils.waitTill(4000);
	                                
	                                break;
	                          }             
	                   }
	                 
	                   }
                    /**Method to delete added collection
                     * 
                     */
	               
	           
	            	   
	         }