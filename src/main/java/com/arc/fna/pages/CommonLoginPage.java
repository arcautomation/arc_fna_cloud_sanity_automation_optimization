package com.arc.fna.pages;


import java.awt.AWTException;
import java.awt.Robot;
import java.awt.event.KeyEvent;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.List;
import java.util.Random;

import org.openqa.selenium.By;
import org.openqa.selenium.Capabilities;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.openqa.selenium.support.CacheLookup;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.LoadableComponent;
import org.openqa.selenium.support.ui.Select;
import org.testng.Assert;

import com.arc.fna.utils.PropertyReader;
import com.arc.fna.utils.RandomEmployeeName;
import com.arc.fna.utils.SkySiteUtils;
import com.arc.fna.utils.randomFileName;
import com.arc.fna.utils.randomSharedName;
import com.arcautoframe.utils.Log;

public class CommonLoginPage  extends LoadableComponent<CommonLoginPage> {

	WebDriver driver;
	private boolean isPageLoaded;
	@Override
	protected void load() {
		// TODO Auto-generated method stub
		
	}
	@Override
	protected void isLoaded() throws Error {
		// TODO Auto-generated method stub
		
	}
	
	
	
	public CommonLoginPage(WebDriver driver) {
		   this.driver = driver;
		   PageFactory.initElements(this.driver, this);
		 }
	
	
	@FindBy(css="div.dropdown:nth-child(6) > a:nth-child(1)")
	@CacheLookup
	WebElement logoutbutton;

	@FindBy(css="#frmCommonLandingInfoLinksActivate > a:nth-child(1)")
	@CacheLookup
	WebElement ActivateFnA;
	
	
	/** 
	 * Method written for checking whether common login page is landed properly
	 * @return
	 * Created By trinanjwan
	 */
	
	
	public boolean landingOfCommonLoginPage()
	{
		SkySiteUtils.waitForElement(driver, logoutbutton, 20);
		Log.message("The Logout button to be appeared");
		if(logoutbutton.isDisplayed())
			return true;
			else
			return false;
	}
	
	
	/** 
	 * Method written for activating the F&A module
	 * @return
	 * Created By trinanjwan
	 */
	
	
	public FnaSanityHomePage activateFnAModule()
	
	{
		SkySiteUtils.waitForElement(driver, ActivateFnA, 20);
		ActivateFnA.click();
		Log.message("Clicked on Activate FnA button to activate F&A");
		return new FnaSanityHomePage(driver).get();

		
	}
	}
	
	
