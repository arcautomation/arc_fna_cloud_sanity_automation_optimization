package com.arc.fna.pages;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.CacheLookup;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.LoadableComponent;
import org.testng.Assert;

import com.arc.fna.utils.PropertyReader;
import com.arc.fna.utils.SkySiteUtils;
import com.arcautoframe.utils.Log;

public class FnaAccountSettingsPage  extends LoadableComponent<FnaAccountSettingsPage> {
	 WebDriver driver;
	    private  boolean isPageLoaded;
	    

	    @Override
		protected void load() {
			// TODO Auto-generated method stub
	
			
			isPageLoaded = true;
			SkySiteUtils.waitForElement(driver, AddLogobtn, 20);
		
		}

		@Override
		protected void isLoaded() throws Error {
			// TODO Auto-generated method stub
			if (!isPageLoaded)
			{
				Assert.fail();
			}
		}
	
		/**
		 * Declaring constructor for initializing web elements using PageFactory class.
		 * @param driver
		 */
		public  FnaAccountSettingsPage(WebDriver driver){
			this.driver=driver;
			PageFactory.initElements(driver, this);
		}

		
		/**
		 * Identifying web elements using FindBy annotation.
		 */

		@FindBy(css="#uploadlogo")
		@CacheLookup
		WebElement AddLogobtn;
		
		@FindBy(css=".stnd-sign-in-container > div:nth-child(2) > div:nth-child(1) > label:nth-child(1)")
		@CacheLookup
		WebElement ComplexPasswordlabel;
		
		@FindBy(css=".stnd-sign-in-container > div:nth-child(2) > div:nth-child(1) > label:nth-child(1) > input:nth-child(1)")
		@CacheLookup
		WebElement Checkbox;
		
		@FindBy(css="div.checkbox:nth-child(3) > label:nth-child(1) > input:nth-child(1)")
		@CacheLookup
		WebElement Checkbox2step;
				
		
		@FindBy(css="#btnSettingsSave")
		@CacheLookup
		WebElement Savebtn;
		
		@FindBy(css="#button-0")
		@CacheLookup
		WebElement ResetPasswordbtn;
		
		
		@FindBy(css="#txtCurrentPassword")
		@CacheLookup
		WebElement OldPasswordfield;
		
		@FindBy(css="#txtNewPassword")
		@CacheLookup
		WebElement NewPasswordfield;
		
		@FindBy(css="#txtConfirmNewPassword")
		@CacheLookup
		WebElement ConfirmPasswordfield;
		
		
		@FindBy(css="#btnLogin")
		@CacheLookup
		WebElement Submitbtn;
		
		@FindBy(css="#UserID")
		@CacheLookup
		WebElement Usernamefield;
		
		@FindBy(css="#txtNewPin")
		@CacheLookup
		WebElement NewPINfield;

		@FindBy(css="#txtConfirmNewPin")
		@CacheLookup
		WebElement ReenterPINfield;
		
		@FindBy(css="#btnSubmitPIN")
		@CacheLookup
		WebElement SubmitPINbtn;
		
		@FindBy(css="#button-0")
		@CacheLookup
		WebElement ResetPINbtn;
		
		
		
		

		/**Method to check the availability of Complex Password section | Scripted by Trinanjwan 
		 */
		

		public boolean availabilityOfComplexPasswordSection()
		{
				driver.switchTo().defaultContent();
				Log.message("Switched to default content");
				driver.switchTo().frame("myFrame");
		        Log.message("Switched to My Frame");
		        SkySiteUtils.waitTill(7000);
				if(ComplexPasswordlabel.isDisplayed())
					return true;
				else
					return false;
	
		}	
		
		/**Method to enable Complex Password from Account settings | Scripted by Trinanjwan 
		 */
		
		
		public boolean enablingComplexPassword()
		{
			driver.switchTo().defaultContent();
			Log.message("Switched to default content");
			driver.switchTo().frame("myFrame");
	        Log.message("Switched to My Frame");
	        SkySiteUtils.waitTill(7000);
			SkySiteUtils.waitForElement(driver, Checkbox, 10);
			Checkbox.click();
			Log.message("Clicked on the checkbox");
			JavascriptExecutor js = (JavascriptExecutor) driver;
	        js.executeScript("window.scrollBy(0,10000)");
	        Log.message("Scrolled down to window");
	        SkySiteUtils.waitForElement(driver, Savebtn, 10);
	        Savebtn.click();
	        Log.message("Clicked on Save button");
	        driver.switchTo().defaultContent();	
	        SkySiteUtils.waitTill(10000);
			ResetPasswordbtn.click();
			Log.message("Clicked on Reset Password button");
			Log.message("Switched to default content");
			SkySiteUtils.waitForElement(driver, OldPasswordfield, 10);
			if(OldPasswordfield.isDisplayed())
				return true;
			else 
				return false;
	
		}
		
		/**Method to Set Complex Password | Scripted by Trinanjwan 
		 */

		public boolean settingComplexPassword()
		{
			
			SkySiteUtils.waitForElement(driver, OldPasswordfield, 10);
			String OldPassword=PropertyReader.getProperty("pwdtri");
			OldPasswordfield.sendKeys(OldPassword);
			Log.message(OldPassword+" is entered is old password field");
			
			SkySiteUtils.waitForElement(driver, NewPasswordfield, 10);
			String NewPassword=PropertyReader.getProperty("complexpasswordtri");
			NewPasswordfield.sendKeys(NewPassword);
			Log.message(NewPassword+" is entered is new password field");
			
			SkySiteUtils.waitForElement(driver, ConfirmPasswordfield, 10);
			ConfirmPasswordfield.sendKeys(NewPassword);
			Log.message(NewPassword+" is entered is confirm password field");
			SkySiteUtils.waitForElement(driver, Submitbtn, 10);
			Submitbtn.click();
			Log.message("Clicked on Submit button");
			SkySiteUtils.waitForElement(driver, Usernamefield, 10);
			Log.message("Username field is displayed now");
			if(Usernamefield.isDisplayed())
				return true;
			else
				return false;
			
		}

		/**Method to enable 2-step authentication from Account settings | Scripted by Trinanjwan 
		 */
		
		
		public void enabling2StepAuthentication()
		{
			driver.switchTo().defaultContent();
			Log.message("Switched to default content");
			driver.switchTo().frame("myFrame");
	        Log.message("Switched to My Frame");
	        SkySiteUtils.waitTill(7000);
			SkySiteUtils.waitForElement(driver, Checkbox2step, 10);
			Checkbox2step.click();
			Log.message("Clicked on the checkbox");
			JavascriptExecutor js = (JavascriptExecutor) driver;
	        js.executeScript("window.scrollBy(0,10000)");
	        Log.message("Scrolled down to window");
	        SkySiteUtils.waitForElement(driver, Savebtn, 10);
	        Savebtn.click();
	        Log.message("Clicked on Save button");
	        driver.switchTo().defaultContent();	
	        Log.message("Switched to default content");
	        SkySiteUtils.waitTill(10000);
			ResetPasswordbtn.click();
			Log.message("Clicked on Reset Password button");	
		}

		
		/**Method to Set 2 Step authentication | Scripted by Trinanjwan 
		 */

		public boolean setting2StepAuthentication()
		{
			
			
			
			//Setting Complex Password
			SkySiteUtils.waitForElement(driver, OldPasswordfield, 10);
			String OldPassword=PropertyReader.getProperty("pwdtri");
			OldPasswordfield.sendKeys(OldPassword);
			Log.message(OldPassword+" is entered is old password field");
			
			SkySiteUtils.waitForElement(driver, NewPasswordfield, 10);
			String NewPassword=PropertyReader.getProperty("complexpasswordtri");
			NewPasswordfield.sendKeys(NewPassword);
			Log.message(NewPassword+" is entered is new password field");
			
			SkySiteUtils.waitForElement(driver, ConfirmPasswordfield, 10);
			ConfirmPasswordfield.sendKeys(NewPassword);
			Log.message(NewPassword+" is entered is confirm password field");
			SkySiteUtils.waitForElement(driver, Submitbtn, 10);
			Submitbtn.click();
			Log.message("Clicked on Submit button");
			
			//Setting PIN
			SkySiteUtils.waitForElement(driver, NewPINfield, 15);
			String NewPin=PropertyReader.getProperty("pintri");
			NewPINfield.sendKeys(NewPin);
			Log.message(NewPin+" is entered is new PIN field");
			
			SkySiteUtils.waitForElement(driver, ReenterPINfield, 15);
			ReenterPINfield.sendKeys(NewPin);
			Log.message(NewPin+" is entered is re-enter PIN field");
			
			SkySiteUtils.waitForElement(driver, SubmitPINbtn, 10);
			SubmitPINbtn.click();
			Log.message("Clicked on Submit PIN button");
			
			
			SkySiteUtils.waitForElement(driver, Usernamefield, 10);
			Log.message("Username field is displayed now");
			if(Usernamefield.isDisplayed())
				return true;
			else
				return false;
			
		}
   
		
		/**Method to Set 2 Step authentication | Scripted by Trinanjwan 
		 */

		public boolean settingPIN()
		{
			

			SkySiteUtils.waitForElement(driver, OldPasswordfield, 10);
			String Password=PropertyReader.getProperty("complexpasswordtri");
			OldPasswordfield.sendKeys(Password);
			Log.message(Password+" is entered in password field");
			
			SkySiteUtils.waitForElement(driver, NewPINfield, 15);
			String NewPin=PropertyReader.getProperty("pintri");
			NewPINfield.sendKeys(NewPin);
			Log.message(NewPin+" is entered is new PIN field");
			
			SkySiteUtils.waitForElement(driver, ReenterPINfield, 15);
			ReenterPINfield.sendKeys(NewPin);
			Log.message(NewPin+" is entered is re-enter PIN field");
			
			SkySiteUtils.waitForElement(driver, SubmitPINbtn, 10);
			SubmitPINbtn.click();
			Log.message("Clicked on Submit PIN button");
			
			
			SkySiteUtils.waitForElement(driver, Usernamefield, 10);
			Log.message("Username field is displayed now");
			if(Usernamefield.isDisplayed())
				return true;
			else
				return false;
			
		}
		
		/**
		 * Method written to verify the check/un-check status of Complex Password and 2 step authentication for F&A | Scripted Trinanjwan
		 */	
		

		public boolean verifyCheckUncheckofComplexPassword()
		
		{
			driver.switchTo().defaultContent();
			Log.message("Switched to default content");
			driver.switchTo().frame("myFrame");
	        Log.message("Switched to My Frame");
	        SkySiteUtils.waitTill(7000);
			
			
			WebElement isChecked1;
			WebElement isChecked2;
			
			isChecked1 = driver.findElement(By.cssSelector(".stnd-sign-in-container > div:nth-child(2) > div:nth-child(1) > label:nth-child(1) > input:nth-child(1)"));
			isChecked2 = driver.findElement(By.cssSelector("div.checkbox:nth-child(3) > label:nth-child(1) > input:nth-child(1)"));
			if (isChecked1.isSelected() && isChecked2.isSelected() )
				return true;
			else
				return false;
				
		}
		
		
		
	
		

}
