package com.arc.fna.pages;


import java.awt.image.BufferedImage;
import java.io.BufferedWriter;
import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Random;

import javax.imageio.ImageIO;

import org.apache.commons.io.FileUtils;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.Point;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.LoadableComponent;
import org.testng.Assert;

import com.arc.fna.utils.PropertyReader;
import com.arc.fna.utils.SkySiteUtils;
import com.arc.fna.utils.randomFileName;
import com.arcautoframe.utils.Log;

public class FNA_AccountSettingPage extends LoadableComponent<FNA_AccountSettingPage> {
	 WebDriver driver;
	    private  boolean isPageLoaded;
	@FindBy(css="#uploadlogo")
	WebElement uploadlogobtn;
	@FindBy(css="#btnGetCroppedImage")
	WebElement savebtn;
	    @Override
	protected void load() {
		// TODO Auto-generated method stub
		isPageLoaded=true;
		SkySiteUtils.waitTill(7000);
		driver.switchTo().frame(driver.findElement(By.id("myFrame")));
		SkySiteUtils.waitForElement(driver, uploadlogobtn, 60);
		Log.message("Account setting page is loaded");
		driver.switchTo().defaultContent();
	}

	@Override
	protected void isLoaded() throws Error {
		// TODO Auto-generated method stub
		if (!isPageLoaded)
		{
			Assert.fail();
		}
	}
	public FNA_AccountSettingPage(WebDriver driver) {
		this.driver=driver;
		PageFactory.initElements(driver, this);
	}
   
	


	/**Method to add logo in account setting
	 *Scripted by : Tarak 
	 * @throws IOException 
	 */
	public boolean addLogo(String tempfilepath,String folderpath) throws IOException {
		
	    driver.switchTo().defaultContent();
	    SkySiteUtils.waitTill(3000);
	    Log.message("Switching to  frame to add logo");
		driver.switchTo().frame(driver.findElement(By.id("myFrame")));
		uploadlogobtn.click();
		Log.message("add logo is clicked");
	    SkySiteUtils.waitTill(5000);	
		//Writing File names into a text file for using in AutoIT Script
		BufferedWriter output;			
	    randomFileName rn=new randomFileName();
		String tmpFileName=tempfilepath+rn.nextFileName()+".txt";		
		output = new BufferedWriter(new FileWriter(tmpFileName,false));	
		String expFilename=null;
	    File[] files = new File(folderpath).listFiles();
						
				for(File file : files)
				{
					if(file.isFile()) 
					{
						expFilename=file.getName();//Getting File Names into a variable
						Log.message("Expected File name is:"+expFilename);	
						output.append('"' + expFilename + '"');
						output.append(" ");
						SkySiteUtils.waitTill(5000);	
					}	
				}	
				output.flush();
				output.close();	
						
				String AutoItexe_Path = PropertyReader.getProperty("AutoItexe1");
				//Executing .exe autoIt file		 
				Runtime.getRuntime().exec(AutoItexe_Path+" "+ folderpath+" "+tmpFileName );			
				Log.message("AutoIT Script Executed!!");				
				SkySiteUtils.waitTill(30000);
						
				try
				{
					File file = new File(tmpFileName);
					if(file.delete())
					{
						Log.message(file.getName() + " is deleted!");
					}	
					else
					{
						Log.message("Delete operation is failed.");
					}
				}
				catch(Exception e)
				{			
					Log.message("Exception occured!!!"+e);
				}	
				SkySiteUtils.waitTill(3000);
				savebtn.click();
				Log.message("save button is clicked");
	            SkySiteUtils.waitTill(5000);
	            if(driver.findElement(By.xpath(".//*[@id='imgClientLogo']")).isDisplayed()==true) {
					Log.message("Addition of logo is done successfully");
					return true;
					
				}else
				{
					return false;
				}
		
}
	
	@FindBy(xpath=".//*[@id='logodeletebutton']/i")
	WebElement deletelogo;

	@FindBy(xpath=".//*[@id='button-1']")
	WebElement Yesbtn;
	@FindBy(xpath=".//*[@id='button-0']")
	WebElement Nobtn;
	/**Method to delete logo
	 * 
	 */
	public void deleteLogo() {
		SkySiteUtils.waitTill(4000);
		deletelogo.click();
		Log.message("delete logo button is clicked");
		driver.switchTo().defaultContent();
		SkySiteUtils.waitForElement(driver, Yesbtn, 40);
		Yesbtn.click();
		Log.message("Yes button is clicked");
		driver.switchTo().frame(driver.findElement(By.id("myFrame")));
	    SkySiteUtils.waitForElement(driver, uploadlogobtn, 60);
		Log.message("Logo is deleted succcesfully");
	}
	/**Method to delete logo and verify deletion
	 * 
	 */
	public boolean deleteLogoandverify() {
		deletelogo.click();
		Log.message("delete logo button is clicked");
		driver.switchTo().defaultContent();
		SkySiteUtils.waitForElement(driver, Yesbtn, 40);
		Yesbtn.click();
		Log.message("Yes button is clicked");
		driver.switchTo().frame(driver.findElement(By.id("myFrame")));
		   SkySiteUtils.waitTill(5000);
           if(!(driver.findElement(By.xpath(".//*[@id='imgClientLogo']")).isDisplayed())) {
				Log.message("Added logo is deleted successfully");
				return true;
				
			}else
			{
				
				return false;
			}
	
	}
	/**method to delete existing logo
	 * Scripted By:Tarak
	 * 
	 */
	public void delete_existinglogo() {
		  driver.switchTo().defaultContent();
		SkySiteUtils.waitTill(6000);
	    driver.switchTo().frame(driver.findElement(By.id("myFrame")));
	    SkySiteUtils.waitTill(2000);
		List<WebElement> ele = driver.findElements(By.xpath("//*[@class='icon icon-close-round icon-lg']"));
		SkySiteUtils.waitTill(2000);
		 //if(driver.findElement(By.xpath(".//*[@id='imgClientLogo']")).isDisplayed()==true) {
		    if(ele.size()>0) {
		       Log.message("Logo already existing"); 
			   driver.switchTo().defaultContent();
			   SkySiteUtils.waitTill(3000);
		       driver.switchTo().frame(driver.findElement(By.id("myFrame")));
			   driver.findElement(By.xpath("//*[@class='icon icon-close-round icon-lg']")).click();
			   Log.message("Delete button is clicked");
		       SkySiteUtils.waitTill(5000);
			   driver.switchTo().defaultContent();
				SkySiteUtils.waitForElement(driver, Yesbtn, 40);
				Yesbtn.click();
				Log.message("Yes button is clicked");
				driver.switchTo().frame(driver.findElement(By.id("myFrame")));
			    SkySiteUtils.waitForElement(driver, uploadlogobtn, 60);
				Log.message("Logo is deleted succcesfully");
			//   Log.message("Existing logo deleted sucessfully");
				
			}
		    
		    else if(ele.size()<0)
			{
				Log.message("No logo exist");
			}
		
	}
	
	
	/**Method to change logo
	 * Scripted by :Tarak
	 * @throws IOException 
	 */
	public boolean changeLogo(String tempfilepath,String folderpath,String filepath1) throws IOException {
		SkySiteUtils.waitForElement(driver, uploadlogobtn, 40);
		uploadlogobtn.click();
		Log.message("Change logo button is clicked ");
		BufferedWriter output;			
	    randomFileName rn=new randomFileName();
		String tmpFileName=tempfilepath+rn.nextFileName()+".txt";		
		output = new BufferedWriter(new FileWriter(tmpFileName,false));	
		String expFilename=null;
	    File[] files = new File(folderpath).listFiles();
						
				for(File file : files)
				{
					if(file.isFile()) 
					{
						expFilename=file.getName();//Getting File Names into a variable
						Log.message("Expected File name is:"+expFilename);	
						output.append('"' + expFilename + '"');
						output.append(" ");
						SkySiteUtils.waitTill(5000);	
					}	
				}	
				output.flush();
				output.close();	
						
				String AutoItexe_Path = PropertyReader.getProperty("AutoItexe1");
				//Executing .exe autoIt file		 
				Runtime.getRuntime().exec(AutoItexe_Path+" "+ folderpath+" "+tmpFileName );			
				Log.message("AutoIT Script Executed!!");				
				SkySiteUtils.waitTill(30000);
						
				try
				{
					File file = new File(tmpFileName);
					if(file.delete())
					{
						Log.message(file.getName() + " is deleted!");
					}	
					else
					{
						Log.message("Delete operation is failed.");
					}
				}
				catch(Exception e)
				{			
					Log.message("Exception occured!!!"+e);
				}			
				savebtn.click();
				Log.message("save button is clicked");
	            SkySiteUtils.waitTill(5000);
	            SkySiteUtils.waitForElement(driver, uploadlogobtn,40);
	            SkySiteUtils.waitTill(5000);
	           
	            File fiscapture = new File(PropertyReader.getProperty("templogopath"));
	  			String filepathtemp = fiscapture.getAbsolutePath().toString();
	        	String savedscreenshot1 = filepath1 + "\\runtimescreenshot.png";
	    		String savedscreenshot2 = filepathtemp + "\\screenshotOfChangedLogo.png";
	    		Log.message("taking screen shot of changed logo");
		        SkySiteUtils.waitTill(5000);
	    		File srcFiler = ((TakesScreenshot) driver).getScreenshotAs(OutputType.FILE);

	    		FileUtils.copyFile(srcFiler, new File(savedscreenshot1));

	    		Log.message("Screenshot taken of changed logo");
	    		SkySiteUtils.waitTill(5000);
	    		Log.message("Checking the saved screenshot with temp screenshot");
	    		BufferedImage imgA = ImageIO.read(new File(savedscreenshot1));

	    		// Thread.sleep(2000);

	    		BufferedImage imgB = ImageIO.read(new File(savedscreenshot2));

	    		// Thread.sleep(2000);

	    		Log.message("Checking the height and width of both the images are same?");

	    		if (imgA.getWidth() != imgB.getWidth() && imgA.getHeight() != imgB.getHeight()) {
	    			Log.message("image height width are different");
	    			return false;
	    		}

	    		int width = imgA.getWidth();
	    		int height = imgA.getHeight();
	    		for (int y = 0; y < height; y++) {
	    			for (int x = 0; x < width; x++) {

	    				if (imgA.getRGB(x, y) != imgB.getRGB(x, y)) {

	    					Log.message("Image are different");
	    					return false;
	    				}
	    			}

	    		}

	    		return true;
	            
		}
	/**
	 * Method to delete screenshot
	 * 
	 * @param Screenshotpath
	 * @return
	 */
	public void delteScreenShot(String Screenshotpath) {
		Log.message("Searching screenshot downloaded");
		File file = new File(Screenshotpath);
		File[] Filearray = file.listFiles();
		if (file.isDirectory()) {
			for (File Myfile : Filearray) {
				if (Myfile.getName().contains(".png")) {
					Myfile.delete();
					Log.message("File is deleted successfully");
				}
			}
		}

	}
	@FindBy(xpath=".//*[@id='rdAlphanumeric']")
	WebElement alphanumericbutn;
	@FindBy(css="#btnSettingsSave")
	WebElement savesettingbtn;
	@FindBy(xpath=".//*[@id='ProjectFiles']/a")
	WebElement documenttab;
	@FindBy(css="#lftpnlMore")
	WebElement moreoption;
	@FindBy(css="#btnFolderSort>a")
	WebElement foldersortingoption;
	/**Method to select foldersorting option and verify the same in document tab
	 * 
	 */
	public boolean selectFolderSortingOption() {
		driver.switchTo().defaultContent();
		driver.switchTo().frame(driver.findElement(By.id("myFrame")));
		Log.message("Switched to frame");
		SkySiteUtils.waitForElement(driver, alphanumericbutn,40);
		alphanumericbutn.click();
		Log.message("Alphanumericbutton is clicked");
		SkySiteUtils.waitForElement(driver, savesettingbtn, 40);
		savesettingbtn.click();
	 	Log.message("save button is clicked");
	    SkySiteUtils.waitTill(4000);	
		driver.switchTo().defaultContent();
		SkySiteUtils.waitTill(2000);
		JavascriptExecutor executor = (JavascriptExecutor)driver;
		executor.executeScript("arguments[0].click();", documenttab);

		Log.message("document tab is clicked");
		driver.switchTo().frame(driver.findElement(By.id("myFrame")));
		SkySiteUtils.waitForElement(driver, moreoption, 40);
		moreoption.click();
		Log.message("more option is clicked");
		foldersortingoption.click();
		Log.message("folder sorting option is clikced");
		SkySiteUtils.waitTill(2000);
		if(driver.findElement(By.xpath(".//*[@id='radName']")).isSelected()==true)
		{
			Log.message("Alphnumeric option selected by default");
            return true;
		}
		else {
			
			   Log.message("Alphabnumeric option is clicked");
			   return false;
			
		}
		
	}
	@FindBy(xpath=".//*[@id='btnFolderSortClose']")
	WebElement closefolderoption;
	/**Method to close folder option 
	 * 
	 * 
	 */
	public FnaHomePage closeFolderOption() {
		SkySiteUtils.waitForElement(driver, closefolderoption, 40);
		closefolderoption.click();
		Log.message("closefolder option is clicked");
		driver.switchTo().defaultContent();
		return new FnaHomePage(driver).get();
		
	
	}
	@FindBy(css="#rdManuallyOrganized")
	WebElement manuallyorganizesoption;
	/**Method to change folder sorting option to manually organized
	 * 
	 */
	 public void setOptionToManuallyOrganized() {
			driver.switchTo().frame(driver.findElement(By.id("myFrame")));
			SkySiteUtils.waitForElement(driver, manuallyorganizesoption, 40);
			manuallyorganizesoption.click();
			Log.message("Manually organized button is clicked");
			SkySiteUtils.waitForElement(driver, savesettingbtn, 40);
			savesettingbtn.click();
			Log.message("save button is clicked");
		    SkySiteUtils.waitTill(3000);
		    if(manuallyorganizesoption.isSelected()) {
		    	Log.message("manually organized button is selected ");
		    }
		    else {
		    	
		    	
		    	Log.message("Manually organized button is not selected");
		    }
		 
		}
	 @FindBy(css="#rdDeletePermanently")
	 WebElement deltepermanentlyoption;
	 @FindBy(xpath=".//*[@id='liRecycle']/a")
	 WebElement Recyclebinoption;
	 @FindBy(xpath=".//*[@id='btnRecycleSetting']")
	 WebElement settingbtn;
	 /**Method to select recyclebin option in account setting and check it reflects in recycle bin tab
	  * 
	  */
	 public boolean setRecyclebinOption() {
		 driver.switchTo().defaultContent();
		 driver.switchTo().frame(driver.findElement(By.id("myFrame")));
			Log.message("Switched to frame");
			SkySiteUtils.waitForElement(driver, deltepermanentlyoption, 40);
			deltepermanentlyoption.click();
			Log.message("Delete Permanently option is clicked");
			SkySiteUtils.waitForElement(driver, savesettingbtn, 40);
			savesettingbtn.click();
			Log.message("save button is clicked");
			driver.switchTo().defaultContent();
			JavascriptExecutor executor = (JavascriptExecutor)driver;
			executor.executeScript("arguments[0].click();", Recyclebinoption);
			//Recyclebinoption.click();
			Log.message("Recyclebin option is clicked");
			 driver.switchTo().frame(driver.findElement(By.id("myFrame")));
			 SkySiteUtils.waitForElement(driver, settingbtn, 40);
			 settingbtn.click();
			 Log.message("Setting button is clicked");
		     SkySiteUtils.waitTill(4000);
			 if(driver.findElement(By.xpath(".//*[@id='rdDeletePermanently']")).isSelected()==true) {
				 Log.message("Delete Permanent option is selected in recyclebin tab");
	
				 return true;
			 }
			 else
			 {
				 Log.message("Option selected from setting tab is not reflected in recyclebin tab");
				 return false;
			 }
		 
		}
	 @FindBy(xpath=".//*[@id='divTeamInfo']/div/div/div[3]/button")
	 WebElement closebtn;
	 /**Method to close folder option 
		 * 
		 * 
		 */
		public FnaHomePage closeSettingTab() {
			SkySiteUtils.waitForElement(driver, closebtn, 40);
			closebtn.click();
			Log.message("closefolder option is clicked");
			driver.switchTo().defaultContent();
			return new FnaHomePage(driver).get();
			
		
		}
		@FindBy(xpath=".//*[@id='rdMoveToRecycle']")
		WebElement MoveToRecyclebin;
		/**Method to select move to recyclebin option
		 * 
		 */
	
		
			 public void setOptionMoveToRecyclebin(){
					driver.switchTo().frame(driver.findElement(By.id("myFrame")));
					SkySiteUtils.waitForElement(driver, MoveToRecyclebin, 40);
					MoveToRecyclebin.click();
					Log.message("Move to Recyclebin button is clicked");
					SkySiteUtils.waitForElement(driver, savesettingbtn, 40);
					savesettingbtn.click();
					Log.message("save button is clicked");
				    SkySiteUtils.waitTill(3000);
				    if(MoveToRecyclebin.isSelected()) {
				    	Log.message("Move to recyclebin option is selected ");
				    }
				    else {
				    	
				    	
				    	Log.message("Move to recyclebin  button is not selected");
				    }
				 
				}
	  @FindBy(xpath=".//*[@id='txtExpiryPeriod']")
	  WebElement expiryperiod;
	  @FindBy(xpath=".//*[@id='divFolderSort']/div[6]/div/div[2]/div/button")
	  WebElement dropdown;
	  @FindBy(xpath=".//*[@id='divFolderSort']/div[6]/div/div[2]/div/div/ul/li[4]/a")
	  WebElement dayoption;
	  @FindBy(xpath=".//*[@id='Collections']/a")
	  WebElement collectiontab;
	  
			 /**Folder to set link expiration (in Days) for link files
			  * 
			  */
			 public void setExpirationInDaysForFiles(String noofdays) {
				 driver.switchTo().defaultContent();
				 
				 driver.switchTo().frame(driver.findElement(By.id("myFrame")));
					Log.message("Switched to frame");
					SkySiteUtils.waitForElement(driver, expiryperiod,50);
			    //String no="5";
			    expiryperiod.click();
			    Log.message("expiry period box clicked");
			    expiryperiod.clear();
			    Log.message("expiry period box cleared");
			    
			    expiryperiod.sendKeys(noofdays);
			    Log.message("expiry period has been set to " +noofdays);
			    SkySiteUtils.waitTill(2000);
			    dropdown.click();
			    Log.message("Drop down is clicked");
			    SkySiteUtils.waitTill(3000);
			    dayoption.click();
			    Log.message("Day option is selected");
			    SkySiteUtils.waitForElement(driver, savesettingbtn, 40);
				savesettingbtn.click();
				Log.message("save button is clicked");
				driver.switchTo().defaultContent();
				SkySiteUtils.waitTill(5000);
				JavascriptExecutor executor = (JavascriptExecutor)driver;
				executor.executeScript("arguments[0].click();", collectiontab);
				//collectiontab.click();
				Log.message("Collection tab is clicked");
				 
				 }
			 @FindBy(xpath=".//*[@id='aspnetForm']/div[3]/section[2]/div/div[2]/nav/div[5]/div[1]/button[2]")
			 WebElement linkfileoption;
			 @FindBy(xpath=".//*[@id='txtExpiryDate_hyperlink']")
			 WebElement expirationdate;
			 /**Open file link option and verify the set expiration date from settings
			  * 
			  */
			 public boolean openLinkFileOptionVerifySetDayPeriod(String noofdays) {
				 
					//Log.message("Switched to default content");
					SkySiteUtils.waitTill(5000);
					//driver.switchTo().frame(driver.findElement(By.id("myFrame")));
					//Log.message("Switched to frame");
					driver.findElement(By.xpath("//*[text()='ParentFolder']")).click();
					Log.message("Folder is selected");
					SkySiteUtils.waitTill(4000);
					driver.findElement(By.xpath(".//*[@id='divGrid']/div[2]/table/tbody/tr[2]/td[1]/img")).click();
					Log.message("File is selected");
					SkySiteUtils.waitForElement(driver, linkfileoption, 40);
					linkfileoption.click();
					Log.message("Link file option is clicked");
					SkySiteUtils.waitTill(3000);
					
					String date1=expirationdate.getAttribute("value");
					Log.message("The date is " +date1);
				   Log.message("split(String regex, int limit) with limit=3:");
				       String array2[]= date1.split("/", 3);
				       for (String temp: array2){
				          Log.message(temp);
				       }
				    Date d=new Date();
			        Log.message("the current date is " +d.toString());
			        String currenttimearray[]=d.toString().split(" ");
			        for(String cdata:currenttimearray)
			        {
			        	Log.message(cdata);
			        }
			        String outputtotaldate=currenttimearray[2] + noofdays;
			        Log.message("Total expected date for expiry is " +outputtotaldate);
			        
			        if(outputtotaldate.equals(array2[2])) {
			        	
			        	Log.message("expiry date is reflected successfully");
			        	return true;
			        }
			        return false;
			 }
				@FindBy(xpath = "//*[@id='Collections']/a")

				WebElement clkcollection;
				@FindBy(xpath = "//*[@id='btnResetSearch']")

				WebElement resetcollection;
				@FindBy(xpath = "//*[@id='setting']")

				WebElement drpSetting;
				@FindBy(css = ".selectedTreeRow")

				WebElement selectedTreeRow;
			 
				/**
				 * Method written for select collection
				 *  Scripted By: Sekhar
				 * 
				 * @return
				 */
				public boolean selectcollection(String Collection_Name) 
				{
					SkySiteUtils.waitTill(5000);
					driver.switchTo().defaultContent();
					SkySiteUtils.waitForElement(driver, drpSetting, 60);
					SkySiteUtils.waitTill(3000);
					clkcollection.click();// click on collection Selection
					Log.message("collection has been clicked.");
					SkySiteUtils.waitTill(5000);
					driver.switchTo().frame(driver.findElement(By.id("myFrame"))); // Back the Switch to Frame
					resetcollection.click();// click on reset button
					Log.message("reset button has been clicked.");
					SkySiteUtils.waitTill(5000);
					int prjCount_prjList = driver.findElements(By.xpath("//*[@id='listGrid']/div[2]/table/tbody/tr/td[1]/img"))
							.size();
					Log.message("prjCount_prjList :" + prjCount_prjList);
					// Loop start '2' as projects list start from tr[2]
					int prj = 0;
					for (prj = 1; prj <= prjCount_prjList; prj++) 
					{
						// x path as dynamically providing all rows(prj) tr[prj]
						String prjName = driver.findElement(By.xpath("(//*[@id='listGrid']/div[2]/table/tbody/tr/td[3]/a)[" + prj + "]")).getText();
						Log.message(prjName);
						SkySiteUtils.waitTill(2000);
						// Checking project name equal with each row of table
						if (prjName.equals(Collection_Name)) 
						{
							Log.message("Select Collection button has been clicked!!!");
							break;
						}
					}
					SkySiteUtils.waitTill(5000);
					// Click on project name where it equal with specified project name
					driver.findElement(By.xpath("(//*[@id='listGrid']/div[2]/table/tbody/tr/td[3]/a)[" + prj + "]")).click();
					SkySiteUtils.waitTill(5000);
					SkySiteUtils.waitForElement(driver, selectedTreeRow, 60);
					SkySiteUtils.waitTill(5000);
					String Collection = selectedTreeRow.getText();
					Log.message("Collection name is:" + Collection);
					SkySiteUtils.waitTill(2000);
					if (Collection.contains(Collection_Name)) 
					{
						Log.message("Select Collection successfull");
						SkySiteUtils.waitTill(2000);
						return true;
					} 
					else
					{
						Log.message("Select Collection Unsuccessfull");
						SkySiteUtils.waitTill(2000);
						return false;
					}

				}
			
		}
