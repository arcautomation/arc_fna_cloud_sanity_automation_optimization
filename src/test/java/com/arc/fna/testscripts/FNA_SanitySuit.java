package com.arc.fna.testscripts;

import java.io.File;
import java.util.HashMap;
import java.util.Map;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.safari.SafariDriver;
import org.openqa.selenium.support.FindBy;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Listeners;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import com.arc.fna.pages.CollectionPackagePage;
import com.arc.fna.pages.CollectionTaskPage;
import com.arc.fna.pages.CommonLoginPage;
import com.arc.fna.pages.FNAAlbumPage;
import com.arc.fna.pages.FnAMyProfilePage;
import com.arc.fna.pages.FnARegistrationPage;
import com.arc.fna.pages.FnaContactTabPage;
import com.arc.fna.pages.FnaCreateGroupPage;
import com.arc.fna.pages.FnaHomePage;
import com.arc.fna.pages.FnaLoginPage;
import com.arc.fna.pages.FnaSanityHomePage;
import com.arc.fna.pages.ProjectsMyProfPage;
import com.arc.fna.utils.PropertyReader;
import com.arcautoframe.utils.EmailReport;
import com.arcautoframe.utils.Log;

@Listeners(EmailReport.class)
public class FNA_SanitySuit {
	 static WebDriver driver;
	    FnaLoginPage loginpage;
	    FnaSanityHomePage fnahomepage;
	    FnARegistrationPage regpage;
	    FnAMyProfilePage myprofpage;
	    CommonLoginPage commonpage;
	    ProjectsMyProfPage projectmyprogpage;
	    FnaContactTabPage fnacontacttabpage;
	    FNAAlbumPage fnaalbumpage;
	    CollectionPackagePage collectionPackagePage;
		CollectionTaskPage collectionTaskPage;
	    @Parameters("browser")
	    @BeforeMethod
	    public WebDriver beforeTest(String browser) {
			
	    	if(browser.equalsIgnoreCase("firefox")) {
	    		File dest = new File("./drivers/win/geckodriver.exe");
	    		//System.setProperty("webdriver.gecko.driver", dest.getAbsolutePath());
	    		System.setProperty("webdriver.firefox.marionette", dest.getAbsolutePath());
	    		driver = new FirefoxDriver();
	    		driver.get(PropertyReader.getProperty("SkysiteStagingURL"));
	        }
	    	else if (browser.equalsIgnoreCase("chrome")) { 
	    		File dest = new File("./drivers/win/chromedriver.exe");
	    		System.setProperty("webdriver.chrome.driver", dest.getAbsolutePath());
	            Map<String, Object> prefs = new HashMap<String, Object>();
	            prefs.put("download.default_directory",  "C:"+File.separator+"Users"+File.separator+System.getProperty("user.name")+ File.separator + "Downloads");
	            ChromeOptions options = new ChromeOptions();
	        	options.addArguments("--start-maximized");
	            options.setExperimentalOption("prefs", prefs);
	            driver = new ChromeDriver( options );
	    		driver.get(PropertyReader.getProperty("SkysiteStagingURL"));
	 
	        } 
	    	else if (browser.equalsIgnoreCase("safari"))
	    	{
				System.setProperty("webdriver.safari.noinstall", "true"); //To stop uninstall each time
				driver = new SafariDriver();
				driver.get(PropertyReader.getProperty("SkysiteStagingURL"));
	    	}
	   return driver;
	}
	    
	    /** TC_001(Sanity) : Verify Login to skysite
	     * @throws Exception 
	        *  
	        */
	        @Test(priority=0,enabled=true,description="TC_001(Sanity) : Verify Login to skysite")
	        public void verifyValidLogin() throws Exception {
	          try {
	        	    Log.testCaseInfo("TC_001(Sanity) : Verify Login to skysite");
	        	    loginpage = new FnaLoginPage(driver).get();
	        	    String uName=PropertyReader.getProperty("EmailSanity");
	        	    String pWord=PropertyReader.getProperty("PasswordSanity");
	        	    fnahomepage=loginpage.login(uName, pWord);
	        	   Log.assertThat(fnahomepage.loginValidation(),"Login is successfull ","Login to skysite failed");
	        	    
	        	
	              }
	          catch(Exception e)
	          {
	                 e.getCause();
	                 Log.exception(e, driver);
	          }
	          finally
	          {
	                 Log.endTestCase();
	                 driver.quit();
	          }
	    	
	        }
	        /** TC_002(Sanity) : Verify registration and activation of FNA
		     * @throws Exception 
		        *  
		        */
		        @Test(priority=1,enabled=true,description="TC_002(Sanity) :Verify registration and activation of FNA")
		        public void verifyValidRegistrationAndFnaActivation() throws Exception {
		          try {
		        	    Log.testCaseInfo("TC_002(Sanity) : Verify registration and activation of FNA");
		        	    loginpage = new FnaLoginPage(driver).get();
		        	    regpage=new FnARegistrationPage(driver).get();
	            	    String Emailrad=regpage.Random_Email();
	            	    projectmyprogpage=regpage.RegistrationWithValidCredential(Emailrad);
	            	    projectmyprogpage.handlingsamplevideoPROJECTS();
	            	    projectmyprogpage.handlingNewFeaturepopover();
	            	    projectmyprogpage.presenceOfPrjDashboardButton();
	            	    commonpage=projectmyprogpage.navigatingToCommonLoginPage();
	            	    fnahomepage=commonpage.activateFnAModule();
	            	    fnahomepage.handlingNewFeaturepopover();
	            	    fnahomepage.handlingDemoVideoepopover();
	            	    Log.assertThat(fnahomepage.presenceOfMyProfileBtn(), "Activation of FnA module sucessfully done", 
	            	    		"Activation of F&A module got failed",driver);       	    
	            	    
		        	
		              }
		          catch(Exception e)
		          {
		                 e.getCause();
		                 Log.exception(e, driver);
		          }
		          finally
		          {
		                 Log.endTestCase();
		                 driver.quit();
		          }
		        }
		 
		        /** TC_003 (Sanity): Verify creation of collection.
		    	 *  Scripted By: Sekhar
		    	 * @throws Exception
		    	 */
		    	@Test(priority = 2, enabled = true, description = "TC_003 (Sanity): Verify creation of collection.")
		    	public void verifyCreationCollection() throws Exception
		    	{
		    		try
		    		{
		    			 Log.testCaseInfo("TC_003 (Sanity): Verify creation of collection.");
		    			 loginpage = new FnaLoginPage(driver).get();   
		    			 String uName=PropertyReader.getProperty("EmailSanity");
			        	     String pWord=PropertyReader.getProperty("PasswordSanity");
			        	     fnahomepage=loginpage.login(uName, pWord);
			        	     Log.assertThat(fnahomepage.loginValidation(),"Login is successfull ","Login to skysite failed");	
		    			     String CollectionName= fnahomepage.Random_Collectionname();					
		    	             Log.assertThat(fnahomepage.Create_Collections(CollectionName),"Creation of collection is successfull","Creation of collection is not working");		
		    	           //  fnahomepage.deleteCollection(CollectionName);
			        	
		              }
		          catch(Exception e)
		          {
		                 e.getCause();
		                 Log.exception(e, driver);
		          }
		          finally
		          {
		                 Log.endTestCase();
		                 driver.quit();
		          }
		    	}

		    	/**TC_004(Sanity): Verify Addition of folder 
		    	 * @throws Exception 
		    	 * 
		    	 */
		    	 @Test(priority=3,enabled=true,description="TC_004(Sanity): Verify Addition of folder ")
		    	 public void verifyAdditionOfFolder() throws Exception {
		    			try {
		    				 Log.testCaseInfo("TC_004(Sanity): Verify Addition of folder ");
		    				 loginpage = new FnaLoginPage(driver).get();   
			    			 String uName=PropertyReader.getProperty("EmailSanity");
				        	     String pWord=PropertyReader.getProperty("PasswordSanity");
				        	     fnahomepage=loginpage.login(uName, pWord);
				        	     Log.assertThat(fnahomepage.loginValidation(),"Login is successfull ","Login to skysite failed");	
			    			    String collectionname=PropertyReader.getProperty("Collectioname_sanity");
			    			    fnahomepage.selectcollection(collectionname);	
		    			String FolderName= fnahomepage.Random_Foldername();		
		    			Log.assertThat(fnahomepage.Adding_Folder(FolderName), "Adding folder Successfull", "Adding folder UnSuccessful", driver);
		    			fnahomepage.deleteFolder();
			        	
			              }
			          catch(Exception e)
			          {
			                 e.getCause();
			                 Log.exception(e, driver);
			          }
			          finally
			          {
			                 Log.endTestCase();
			                 driver.quit();
			          }
		    	 
		    			
		    	 }	

		    	
		    	 /**TC_005(Sanity): Verify Upload of file 
			    	 * @throws Exception 
			    	 * 
			    	 */
			    	 @Test(priority=4,enabled=true,description="TC_005(Sanity): Verify Upload of file  ")
			    	 public void verifyUploadOfFile() throws Exception {
			    			try {
			    				     Log.testCaseInfo("TC_005(Sanity): Verify Upload of file ");
			    	
				    				 loginpage = new FnaLoginPage(driver).get();   
					    			 String uName=PropertyReader.getProperty("EmailSanity");
						        	     String pWord=PropertyReader.getProperty("PasswordSanity");
						        	     fnahomepage=loginpage.login(uName, pWord);
						        	     Log.assertThat(fnahomepage.loginValidation(),"Login is successfull ","Login to skysite failed");	
					    			    String collectionname=PropertyReader.getProperty("Collectioname_sanity");
					    			    fnahomepage.selectcollection(collectionname);	
				    			   String FolderName= fnahomepage.Random_Foldername();		
				    			    Log.assertThat(fnahomepage.Adding_Folder(FolderName), "Adding folder Successfull", "Adding folder UnSuccessful", driver);
				    				File Path=new File(PropertyReader.getProperty("FolderPath"));			
				    				String FolderPath = Path.getAbsolutePath().toString();
				    				
				    				Log.assertThat(fnahomepage.UploadFileForEnableViewer(FolderPath), "Upload file Successfull", "Upload file UnSuccessful", driver);
			    			        fnahomepage.deleteFolder();
				        	     
				              }
				          catch(Exception e)
				          {
				                 e.getCause();
				                 Log.exception(e, driver);
				          }
				          finally
				          {
				                 Log.endTestCase();
				                 driver.quit();
				          }
			    	 
			    			
			    	 }	
			    	 

			    	 /**TC_006(Sanity): Verify Download of file 
				    	 * @throws Exception 
				    	 * 
				    	 */
				    	 @Test(priority=5,enabled=true,description="TC_006(Sanity): Verify Download of file  ")
				    	 public void verifyDownloadOfFile() throws Exception {
				    			try {
				    				     Log.testCaseInfo("TC_006(Sanity): Verify Download of file  ");
				    	
					    				 loginpage = new FnaLoginPage(driver).get();   
						    			 String uName=PropertyReader.getProperty("EmailSanity");
							        	     String pWord=PropertyReader.getProperty("PasswordSanity");
							        	     fnahomepage=loginpage.login(uName, pWord);
							        	     Log.assertThat(fnahomepage.loginValidation(),"Login is successfull ","Login to skysite failed");	
						    			    String collectionname=PropertyReader.getProperty("Collectioname_sanity");
						    			    fnahomepage.selectcollection(collectionname);
						    			    String Foldername=PropertyReader.getProperty("Foldernametoselect");
						    			    fnahomepage.selectFolder_File(Foldername);
						    			    String usernamedir=System.getProperty("user.name");
										    String downloadpath="C:\\" + "Users\\" + usernamedir + "\\Downloads";
										    String filename=PropertyReader.getProperty("FiletoDownload");
										Log.assertThat( fnahomepage.downloadFile(downloadpath,filename),"File is downloaded successfully","File download got failed!!!!");   
				    		     }
						          catch(Exception e)
						          {
						                 e.getCause();
						                 Log.exception(e, driver);
						          }
						          finally
						          {
						                 Log.endTestCase();
						                 driver.quit();
						          }
				    	 }
				    	 
				    	 /**TC_007(Sanity): Verify File Open in Viewer 
					    	 * @throws Exception 
					    	 * 
					    	 */
					    	 @Test(priority=6,enabled=true,description="TC_007(Sanity): Verify File Open in Viewer ")
					    	 public void verifyFileOpenInViewer() throws Exception {
					    			try {
					    				     Log.testCaseInfo("TC_007(Sanity): Verify File Open in Viewer   ");
					    	
						    				 loginpage = new FnaLoginPage(driver).get();   
							    			 String uName=PropertyReader.getProperty("EmailSanity");
								        	     String pWord=PropertyReader.getProperty("PasswordSanity");
								        	     fnahomepage=loginpage.login(uName, pWord);
								        	     Log.assertThat(fnahomepage.loginValidation(),"Login is successfull ","Login to skysite failed");	
							    			    String collectionname=PropertyReader.getProperty("Collectioname_sanity");
							    			    fnahomepage.selectcollection(collectionname);
							    			    String Foldername=PropertyReader.getProperty("Foldernametoselect");
							    			    fnahomepage.selectFolder_File(Foldername);
							    			   Log.assertThat(fnahomepage.fileOpenInViewer(),"File is opened in viewer successfully","File cannot be opened in viewer");
					    		     }
							          catch(Exception e)
							          {
							                 e.getCause();
							                 Log.exception(e, driver);
							          }
							          finally
							          {
							                 Log.endTestCase();
							                 driver.quit();
							          }
					    	 }
					    	 
					    	 
					    	  /** TC_008(Sanity):Verify Export of Address book in Excel format
					     	 * @throws Exception 
					     	  * 
					     	  */
					          @Test(priority=7,enabled=true,description="TC_008(Sanity):Verify Export of Address book in Excel format")
					          public void verifyExportAddressBook() throws Exception {
					         	 try {
					         		    Log.testCaseInfo("TC_008(Sanity):Verify Export of Address book in Excel format");
					         		   loginpage = new FnaLoginPage(driver).get();   
						    			 String uName=PropertyReader.getProperty("EmailSanity");
							        	     String pWord=PropertyReader.getProperty("PasswordSanity");
							        	     fnahomepage=loginpage.login(uName, pWord);
							        	     Log.assertThat(fnahomepage.loginValidation(),"Login is successfull ","Login to skysite failed");	
					        	     	  fnahomepage.loginValidation();
					        	     	   fnahomepage.contactIconPresent();
					        	     	  fnacontacttabpage=fnahomepage.contactClick();
					        	     	    String usernamedir=System.getProperty("user.name");
					        	     	    String downloadpath="C:\\" + "Users\\" + usernamedir + "\\Downloads";
					        	     	  Log.assertThat(fnacontacttabpage.exportAddressBook(downloadpath),"Contact file has been downloaded and found in the folder and the file is not blank","Contact file is not found in the folder");
					        	     	 
					         		   }
					     	    catch(Exception e) {
					     	    	 e.getCause();
					                  Log.exception(e, driver);
					     	    }
					         	 finally {
					         		 
					         		 Log.endTestCase();
					                  driver.quit();
					         	 }
					         	 
					          }
					          /**TC_009(Sanity): Verify Zip Download of album
						    	 * @throws Exception 
						    	 * 
						    	 */
						    	 @Test(priority=8,enabled=true,description="TC_009(Sanity): Verify Zip Download of album ")
						    	 public void verifyZipDownloadOfAlbum() throws Exception {
						    			try {
						    				     Log.testCaseInfo("TC_009(Sanity): Verify Zip Download of album  ");
						    	
							    				 loginpage = new FnaLoginPage(driver).get();   
								    			 String uName=PropertyReader.getProperty("EmailSanity");
									        	     String pWord=PropertyReader.getProperty("PasswordSanity");
									        	     fnahomepage=loginpage.login(uName, pWord);
									        	     Log.assertThat(fnahomepage.loginValidation(),"Login is successfull ","Login to skysite failed");	
								    			    String collectionname=PropertyReader.getProperty("Collectioname_sanity");
								    			    fnahomepage.selectcollection(collectionname);
								    			    fnaalbumpage=fnahomepage.clickAlbumIcon();
								    			    String albumname=PropertyReader.getProperty("Albumname");
								    			    		
								    			    String usernamedir=System.getProperty("user.name");
												    String downloadpath="C:\\" + "Users\\" + usernamedir + "\\Downloads";
								       	     	    Log.assertThat(fnaalbumpage.downloadAlbum(downloadpath,albumname),"Album is downloaded successfully and found in the download folder","Album is not found in the download folder");
								    			  	 
						         		   }
						     	    catch(Exception e) {
						     	    	 e.getCause();
						                  Log.exception(e, driver);
						     	    }
						         	 finally {
						         		 
						         		 Log.endTestCase();
						                  driver.quit();
						         	 }
						    			
						    			
						   }
						    	 
						    	  /**TC_010(Sanity): Verify Addition of new contact
							    	 * @throws Exception 
							    	 * 
							    	 */
							    	 @Test(priority=9,enabled=true,description="TC_010(Sanity): Verify Addition of new contact ")
							    	 public void verifyAdditionOfNewContact() throws Exception {
							    			try {
							    				     Log.testCaseInfo("TC_010(Sanity): Verify Addition of new contact ");
							    	
								    				 loginpage = new FnaLoginPage(driver).get();   
									    			 String uName=PropertyReader.getProperty("EmailSanity");
										        	     String pWord=PropertyReader.getProperty("PasswordSanity");
										        	     fnahomepage=loginpage.login(uName, pWord);
										        	     Log.assertThat(fnahomepage.loginValidation(),"Login is successfull ","Login to skysite failed");	
										        	     fnahomepage.contactIconPresent();
									        	     	  fnacontacttabpage=fnahomepage.contactClick();
									        	     	 String contactname=fnacontacttabpage.Random_Contactname();
									        	      	 String Lastname1=fnacontacttabpage.Random_LastName();
									        	     	 String emailid=fnacontacttabpage.Random_Email();
									        	         Log.assertThat(fnacontacttabpage.CreateNewContact_cntcsel(contactname, emailid,Lastname1),"Contact added successfully","Contact Addition got failed");
									        	        fnacontacttabpage.deleteContact(contactname);
							    			}
								     	    catch(Exception e) {
								     	    	 e.getCause();
								                  Log.exception(e, driver);
								     	    }
								         	 finally {
								         		 
								         		 Log.endTestCase();
								                  driver.quit();
								         	 }
								    			
							    }
							    	 /** TC_0011 (Sanity): Verify user add new package and add attachment from collection in packages.
							    		 *  Scripted By: Sekhar
							    		 * @throws Exception
							    		 */
							    		@Test(priority = 10, enabled = true, description = "TC_0011 (Sanity): Verify user add new package and add attachment from collection in packages.")
							    		public void verifyCreatePackage_Within_Packages() throws Exception
							    		{
							    				
							    			try
							    			{		
							    				Log.testCaseInfo("TC_0011 (Sanity): Verify user add new package and add attachment from collection in packages.");
							    				 loginpage = new FnaLoginPage(driver).get();   
								    			 String uName=PropertyReader.getProperty("EmailSanity");
									        	     String pWord=PropertyReader.getProperty("PasswordSanity");
									        	     fnahomepage=loginpage.login(uName, pWord);
									        	     Log.assertThat(fnahomepage.loginValidation(),"Login is successfull ","Login to skysite failed");	
								    			    String collectionname=PropertyReader.getProperty("Collectioname_sanity");
								    			    fnahomepage.selectcollection(collectionname);
							    				driver.switchTo().defaultContent();
							    				collectionPackagePage = new CollectionPackagePage(driver).get();			
							    				String PackageName= collectionPackagePage.Random_PackageName();	
							    				String Description = PropertyReader.getProperty("Description");
							    				Log.assertThat(collectionPackagePage.Add_Attachment_FromCollection(PackageName,Description), "Add attachment fron collection in Package Successfull", "Add attachment fron collection in Package UnSuccessful", driver);
							    			   Log.assertThat(collectionPackagePage.verifyAdditionOfPackage(PackageName), "Addition of package is successfull", "Package addition got failed");
							    			}
							    			catch(Exception e)
							    			{
							    				e.getCause();
							    				Log.exception(e, driver);
							    			}
							    			finally
							    			{
							    				Log.endTestCase();
							    				driver.quit();
							    			}
							    		}
							    		
							    		
							    		/** TC_0012 (Sanity): Verify  add task and Attach file(internal and external)in task.
							    		 
							    		 * @throws Exception
							    		 */
							    		@Test(priority = 11, enabled = true, description = "TC_0012 (Sanity): Verify  add task and Attach file(internal and external)in task.")
							    		public void verifyAddTaskAndAttach_File_Task () throws Exception
							    		{			
							    			try
							    			{		
							    				Log.testCaseInfo("TC_0012 (Sanity): Verify  add task and Attach file(internal and external)in task.");
							    				
							    				 loginpage = new FnaLoginPage(driver).get();   
								    			 String uName=PropertyReader.getProperty("EmailSanity");
									        	     String pWord=PropertyReader.getProperty("PasswordSanity");
									        	     fnahomepage=loginpage.login(uName, pWord);
									        	     Log.assertThat(fnahomepage.loginValidation(),"Login is successfull ","Login to skysite failed");	
									        	     String collectionname=PropertyReader.getProperty("Collectioname_sanity");
									    			    fnahomepage.selectcollection(collectionname);
							    				driver.switchTo().defaultContent();
							    				collectionTaskPage = new CollectionTaskPage(driver).get();
							    				String TaskName= collectionTaskPage.Random_Task();
							    				String SubjectName = collectionTaskPage.Random_Subject();
							    				String ContactName = PropertyReader.getProperty("CollectionEmp");	
							    				String Email = PropertyReader.getProperty("CollectionEmployee");
							    				Log.assertThat(collectionTaskPage.Add_Task_withSubject(TaskName,SubjectName,ContactName,Email), "Add team member Successful with valid credential", "Add team member unsuccessful with valid credential", driver);
							    				File Path=new File(PropertyReader.getProperty("FolderPath"));			
							    				String FolderPath = Path.getAbsolutePath().toString();		
							    				Log.assertThat(collectionTaskPage.Attach_file_task(FolderPath,TaskName,SubjectName,ContactName), "With Attach file in collection task Successful with valid credential", "With Attach file in collection task unsuccessful with valid credential", driver);
							    					
							    			}
							    			catch(Exception e)
							    			{
							    				e.getCause();
							    				Log.exception(e, driver);
							    			}
							    			finally
							    			{
							    				Log.endTestCase();
							    				driver.quit();
							    			}
							    		}
							    		
							    		/** TC_013 (Sanity): Link File with enable viewer
							    		
							    		 * @throws Exception
							    		 */
							    		@Test(priority = 12, enabled = true, description = "TC_013 (Sanity): Link File with enable viewer")
							    		public void verifyEnableviewer_Sendlink() throws Exception
							    		{
							    			try
							    			{			
							    				Log.testCaseInfo("TC_013 (Sanity):Link File with enable viewer");
							    				 loginpage = new FnaLoginPage(driver).get();   
								    			 String uName=PropertyReader.getProperty("EmailSanity");
									        	     String pWord=PropertyReader.getProperty("PasswordSanity");
									        	     fnahomepage=loginpage.login(uName, pWord);
									        	     Log.assertThat(fnahomepage.loginValidation(),"Login is successfull ","Login to skysite failed");	
									        	     String collectionname=PropertyReader.getProperty("Collectioname_sanity");
									    			    fnahomepage.selectcollection(collectionname);
									        	     
								    	             String FolderName= fnahomepage.Random_Foldername();		
									    			    Log.assertThat(fnahomepage.Adding_Folder(FolderName), "Adding folder Successfull", "Adding folder UnSuccessful", driver);
							    				
							    				File Path=new File(PropertyReader.getProperty("FolderPath"));			
							    				String FolderPath = Path.getAbsolutePath().toString();
							    				
							    				Log.assertThat(fnahomepage.UploadFileForEnableViewer(FolderPath), "Upload file Successfull", "Upload file UnSuccessful", driver);
							    				
							    				//String FilePath = PropertyReader.getProperty("FilePath");
							    				File Path1=new File(PropertyReader.getProperty("FilePath11"));			
							    				String FilePath = Path1.getAbsolutePath().toString();
							    				
							    				Log.assertThat(fnahomepage.EnableViewer_sendlink_revision(FilePath), "Enable Viewer with send link  Successfull", "Enable Viewer with send link UnSuccessful", driver);
							    			fnahomepage.switchBackToFolderLevel();
							    			fnahomepage.deleteFolder();
							    			}
							    			catch(Exception e)
							    			{
							    				e.getCause();
							    				Log.exception(e, driver);
							    			}
							    			finally
							    			{
							    				Log.endTestCase();
							    				driver.quit();
							    			}
							    		}
							    		
							    		
}
